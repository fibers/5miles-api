import os
from fabric.api import env, require, run, sudo, cd

env.use_ssh_config = True
env.hosts = ['ec2_01']
env.user_home = '/home/ec2-user/'
env.web_root = '/var/www/html/'
env.api_filename = 'fivemiles-api.html'

def publish():
    with cd(env.user_home):
        sudo('cp %s %s' % (env.api_filename, env.web_root))



