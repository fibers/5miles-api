# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.17)
# Database: bizdb
# Generation Time: 2014-09-25 08:24:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table fmmc_cron
# ------------------------------------------------------------

LOCK TABLES `fmmc_cron` WRITE;
/*!40000 ALTER TABLE `fmmc_cron` DISABLE KEYS */;

INSERT INTO `fmmc_cron` (`id`, `name`, `need_mail`, `need_push`, `mail_template_id`, `push_template_id`, `status`, `push_quota`, `comment`, `created_at`, `updated_at`)
VALUES
	(1,'CRON_REMIND_TO_LIST',1,1,2,1,1,-1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(2,'CRON_REMIND_TO_MAKE_OFFER',1,1,3,2,1,-1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(3,'CRON_SOLD_OUT_BROADCAST',1,1,4,3,1,-1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(4,'CRON_FOLLOWERS_LIST',1,0,5,NULL,1,-1,'','2014-09-24 22:01:13','2014-09-24 22:01:13');

/*!40000 ALTER TABLE `fmmc_cron` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fmmc_mail_template
# ------------------------------------------------------------

LOCK TABLES `fmmc_mail_template` WRITE;
/*!40000 ALTER TABLE `fmmc_mail_template` DISABLE KEYS */;

INSERT INTO `fmmc_mail_template` (`id`, `name`, `content`, `subject`, `status`, `comment`, `created_at`, `updated_at`)
VALUES
	(1,'TMPL_WELCOME','<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html>\n	<head>\n        <title>Get Started Fast with 5miles! </title>\n        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n    </head>\n    <body style=\'font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;font-size: 14px;line-height: 1.42857143;color: #333;background-color: #fff;\'>\n        <div style=\"min-width:360px;max-width: 560px;margin: 0px auto;\">\n            <div style=\'height: 100px;width: 100%;border-bottom: 3px #ff8830 solid;background:url(\"http://5milesapp.com/images/logoOrange.png\") no-repeat left center;\'></div>\n            <div style=\"padding: 20px 0px;\">\n                <p>Hi <%= username %>,</p>\n                <p>We’re excited to welcome you to the 5miles community!</p>\n                <p>Ready to make some cash? Let’s do it.</p>\n                <p>Open 5miles now to begin your journey.</p>\n\n                <div style=\"padding-top: 20px;\">\n                    <p>Questions? Email us anytime at:\n                        <a href=\"mailto:hello@5milesapp.com\" target=\"_blank\">hello@5milesapp.com.</a>\n                    </p>\n                    <p>Feel like socialising? (Don’t worry – we don’t bite!)</p>\n                    <p>\n                        <a href=\"http://facebook.com/5milesapp\" target=\"_blank\">http://facebook.com/5milesapp</a>\n                    </p>\n                    <p>\n                        <a href=\"http://twitter.com/5milesapp\" target=\"_blank\">http://twitter.com/5milesapp</a>\n                    </p>\n                    <p>\n                        <a href=\"http://instagram.com/5milesapp\" target=\"_blank\">http://instagram.com/5milesapp</a>\n                    </p>\n                </div>\n            </div>\n        </div>\n    </body>\n</html>\n','Get Started Fast with 5miles! ',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(2,'TMPL_REMIND_TO_LIST','快发商品啊','快发',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(3,'TMPL_REMIND_TO_MAKE_OFFER','有人给你offer啦，快去看看吧','有人给你offer啦',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(4,'TMPL_SOLD_OUT_BROADCAST','你正在买的或者喜欢的东西卖出去啦，抱歉啊','东西被别人抢走啦',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(5,'TMPL_FOLLOWERS_LIST','昨天有人follow你啦','昨天有人follow你啦',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13');

/*!40000 ALTER TABLE `fmmc_mail_template` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fmmc_push_template
# ------------------------------------------------------------

LOCK TABLES `fmmc_push_template` WRITE;
/*!40000 ALTER TABLE `fmmc_push_template` DISABLE KEYS */;

INSERT INTO `fmmc_push_template` (`id`, `name`, `content`, `status`, `comment`, `created_at`, `updated_at`)
VALUES
	(1,'TMPL_REMIND_TO_LIST','Tap on the 5miles app to start selling for free!',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(2,'TMPL_REMIND_TO_MAKE_OFFER','Buyers love your stuff. Tap and share to turn more preloved stuff into cash! (y)',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13'),
	(3,'TMPL_SOLD_OUT_BROADCAST','Sorry! The listing you liked: #{item_title} was just sold. :v Don\'t miss out next time!',1,'','2014-09-24 22:01:13','2014-09-24 22:01:13');

/*!40000 ALTER TABLE `fmmc_push_template` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
