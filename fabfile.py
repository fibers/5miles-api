import os
from fabric.api import env, require, run, sudo, cd, hosts

env.use_ssh_config = True
#env.hosts = ['rock3-hg, rock3-gmworker']
env.codefolder_name = 'fivemiles-biz'
env.project_name = 'pinnacle'
env.webapps_root = '/var/www/deploy_fivemiles/'
env.repo_base = os.path.join(env.webapps_root, env.codefolder_name)
env.project_home = os.path.join(env.repo_base, env.project_name)

def whoami():
    run('whoami')

def update_code():
    with cd(env.repo_base):
        run('git pull')

def install_packages():
    with cd(env.repo_base):
        sudo('pip install -r requirements/base.txt')

def manage_py(command, use_sudo=False):
    if use_sudo:
        func = sudo
    else:
        func = run
    with cd(env.project_home):
        func('python manage.py %s' % command)

def migrate_db():
    manage_py('migrate')

def stop_gunicorn():
    run('supervisorctl stop %s' % env.project_name)

def reload_gunicorn():
    #run('supervisorctl stop %s' % env.project_name)
    #run('supervisorctl start %s' % env.project_name)
    run('supervisorctl reload')

# @hosts('api')
def deploy():
    update_code()
    migrate_db()
    reload_gunicorn()

# @hosts('api')
def fulldeploy():
    update_code()
    install_packages()
    migrate_db()
    reload_gunicorn()

def shutdown_gmworkers():
    run('supervisorctl shutdown')

def update_gmworkers_source():
    with cd('/home/ec2-user/deploy/fivemiles-backend/'):
        run('git pull')

def start_gmworkers():
    with cd('/home/ec2-user/deploy/'):
        run('supervisord -c supervisord.conf')
        #run('supervisorctl start msg_workers:*')

@hosts('rock3-gmworker')
def reload_gmworkers():
    #env.hosts = ['rock3-gmworker']
    update_gmworkers_source()
    shutdown_gmworkers()
    start_gmworkers()
