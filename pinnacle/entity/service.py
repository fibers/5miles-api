# -*- coding: utf-8 -*-
from common.utils import get_i18n_message
from message.biz import is_subscribed, subscribe_notification
from message.models import NOTIFICATION_METHOD_EMAIL
from message.models import NOTIFICATION_TYPE_ALL

__author__ = 'dragon'

import jwt

from entity.exceptions import ItemNotFoundError, InvalidParameterError, InvalidOperationError
from entity.service_base import BaseService, singleton
from pinnacle import settings
from common.utils import get_utcnow, get_now_timestamp
from .models import Item, User, Image
from common.biz import *
from common.utils import robot_or_not
from .utils import generate_access_token, obfuscate_id
from .tasks import lookup_user_position, facebook_me_job, lookup_item_position, user_async_checkin
from message.tasks import welcome, notify, send_mail
from service_base import SrService
from common.es_dao import es_dao_user, es_dao_item

@singleton
class ItemService(BaseService):
    srs = SrService()

    def __init__(self):
        pass

    def item_exists(self, item_id, is_raise_error=False):
        if not self.r.exists('item:%s' % item_id):
            if is_raise_error:
                raise ItemNotFoundError(item_id)
            else:
                return False
        return True

    def get_item_state(self, item_id):
        item_state = self.r.hget('item:%s' % item_id, 'state')
        if not item_state or item_state == '':
            item_state = ITEM_STATE_LISTING
        else:
            item_state = int(item_state)

        return item_state

    def reindex_item(self, item_id):
        item = r.hgetall('item:%s' % item_id)
        fuzzy_item_id = item['id']
        title = item['title']
        description = item['desc']
        category_id = item['cat_id']
        images = []
        if "images" in item and item["images"] != '':
            images = json.loads(item["images"])
        media_link = item['mediaLink']
        price = item['price']
        currency_code = item['currency']
        local_price = item['local_price']
        lat = json.loads(item['location'])['lat']
        lon = json.loads(item['location'])['lon']
        original_price = item['original_price'] if 'original_price' in item else ''
        brand_id = item['brand_id'] if 'brand_id' in item else ''
        brand_name = item['brand_name'] if 'brand_name' in item else ''
        weight = item['weight'] if 'weight' in item else 1

        owner_id = item['user_id']
        item_num = ItemService().get_post_index(owner_id, get_after_plus=True)
        owner = get_user_from_id(item['user_id'],item_num=item_num+1)

        created_at = item['created_at']
        updated_at = item['updated_at']
        is_new = int(item['is_new']) if 'is_new' in item else 0
        modified_at = utils_func.get_now_ts()
        country = item['country'] if 'country' in item else None
        region = item['region'] if 'region' in item else None
        city = item['city'] if 'city' in item else None

        doc = get_item_es_json(fuzzy_item_id, title, description, category_id, images, media_link, price, currency_code,
                               local_price, original_price, brand_id, brand_name, lat, lon, owner, weight,
                               get_category_as_json(category_id), created_at, modified_at, updated_at, is_new, country, region, city)
        #index_item_in_searchengine(item_id, doc)
        #EsService().index_item(item_id, doc)
        es_dao_item().index(item_id, doc)

    def recover_list(self, item_id):
        if not self.item_exists(item_id):
            raise ItemNotFoundError(item_id)

        owner_id = self.r.get('item:%s:owner' % item_id)
        self.reindex_item(item_id)
        with self.r.pipeline() as pipe:
            pipe.hmset('item:%s' % item_id, {'state': ITEM_STATE_LISTING})
            pipe.zadd('user:%s:user_selling_list' % owner_id, item_id, utils_func.get_now_ts())
            pipe.execute()

        UserService().update_item_num(owner_id)

        items = Item.objects.filter(pk=item_id)
        if items:
            items[0].state = ITEM_STATE_LISTING
            items[0].update_time = get_utcnow()
            items[0].save()

    def unlist_item(self, item_id, user_id='', is_check_owner=True):

        self.item_exists(item_id, is_raise_error=True)

        owner_id = self.r.get('item:%s:owner' % item_id)
        if is_check_owner:
            if owner_id != str(user_id):
                raise InvalidOperationError()

        with self.r.pipeline() as pipe:
            pipe.hmset('item:%s' % item_id, {'state': ITEM_STATE_UNLISTED})
            pipe.zrem('user:%s:user_selling_list' % owner_id, item_id)
            pipe.execute()

        es_dao_item().delete(item_id)

        UserService().update_item_num(owner_id)

        items = Item.objects.filter(pk=item_id)
        if items:
            items[0].state = ITEM_STATE_UNLISTED
            items[0].update_time = get_utcnow()
            items[0].save()
        pass

    def recover_unlist(self, item_id):
        self.unlist_item(item_id, is_check_owner=False)

    def recover_sold(self, item_id):
        if not self.item_exists(item_id):
            raise ItemNotFoundError(item_id)

        self.r.hmset('item:%s' % item_id, {'state': ITEM_STATE_SOLD})
        items = Item.objects.filter(pk=item_id)
        if items:
            items[0].state = ITEM_STATE_SOLD
            items[0].update_time = get_utcnow()
            items[0].save()

    def disapprove(self, item_id):
        owner_id = self.r.get('item:%s:owner' % item_id)
        with self.r.pipeline() as pipe:
            pipe.hset('item:%s' % item_id, 'state', ITEM_STATE_UNAPPROVED)
            pipe.zrem('user:%s:user_selling_list' % owner_id, item_id)
            pipe.execute()

        #utils_es.delete_item_in_searchengine(item_id)
        es_dao_item().delete(item_id)
        UserService().update_item_num(owner_id)
        items = Item.objects.filter(pk=item_id)
        if items:
            items[0].state = ITEM_STATE_UNAPPROVED
            items[0].update_time = get_utcnow()
            items[0].save()

    def get_owner_id(self, item_id):
        return self.r.get('item:%s:owner' % item_id)

    def update_es_verification(self, item_id):
        owner_id = self.get_owner_id(item_id)
        user_verified = UserService().is_user_verified(owner_id)
        # user_verified = 1 if user_verified else 0
        if es_dao_item().exists(item_id):
            es_dao_item().update(item_id, {"doc": {'owner': {'verified': user_verified}}})

    def get_post_index(self, user_id ,get_after_plus=False):
        post_index_ex = 15*24*3600

        r_ex = SrService().get_redis_ex()

        key = "user:%s:sp_idx" % user_id
        #logger.info("key=%s" % key)
        sp_idx = r_ex.get(key)
        logger.info("key=%s sp_idx=%s after=%s" % (key,sp_idx,get_after_plus))
        if sp_idx>=0:
            item_cnt = sp_idx
            #logger.info("get_after_plus=%s" % get_after_plus)
            if get_after_plus:
                item_cnt = r_ex.incr(key)
        else:
            item_cnt = Item.objects.filter(user_id=user_id).count()
            logger.info("load item_cnt=%s" % item_cnt)
            res = r_ex.set(key, item_cnt ,ex=post_index_ex)

        return item_cnt


    def update_es_verification_by_user(self, user_id):
        item_ids = UserService().get_my_selling_list(user_id)
        for item_id in item_ids:
            self.update_es_verification(item_id)

    def item_key(self ,item_id):
        return 'item:%s' % item_id

    def get_item_by_db(self, item_id):
        items = Item.objects.filter(pk=item_id)
        if items:
            return items[0]
        else:
            return None

    def lbs_update(self,item_id,lat,lon, fz_id=None ,is_update_owner=False):

        if fz_id:
            item_id = unfuzzy_item_id(fz_id)

        logger.info("item_id=%s"% item_id)
        if self.item_exists(item_id):
            #res = r.hgetall(self.item_key(item_id))
            #logger.info(res)
            user_id = self.get_owner_id(item_id)

            # 修改redis
            location_redis = json.dumps({'lat': lat, 'lon': lon})
            #logger.debug(location_redis)
            r.hmset(self.item_key(item_id), {'location': location_redis})

            # 修改ES
            location_doc = {'location': {'lat': lat, 'lon': lon}}

            if is_update_owner:
                location_doc['owner'] = {'location': {'lat': lat, 'lon': lon}}

            logger.debug(location_doc)
            es_dao_item().update_fields(item_id, **location_doc)

            # 修改Mysql
            item = self.get_item_by_db(item_id)
            if item:
                logger.debug("save to mysql=%s" % item_id)
                item.lat = lat
                item.lon = lon
                item.save()

            if is_update_owner:
                UserService().lbs_update(user_id , lat, lon)

            lookup_item_position.apply_async((user_id, item_id, lat, lon, None), queue='celery.position')

        else:
            raise ItemNotFoundError(item_id)


@singleton
class UserService(BaseService):

    def user_exists(self, user_id):
        if not self.r.exists('user:%s' % user_id):
            return False

        return True

    def get_user_state(self, user_id):
        user_state = self.r.hget('user:%s' % user_id, 'state')
        if not user_state:
            user_state = USER_STATE_VALID
        else:
            user_state = int(user_state)

        return user_state

    def disable_user(self, user_id):
        user = User.objects.get(pk=user_id)
        if user:
            user.state = USER_STATE_FORBIDDEN
            user.save()
        self.r.hset('user:%s' % user_id, 'state', USER_STATE_FORBIDDEN)

    def deactive_user(self, user_id, deactive_comment=None):
        user = User.objects.get(pk=user_id)
        if user:
            user.deactive_comment = deactive_comment
            user.state = USER_STATE_DEACTIVE
            user.save()
        self.r.hset('user:%s' % user_id, 'state', USER_STATE_DEACTIVE)

    def get_devices(self, user_id):
        """
        获取用户设备信息
        :param user_id: 用户ID
        :return: [{'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device}]
        """
        devices = []
        if self.r.scard('push:parse:user:%s:insts' % user_id) > 0:
            for inst in self.r.smembers('push:parse:user:%s:insts' % user_id):
                device_info = self.r.hmget('push:parse:inst:%s' % inst, 'os', 'os_version', 'app_version', 'device')
                devices.append({'os': device_info[0], 'os_version': device_info[1], 'app_version': device_info[2], 'device': device_info[3]})
        elif self.r.scard('user:%s:installation_ids' % user_id) > 0:
            for inst in self.r.smembers('user:%s:installation_ids' % user_id):
                device_info = self.r.hmget('user:%s:installation_id:%s:device' % (user_id, inst), 'os', 'os_version', 'app_version', 'device')
                devices.append({'os': device_info[0], 'os_version': device_info[1], 'app_version': device_info[2], 'device': device_info[3]})
        elif self.r.scard('user:%s:clients' % user_id) > 0:
            for inst in self.r.smembers('user:%s:clients' % user_id):
                device_info = self.r.hmget('user:%s:client:%s' % (user_id, inst), 'os', 'os_version', 'app_version', 'device')
                devices.append({'os': device_info[0], 'os_version': device_info[1], 'app_version': device_info[2], 'device': device_info[3]})

        return devices

    def signup(self, signup_type, nickname, lat, lon, os, os_version, app_version, device, appsflyer_user_id=None,
               ip_address=None, email=None, fb_user_id=None, password=None,
               device_id=None, time_zone=None, anonymous_user_id=None, portrait=settings.DEFAULT_AVATAR_LINK, send_password=False,
               **kwargs):
        if signup_type == 'facebook':
            user_id_key = 'login:facebook:%s:user_id' % fb_user_id
        elif signup_type == 'email':
            user_id_key = 'login:email:%s:user_id' % email
        elif signup_type == 'anonymous':
            user_id_key = 'login:anonymous:%s:user_id' % device_id

        user_id = int(anonymous_user_id) if anonymous_user_id else self.r.incr('global:next_user_id')
        plain_user_id = str(user_id)
        token = generate_access_token(plain_user_id)
        fuzzy_user_id = obfuscate_id(user_id)
        timestamp = utils_func.get_now_ts()
        is_robot = 0 if signup_type == 'facebook' else robot_or_not(email)
        r_attributes = {'id': fuzzy_user_id, 'nickname': nickname, 'email': email, 'portrait': portrait,
                        'created_at': timestamp, 'last_login_at': timestamp, 'is_robot': is_robot,
                        'state': USER_STATE_VALID, 'lat': lat, 'lon': lon, 'timezone': time_zone}
        db_attributes = {'id': user_id, 'fuzzy_user_id': fuzzy_user_id, 'nickname': nickname, 'email': email,
                         'portrait': portrait, 'is_robot': is_robot,
                         'state': USER_STATE_VALID, 'latitude': lat, 'longitude': lon,
                         'ip_address': ip_address if ip_address else '', 'os': os, 'os_version': os_version, 'app_version': app_version,
                         'device': device, 'timezone': time_zone}
        if appsflyer_user_id:
            r_attributes['appsflyer_user_id'] = db_attributes['appsflyer_user_id'] = appsflyer_user_id

        if signup_type == 'facebook':
            r_attributes['fb_user_id'] = db_attributes['fb_user_id'] = fb_user_id
            r_attributes['email_verified'] = db_attributes['email_verified'] = True
            r_attributes['is_anonymous'] = db_attributes['is_anonymous'] = 0
        elif signup_type == 'email':
            r_attributes['is_anonymous'] = db_attributes['is_anonymous'] = 0
        elif signup_type == 'anonymous':
            r_attributes['is_anonymous'] = db_attributes['is_anonymous'] = 1

        with self.r.pipeline() as pipe:
            pipe.set(user_id_key, user_id)
            pipe.hmset('user:%s' % user_id, r_attributes)
            pipe.set('fuzzy_id:%s:user_id' % fuzzy_user_id, user_id)
            if signup_type == 'email':
                pipe.set('user:%s:password' % user_id, password)
            pipe.execute()

        users = User.objects.filter(pk=user_id)
        if users:
            if fb_user_id:
                users[0].fb_user_id = fb_user_id
                users[0].email_verified = True
            if appsflyer_user_id:
                users[0].appsflyer_user_id = appsflyer_user_id

            users[0].nickname = nickname
            users[0].portrait = portrait
            users[0].email = email
            users[0].is_anonymous = 0
            users[0].save()
        else:
            user = User(**db_attributes)
            user.save()

        if signup_type != 'anonymous':
            lookup_user_position.apply_async((user_id, lat, lon, ip_address), queue='celery.position')

        if signup_type == 'email':
            # 发送欢迎邮件
            encoded_email = jwt.encode({'email': email}, settings.JWT_SECRET_KEY)
            data = {'nickname': nickname.encode('utf-8'), 'encoded_email': encoded_email}
            if send_password:
                data['password'] = password
                welcome.delay(str(user_id), email, data, True)
            else:
                welcome.delay(str(user_id), email, data)

            # 48小时之后提醒用户发商品
            # 07/22/2015 暂停
            #notify.delay(str(user_id), 'TRI_REMIND_TO_LIST', {'nickname': nickname.encode('utf-8')})
        elif signup_type == 'facebook':
            link_facebook(user_id, fb_user_id, kwargs['fb_token'], kwargs['fb_token_expires'])
            facebook_me_job.apply_async((user_id, 'SIGNUP', nickname, kwargs['fb_token'], kwargs['fb_token_expires']), countdown=3)

        if signup_type != 'anonymous':
            es_dao_user().index(int(plain_user_id), {'id': plain_user_id, 'location': {'lat': lat, 'lon': lon},
                                                     'nickname': nickname, 'is_robot': is_robot, 'item_num': 0})

        # 匿名注册转为正式注册之后删除匿名注册信息
        if signup_type != 'anonymous' and anonymous_user_id and device_id:
            self.r.delete('login:anonymous:%s:user_id' % device_id)

        result = r.hgetall('user:%s' % user_id)
        result['token'] = token
        result['mobile_phone_verified'] = True if 'mobile_phone' in result and result['mobile_phone'] is not None else False
        return result

    def login(self, login_type, user_id, appsflyer_user_id, lat, lon, ip_address, os, os_version, app_version, device,
              nickname=None, email=None, portrait=None, fb_user_id=None, **kwargs):
        '''
        去活用户重新登录,只需要重置其state为USER_STATE_VALID即可
        '''
        timestamp = utils_func.get_now_ts()
        token = generate_access_token(user_id)
        attributes = {'last_login_at': timestamp, 'appsflyer_user_id': appsflyer_user_id, 'state': USER_STATE_VALID}
        if login_type == 'facebook':
            link_facebook(user_id, fb_user_id, kwargs['fb_token'], kwargs['fb_token_expires'])
        self.r.hmset('user:%s' % user_id, attributes)

        user = User.objects.get(pk=user_id)
        user.last_login_at = datetime.datetime.utcnow().replace(tzinfo=utc)
        user.appsflyer_user_id = appsflyer_user_id
        user.current_os = os
        user.current_os_version = os_version
        user.current_app_version = app_version
        user.current_device = device
        user.state = USER_STATE_VALID
        user.save()

        result = self.r.hgetall('user:%s' % user_id)
        result['token'] = token
        if self.r.exists('user:%s:linked_facebook' % user_id):
            linked_facebook = r.hgetall('user:%s:linked_facebook' % user_id)
            result['facebook_verified'] = True
            result['fb_user_id'] = linked_facebook['fb_user_id']
            result['fb_token_expires'] = int(linked_facebook['fb_token_expires'])
        else:
            result['facebook_verified'] = False

        if 'is_robot' in result:
            del result['is_robot']

        if 'verified' in result:
            result['verified'] = bool(result['verified'])
        else:
            result['verified'] = False

        result['mobile_phone_verified'] = True if 'mobile_phone' in result and result['mobile_phone'] is not None else False

        # if login_type != 'anonymous':
        #     lookup_user_position.apply_async((user_id, lat, lon, ip_address), queue='celery.position')

        # 以下代码处理6.6日事故造成的部分用户ID重复问题，记录所有这些用户重新登录的状态，不再提示重新登录
        user_id = int(user_id)
        if ((user_id in range(691756, 692552) or user_id in [1, 434012, 486139])
                and self.r.get('relogin:%s' % user_id) != 'true'):
            self.r.set('relogin:%s' % user_id, 'true')

        return result

    def is_user_verified(self, user_id):
        verified = self.r.hget('user:%s' % user_id, 'verified')
        if not verified:
            return False
        else:
            return bool(verified)

    def get_my_selling_list(self, user_id):
        return self.r.zrevrange('user:%s:my_selling_list' % user_id, 0, -1)

    def block_user(self, user_id, target_user):
        key = 'user:%s:blacklist' % user_id
        if self.is_blocked(user_id, target_user):
            return False

        with self.r.pipeline() as pipe:
            pipe.sadd(key, target_user)
            pipe.zrem('user:%s:followers' % user_id, target_user)
            pipe.zrem('user:%s:following' % target_user, user_id)
            pipe.execute()

        # 从自己商品的liker中去掉对方
        selling_list = self.r.zrevrange('user:%s:user_selling_list' % user_id, 0, -1)
        for item_id in selling_list:
            self.r.zrem('item:%s:new_likers' % item_id, target_user)
            self.r.zrem('user:%s:likes' % target_user, item_id)

        return True

    def unblock_user(self, user_id, target_user):
        key = 'user:%s:blacklist' % user_id
        self.r.srem(key, target_user)

    def is_blocked(self, target_user, source_user):
        key = 'user:%s:blacklist' % target_user
        if self.r.sismember(key, source_user):
            return True
        else:
            return False

    def is_following(self, user_id, other_id):
        """
        检查用户是否follow了另一个用户
        :param user_id: 用户ID
        :param other_id:  对方ID
        :return: True / False
        """
        if self.r.zrank('user:%s:following' % user_id, other_id) is None:
            return False
        else:
            return True

    def follow(self, user_id, target_user):
        if target_user is None:
            raise InvalidParameterError('user id is wrong')
        elif target_user == str(user_id):
            raise InvalidParameterError('cannot follow self')

        timestamp = get_now_timestamp()
        with self.r.pipeline() as pipe:
            pipe.zadd('user:%s:followers' % target_user, user_id, timestamp)
            pipe.zadd('user:%s:following' % user_id, target_user, timestamp)
            pipe.execute()

        # 通知被follow的用户有新的follower
        follower_info = self.r.hmget('user:%s' % user_id, 'nickname', 'portrait', 'id')
        data = {'follower_nickname': follower_info[0], 'follower_fuzzy_id': follower_info[2],
                'follower_portrait': follower_info[1], 'image_type': 1}
        notify.delay(target_user, 'TRI_FOLLOW_1', data)

    def get_followings(self, user_id, offset=0, limit=0):
        return self.r.zrevrange('user:%s:following' % user_id, offset, offset + limit - 1)

    def get_followers(self, user_id, offset=0, limit=0):
        return self.r.zrevrange('user:%s:followers' % user_id, offset, offset + limit - 1)

    def get_listing_num(self,user_id):
        return self.r.zcard('user:%s:user_selling_list' % user_id)

    def update_item_num(self, user_id):
        # 更新用户索引中的商品总数

        item_num = self.get_listing_num(user_id)

        es_dao_user().update_field(user_id, 'item_num', item_num)
        #es_dao_item().update_fields(item_id, {'owner':{'item_num', item_num}})

    def get_real_id(self , user_fuzzy_id):
        return r.get('fuzzy_id:%s:user_id' % user_fuzzy_id)

    def lbs_update_to_db(self,user_id,lat,lon, zipcode=None):
        # 修改Mysql
        user = User.objects.get(pk=user_id)
        if user:
            logger.debug("lbs_update_to_db=%s" % user_id)
            user.latitude = lat
            user.longitude = lon
            if zipcode:
                user.zipcode = zipcode
            user.save()

    def user_key(self,user_id):
        return 'user:%s' % user_id

    def lbs_update(self,user_id, lat,lon ,fz_id = None, zipcode = None):

        if fz_id:
            user_id = self.get_real_id(fz_id)

        if self.user_exists(user_id):
            #res = r.hgetall(self.user_key(user_id))
            #logger.debug(res)
            pos = {'lat': lat, 'lon': lon}

            if zipcode:
                pos['zipcode'] = zipcode

            r.hmset(self.user_key(user_id), pos)
            self.lbs_update_to_db(user_id, lat, lon, zipcode)
            lookup_user_position.apply_async((user_id, lat, lon, None), queue='celery.position')

    @staticmethod
    def user_daily_checkin(user_id):
        ts = r.hget('user:%s' % user_id, 'last_active_at')
        nts = get_now_timestamp()
        dts = (nts/USER_CHECKIN_INTERVAL)*USER_CHECKIN_INTERVAL
        db_ts = get_utcnow()
        if not ts or int(ts) < dts:
            r.hmset('user:%s' % user_id, {'last_active_at': nts})
            user_async_checkin.apply_async((user_id, nts, db_ts), queue='celery.misc')

    def fill_user_detail(self, user_id, **kwargs):
        to_update = {}
        update_email = False
        update_location = False
        former_email = r.hget('user:%s' % user_id, 'email')
        if 'portrait' in kwargs and kwargs['portrait'] != '':
            to_update['portrait'] = kwargs['portrait']
        if 'nickname' in kwargs and kwargs['nickname'] != '':
            to_update['nickname'] = kwargs['nickname']
        if 'email' in kwargs and kwargs['email'] != '' and kwargs['email'] != former_email:
            to_update['email'] = kwargs['email']
            update_email = True

        if ('lat' in kwargs and abs(kwargs['lat']) > 0) \
                or ('lon' in kwargs and abs(kwargs['lon']) > 0):
            to_update['zipcode'] = kwargs['zipcode']
            to_update['lat'] = kwargs['lat']
            to_update['lon'] = kwargs['lon']
            to_update['country'] = kwargs['country']
            to_update['region'] = kwargs['region']
            to_update['city'] = kwargs['city']
            update_location = True

        if len(to_update) > 0:
            r.hmset('user:%s' % user_id, to_update)

        if update_email:
            r.hdel('user:%s' % user_id, 'email_verified')

        profile_info = r.hmget('user:%s' % user_id, 'nickname', 'portrait', 'email',
                               'zipcode', 'lat', 'lon', 'country', 'region', 'city')
        nickname = profile_info[0]
        portrait = profile_info[1]
        email = profile_info[2]
        zipcode = profile_info[3]
        lat = profile_info[4]
        lon = profile_info[5]
        country = profile_info[6]
        region = profile_info[7]
        city = profile_info[8]

        user = User.objects.get(pk=user_id)
        user.nickname = nickname
        user.portrait = portrait
        user.email = email
        # 1.修改邮箱后，邮箱的认证信息消失
        if update_email:
            user.email_verified = False
        if update_location:
            user.zipcode = zipcode
            user.latitude = lat
            user.longitude = lon
            user.country = country
            user.region = region
            user.city = city
        user.save()

        if update_email:
            fb_user_id = self.r.hget('user:%s' % user_id, 'fb_user_id')
            if not fb_user_id:
                with self.r.pipeline() as p:
                    p.delete('login:email:%s:user_id' % former_email)
                    p.set('login:email:%s:user_id' % email, user_id)
                    p.execute()

            # 发送验证邮件（由于用户之前的邮件地址可能是错误的导致系统自动退订，因此重发验证邮件之前需要修改为订阅状态，否则不会发送）
            if not is_subscribed(user_id, NOTIFICATION_METHOD_EMAIL, NOTIFICATION_TYPE_ALL):
                subscribe_notification(user.id, NOTIFICATION_TYPE_ALL, NOTIFICATION_METHOD_EMAIL, 1)

            encoded_email = jwt.encode({'email': email, 'fb_user_id': fb_user_id}, settings.JWT_SECRET_KEY)
            data = {'nickname': nickname.encode('utf-8'), 'encoded_email': encoded_email}
            send_mail(user_id, 'TMPL_VERIFY_EMAIL', data=data)
        if update_location:
            es_dao_user().update(user_id, {"doc": {"location": {"lat": lat, "lon": lon}}})

        result = {'nickname': nickname, 'portrait': portrait, 'email': email, 'zipcode': zipcode}

        return result

    def unbind_mobile(self, user_id):
        user = User.objects.get(pk=user_id)
        user.mobile_phone = ''
        user.save()
        self.r.hmset('user:%s' % user_id, {'mobile_phone': '', 'verified': False})

        item_ids = UserService().get_my_selling_list(user_id)
        for item_id in item_ids:
            if es_dao_item().exists(item_id):
                es_dao_item().update(item_id, {"doc": {'owner': {'verified': False}}})


    def send_seller_guide(self ,user_id,item_id ):
        # 获得该用户的商品发布次数，如果缓存中没有该数据从DB中加载，
        item_cnt = ItemService().get_post_index(user_id, get_after_plus=True)
        if item_cnt == 1:
            logger.info("TRI_SELLER_GUIDE=%s" % user_id)
            # 给用户发送卖家指导
            nickname = r.hget('user:%s' % user_id, 'nickname')
            notify(user_id, 'TRI_SELLER_GUIDE', {'nickname': nickname})

        logger.debug("item_cnt=%s" % item_cnt)
        # 把该数据更新到ES中
        doc = {}
        doc['owner'] = {'post_index': item_cnt}
        es_dao_item().update_fields(item_id ,**doc)

    def clear_portrait(self, user_id):
        User.objects.filter(pk=user_id).update(portrait=settings.DEFAULT_AVATAR_LINK)
        self.r.hset('user:%s' % user_id, 'portrait', settings.DEFAULT_AVATAR_LINK)

@singleton
class ItemImageService(BaseService):

    def set_first_image(self, item_id, image_id):
        """
        设置商品首图
        :param item_id: 商品ID
        :param image_id: 设置为首图的图片ID
        :return: None
        """
        images = json.loads(self.r.hget('item:%s' % item_id, 'images'))
        first_image = Image.objects.get(pk=image_id)
        first_image_dict = {'imageLink': first_image.image_path, 'height': first_image.image_height,
                            'width': first_image.image_width}
        for image in images:
            if first_image.image_path == image['imageLink']:
                images.remove(image)
                images.insert(0, first_image_dict)
                break

        es_dao_item().update_field(item_id, 'images', images)
        r.hset('item:%s' % item_id, 'images', json.dumps(images))
        first_image.is_first = 1
        first_image.save()
        Image.objects.filter(item_id=item_id).exclude(id=image_id).update(is_first=0)


@singleton
class CategoryService(BaseService):

    def __init__(self):
        srs = SrService()
        self.cache = srs.get_redis_ex()

    def get_map_to_id(self, category_id):
        map_to_category = self.cache.get('category:%s:map_to_category' % category_id)
        if not map_to_category:
            cat = Category.objects.get(pk=category_id)
            map_to_category = cat.map_to_id
            if not map_to_category:
                map_to_category = -1
            self.cache.set('category:%s:map_to_category' % category_id, map_to_category, 30 * 60)

        return int(map_to_category) if int(map_to_category) != -1 else category_id

    def get_categories(self, language_code='en'):
        key = 'categories_%s' % language_code
        categories = self.cache.get(key)
        if categories:
            return json.loads(categories)

        categories_list = Category.objects.filter(status=2).order_by('order_num')
        objects = []
        for category in categories_list:
            cat_dict = dict()
            cat_dict['id'] = category.id
            cat_dict['icon'] = category.icon
            cat_dict['title'] = category.title
            msg_id = category.title_i18n_msg_id
            if msg_id and msg_id != -1:
                cat_dict['title'] = get_i18n_message(msg_id, language_code)

            objects.append(cat_dict)

        self.cache.set(key, json.dumps(objects), 60 * 60)

        return objects

    def get_categories_map(self):
        categories = self.get_categories()
        categories_list = []
        for it in categories:
            categories_list.append((it['id'],it['title']))
        return dict(tuple(categories_list))

