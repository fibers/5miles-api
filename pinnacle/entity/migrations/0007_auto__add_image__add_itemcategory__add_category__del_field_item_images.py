# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Image'
        db.create_table(u'entity_image', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('image_path', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('image_width', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('image_height', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal(u'entity', ['Image'])

        # Adding model 'ItemCategory'
        db.create_table(u'entity_itemcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cat_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('item_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'entity', ['ItemCategory'])

        # Adding model 'Category'
        db.create_table(u'entity_category', (
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('parent_id', self.gf('django.db.models.fields.IntegerField')(default=-1, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('status', self.gf('django.db.models.fields.SmallIntegerField')(default=0, db_index=True)),
            ('order', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'entity', ['Category'])

        # Deleting field 'Item.images'
        db.delete_column(u'entity_item', 'images')

        # Adding field 'Item.internal_status'
        db.add_column(u'entity_item', 'internal_status',
                      self.gf('django.db.models.fields.SmallIntegerField')(null=True),
                      keep_default=False)

        # Adding field 'Item.update_time'
        db.add_column(u'entity_item', 'update_time',
                      self.gf('django.db.models.fields.DateTimeField')(null=True),
                      keep_default=False)

        # Adding index on 'Item', fields ['user_id']
        db.create_index(u'entity_item', ['user_id'])

        # Adding index on 'Item', fields ['fuzzy_item_id']
        db.create_index(u'entity_item', ['fuzzy_item_id'])

        # Adding index on 'Item', fields ['lon']
        db.create_index(u'entity_item', ['lon'])

        # Adding index on 'Item', fields ['lat']
        db.create_index(u'entity_item', ['lat'])

        # Adding index on 'Item', fields ['cat_id']
        db.create_index(u'entity_item', ['cat_id'])

        # Deleting field 'User.phone'
        db.delete_column(u'entity_user', 'phone')

        # Adding field 'User.first_name'
        db.add_column(u'entity_user', 'first_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'User.last_name'
        db.add_column(u'entity_user', 'last_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'User.password'
        db.add_column(u'entity_user', 'password',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.is_active'
        db.add_column(u'entity_user', 'is_active',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'User.account_type_id'
        db.add_column(u'entity_user', 'account_type_id',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Adding field 'User.upgrade_type_id'
        db.add_column(u'entity_user', 'upgrade_type_id',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Adding field 'User.trade_status'
        db.add_column(u'entity_user', 'trade_status',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'User.accumulate_credits'
        db.add_column(u'entity_user', 'accumulate_credits',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.start_date'
        db.add_column(u'entity_user', 'start_date',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.expire_date'
        db.add_column(u'entity_user', 'expire_date',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.about'
        db.add_column(u'entity_user', 'about',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.mobile_phone'
        db.add_column(u'entity_user', 'mobile_phone',
                      self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.mobile_phone_country'
        db.add_column(u'entity_user', 'mobile_phone_country',
                      self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.language'
        db.add_column(u'entity_user', 'language',
                      self.gf('django.db.models.fields.CharField')(default='EN', max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.country'
        db.add_column(u'entity_user', 'country',
                      self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.seller_positive_rating'
        db.add_column(u'entity_user', 'seller_positive_rating',
                      self.gf('django.db.models.fields.IntegerField')(default=0, null=True),
                      keep_default=False)

        # Adding field 'User.seller_total_rating'
        db.add_column(u'entity_user', 'seller_total_rating',
                      self.gf('django.db.models.fields.IntegerField')(default=0, null=True),
                      keep_default=False)

        # Adding field 'User.buyer_positive_rating'
        db.add_column(u'entity_user', 'buyer_positive_rating',
                      self.gf('django.db.models.fields.IntegerField')(default=0, null=True),
                      keep_default=False)

        # Adding field 'User.buyer_total_rating'
        db.add_column(u'entity_user', 'buyer_total_rating',
                      self.gf('django.db.models.fields.IntegerField')(default=0, null=True),
                      keep_default=False)

        # Adding field 'User.seller_rating_score'
        db.add_column(u'entity_user', 'seller_rating_score',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=7, decimal_places=5),
                      keep_default=False)

        # Adding field 'User.rating_update_time'
        db.add_column(u'entity_user', 'rating_update_time',
                      self.gf('django.db.models.fields.DateTimeField')(null=True),
                      keep_default=False)

        # Adding field 'User.buyer_rating_score'
        db.add_column(u'entity_user', 'buyer_rating_score',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=7, decimal_places=5),
                      keep_default=False)

        # Adding field 'User.longitude'
        db.add_column(u'entity_user', 'longitude',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=13, decimal_places=10),
                      keep_default=False)

        # Adding field 'User.latitude'
        db.add_column(u'entity_user', 'latitude',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=13, decimal_places=10),
                      keep_default=False)

        # Adding field 'User.address'
        db.add_column(u'entity_user', 'address',
                      self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.from_channel'
        db.add_column(u'entity_user', 'from_channel',
                      self.gf('django.db.models.fields.CharField')(db_index=True, max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding index on 'User', fields ['email']
        db.create_index(u'entity_user', ['email'])

        # Adding index on 'User', fields ['state']
        db.create_index(u'entity_user', ['state'])

        # Adding index on 'User', fields ['fuzzy_user_id']
        db.create_index(u'entity_user', ['fuzzy_user_id'])


        # Changing field 'User.portrait'
        db.alter_column(u'entity_user', 'portrait', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'User.nickname'
        db.alter_column(u'entity_user', 'nickname', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):
        # Removing index on 'User', fields ['fuzzy_user_id']
        db.delete_index(u'entity_user', ['fuzzy_user_id'])

        # Removing index on 'User', fields ['state']
        db.delete_index(u'entity_user', ['state'])

        # Removing index on 'User', fields ['email']
        db.delete_index(u'entity_user', ['email'])

        # Removing index on 'Item', fields ['cat_id']
        db.delete_index(u'entity_item', ['cat_id'])

        # Removing index on 'Item', fields ['lat']
        db.delete_index(u'entity_item', ['lat'])

        # Removing index on 'Item', fields ['lon']
        db.delete_index(u'entity_item', ['lon'])

        # Removing index on 'Item', fields ['fuzzy_item_id']
        db.delete_index(u'entity_item', ['fuzzy_item_id'])

        # Removing index on 'Item', fields ['user_id']
        db.delete_index(u'entity_item', ['user_id'])

        # Deleting model 'Image'
        db.delete_table(u'entity_image')

        # Deleting model 'ItemCategory'
        db.delete_table(u'entity_itemcategory')

        # Deleting model 'Category'
        db.delete_table(u'entity_category')


        # User chose to not deal with backwards NULL issues for 'Item.images'
        raise RuntimeError("Cannot reverse this migration. 'Item.images' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Item.images'
        db.add_column(u'entity_item', 'images',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)

        # Deleting field 'Item.internal_status'
        db.delete_column(u'entity_item', 'internal_status')

        # Deleting field 'Item.update_time'
        db.delete_column(u'entity_item', 'update_time')

        # Adding field 'User.phone'
        db.add_column(u'entity_user', 'phone',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True),
                      keep_default=False)

        # Deleting field 'User.first_name'
        db.delete_column(u'entity_user', 'first_name')

        # Deleting field 'User.last_name'
        db.delete_column(u'entity_user', 'last_name')

        # Deleting field 'User.password'
        db.delete_column(u'entity_user', 'password')

        # Deleting field 'User.is_active'
        db.delete_column(u'entity_user', 'is_active')

        # Deleting field 'User.account_type_id'
        db.delete_column(u'entity_user', 'account_type_id')

        # Deleting field 'User.upgrade_type_id'
        db.delete_column(u'entity_user', 'upgrade_type_id')

        # Deleting field 'User.trade_status'
        db.delete_column(u'entity_user', 'trade_status')

        # Deleting field 'User.accumulate_credits'
        db.delete_column(u'entity_user', 'accumulate_credits')

        # Deleting field 'User.start_date'
        db.delete_column(u'entity_user', 'start_date')

        # Deleting field 'User.expire_date'
        db.delete_column(u'entity_user', 'expire_date')

        # Deleting field 'User.about'
        db.delete_column(u'entity_user', 'about')

        # Deleting field 'User.mobile_phone'
        db.delete_column(u'entity_user', 'mobile_phone')

        # Deleting field 'User.mobile_phone_country'
        db.delete_column(u'entity_user', 'mobile_phone_country')

        # Deleting field 'User.language'
        db.delete_column(u'entity_user', 'language')

        # Deleting field 'User.country'
        db.delete_column(u'entity_user', 'country')

        # Deleting field 'User.seller_positive_rating'
        db.delete_column(u'entity_user', 'seller_positive_rating')

        # Deleting field 'User.seller_total_rating'
        db.delete_column(u'entity_user', 'seller_total_rating')

        # Deleting field 'User.buyer_positive_rating'
        db.delete_column(u'entity_user', 'buyer_positive_rating')

        # Deleting field 'User.buyer_total_rating'
        db.delete_column(u'entity_user', 'buyer_total_rating')

        # Deleting field 'User.seller_rating_score'
        db.delete_column(u'entity_user', 'seller_rating_score')

        # Deleting field 'User.rating_update_time'
        db.delete_column(u'entity_user', 'rating_update_time')

        # Deleting field 'User.buyer_rating_score'
        db.delete_column(u'entity_user', 'buyer_rating_score')

        # Deleting field 'User.longitude'
        db.delete_column(u'entity_user', 'longitude')

        # Deleting field 'User.latitude'
        db.delete_column(u'entity_user', 'latitude')

        # Deleting field 'User.address'
        db.delete_column(u'entity_user', 'address')

        # Deleting field 'User.from_channel'
        db.delete_column(u'entity_user', 'from_channel')


        # Changing field 'User.portrait'
        db.alter_column(u'entity_user', 'portrait', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'User.nickname'
        db.alter_column(u'entity_user', 'nickname', self.gf('django.db.models.fields.CharField')(default='', max_length=50))

    models = {
        u'entity.category': {
            'Meta': {'object_name': 'Category'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'default': '-1', 'db_index': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'entity.image': {
            'Meta': {'object_name': 'Image'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_height': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'image_path': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'image_width': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.item': {
            'Meta': {'object_name': 'Item'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '30'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fuzzy_item_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'internal_status': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'db_index': 'True'}),
            'local_price': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'lon': ('django.db.models.fields.FloatField', [], {'db_index': 'True'}),
            'mediaLink': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.itemcategory': {
            'Meta': {'object_name': 'ItemCategory'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.user': {
            'Meta': {'object_name': 'User'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'account_type_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'accumulate_credits': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'buyer_positive_rating': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'buyer_rating_score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '5'}),
            'buyer_total_rating': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'blank': 'True'}),
            'expire_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'from_channel': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fuzzy_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'EN'", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'last_login_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '13', 'decimal_places': '10'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '13', 'decimal_places': '10'}),
            'mobile_phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'mobile_phone_country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'portrait': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'rating_update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'seller_positive_rating': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'seller_rating_score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '5'}),
            'seller_total_rating': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'trade_status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'upgrade_type_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        }
    }

    complete_apps = ['entity']