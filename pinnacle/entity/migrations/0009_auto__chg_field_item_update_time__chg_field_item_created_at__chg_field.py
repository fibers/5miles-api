# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Item.update_time'
        db.alter_column(u'entity_item', 'update_time', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, null=True))

        # Changing field 'Item.created_at'
        db.alter_column(u'entity_item', 'created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Category.description'
        db.alter_column(u'entity_category', 'description', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'Category.created_at'
        db.alter_column(u'entity_category', 'created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Category.publish_date'
        db.alter_column(u'entity_category', 'publish_date', self.gf('django.db.models.fields.DateTimeField')(null=True))

        # Changing field 'User.buyer_total_rating'
        db.alter_column(u'entity_user', 'buyer_total_rating', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'User.seller_positive_rating'
        db.alter_column(u'entity_user', 'seller_positive_rating', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'User.buyer_positive_rating'
        db.alter_column(u'entity_user', 'buyer_positive_rating', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'User.portrait'
        db.alter_column(u'entity_user', 'portrait', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'User.seller_total_rating'
        db.alter_column(u'entity_user', 'seller_total_rating', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'User.mobile_phone'
        db.alter_column(u'entity_user', 'mobile_phone', self.gf('django.db.models.fields.CharField')(default='', max_length=30))

        # Changing field 'User.from_channel'
        db.alter_column(u'entity_user', 'from_channel', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'User.last_login_at'
        db.alter_column(u'entity_user', 'last_login_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True))

        # Changing field 'User.address'
        db.alter_column(u'entity_user', 'address', self.gf('django.db.models.fields.CharField')(default='', max_length=1000))

        # Changing field 'User.password'
        db.alter_column(u'entity_user', 'password', self.gf('django.db.models.fields.CharField')(default='', max_length=50))

        # Changing field 'User.nickname'
        db.alter_column(u'entity_user', 'nickname', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

        # Changing field 'User.about'
        db.alter_column(u'entity_user', 'about', self.gf('django.db.models.fields.TextField')(default=''))

        # Changing field 'User.language'
        db.alter_column(u'entity_user', 'language', self.gf('django.db.models.fields.CharField')(max_length=30))

        # Changing field 'User.mobile_phone_country'
        db.alter_column(u'entity_user', 'mobile_phone_country', self.gf('django.db.models.fields.CharField')(default='', max_length=30))

        # Changing field 'User.country'
        db.alter_column(u'entity_user', 'country', self.gf('django.db.models.fields.CharField')(default='', max_length=30))

        # Changing field 'User.created_at'
        db.alter_column(u'entity_user', 'created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

    def backwards(self, orm):

        # Changing field 'Item.update_time'
        db.alter_column(u'entity_item', 'update_time', self.gf('django.db.models.fields.DateTimeField')(null=True))

        # Changing field 'Item.created_at'
        db.alter_column(u'entity_item', 'created_at', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Category.description'
        db.alter_column(u'entity_category', 'description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Category.created_at'
        db.alter_column(u'entity_category', 'created_at', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Category.publish_date'
        db.alter_column(u'entity_category', 'publish_date', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'User.buyer_total_rating'
        db.alter_column(u'entity_user', 'buyer_total_rating', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'User.seller_positive_rating'
        db.alter_column(u'entity_user', 'seller_positive_rating', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'User.buyer_positive_rating'
        db.alter_column(u'entity_user', 'buyer_positive_rating', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'User.portrait'
        db.alter_column(u'entity_user', 'portrait', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'User.seller_total_rating'
        db.alter_column(u'entity_user', 'seller_total_rating', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'User.mobile_phone'
        db.alter_column(u'entity_user', 'mobile_phone', self.gf('django.db.models.fields.CharField')(max_length=30, null=True))

        # Changing field 'User.from_channel'
        db.alter_column(u'entity_user', 'from_channel', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'User.last_login_at'
        db.alter_column(u'entity_user', 'last_login_at', self.gf('django.db.models.fields.DateTimeField')(null=True))

        # Changing field 'User.address'
        db.alter_column(u'entity_user', 'address', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True))

        # Changing field 'User.password'
        db.alter_column(u'entity_user', 'password', self.gf('django.db.models.fields.CharField')(max_length=50, null=True))

        # Changing field 'User.nickname'
        db.alter_column(u'entity_user', 'nickname', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'User.about'
        db.alter_column(u'entity_user', 'about', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'User.language'
        db.alter_column(u'entity_user', 'language', self.gf('django.db.models.fields.CharField')(max_length=30, null=True))

        # Changing field 'User.mobile_phone_country'
        db.alter_column(u'entity_user', 'mobile_phone_country', self.gf('django.db.models.fields.CharField')(max_length=30, null=True))

        # Changing field 'User.country'
        db.alter_column(u'entity_user', 'country', self.gf('django.db.models.fields.CharField')(max_length=30, null=True))

        # Changing field 'User.created_at'
        db.alter_column(u'entity_user', 'created_at', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        u'entity.category': {
            'Meta': {'object_name': 'Category'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'default': '-1', 'db_index': 'True'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'short_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'entity.image': {
            'Meta': {'object_name': 'Image'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_height': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'image_path': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'image_width': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.item': {
            'Meta': {'object_name': 'Item'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '30'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fuzzy_item_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'internal_status': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'db_index': 'True'}),
            'local_price': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'lon': ('django.db.models.fields.FloatField', [], {'db_index': 'True'}),
            'mediaLink': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.itemcategory': {
            'Meta': {'object_name': 'ItemCategory'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.user': {
            'Meta': {'object_name': 'User'},
            'about': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'account_type_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'accumulate_credits': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'buyer_positive_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'buyer_rating_score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '5'}),
            'buyer_total_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'blank': 'True'}),
            'expire_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'from_channel': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'blank': 'True'}),
            'fuzzy_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'EN'", 'max_length': '30', 'blank': 'True'}),
            'last_login_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '13', 'decimal_places': '10'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '13', 'decimal_places': '10'}),
            'mobile_phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'mobile_phone_country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'portrait': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'rating_update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'seller_positive_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'seller_rating_score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '5'}),
            'seller_total_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'trade_status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'upgrade_type_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        }
    }

    complete_apps = ['entity']