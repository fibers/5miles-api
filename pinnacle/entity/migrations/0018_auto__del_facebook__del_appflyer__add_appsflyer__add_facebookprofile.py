# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Facebook'
        db.delete_table(u'entity_facebook')

        # Deleting model 'AppFlyer'
        db.delete_table(u'entity_appflyer')

        # Adding model 'AppsFlyer'
        db.create_table(u'entity_appsflyer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('app_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('platform', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('ip', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('customer_user_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('click_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('install_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('agency', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('media_source', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('campaign', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('campaign_id', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('os_version', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('sdk_version', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('app_version', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('click_url', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('mac', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('cost_per_install', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('country_code', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'entity', ['AppsFlyer'])

        # Adding model 'FacebookProfile'
        db.create_table(u'entity_facebookprofile', (
            ('id', self.gf('django.db.models.fields.CharField')(max_length=50, primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('verified', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('locale', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('timezone', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('updated_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('age_range_min', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('age_range_max', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('user_currency', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('usd_exchange_inverse', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('usd_exchange', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('currency_offset', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal(u'entity', ['FacebookProfile'])


    def backwards(self, orm):
        # Adding model 'Facebook'
        db.create_table(u'entity_facebook', (
            ('user_currency', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('usd_exchange', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('age_range_min', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('locale', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('verified', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('id', self.gf('django.db.models.fields.CharField')(max_length=50, primary_key=True)),
            ('currency_offset', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('usd_exchange_inverse', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('timezone', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('age_range_max', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('updated_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'entity', ['Facebook'])

        # Adding model 'AppFlyer'
        db.create_table(u'entity_appflyer', (
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('click_url', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('click_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('campaign', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('os_version', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('media_source', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('ip', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('sdk_version', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('agency', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('app_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('campaign_id', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('cost_per_install', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('mac', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('platform', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('country_code', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('app_version', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('install_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer_user_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'entity', ['AppFlyer'])

        # Deleting model 'AppsFlyer'
        db.delete_table(u'entity_appsflyer')

        # Deleting model 'FacebookProfile'
        db.delete_table(u'entity_facebookprofile')


    models = {
        u'entity.appsflyer': {
            'Meta': {'object_name': 'AppsFlyer'},
            'agency': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'app_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'app_version': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'campaign': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'campaign_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'click_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'click_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cost_per_install': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'country_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'customer_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'install_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'mac': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'media_source': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'os_version': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'platform': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'sdk_version': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        u'entity.category': {
            'Meta': {'object_name': 'Category'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'order_num': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'default': '-1', 'db_index': 'True'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'short_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'entity.cron': {
            'Meta': {'object_name': 'Cron', 'db_table': "'fmmc_cron'"},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail_template': ('django.db.models.fields.related.ForeignKey', [], {'default': '-1', 'to': u"orm['entity.MailTemplate']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'need_mail': ('django.db.models.fields.BooleanField', [], {}),
            'need_push': ('django.db.models.fields.BooleanField', [], {}),
            'push_quota': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'push_template': ('django.db.models.fields.related.ForeignKey', [], {'default': '-1', 'to': u"orm['entity.PushTemplate']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'entity.facebookprofile': {
            'Meta': {'object_name': 'FacebookProfile'},
            'age_range_max': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'age_range_min': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'currency_offset': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'locale': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'timezone': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'updated_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'usd_exchange': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'usd_exchange_inverse': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'user_currency': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'entity.image': {
            'Meta': {'object_name': 'Image'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_height': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'image_path': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'image_width': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.item': {
            'Meta': {'object_name': 'Item'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '30'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fuzzy_item_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'internal_status': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'lat': ('django.db.models.fields.DecimalField', [], {'max_digits': '13', 'decimal_places': '10', 'db_index': 'True'}),
            'local_price': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'lon': ('django.db.models.fields.DecimalField', [], {'max_digits': '13', 'decimal_places': '10', 'db_index': 'True'}),
            'mediaLink': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'num_in_stock': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.itemcategory': {
            'Meta': {'object_name': 'ItemCategory'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        u'entity.mailtemplate': {
            'Meta': {'object_name': 'MailTemplate', 'db_table': "'fmmc_mail_template'"},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'entity.pushtemplate': {
            'Meta': {'object_name': 'PushTemplate', 'db_table': "'fmmc_push_template'"},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'entity.user': {
            'Meta': {'object_name': 'User'},
            'about': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'account_type_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'accumulate_credits': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'buyer_positive_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'buyer_rating_score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '5'}),
            'buyer_total_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'blank': 'True'}),
            'expire_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fb_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'flyer_user_id': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'blank': 'True'}),
            'from_channel': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'blank': 'True'}),
            'fuzzy_user_id': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'blank': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'EN'", 'max_length': '30', 'blank': 'True'}),
            'last_login_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '13', 'decimal_places': '10'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '13', 'decimal_places': '10'}),
            'mobile_phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'mobile_phone_country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'portrait': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'rating_update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'seller_positive_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'seller_rating_score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '5'}),
            'seller_total_rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'trade_status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'upgrade_type_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        }
    }

    complete_apps = ['entity']