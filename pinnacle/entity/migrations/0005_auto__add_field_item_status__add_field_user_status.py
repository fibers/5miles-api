# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Item.status'
        db.add_column(u'entity_item', 'status',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=1),
                      keep_default=False)

        # Adding field 'User.status'
        db.add_column(u'entity_user', 'status',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Item.status'
        db.delete_column(u'entity_item', 'status')

        # Deleting field 'User.status'
        db.delete_column(u'entity_user', 'status')


    models = {
        u'entity.item': {
            'Meta': {'object_name': 'Item'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '30'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fuzzy_item_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'local_price': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'lon': ('django.db.models.fields.FloatField', [], {}),
            'media_link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'entity.user': {
            'Meta': {'object_name': 'User'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fuzzy_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'last_login_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'portrait': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'})
        }
    }

    complete_apps = ['entity']