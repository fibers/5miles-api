# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Item.status'
        db.delete_column(u'entity_item', 'status')

        # Deleting field 'Item.media_link'
        db.delete_column(u'entity_item', 'media_link')

        # Adding field 'Item.mediaLink'
        db.add_column(u'entity_item', 'mediaLink',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'User.status'
        db.delete_column(u'entity_user', 'status')

        # Adding field 'User.state'
        db.add_column(u'entity_user', 'state',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Item.status'
        db.add_column(u'entity_item', 'status',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Item.media_link'
        db.add_column(u'entity_item', 'media_link',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'Item.mediaLink'
        db.delete_column(u'entity_item', 'mediaLink')

        # Adding field 'User.status'
        db.add_column(u'entity_user', 'status',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=1),
                      keep_default=False)

        # Deleting field 'User.state'
        db.delete_column(u'entity_user', 'state')


    models = {
        u'entity.item': {
            'Meta': {'object_name': 'Item'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '30'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fuzzy_item_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'local_price': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'lon': ('django.db.models.fields.FloatField', [], {}),
            'mediaLink': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'entity.user': {
            'Meta': {'object_name': 'User'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fuzzy_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'last_login_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'portrait': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['entity']