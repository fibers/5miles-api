# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Item'
        db.create_table(u'entity_item', (
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('fuzzy_item_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('state', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('desc', self.gf('django.db.models.fields.TextField')()),
            ('cat_id', self.gf('django.db.models.fields.IntegerField')()),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('currency', self.gf('django.db.models.fields.CharField')(default='USD', max_length=30)),
            ('local_price', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('lat', self.gf('django.db.models.fields.FloatField')()),
            ('lon', self.gf('django.db.models.fields.FloatField')()),
            ('images', self.gf('django.db.models.fields.TextField')()),
            ('media_link', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'entity', ['Item'])

        # Adding model 'User'
        db.create_table(u'entity_user', (
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('fuzzy_user_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('nickname', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('portrait', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('last_login_at', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'entity', ['User'])


    def backwards(self, orm):
        # Deleting model 'Item'
        db.delete_table(u'entity_item')

        # Deleting model 'User'
        db.delete_table(u'entity_user')


    models = {
        u'entity.item': {
            'Meta': {'object_name': 'Item'},
            'cat_id': ('django.db.models.fields.IntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '30'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fuzzy_item_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'local_price': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'lon': ('django.db.models.fields.FloatField', [], {}),
            'media_link': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'state': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'entity.user': {
            'Meta': {'object_name': 'User'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fuzzy_user_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'last_login_at': ('django.db.models.fields.DateTimeField', [], {}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'portrait': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['entity']