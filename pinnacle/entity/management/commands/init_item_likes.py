# -*- coding: utf-8 -*-
__author__ = 'dragon'

import logging
import redis
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import search_item_by_id, update_field_in_searchengine
from entity.models import Item

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)


class Command(BaseCommand):
    args = ''
    help = u'初始化mysql与elasticsearch中商品的like数'

    def handle(self, *args, **options):
        try:
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                item_key = 'item:%s' % item_id
                if not r.exists(item_key):
                    continue

                likes = r.zcard('item:%s:new_likers' % item_id)

                # 更新es
                doc = search_item_by_id(item_id)
                hits = doc['hits']['hits']
                if len(hits) == 0:
                    continue

                if 'likes' not in hits[0]['_source']:
                    try:
                        print 'doc: item_id => %s, likes => %d' % (item_id, likes)
                        update_field_in_searchengine(item_id, 'likes', likes)
                    except Exception, e:
                        print e

                # 更新mysql
                Item.objects.filter(pk=item_id, likes=0).update(likes=likes)
        except Exception, e:
            print e
            raise CommandError(e)


