# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, UserService, ItemService


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        srs = SrService()
        user_service = UserService()
        item_service = ItemService()

        try:
            for user_id in range(691756, 692552):
                print 'disable user: %s' % user_id
                user_service.disable_user(user_id)
                selling_list = srs.get_redis().zrevrange('user:%s:user_selling_list' % user_id, 0, -1)
                for item_id in selling_list:
                    print 'unlist item: %s, user_id: %s' % (item_id, user_id)
                    item_service.unlist_item(item_id, user_id)

        except Exception, e:
            raise CommandError(e)


