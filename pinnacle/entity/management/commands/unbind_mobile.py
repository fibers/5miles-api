# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, UserService


class Command(BaseCommand):
    args = '<user_id>'
    help = 'Unbind mobile phone'

    def handle(self, *args, **options):
        if len(args) == 0:
            raise CommandError('Insufficient arguments')

        try:
            UserService().unbind_mobile(user_id=args[0])
        except Exception, e:
            raise CommandError(e)


