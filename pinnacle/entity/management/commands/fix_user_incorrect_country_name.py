# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService
from entity.models import User
from entity.utils import correct_country_name

class Command(BaseCommand):
    args = ''
    help = '<Fix incorrect country name, run everyday, until bug was fixed>'

    def handle(self, *args, **options):
        srs = SrService()
        r = srs.get_redis()
        try:
            users = User.objects.raw("select * from entity_user where length(country) < 3 and country != '' and zipcode != ''")
            for u in users:
                print 'country: %s' % u.country
                country_name = correct_country_name(u.country)
                print 'change to: %s' % country_name
                u.country = country_name
                u.save()
                r.hset('user:%s' % u.id, 'country', country_name)
        except Exception, e:
            raise CommandError(e)


