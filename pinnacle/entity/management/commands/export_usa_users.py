# -*- coding: utf-8 -*-

__author__ = 'dragon'

import logging
import redis

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from entity.models import User

logger = logging.getLogger('pinnacle')

class Command(BaseCommand):
    args = '<os version: ios/android>'
    help = 'Export American users'

    def handle(self, *args, **options):
        try:
            f = open('./usa_users_%s.csv' % args[0], 'w')
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            users = User.objects.filter(country='United States', is_robot=0)
            for user in users:
                logger.debug(user.email)
                clients = r.smembers('user:%s:clients' % user.id)
                for client in clients:
                    os = r.hget('user:%s:client:%s' % (user.id, client), 'os')
                    logger.debug(os)
                    if os == args[0]:
                        f.write(user.email + '\n')
                        break

            logger.info('Done!')
        except Exception, e:
            logger.exception()
            raise CommandError(e)
