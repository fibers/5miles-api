# -*- coding: utf-8 -*-
from common.es_dao import es_dao_item

__author__ = 'dragon'

import redis
import json

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch
from entity.models import Item, Image

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
es = Elasticsearch(settings.ES_HOSTS)


class Command(BaseCommand):
    args = '<item_id1, item_id2...>'
    help = u'纠正图片高宽（高宽弄反了）'

    def handle(self, *args, **options):
        try:
            if len(args) == 0:
                raise CommandError('Item ID required')

            for item_id in args:
                print 'fixing item: %s' % item_id
                images = json.loads(r.hget('item:%s' % item_id, 'images'))
                print images

                for image in images:
                    image['width'], image['height'] = image['height'], image['width']

                es_dao_item().update(item_id, {'doc': {'images': images}})
                r.hset('item:%s' % item_id, 'images', json.dumps(images))
                Image.objects.filter(item_id=item_id).delete()
                for i in images:
                    image = Image(item_id=item_id, image_path=i['imageLink'], image_height=i['height'], image_width=i['width'])
                    image.save()
        except Exception, e:
            print e
            raise CommandError(e)


