# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, UserService, ItemService


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        item_service = ItemService()

        try:
            for item_id in range(469320, 470168):
                print 'unlist item: %s' % item_id
                item_service.unlist_item(item_id, is_check_owner=False)

        except Exception, e:
            raise CommandError(e)


