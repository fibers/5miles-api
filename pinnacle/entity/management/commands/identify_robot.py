__author__ = 'dragon'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class Command(BaseCommand):
    help = 'Identify robot'

    def handle(self, *args, **options):
        try:
            print 'Connecting to redis'
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_user_id = int(r.incr('global:next_user_id'))
            for user_id in range(1, current_user_id + 1):
                user = r.hmget('user:%s' % user_id, 'is_robot', 'email')
                if user[0] is None:
                    is_robot = 1 if user[1].endswith('@niu.la') else 0
                    print 'email: %s, is_robot: %s' % (user[1], user[0])
                    r.hset('user:%s' % user_id, 'is_robot', is_robot)

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)