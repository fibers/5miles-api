# -*- coding: utf-8 -*-
from common import utils_es
from entity.service import ItemService

__author__ = 'chuyouxin'

import sys
import os
import time
import redis
import json

from pytz import timezone
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils import timezone
from django.db import connection

from entity.models import User
from entity.models import Item, ViewItem
from entity.models import Category
from entity.models import ItemCategory
from spam.models import UserReportRecord
from spam.models import ItemReportRecord

from common.utils_es import *
from common.biz import *
from common.utils import *

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
from entity.tasks import lookup_geoip

from raven import Client
client = Client(settings.RAVEN_CONFIG['dsn'])

thrift_path = os.path.join(os.path.dirname(__file__), '../../gen-py')
sys.path.append(thrift_path)
from BackendApi import IThriftBackendApiService
# from entity.tasks import blur_detect
#from common.biz import reindex_item

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'connect to redis'
logger = logging.getLogger('pinnacle')

class BackendApiHandler:

    def __init__(self):
        self.log = {}

    def updateProductById(self, product_id, paramsMap):
        logger.info("update the Product information(product_id:%s,paramsMap:%s)." % (product_id, paramsMap))
        result = 0
        update_flag = False
        try:

            if paramsMap and len(paramsMap) > 0:
                Item.objects.filter(id=product_id).update(**paramsMap)

                if settings.PROMOTION_POST_ITEM_SWITCH:
                    if 'internal_status' in paramsMap:
                        current_time_stamp = get_now_timestamp()

                        internal_status = int(paramsMap["internal_status"])
                        # internal_status:0表示待审核,1表示审核通过,2表示审核通过有风险,3表示审核不通过
                        if internal_status in [ITEM_INTERNAL_STATUS_APPROVED, ITEM_INTERNAL_STATUS_APPROVED_RISKY]:
                            self.promotion_post_item(current_time_stamp, product_id, 1)
                        elif internal_status == ITEM_INTERNAL_STATUS_UNAPPROVED:
                            self.promotion_post_item(current_time_stamp, product_id, 0)

                # synchronize redis
                if "state" in paramsMap:
                    state = int(paramsMap["state"])
                    item_key = 'item:%d' % product_id
                    if r.exists(item_key):
                        item_infos = r.hmget(item_key, 'state', 'user_id', 'created_at')
                        prev_state = item_infos[0]
                        user_id = item_infos[1]
                        created_at = item_infos[2]

                        with r.pipeline() as pipe:
                            if 'internal_status' in paramsMap:
                                internal_status = int(paramsMap["internal_status"])
                                # 从审核通过的商品列表中移除指定商品
                                if internal_status == ITEM_INTERNAL_STATUS_UNAPPROVED:
                                    pipe.hset(item_key, 'state', ITEM_STATE_UNAPPROVED)
                                    pipe.zrem('user:%s:user_selling_list' % user_id, product_id)
                                    utils_es.delete_item_in_searchengine(product_id)
                                else:
                                    pipe.hset(item_key, 'state', ITEM_STATE_LISTING)
                                    pipe.zadd('user:%s:user_selling_list' % user_id, product_id, created_at)

                            pipe.execute()

                        if int(prev_state) == ITEM_STATE_UNAPPROVED \
                                and internal_status != ITEM_INTERNAL_STATUS_UNAPPROVED:
                            ItemService().reindex_item(product_id)
                            # images_json = r.hget(item_key, 'images')
                            # if images_json and images_json != '':
                            #     images = json.loads(images_json)
                            #     if len(images) > 0:
                            #         blur_detect.delay(product_id, images[0]['imageLink'])
                    else:
                        logger.warn("The item is not exists, item_key:%s" % item_key)
            else:
                result = -2
                logger.warn('WARN', "There is no information to update")
        except Exception, e:
            logger.error(e)
            capture_exception()
            result = -1
        finally:
            self.close_django_conn()

        return result


    def updateCategoryById(self, categoryId, paramsMap):
        logger.info("update the Category information(category_id:%s,paramsMap:%s)." % (categoryId, paramsMap))
        result = 0
        update_flag = False
        try:
            if paramsMap and len(paramsMap) > 0:
                Category.objects.filter(id=categoryId).update(**paramsMap)
            else:
                result = -2
                logger.warn("There is no information to update")
        except Exception, e:
            logger.error(e)
            capture_exception()
            result = -1
        finally:
            self.close_django_conn()

        return result


    def updateProductCategory(self, productId, categoryId):
        logger.info("update_product_category(product_id:%s,category_id:%s)" % (productId, categoryId))
        result = 0
        original_cat_id = -1
        update_cache_success = False
        update_index_success = False
        try:
            # update information with the redis
            item_key = 'item:%d' % productId
            d = r.hgetall(item_key)

            if 'cat_id' in d:

                # update information with redis
                original_cat_id = d["cat_id"]
                d["cat_id"] = categoryId
                r.hmset(item_key, d)
                update_cache_success = True

                # update information with elasticsearch index
                partial_doc = {'cat_id': categoryId, 'cat': get_category_as_json(categoryId)}
                update_fields_in_searchengine(productId, **partial_doc)
                update_index_success = True

                # update information with db
                item_category = ItemCategory.objects.get(item_id=productId)
                item_category.cat_id = categoryId
                item_category.save()
            else:
                logger.warn("Can't get info from redis(item_id:%d,cat_id:%s)" % (productId, categoryId))
                result = -2
        except Exception, e:
            logger.error(e)
            capture_exception()
            # Failure to update db, so we must recovery the info from redis
            if update_index_success:
                partial_doc = {'cat_id': int(original_cat_id), 'cat': get_category_as_json(original_cat_id)}
                update_fields_in_searchengine(productId, **partial_doc)

            if update_cache_success:
                item_key = 'item:%d' % productId
                d = r.hgetall(item_key)
                d["cat_id"] = int(original_cat_id)
                r.hmset(item_key, d)
            result = -1
        finally:
            self.close_django_conn()

        return result


    def get_comments_by_item_id(self, item_id):
        logger.info("get_comments_by_item_id(item_id:%s)." % item_id)
        try:
            comment_count = r.llen('item:%s:comments' % item_id)
            comment_ids = r.lrange('item:%s:comments' % item_id, 0, comment_count)
            comments = []
            for comment_id in comment_ids:
                comment = r.hgetall('comment:%s' % comment_id)

                plain_author_id = comment['author'] if 'author' in comment else comment['authorID']
                comment['author'] = get_user_from_id(plain_author_id)
                comment['author_id'] = plain_author_id

                if 'authorID' in comment:
                    comment.pop('authorID')

                comments.append(comment)

            meta = {'count': '%d' % comment_count}
            return json.dumps({'meta': meta, 'objects': comments})
        except Exception, e:
            logger.error(e)
            capture_exception()

        return json.dumps({'meta': {'count': 0}, 'objects': []})

    def get_offers_by_item_id(self, item_id):
        logger.info("get_offers_by_item_id(item_id:%s)." % item_id)
        try:
            # is owner item?
            owner = r.get('item:%s:owner' % item_id)
            seller = get_user_from_id(int(owner))

            offers = []
            offerline_ids = r.lrange('item:%s:offerlines' % item_id, 0, -1)
            for offerline_id in offerline_ids:
                offerline_records = r.zrange('offerline:%s:entries' % offerline_id, 0, -1)
                if len(offerline_records) == 0:
                    continue

                buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')
                buyer = get_user_from_id(buyer_id)
                buyer['buyer_id'] = buyer_id

                for o_id in offerline_records:
                    offer = r.hgetall('offer:%s' % o_id)
                    # offer_type: 0-chat,1-accept,3-delivery,4-rate
                    offerline = {'id': offerline_id, 'buyer': buyer, 'type': offer['type'], 'description': '%s' % offer['text'], 'price': offer['price'], 'timestamp': offer['timestamp'], 'direct': 0 if offer['from'] == owner else 1}
                    offers.append(offerline)

            return json.dumps({'meta': seller, 'objects': offers})
        except Exception, e:
            logger.error('ERROR', e)
            capture_exception()

        return json.dumps({'meta': {}, 'objects': []})

    def get_offer_count_by_item_ids(self, item_ids):
        logger.info("get_offer_count_by_item_ids(item_ids:%s)." % item_ids)
        try:
            offers = []
            for item_id in item_ids:
                offer_count = r.llen('item:%s:offerlines' % item_id)
                offer = {'item_id': '%s' % item_id, 'count': '%d' % offer_count}
                offers.append(offer)

            return json.dumps({'meta': {}, 'objects': offers})
        except Exception, e:
            logger.error(e)
            capture_exception()

        return json.dumps({'meta': {}, 'objects': []})


    def batch_update_item_weight(self, item_ids, weight):
        logger.info("batch_update_item_weight(item_ids:%s,weight:%s)." % (item_ids, weight))
        result = 'ok'
        items = []
        if item_ids:
            for item_id in item_ids:
                try:
                    # 批量更新数据库中的信息
                    weight_updated_at = get_now_timestamp_str2()
                    Item.objects.filter(pk=item_id).update(weight=weight, weight_updated_at=weight_updated_at)
                    # Item.objects.filter(pk__in=item_ids).update(weight=weight)
                    # 批量更新es中的信息
                    update_field_in_searchengine(item_id, 'weight', weight)
                except Exception, e1:
                    logger.error('batch_update_item_weight(item_id:%s,weight:%s)' % (item_id, weight), e1)
                    client.captureException()
                    result = 'failed'
                    items.append(item_id)
                finally:
                    self.close_django_conn()

        return json.dumps({'meta': {'result': result}, 'objects': json.dumps(items)})


    def get_user_statistic_data(self, user_id):
        statistic_data = {}
        try:
            # 查看商品数量
            view_count = ViewItem.objects.filter(user_id=user_id).values('item_id').distinct().count()

            # Like商品数量
            likes = r.zcard('user:%s:likes' % user_id)

            # 当前人Offer的商品数
            offer_items = r.zcard('user:%s:purchases' % user_id)

            # Buy商品数(已经购买)
            buy_items = 0
            item_ids = r.zrevrange('user:%s:purchases' % user_id, 0, -1)
            if item_ids:
                for item_id in item_ids:
                    item_state = int(r.hget('item:%s' % item_id, 'state'))
                    if item_state in [ITEM_STATE_SOLD, ITEM_STATE_DONE_DELIVERY, ITEM_STATE_RATED_SELLER]:
                        buy_items += 1

            total_offered_items = 0
            sell_items = 0
            chats = 0
            reply_offers = 0

            buyers = []

            items = r.zrevrange('user:%s:my_selling_list' % user_id, 0, -1)
            if items:
                for item_id in items:
                    item_state = r.hget('item:%s' % item_id, 'state')
                    item_state = int(item_state) if item_state is not None else -1
                    # 卖出商品数
                    if item_state in [ITEM_STATE_SOLD, ITEM_STATE_DONE_DELIVERY, ITEM_STATE_RATED_SELLER]:
                        sell_items += 1

                    # 被Offer人数:卖家商品被offer的总人数
                    offerlines = r.lrange('item:%s:offerlines' % item_id, 0, -1)
                    if offerlines and len(offerlines) > 0:
                        for offerline_id in offerlines:
                            buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')

                            if buyer_id != user_id:
                                if buyer_id not in buyers:
                                    buyers.append(buyer_id)

                            # Chat消息数
                            offer_ids = r.zrange('offerline:%s:entries' % offerline_id, 0, -1)
                            if offer_ids and len(offer_ids) > 0:
                                for offer_id in offer_ids:
                                    offer_info = r.hmget('offer:%s' % offer_id, 'type', 'from')
                                    type = int(offer_info[0])
                                    sell_offer = int(offer_info[1])
                                    if type == 2:
                                        chats += 1
                                    elif type == 0 or type == 1:
                                        if sell_offer == user_id:
                                            # 回复Offer数:卖家回复后就算一条,连续回复算多条
                                            reply_offers += 1

                        # 被Offer商品数
                        total_offered_items += 1

            statistic_data['item_view_count'] = view_count
            statistic_data['likes'] = likes
            statistic_data['offer_items'] = offer_items
            statistic_data['chats'] = chats
            statistic_data['buy_items'] = buy_items
            statistic_data['sell_items'] = sell_items
            statistic_data['total_offered_items'] = total_offered_items
            statistic_data['total_offers'] = len(buyers)
            statistic_data['reply_offers'] = reply_offers

        except Exception, e:
            logger.error('get_user_statistic_data(user_id:%s),except:%s' % (user_id, e.message))
            capture_exception()
        finally:
            self.close_django_conn()

        return json.dumps({'meta': {}, 'objects': statistic_data})

    def batch_update_user_weight(self, user_ids, weight):
        logger.info("batch_update_user_weight(user_ids:%s,weight:%s)." % (user_ids, weight))
        result = 'ok'
        users = []
        if user_ids:
            for user_id in user_ids:
                try:
                    # 批量更新数据库中的信息
                    weight_updated_at = get_now_timestamp_str2()
                    User.objects.filter(pk=user_id).update(weight=weight, weight_updated_at=weight_updated_at)

                    items = Item.objects.filter(user_id=user_id, state=ITEM_STATE_LISTING).values('id')
                    owner = get_user_from_id(user_id)
                    owner['weight'] = weight
                    # 批量更新es中的信息
                    if items and len(items) > 0:
                        for item in items:
                            item_id = int(item['id'])
                            update_field_in_searchengine(item_id, 'owner', owner)
                except Exception, e1:
                    logger.error('batch_update_user_weight(user_id:%s,weight:%s),error message:%s' % (user_id, weight, e1.message))
                    capture_exception()
                    result = 'failed'
                    users.append(user_id)
                finally:
                    self.close_django_conn()

        return json.dumps({'meta': {'result': result}, 'objects': json.dumps(users)})


    def batch_update_image_blur_weight(self, item_ids, weight):
        logger.info("batch_update_image_blur_weight(item_ids:%s,weight:%s)." % (item_ids, weight))
        result = 'ok'
        items = []
        if item_ids:
            for item_id in item_ids:
                try:
                    update_field_in_searchengine(item_id, 'image_blur', weight)
                except Exception, e1:
                    logger.error('batch_update_image_blur_weight(item_ids:%s,weight:%s),error message:%s' % (item_ids, weight, e1.message))
                    capture_exception()
                    result = 'failed'
                    items.append(item_id)

        return json.dumps({'meta': {'result': result}, 'objects': json.dumps(items)})


    def batch_update_category_weight(self, cat_id, weight):
        logger.info("batch_update_category_weight(cat_id:%s,weight:%s)." % (cat_id, weight))
        result = 'ok'

        try:
            weight_updated_at = get_now_timestamp_str2()
            # 1.批量更新数据库中商品分类权重
            Category.objects.filter(pk=cat_id).update(weight=weight, weight_updated_at=weight_updated_at)
        except Exception, e1:
            logger.error('failed to batch update category weight in db,cat_id:%s,weight:%s,error message:%s' % (cat_id, weight, e1))
            capture_exception()
            result = 'failed'
            return json.dumps({'meta': {'result': result}, 'objects': json.dumps([])})
        finally:
            self.close_django_conn()

        items = []
        # 2.批量更新es中的商品权重
        results = search_items_by_category(cat_id, '_id')
        total = results['hits']['total']
        logger.info('total item: %s' % total)
        for hit in results['hits']['hits']:
            item_id = hit['_id']
            try:
                update_field_in_searchengine(int(item_id), 'cat', {'weight': weight})
            except Exception, e1:
                logger.error('batch_update_category_weight(cat_id:%s,weight:%s),error message:%s' % (cat_id, weight, e1))
                capture_exception()
                result = 'failed'
                items.append(item_id)

        return json.dumps({'meta': {'result': result}, 'objects': json.dumps(items)})


    def close_django_conn(self):
        if connection:
            connection.close()


    def promotion_post_item(self, current_time_stamp, item_id, state):
        # 判断活动的开始时间,并且商品是在活动区间上传的
        current_time = get_timestamp_str2(long(current_time_stamp), '%Y-%m-%d %H:%M:%S')
        zone_current_time = get_zone_date(current_time, settings.PROMOTION_POST_ITEM_ZONE_TIME, '%Y-%m-%d %H:%M:%S')
        if zone_current_time >= settings.PROMOTION_POST_ITEM_START_TIME \
                and zone_current_time < settings.PROMOTION_POST_ITEM_END_TIME:

            if state == ITEM_INTERNAL_STATUS_APPROVED:
                item_key = 'item:%s' % item_id
                item_infos = r.hmget(item_key, 'created_at', 'cat_id')
                created_at = item_infos[0]
                created_at_timestamp = item_infos[0]
                cat_id = int(item_infos[1])
                created_at = get_timestamp_str2(long(created_at), '%Y-%m-%d %H:%M:%S')
                zone_created_at = get_zone_date(created_at, settings.PROMOTION_POST_ITEM_ZONE_TIME, '%Y-%m-%d %H:%M:%S')

                user_id = r.get('item:%s:owner' % item_id)
                # fb_user_id = r.hget('user:%s' % user_id, 'fb_user_id')
                user_infos = User.objects.filter(pk=user_id).values('country', 'ip_address')
                country = ''
                if user_infos and len(user_infos) > 0:
                    country = user_infos[0]['country']
                    ip_address = user_infos[0]['ip_address']
                    if country == '':
                        # 通过ip获取country
                        country_json = lookup_geoip(user_id, ip_address)
                        country = country_json['country'] if country_json is not None else ''

                # 规则:
                # 1.上传的商品必须在活动区间之内
                # 2.必须指定的分类
                # 3.必须使用facebook登陆
                # 4.必须是美国地区的用户
                if zone_created_at >= settings.PROMOTION_POST_ITEM_START_TIME \
                        and zone_created_at < settings.PROMOTION_POST_ITEM_END_TIME \
                        and cat_id in settings.PROMOTION_POST_ITEM_CAT_IDS \
                        and country in settings.PROMOTION_POST_ITEM_COUNTRIES:
                    # 审核通过则就把发布商品人得数量+1,否则-1,
                    # 需要考虑重复点击的问题
                    promotion_item_num_key = 'promotion:post_item_num:charts'
                    promotion_item_num_timestamp_key = 'promotion:post_item_num:time:charts'
                    promotion_item_key = 'promotion:post_item:charts'

                    item_is_approve = r.zscore(promotion_item_key, item_id)
                    if not item_is_approve:

                        # 防止重复操作
                        r.zadd(promotion_item_key, item_id, current_time_stamp)

                        score = r.zscore(promotion_item_num_key, user_id)
                        new_score = 1

                        # 商品有效
                        new_score = score + 1 if score else 1
                        r.zadd(promotion_item_num_key, user_id, new_score)
                        upload_item_timestamp = r.zscore(promotion_item_num_timestamp_key, user_id)
                        if upload_item_timestamp is None or float(created_at_timestamp) > upload_item_timestamp:
                            r.zadd(promotion_item_num_timestamp_key, user_id, created_at_timestamp)

                        # 计算太平洋时区/洛杉矶时区的当天日期
                        current_date = get_zone_date(current_time, settings.PROMOTION_POST_ITEM_ZONE_TIME, '%Y-%m-%d')
                        # tz = timezone('America/Los_Angeles')
                        # los_date = datetime.datetime.strptime(current_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
                        # los_date_str = los_date.astimezone(tz).strftime('%Y-%m-%d')

                        # 红包池
                        red_package_key = 'promotion:red_package_five:charts'
                        red_package_day_key = 'promotion:red_package_five:%s:charts' % current_date
                        post_item_num = new_score
                        if post_item_num >= settings.PROMOTION_POST_ITEM_FIVE_RED_PACKAGE_TARGET:
                            # 判断当前用户是否已经获取到红包,如果已经获取到,则不更新,否则更新
                            c_t = r.zscore(red_package_key, user_id)
                            if c_t is None:
                                user_num = r.zcard(red_package_key)
                                surplus_num = settings.PROMOTION_POST_ITEM_FIVE_RED_PACKAGE_TOTAL_NUM - user_num
                                if surplus_num > 0:
                                    r.zadd(red_package_key, user_id, current_time_stamp)
                                    r.zadd(red_package_day_key, user_id, current_time_stamp)
                    else:
                        logger.warn('[promotion] - the item has been approved, item_id=%s' % item_id)
                else:
                    logger.warn('[promotion] - does not meet the requirement, item_id=%s, upload_time=%s,cat_id=%s,country=%s' % (item_id, zone_created_at, cat_id, country))
        else:
            logger.warn('[promotion] - promotion is not started, start time:%s~%s, current time:%s' % (settings.PROMOTION_POST_ITEM_START_TIME,
                                                                   settings.PROMOTION_POST_ITEM_END_TIME, zone_current_time))

class Command(BaseCommand):
    args = '<port>'
    help = 'Run BackendApi thrift server at a given port'

    def handle(self, *args, **options):
        try:
            if len(args) > 0:
                port = int(args[0])
            else:
                port = settings.BACKENDAPI_THRIFT_PORT

            handler = BackendApiHandler()
            processor = IThriftBackendApiService.Processor(handler)
            transport = TSocket.TServerSocket(port=port)
            tfactory = TTransport.TBufferedTransportFactory()
            pfactory = TBinaryProtocol.TBinaryProtocolFactory()

            server = TServer.TThreadPoolServer(processor, transport, tfactory, pfactory)

            print 'Starting BackendApi Thrift server on port: %d' % port
            server.serve()
            print 'Done!'
        except Exception, e:
            print e
            raise CommandError('port number not provided')
