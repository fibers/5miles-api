# -*- coding: utf-8 -*-
from common.es_dao import es_dao_user

__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService


class Command(BaseCommand):
    args = ''
    help = 'repair es user location'

    def handle(self, *args, **options):
        srs = SrService()
        r = srs.get_redis()
        try:
            current_user_id = int(r.get('global:next_user_id'))
            for user_id in range(1, current_user_id + 1):
                if not r.exists('user:%s' % user_id):
                    continue

                print 'processing user: %s' % user_id
                user_info = r.hmget('user:%s' % user_id, 'nickname', 'lat', 'lon', 'verified', 'is_robot', 'is_anonymous')
                if user_info[5] == '1':
                    continue

                location_dict = {"doc": {"location": {'lat': float(user_info[1]) if user_info[1] else 0, 'lon': float(user_info[2]) if user_info[2] else 0}}}
                es_dao_user().update(user_id, location_dict)

                print 'Done'
        except Exception, e:
            raise CommandError(e)


