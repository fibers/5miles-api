# -*- coding: utf-8 -*-
__author__ = 'dragon'

import redis
import logging

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_field_in_searchengine, search_item_by_id
from common.biz import get_category_as_json

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):
    help = u'初始化商品文档的品类属性，包括品类ID、性别定位'

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                doc = search_item_by_id(item_id)
                hits = doc['hits']['hits']
                if len(hits) == 0:
                    continue

                cat_id = hits[0]['_source']['cat_id']
                logger.info('update item: %d' % item_id)
                try:
                    update_field_in_searchengine(item_id, 'cat', get_category_as_json(cat_id))
                except Exception, e:
                    logger.error('update item:%d failed: %s' % (item_id, e))

            logger.info('Done!')
        except Exception, e:
            logger.error(e)
            raise CommandError(e)