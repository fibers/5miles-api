# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis
import json

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch
from entity.models import Item, User, Image

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'

import geoip2.database
reader = geoip2.database.Reader(settings.GEOIP['DB_URL'])
print 'load geoip2'

class Command(BaseCommand):
    help = 'delete image url'

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_item_id = r.get("global:next_item_id")
            for item_id in range(1, int(current_item_id) + 1):
                # 删除redis中得image节点中的url
                if r.exists('item:%s' % item_id):
                    images = r.hget('item:%s' % item_id, 'images')
                    if images:

                        # 判断redis中是否包含url字段(部分包含的也要处理)
                        is_updated = False
                        for image in json.loads(images):
                            if 'url' in image:
                                is_updated = True

                        if is_updated:
                            imagestr = ''
                            for image in json.loads(images):
                                width = image['width'] if 'width' in image else 0
                                height = image['height'] if 'height' in image else 0
                                imageLink = image['imageLink']

                                if imagestr != '':
                                    imagestr = "%s," % imagestr

                                imagestr = '%s{"imageLink": "%s", "width": %s, "height": %s}' % (imagestr, imageLink, width, height)

                            if imagestr != '':
                                print '%s=>%s' % (item_id, imagestr)
                                # 删除redis中的image节点中的url
                                if r.exists('item:%s' % item_id):
                                    r.hmset('item:%s' % item_id, {'images': u'[%s]' % imagestr})

                                # 删除es中的image节点中的url
                                count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})
                                if count['count'] > 0:
                                    es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id, body={"doc": {"images": json.loads('[%s]' % imagestr)}})

            print 'Finished!'
        except Exception, e:
            print e
            raise CommandError(e)