__author__ = 'wangguanglei'

import redis
from django.core.management.base import BaseCommand, CommandError
from entity.service import UserService
from django.conf import settings
from entity.models import User

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = '<user_id>'
    help = 'enable user'

    def handle(self, *args, **options):
        if len(args) < 1:
            raise CommandError('Invalid argument')

        user_id = int(args[0])

        if not UserService().user_exists(user_id):
            print 'User %s Not Exist' % user_id
            raise Exception

        user_state = UserService().get_user_state(user_id)
        if user_state != 0:
            print 'Only Disabled User can be enable, user:%s' % user_id
            raise Exception

        try:
            user = User.objects.get(pk=user_id)
            if user:
                user.state = 1
                user.save()
            self.r.hset('user:%s' % user_id, 'state', 1)
        except Exception, e:
            raise CommandError(e)
