# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis
import json
import logging
import urllib
import urllib2
import time

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch

from entity.models import Item, User

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'

import geoip2.database
reader = geoip2.database.Reader(settings.GEOIP['DB_URL'])
print 'load geoip2'

logger = logging.getLogger('pinnacle')

def lat_or_lon_format(lat_or_lon):
    # lat和lon保留三位精度,提高命中缓存的概率
    return float('%.3f' % lat_or_lon)


def geoip_position(ip_address):
    country = ''
    region = ''
    city = ''
    lat = 0
    lon = 0
    try:
        response = reader.city(ip_address)
        lat = lat_or_lon_format(float(response.location.latitude))
        lon = lat_or_lon_format(float(response.location.longitude))
        country = response.country.name
        city = response.city.name
        subdivisions = response.subdivisions
        if subdivisions:
            for sub in subdivisions:
                region = sub.iso_code
    except Exception, e:
        print e

    return {'country': country, 'region': region, 'city': city, 'lat': lat, 'lon': lon}


def geocode(lat, lon):
        country = ''
        city = ''
        region = ''
        try:
            # 从缓存中获取位置信息
            geo_args = {'latlng': '%s,%s' % (lat, lon), 'sensor': 'false'}

            url = '%s?%s' % (settings.GEOCODE_BASE_URL, urllib.urlencode(geo_args))
            results = json.load(urllib2.urlopen(url, timeout=6))['results']
            if results and len(results) > 0:
                for rr in results:
                    address_components = rr['address_components']
                    if address_components:
                        for ac in address_components:
                            if 'country' in ac['types'] and country == '':
                                country = ac['long_name']
                            if 'administrative_area_level_1' in ac['types'] and region == '':
                                region = ac['short_name']
                            if 'locality' in ac['types'] and city == '':
                                city = ac['long_name']

        except Exception, e:
            print e

        return {'country': country, 'city': city, 'region': region, 'lat': lat, 'lon': lon}


def gen_positions():
    try:
        file_output = open('/tmp/item_city_new.txt', 'w+')

        file_read = open('/tmp/item_city.txt')
        line = file_read.readline()

        lat_lon_map = {}
        while line:
            msg = line.replace('\n', '').split('\t')
            item_id = msg[0]
            fuzzy_item_id = msg[1]
            item_country = msg[2]
            item_region = msg[3]
            item_city = msg[4]
            item_lat = msg[5]
            item_lon = msg[6]
            ip_address = msg[7]
            user_country = msg[8]
            user_region = msg[9]
            user_city = msg[10]
            if item_region == '':
                if float(item_lat) != 0 or float(item_lon) != 0:
                    # 需要通过google api中获取位置信息
                    lat_lon_key = '%s_%s' % (item_lat, item_lon)
                    if lat_lon_key not in lat_lon_map:
                        positions = geocode(item_lat, item_lon)
                        time.sleep(1)
                        lat_lon_map[lat_lon_key] = positions
                    else:
                        positions = lat_lon_map[lat_lon_key]
                    new_country = positions['country'].encode('utf-8', 'ignore')
                    new_region = positions['region'].encode('utf-8', 'ignore')
                    new_city = positions['city'].encode('utf-8', 'ignore')

                    line_msg = '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' % (item_id, fuzzy_item_id, item_country, item_region,
                                                                              item_city, item_lat, item_lon, ip_address,
                                                                              user_country, user_region, user_city,
                                                                              new_country, new_region, new_city, positions['lat'], positions['lon'])
                    file_output.write('%s\n' % line_msg)
                else:
                    # 需要通过geoip获取位置信息
                    if ip_address != '':
                        positions = geoip_position(ip_address)
                        lat = positions['lat']
                        lon = positions['lon']
                        lat_lon_key = '%s_%s' % (lat, lon)
                        if lat_lon_key not in lat_lon_map:
                            positions = geocode(lat, lon)
                            time.sleep(1)
                            lat_lon_map[lat_lon_key] = positions
                        else:
                            positions = lat_lon_map[lat_lon_key]

                        new_country = positions['country'].encode('utf-8', 'ignore')
                        new_region = positions['region'].encode('utf-8', 'ignore')
                        new_city = positions['city'].encode('utf-8', 'ignore')

                        line_msg = '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s' % (item_id, fuzzy_item_id, item_country, item_region,
                                                                              item_city, item_lat, item_lon, ip_address,
                                                                              user_country, user_region, user_city,
                                                                              new_country, new_region, new_city, positions['lat'], positions['lon'])
                        file_output.write('%s\n' % line_msg)

            print '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' % (item_id, fuzzy_item_id, item_country, item_region, item_city,
                                                        item_lat, item_lon, ip_address, user_country, user_region, user_city)

            line = file_read.readline()
    except Exception, e:
        print e


def init_data():
    try:
        file_read = open('/tmp/item_city_new.txt')
        line = file_read.readline()

        while line:
            msg = line.replace('\n', '').split('|')
            item_id = msg[0]
            fuzzy_item_id = msg[1]
            country = msg[11]
            region = msg[12]
            city = msg[13]
            lat = msg[14]
            lon = msg[15]

            print '%s,%s,%s,%s,%s,%s,%s' % (item_id, fuzzy_item_id, country, region, city, lat, lon)

            # 更新es信息
            count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})
            if count['count'] > 0:
                es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id,
                          body={'doc': {'country': country, 'region': region, 'city': city, 'location': {'lat': float(lat), 'lon': float(lon)}}})
            else:
                print '[warn]: %s is not exits in es' % item_id

            # 更新数据库
            Item.objects.filter(id=item_id).update(country=country, region=region, city=city, lat=float(lat), lon=float(lon))

            # 更新redis
            if r.exists('item:%s' % item_id):
                r.hmset('item:%s' % item_id, {'country': country, 'region': region, 'city': city, 'location': json.dumps({'lat': float(lat), 'lon': float(lon)})})

            line = file_read.readline()
    except Exception, e:
        print e


class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            # gen_positions()
            init_data()

            print 'Complete synchronization!'
        except Exception, e:
            print e
            raise CommandError(e)
