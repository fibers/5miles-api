__author__ = 'wangguanglei'

import redis
from django.core.management.base import BaseCommand, CommandError
from entity.models import Item, ItemCategory, Image
from common.biz import *
from common.utils import get_timestamp, lat_or_lon_format
from entity.service import UserService
from common import utils

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        try:
            item_fs = open('/tmp/reload_items.txt')
            line = item_fs.readline()
            while line:
                item_id = int(line.replace('\n', ''))
                item = Item.objects.get(pk=item_id)
                if item.weight:
                    line = item_fs.readline()
                    continue
                item.weight = 5
                item.state = ITEM_STATE_LISTING
                item.save()
                cats = ItemCategory.objects.filter(item_id=item_id)
                imgs = Image.objects.filter(item_id=item_id)
                img_str = '['
                for img in imgs:
                    if img_str != '[':
                        img_str += ','
                    img_str += '{"imageLink":"' + img.image_path + '","width":' + str(img.image_width) + ',"height":' + str(img.image_height) + '}'
                img_str += ']'
                item_id = item.id
                fuzzy_item_id = item.fuzzy_item_id
                nowts = utils.get_now_timestamp()
                '''
                with r.pipeline() as pipe:
                    pipe.set('fuzzy_item_id:%s:item_id' % fuzzy_item_id, item_id)
                    pipe.hmset('item:%s' % item_id,
                               get_item_redis_json(fuzzy_item_id, 0, item.title, item.desc, cats[0].cat_id, item.price,
                                                   item.currency, item.local_price, lat_or_lon_format(item.lat), lat_or_lon_format(item.lon), img_str,
                                                   item.mediaLink, item.shipping_method, item.user_id,
                                                   item.original_price, item.brand_id, item.brand_name, 0,
                                                   get_timestamp(item.created_at) if item.created_at else nowts,
                                                   get_timestamp(item.modified_at) if item.modified_at else nowts,
                                                   get_timestamp(item.update_time) if item.update_time else nowts))
                    pipe.set('item:%d:owner' % item_id, item.user_id)

                    pipe.zadd('user:%s:my_selling_list' % item.user_id, item_id, get_timestamp(item.modified_at) if item.modified_at else nowts)
                    pipe.zadd('user:%s:user_selling_list' % item.user_id, item_id, get_timestamp(item.modified_at) if item.modified_at else nowts)
                    pipe.execute()
                '''

                user_items_num = UserService().get_listing_num(item.user_id)
                owner = get_user_from_id(item.user_id, item_num=user_items_num+1)
                doc = get_item_es_json(fuzzy_item_id, item.title, item.desc, cats[0].cat_id, json.loads(img_str), item.mediaLink, item.price, item.currency,
                                       item.local_price, item.original_price, item.brand_id, item.brand_name, lat_or_lon_format(item.lat), lat_or_lon_format(item.lon), owner, item.weight,
                                       get_category_as_json(cats[0].cat_id), get_timestamp(item.created_at) if item.created_at else nowts,
                                       get_timestamp(item.modified_at) if item.modified_at else nowts, get_timestamp(item.update_time) if item.update_time else nowts, 0)
                es_dao_item().index(item_id, doc)
                UserService().update_item_num(item.user_id)
                print 'item %s old_state %s' % (item.id, item.state)
                line = item_fs.readline()
        except Exception, e:
            raise CommandError(e)
