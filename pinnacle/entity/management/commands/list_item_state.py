__author__ = 'wangguanglei'

import redis
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        try:
            unlisted_cnt = 0
            for item_id in range(469320, 470168):
                if not r.exists('item:%s' % item_id):
                    print 'item:%s not exists.' % item_id
                    continue

                s = r.hget('item:%s' % item_id, 'state')
                print 'item:%s,state:%s' % (item_id, s)
                if int(s) == 6:
                    unlisted_cnt += 1
            print unlisted_cnt
        except Exception, e:
            raise CommandError(e)
