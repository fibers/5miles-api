# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, UserService


class Command(BaseCommand):
    args = '<user_id, lat, lon>'
    help = 'modify user location'

    def handle(self, *args, **options):
        if len(args) < 3:
            raise CommandError('Invalid argument')

        try:
            UserService().lbs_update(user_id=args[0], lat=float(args[1]), lon=float(args[2]), zipcode=args[3] if len(args)>3 else None)
        except Exception, e:
            raise CommandError(e)
