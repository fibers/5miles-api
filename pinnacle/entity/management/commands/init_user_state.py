# -*- coding: utf-8 -*-
author = 'dragon'

import redis
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.models import User
from common.biz import USER_STATE_VALID

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)


class Command(BaseCommand):
    args = ''
    help = u'初始化所有用户状态为有效'

    def handle(self, *args, **options):
        try:
            current_user_id = int(r.get('global:next_user_id'))
            for user_id in range(1, current_user_id + 1):
                if not r.exists('user:%s' % user_id):
                    continue

                print 'processing user: %s' % user_id
                r.hset('user:%s' % user_id, 'state', USER_STATE_VALID)
                print 'Done'
        except Exception, e:
            print e
            raise CommandError(e)


