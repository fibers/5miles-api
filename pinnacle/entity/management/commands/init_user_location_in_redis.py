# -*- coding: utf-8 -*-
__author__ = 'dragon'

import logging
import redis
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.models import User


class Command(BaseCommand):
    args = ''
    help = u'用User表中的位置信息初始化redis相应的记录'

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_user_id = int(r.get('global:next_user_id'))
            for user_id in range(1, current_user_id + 1):
                if not r.exists('user:%s' % user_id):
                    continue

                users = User.objects.filter(pk=user_id, is_robot=0)
                if users:
                    print 'processing user#%s, country: %s, region: %s, city: %s, lat: %s, lon: %s' \
                          % (user_id, users[0].country, users[0].region, users[0].city, users[0].latitude, users[0].longitude)
                    r.hmset('user:%s' % user_id,
                            {'country': users[0].country, 'region': users[0].region, 'city': users[0].city,
                             'lat': users[0].latitude if users[0].latitude is not None else '',
                             'lon': users[0].longitude if users[0].longitude is not None else ''})
        except Exception, e:
            print e
            raise CommandError(e)


