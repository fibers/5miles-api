# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import re
import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_field_in_searchengine
from elasticsearch import Elasticsearch
from entity.models import User

craigslist_reg = 'az[0-9_]*@worldbuz.com'


class Command(BaseCommand):
    help = 'Add is_robot field to elasticsearch'

    def handle(self, *args, **options):
        try:
            es = Elasticsearch(settings.ES_HOSTS)
            print 'Connecting to elasticsearch'

            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            print 'Connecting to redis'

            current_item_id = int(r.get('global:next_item_id'))
            # for item_id in range(197824, current_item_id + 1):
            for item_id in range(1, current_item_id + 1):
                item_key = 'item:%s' % item_id
                if r.exists(item_key):
                    user_id = r.hget('item:%s' % item_id, 'user_id')

                    # 更新redis中用户是否为机器人的标示
                    user_key = 'user:%s' % user_id
                    if r.exists(user_key):
                        email = r.hget(user_key, 'email')

                        p_craigslist = re.compile(craigslist_reg)
                        if p_craigslist.match(email):
                            print '%s,%s,%s' % (item_id, user_id, email)

                            is_robot = 1
                            r.hset(user_key, 'is_robot', is_robot)

                            User.objects.filter(id=user_id).update(is_robot=is_robot)

                            # 更新es中用户是否为机器人的标示
                            try:
                                count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})
                                if count['count'] > 0:
                                    es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id,
                                              body={"doc": {'owner': {'is_robot': int(is_robot)}}})
                            except Exception:
                                print 'update item:%d failed' % item_id

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)