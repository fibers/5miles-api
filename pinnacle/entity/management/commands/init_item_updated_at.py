# -*- coding: utf-8 -*-
__author__ = 'dragon'

import redis
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import search_item_by_id, update_field_in_searchengine

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)


class Command(BaseCommand):
    args = ''
    help = u'初始化redis与elasticsearch中商品的更新时间'

    def handle(self, *args, **options):
        try:
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                # 更新redis
                item_key = 'item:%s' % item_id
                if not r.exists(item_key):
                    continue

                item_info = r.hmget(item_key, 'user_id', 'created_at', 'updated_at')
                try:
                    user_id = int(item_info[0])
                    created_at = item_info[1]
                    updated_at = item_info[2]

                    if not updated_at:
                        print 'redis: item_id => %s, user_id => %s, created_at => %s, updated_at => %s' \
                              % (item_id, user_id, created_at, updated_at)
                        r.hset(item_key, 'updated_at', created_at)
                except Exception, e:
                    print e

                # 更新es
                doc = search_item_by_id(item_id)
                hits = doc['hits']['hits']
                if len(hits) == 0:
                    continue

                if 'updated_at' not in hits[0]['_source'] \
                        or not hits[0]['_source']['updated_at']:
                    try:
                        print 'doc: item_id => %s, user_id => %s, created_at => %s, updated_at => %s' \
                              % (item_id, user_id, created_at, updated_at)
                        update_field_in_searchengine(item_id, 'updated_at', int(created_at))
                    except Exception, e:
                        print e

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)


