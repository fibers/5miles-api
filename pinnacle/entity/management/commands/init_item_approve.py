# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'


import redis
import logging

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.biz import ITEM_STATE_LISTING, ITEM_STATE_UNAPPROVED, ITEM_STATE_UNAVAILABLE

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                item_key = 'item:%s' % item_id
                if r.exists(item_key):
                    item_info = r.hmget('item:%s' % item_id, 'user_id', 'state', 'created_at')
                    try:
                        user_id = int(item_info[0])
                        state = int(item_info[1])
                        created_at = item_info[2]

                        print '%s,%s,%s,%s' % (user_id, item_id, state, created_at)

                        # 后台审核不通过
                        if state != ITEM_STATE_UNAVAILABLE:
                            r.zadd('user:%s:my_selling_list' % user_id, item_id, created_at)
                            if state != ITEM_STATE_UNAPPROVED:
                                r.zadd('user:%s:user_selling_list' % user_id, item_id, created_at)

                    except Exception, e:
                        logger.error(e)

            print 'Complete synchronization!'
        except Exception, e:
            print e