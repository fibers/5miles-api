# -*- coding: utf-8 -*-
__author__ = 'dragon'

import redis
from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, UserService, ItemService

from django.conf import settings

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = '<user_id, lat, lon>'
    help = 'modify user location with items'

    def handle(self, *args, **options):
        if len(args) < 3:
            raise CommandError('Invalid argument')

        try:
            UserService().lbs_update(user_id=args[0], lat=float(args[1]), lon=float(args[2]), zipcode=args[3] if len(args)>3 else None)

            items = r.zrange('user:%s:my_selling_list' % args[0], 0, -1)
            if items:
                for item in items:
                    try:
                        ItemService().lbs_update(item_id=item, lat=float(args[1]), lon=float(args[2]), is_update_owner=False)
                    except Exception, ex:
                        print ex
        except Exception, e:
            raise CommandError(e)
