# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, ItemService


class Command(BaseCommand):
    args = ''
    help = u'初始化es文档中的用户认证字段'

    def handle(self, *args, **options):
        srs = SrService()
        item_service = ItemService()
        redis = srs.get_redis()
        try:
            current_item_id = int(redis.get('global:next_item_id'))
            for i in xrange(current_item_id + 1):
                item_id = i + 1
                if not redis.exists('item:%s' % item_id):
                    continue

                item_service.update_es_verification(item_id)
        except Exception, e:
            raise CommandError(e)


