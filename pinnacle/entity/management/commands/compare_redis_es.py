# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch
from entity.models import Item, Image

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'

class Command(BaseCommand):

    def handle(self, *args, **options):

        # file_output = open('/tmp/es_yes_redis_not.txt', 'w+')
        try:
            current_item_id = r.get("global:next_item_id")
            for item_id in range(1, int(current_item_id) + 1):
                item_key = 'item:%s' % item_id
                print "total_item:%s,item_id:%s" % (current_item_id, item_id)
                images = r.hget(item_key, "images")
                if images:
                    if not ('imageLink' in images):

                        # 从redis中获取商品价格,更新es、数据库中的价格
                        # price = r.hget(item_key, "price")
                        # es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id, body={"doc": {"price": float(price)}})
                        # Item.objects.filter(pk=int(item_id)).update(price=price)

                        # 从es中获取图片列表更新redis中的图片列表以及数据库中的图片信息
                        result = es.search(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})

                        hits = result['hits']['hits']
                        for hit in hits:
                            images = hit['_source']['images']
                            imagestr = ''
                            image_list = []
                            for image in images:
                                width = image['width'] if 'width' in image else 0
                                height = image['height'] if 'height' in image else 0
                                imageLink = image['imageLink']

                                if imagestr != '':
                                    imagestr = "%s," % imagestr

                                imagestr = '%s{"imageLink": "%s", "width": %s, "height": %s}' % (imagestr, imageLink, width, height)
                                image_list.append(Image(item_id=item_id, image_path=imageLink, image_height=height, image_width=width))

                            # 更新数据库信息
                            Image.objects.bulk_create(image_list)

                            # 更新redis信息
                            r.hmset('item:%s' % item_id ,{'images': u'[%s]' % imagestr})

                        # file_output.writelines("%s\n" % str(item_id))

            print 'Complete synchronization!'
        except Exception, e:
            print e
            raise CommandError(e)
        # finally:
        #     if file_output is not None:
        #         file_output.close()