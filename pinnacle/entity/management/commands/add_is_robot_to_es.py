__author__ = 'dragon'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch

class Command(BaseCommand):
    help = 'Add is_robot field to elasticsearch'

    def handle(self, *args, **options):
        try:
            es = Elasticsearch(settings.ES_HOSTS)
            print 'Connecting to redis'
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                user_id = r.get('item:%s:owner' % item_id)
                is_robot = r.hget('user:%s' % user_id, 'is_robot')
                print 'user_id %s is_robot? %s' % (user_id, is_robot)
                if not is_robot:
                    is_robot = 0

                print 'update item: %d' % item_id
                # update_field_in_searchengine(item_id, 'owner.is_robot', is_robot)
                try:
                    es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id,
                              body={"doc": {'owner': {'is_robot': int(is_robot)}}})
                except Exception:
                    print 'update item:%d failed' % item_id

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)