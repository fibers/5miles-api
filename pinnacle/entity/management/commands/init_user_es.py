# -*- coding: utf-8 -*-
from common.es_dao import es_dao_user

__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        srs = SrService()
        r = srs.get_redis()
        try:
            current_user_id = int(r.get('global:next_user_id'))
            for user_id in range(1, current_user_id + 1):
                if not r.exists('user:%s' % user_id):
                    continue

                print 'processing user: %s' % user_id
                user_info = r.hmget('user:%s' % user_id, 'nickname', 'lat', 'lon', 'verified', 'is_robot', 'is_anonymous')
                if user_info[5] == '1':
                    continue

                item_num = r.zcard('user:%s:user_selling_list' % user_id)
                last_posted_at = None
                if item_num > 0:
                    last_item = r.zrevrange('user:%s:user_selling_list' % user_id, 0, 0)
                    # print 'last_item: %s' % last_item
                    last_posted_at = r.hget('item:%s' % last_item[0], 'created_at')
                    # print 'last_posted_at: %s' % last_posted_at

                user_doc = {'id': user_id, 'location': {'lat': float(user_info[1]) if user_info[1] else 0, 'lon': float(user_info[2]) if user_info[2] else 0},
                            'nickname': user_info[0], 'item_num': item_num, 'is_robot': int(user_info[4]) if user_info[4] else 0}
                if last_posted_at:
                    user_doc['last_posted_at'] = long(last_posted_at)

                es_dao_user().index(user_id, user_doc)

                print 'Done'
        except Exception, e:
            raise CommandError(e)


