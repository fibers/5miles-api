# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import urllib
import urllib2
import datetime
import threading
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.http.request import QueryDict
from django.http import HttpResponse, HttpRequest
from apilog.service import AnalyticsService

api_host = 'http://54.64.120.187'


def get_now_timestamp_str2():
    now = datetime.datetime.utcnow()
    return now.strftime('%Y-%m-%d %H:%M:%SZ')


def send_analytics_log(user_id):
    try:
        ext_params = {}
        request = HttpRequest()
        request.user_id = user_id
        request.REQUEST = {}
        index = 1

        api_url = 'api/v2/item_detail/?item_id=3vl0b7rvlKgnXwZz'
        params = None
        while True:
            ext_params['exec_time'] = index
            ext_params['status_code'] = 200
            AnalyticsService().record_analytic_data(request, ext_params)
            # result = send_request(api_url)

            # print index, result
            if index == 100:
                break

            index += 1

        print 'Complete synchronization!'
    except Exception, e:
        print e


def send_request(api_url):
    start_time = datetime.datetime.now()
    try:
        postData = None
        req = urllib2.Request('%s/%s' % (api_host, api_url), postData)
        req.add_header('cache-control', 'no-cache')
        req.add_header('accept', '*/*')
        req.add_header('X-FIVEMILES-USER-ID', '1')
        req.add_header('X-FIVEMILES-USER-TOKEN', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxfQ.I-FweT6e62RZ-wEvs3wZzC-YjvYDYbwZmDnTPjnaaew')
        req.add_header('connection', 'keep-alive')
        resp = urllib2.urlopen(req)
        status = 0 if resp.code == 200 else -1
    except Exception, e:
        status = -1
        print api_url, e

    end_time = datetime.datetime.now()
    return status, (end_time - start_time).seconds


def record_item_exposure(user_id):
    try:
        ext_params = {}
        index = 1

        request = HttpRequest()
        request.user_id = user_id
        request.path_info = 'api/v2/suggest/'
        request.REQUEST = {}

        items = [{'id': 'WVOmKLJKkJGN61nb'}, {'id': 'O6zdMEg5VJ42BwXG'}, {'id': '817ZR4JyRrvYkmKj'}]
        offset = index

        while True:
            AnalyticsService().record_item_exposure(request, items, offset, ext_params)

            print index
            if index == 10:
                break

            index += 1

        print 'Complete synchronization!'
    except Exception, e:
        print e


threads = []
for i in range(1, 2):
    t = threading.Thread(target=send_analytics_log, args=(i,))
    # t = threading.Thread(target=record_item_exposure, args=(i,))
    threads.append(t)


class Command(BaseCommand):

    def handle(self, *args, **options):

        log_start_time = datetime.datetime.now()

        for t in threads:
            t.setDaemon(True)
            t.start()

        for t in threads:
            t.join()

        log_end_time = datetime.datetime.now()
        print "[testing log] - Total time costs  :   %ds" % (log_end_time - log_start_time).seconds
        print 'thread over'