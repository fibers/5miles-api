__author__ = 'wangguanglei'

import redis
from django.core.management.base import BaseCommand, CommandError
from entity.models import Item, ItemCategory, Image
from common.biz import *
from common.utils import get_timestamp, lat_or_lon_format

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        if len(args) < 1:
            raise CommandError('Invalid argument')

        item_id = int(args[0])
        try:
            item = Item.objects.get(pk=item_id)
            cats = ItemCategory.objects.filter(item_id=item_id)
            imgs = Image.objects.filter(item_id=item_id)
            img_str = '['
            for img in imgs:
                if img_str != '[':
                    img_str += ','
                img_str += '{"imageLink":"' + img.image_path + '","width":' + str(img.image_width) + ',"height":' + str(img.image_height) + '}'
            img_str += ']'
            item_id = item.id
            fuzzy_item_id = item.fuzzy_item_id
            with r.pipeline() as pipe:
                pipe.set('fuzzy_item_id:%s:item_id' % fuzzy_item_id, item_id)
                pipe.hmset('item:%s' % item_id,
                           get_item_redis_json(fuzzy_item_id, 0, item.title, item.desc, cats[0].cat_id, item.price,
                                               item.currency, item.local_price, lat_or_lon_format(item.lat), lat_or_lon_format(item.lon), img_str,
                                               item.mediaLink, item.shipping_method, item.user_id,
                                               item.original_price, item.brand_id, item.brand_name, 0,
                                               get_timestamp(item.created_at),
                                               get_timestamp(item.modified_at),
                                               get_timestamp(item.update_time),
                                               item.country, item.region, item.city))
                pipe.set('item:%d:owner' % item_id, item.user_id)

                pipe.zadd('user:%s:my_selling_list' % item.user_id, item_id, get_timestamp(item.modified_at))
                pipe.zadd('user:%s:user_selling_list' % item.user_id, item_id, get_timestamp(item.modified_at))
                pipe.execute()
        except Exception, e:
            raise CommandError(e)



