__author__ = 'dragon'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.models import User, Item, ItemCategory, Category, Image, FacebookProfile
from spam.models import ItemReportRecord, UserReportRecord
from message.models import UserSubscription
#from common.utils_es import delete_item_mapping, remove_all_items_in_searchengine
from common.es_dao import es_dao_user, es_dao_item


class Command(BaseCommand):
    help = 'Clear all data in redis, elasticsearch and database!!!'

    def handle(self, *args, **options):
        try:
            print 'Connecting to redis'
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)

            print 'Delete all keys in all databases on the current host'
            r.flushall()

            print 'Remove all items in search engine'
            es_dao_item().delete_mapping()
            es_dao_user().delete_mapping()
            #remove_all_items_in_searchengine()

            print 'Clear database'
            UserSubscription.objects.all().delete()
            FacebookProfile.objects.all().delete()
            UserReportRecord.objects.all().delete()
            ItemReportRecord.objects.all().delete()
            ItemCategory.objects.all().delete()
            Image.objects.all().delete()
            Item.objects.all().delete()
            # Category.objects.all().delete()
            User.objects.all().delete()

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)