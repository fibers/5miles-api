# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'


import redis
import logging

from django.core.management.base import BaseCommand
from django.conf import settings
from common.utils import get_now_timestamp
from common.es_dao import es_dao_item

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            current_item_id = int(r.get('global:next_item_id'))
            nts = get_now_timestamp()
            for item_id in range(1, current_item_id + 1):
                item_key = 'item:%s' % item_id
                if r.exists(item_key) and es_dao_item().exists(item_id):
                    try:
                        print '%s,%s' % (item_id, nts)
                        es_dao_item().update(item_id, {"doc": {"last_active_at": nts}})
                    except Exception, e:
                        logger.error(e)

            print 'Completed init data!'
        except Exception, e:
            print e