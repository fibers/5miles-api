# -*- coding: utf-8 -*-
__author__ = 'dragon'

import math

import redis
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch

from common.biz import get_category_as_json
from common.utils_es import update_fields_in_searchengine
from entity.models import ItemCategory


r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
es = Elasticsearch(settings.ES_HOSTS)


class Command(BaseCommand):
    args = '<type, target_cat_id, keyword|source_cat_id>'
    help = u'迁移商品分类'
    PAGE_SIZE = 2

    @staticmethod
    def keyword_query(keyword):
        return {'multi_match': {'query': keyword, 'type': 'best_fields',
                'fields': ['title^10', 'brand_name'], 'tie_breaker': 0.3, 'minimum_should_match': '75%'}}

    @staticmethod
    def update_item_category(item_id, target_cat_id):
        target_cat_id = int(target_cat_id)
        try:
            item_key = 'item:%s' % item_id
            if r.exists(item_key):
                r.hset(item_key, 'cat_id', target_cat_id)

                partial_doc = {'cat_id': target_cat_id, 'cat': get_category_as_json(target_cat_id)}
                update_fields_in_searchengine(item_id, **partial_doc)

                item_category = ItemCategory.objects.get(item_id=item_id)
                item_category.cat_id = target_cat_id
                item_category.save()
        except Exception, e:
            print 'failed to change category: %s, %s, %s' % (item_id, target_cat_id, e)

    def batch_update(self, es_body, target_cat_id):
        count = es.count(index=settings.ES_INDEX_MAIN,
                         doc_type=settings.ES_DOCTYPE_ITEM,
                         body=es_body)
        print 'count: %s' % count
        total_count = count['count']
        print 'total_count: %d' % total_count
        ids = es.search(index=settings.ES_INDEX_MAIN,
                        doc_type=settings.ES_DOCTYPE_ITEM,
                        body=es_body, fields='_id')
        # print 'processing offset: %d, size: %d' % (es_body['from'], es_body['size'])
        # print 'results: %s' % results
        print ids
        for hit in ids['hits']['hits']:
            item_id = hit['_id']
            print 'processing item: %s' % item_id
            self.update_item_category(item_id, target_cat_id)

    def handle(self, *args, **options):
        if len(args) < 3:
            raise CommandError('insufficient arguments')

        try:
            if args[0] == 'keyword':
                for kw in args[2].split(','):
                    es_body = dict()
                    es_body['query'] = {}
                    es_body['query']['filtered'] = {}
                    es_body['query']['filtered']['query'] = self.keyword_query(kw)
                    es_body['query']['filtered']['filter'] = {'term': {'cat_id': int(args[3])}}
                    self.batch_update(es_body, args[1])

            elif args[0] == 'category':
                es_body = {'query': {'term': {'cat_id': int(args[2])}}}
                self.batch_update(es_body, args[1])
        except Exception, e:
            print e
            raise CommandError(e)


