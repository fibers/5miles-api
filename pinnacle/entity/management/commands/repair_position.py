# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis
import json

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch
from entity.models import Item, User

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'

import geoip2.database
reader = geoip2.database.Reader(settings.GEOIP['DB_URL'])
print 'load geoip2'

class Command(BaseCommand):
    help = 'Repair position'

    def handle(self, *args, **options):
        try:
            items = Item.objects.filter(lat=0, lon=0)
            if items:
                for item in items:
                    user = User.objects.get(pk=item.user_id)

                    if user:
                        ip_address = user.ip_address
                        # 根据ip获取经纬信息
                        try:
                            if ip_address != '' or ip_address is not None:
                                response = reader.city(ip_address)
                                lat = response.location.latitude
                                lon = response.location.longitude

                                print "item:%s,fuzzy_item_id:%s,ip:%s=>%s,%s" % (item.id, item.fuzzy_item_id, ip_address, lat, lon)

                                # 更新redis
                                if r.exists('item:%s' % item.id):
                                    r.hmset('item:%s' % item.id, {'location': json.dumps({'lat': lat, 'lon': lon})})
                                else:
                                    print '[warn]: %s is not exits in redis' % item.id

                                # 更新es
                                count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item.id)}}})
                                if count['count'] > 0:
                                    es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item.id,
                                              body={"doc": {"location": {"lat": lat, "lon": lon}}})
                                else:
                                    print '[warn]: %s is not exits in es' % item.id

                                # 更新数据库
                                Item.objects.filter(pk=item.id).update(lon=lon, lat=lat)

                            else:
                                print "[warn]: item:%s,fuzzy_item_id:%s" % (item.id, item.fuzzy_item_id)
                        except:
                            print "[warn]: item:%s,fuzzy_item_id:%s,ip:%s" % (item.id, item.fuzzy_item_id, ip_address)

            print 'Finished!'
        except Exception, e:
            print e
            raise CommandError(e)