__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.tasks import sync_redis_to_db

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            current_item_id = r.get("global:next_item_id")

            for item_id in range(1, int(current_item_id) + 1):
                print "sync_redis_to_db('item', 'C', %s)" % (item_id)
                sync_redis_to_db('item', 'C', item_id)

            print 'Complete synchronization!'
        except Exception, e:
            print e
            raise CommandError(e)