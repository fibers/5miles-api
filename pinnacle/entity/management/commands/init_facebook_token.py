# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import re
import json
import logging
import redis

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from entity.models import FacebookProfile, User
from common.biz import link_facebook

logger = logging.getLogger('pinnacle')

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
regex = re.compile(r'\\(?![/u"])')

class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            # 1.从日志文件中获取facebook token信息(user_id,fb_userid,token)写入临时文件中
            file_read = open('/tmp/facebook_token.txt')
            line = file_read.readline()

            while line:
                facebook = line.replace('\n', '').split('|')

                user_id = facebook[0]

                if facebook[1] != "":

                    if '":"' in facebook[1]:
                        facebook_str = facebook[1]
                    else:
                        facebook_str = '"%s"' % facebook[1].replace(':', '":"').replace(',', '","').replace('"http":"//', '"http://').replace('"https":"//', '"https://')
                    facebook_json = json.loads('{%s}' % regex.sub(r"\\\\", facebook_str))
                    fb_user_id = facebook_json['fb_userid'] if 'fb_userid' in facebook_json else ''
                    fb_token = facebook_json['fb_token'] if 'fb_token' in facebook_json else ''
                    fb_token_expires = 1429027200

                    if user_id == '':
                        users = User.objects.filter(fb_user_id=fb_user_id).values('id')
                        if len(users) > 0:
                            user_id = users[0]['id']
                    if fb_token != '' and fb_user_id != '' and r.exists('user:%s' % user_id) and not r.exists('user:%s:linked_facebook' % user_id):
                        print '%s,%s,%s,%s' % (user_id, fb_user_id, fb_token, fb_token_expires)
                        # 2.更新redis信息
                        link_facebook(user_id, fb_user_id, fb_token, fb_token_expires)
                        # 3.更新数据库信息
                        FacebookProfile.objects.filter(id=fb_user_id).update(fb_token=fb_token, fb_token_expires=fb_token_expires)
                    else:
                        logger.error('[error] - %s,%s,%s,%s' % (user_id, fb_user_id, fb_token, fb_token_expires))
                else:
                    print 'Facebook infomation is empty'

                line = file_read.readline()
            logger.info('Done!')
        except Exception, e:
            logger.exception(e)
            raise CommandError(e)
        finally:
            if file_read:
                file_read.close()