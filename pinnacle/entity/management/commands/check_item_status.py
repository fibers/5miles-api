# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.models import Item

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            error_states = []
            # 把redis中得状态同步到数据库中
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                item_key = 'item:%s' % item_id
                print item_id
                if r.exists(item_key):
                    data = r.hget('item:%s' % item_id, 'state')
                    redis_state = int(data[0])

                    try:
                        item = Item.objects.get(pk=item_id)
                        db_state = item.state
                        if redis_state != db_state:
                            error_states.append('item_id:%s,redis:%s,db:%s' % (item_id, redis_state, db_state))
                    except Exception, e1:
                        error_states.append('item_id:%s,redis:%s,db:%s' % (item_id, redis_state, -1))
                        print e1

            print 'error status list:'
            print error_states
            print 'Complete synchronization!'
        except Exception, e:
            print e
            raise CommandError(e)