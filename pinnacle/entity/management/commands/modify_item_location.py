# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService, UserService, ItemService


class Command(BaseCommand):
    args = '<item_id, lat, lon>'
    help = 'modify item location'

    def handle(self, *args, **options):
        if len(args) < 3:
            raise CommandError('Invalid argument')

        try:
            ItemService().lbs_update(item_id=args[0], lat=float(args[1]), lon=float(args[2]), is_update_owner=False)
        except Exception, e:
            raise CommandError(e)
