# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'

class Command(BaseCommand):

    def handle(self, *args, **options):

        file_output = open('/tmp/redis_not_exist.txt', 'w+')
        file_object = None
        try:
            file_object = open('/tmp/error_data.txt')
            line = file_object.readline()

            while line:
                item_id = line.replace('\n', '')
                item_key = 'item:%s' % item_id
                # 从redis中获取商品价格,更新es、数据库中的价格
                price = r.hget(item_key, "price")

                result = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query':{'term':{'_id':str(item_id)}}})
                count = result['count']
                if count > 0:
                    es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id, body={"doc": {"price": float(price)}})
                else:
                    file_output.writelines("%s\n" % str(item_id))

                line = file_object.readline()

            print 'Complete synchronization!'
        except Exception, e:
            print e
            raise CommandError(e)
        finally:
            if file_object is not None:
                file_object.close()
            if file_output is not None:
                file_output.close()