# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis
import json

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch
from entity.models import Item, User, Image

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            items = Item.objects.filter(state=5).order_by('-update_time')[1:300]
            if items:
                for item in items:
                    item_id = item.id
                    user_id = item.user_id
                    count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})
                    es_state = r.hget('item:%s' % item.id, 'state')
                    if count['count'] == 0 and int(es_state) == 5:
                        with r.pipeline() as pipe:
                            pipe.zrem('user:%s:my_selling_list' % user_id, item_id)
                            # 把商品从审核通过与审核不通过列表中移除
                            pipe.zrem('user:%s:user_selling_list' % user_id, item_id)
                            pipe.execute()
                    else:
                        print '%s:%s,%s,%s' % (item.user_id, count['count'], es_state, item.update_time)


            print 'Finished!'
        except Exception, e:
            print e
            raise CommandError(e)