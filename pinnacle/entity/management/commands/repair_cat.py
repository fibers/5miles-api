# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import re
import redis
import logging

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_fields_in_searchengine
from elasticsearch import Elasticsearch
from entity.models import ItemCategory
from common.biz import get_category_as_json

es = Elasticsearch(settings.ES_HOSTS)
print 'Connecting to elasticsearch'

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):

    def get_cat_items(self, cat_id):
        try:
            return ItemCategory.objects.filter(cat_id=cat_id).values_list('item_id')
        except Exception, e:
            logger.error('failed to get cat items: %s, %s' % (cat_id, e))


    def edit_cat(self, item_id, cat_id):
        try:
            item_key = 'item:%s' % item_id
            if r.exists(item_key):
                d = r.hgetall(item_key)
                if 'cat_id' in d:
                    # 1.修复redis
                    r.hset(item_key, 'cat_id', cat_id)

                    # 3.修复es
                    partial_doc = {'cat_id': cat_id, 'cat': get_category_as_json(cat_id)}
                    update_fields_in_searchengine(item_id, **partial_doc)

                    # 2.修复数据库
                    item_category = ItemCategory.objects.get(item_id=item_id)
                    item_category.cat_id = cat_id
                    item_category.save()

        except Exception, e:
            logger.error('failed to edit cat: %s, %s, %s' % (item_id, cat_id, e))


    def handle(self, *args, **options):
        try:
            # 获取所有5的分类
            five_cat_output = open('/tmp/cat_5.txt', 'w+')
            five_items = self.get_cat_items(5)
            if five_items:
                for item_id in five_items:
                    five_cat_output.write('%s\n' % item_id)
            five_cat_output.close()

            # 获取所有6的分类
            six_cat_output = open('/tmp/cat_6.txt', 'w+')
            six_items = self.get_cat_items(6)
            if six_items:
                for item_id in six_items:
                    six_cat_output.write('%s\n' % item_id)
            six_cat_output.close()

            # 修复数据
            five_cat_read = open('/tmp/cat_5.txt')
            line = five_cat_read.readline()
            while line:
                item_id = line.replace('\n', '')

                print '5=>6: %s' % item_id
                self.edit_cat(item_id, 6)
                line = five_cat_read.readline()


            six_cat_read = open('/tmp/cat_6.txt')
            line = six_cat_read.readline()
            while line:
                item_id = line.replace('\n', '')

                print '6=>5: %s' % item_id
                self.edit_cat(item_id, 5)
                line = six_cat_read.readline()

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)