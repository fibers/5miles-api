# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import re
import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_field_in_searchengine
from elasticsearch import Elasticsearch
from entity.models import User
from common.biz import unfuzzy_user_id


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            print 'Connecting to redis'
            print settings.REDIS_HOST

            file_output = open('/tmp/users_items_position.txt', 'w+')

            current_user_id = int(r.get('global:next_user_id'))
            for user_id in range(1, current_user_id + 1):
                user_key = 'user:%s' % user_id
                if r.exists(user_key):
                    user = r.hmget('user:%s' % user_id, 'id', 'country', 'region', 'city')
                    fuzzy_user_id = user[0]
                    country = user[1] if user[1] is not None else ''
                    region = user[2] if user[2] is not None else ''
                    city = user[3] if user[3] is not None else ''
                    if city == 'None' or city == '' or region == 'None' or region == '' or country == 'None' or country == '':
                        # 获取用户第一次上传商品的位置
                        item_count = r.zcard('user:%s:my_selling_list' % user_id)
                        if item_count > 0:
                            first_item_id = r.zrange('user:%s:my_selling_list' % user_id, 0, 0)[0]
                            item_key = 'item:%s' % first_item_id
                            if r.exists(item_key):
                                item = r.hmget(item_key, 'id', 'country', 'region', 'city')
                                fuzzy_item_id = item[0]
                                item_country = item[1] if item[1] is not None else ''
                                item_region = item[2] if item[2] is not None else ''
                                item_city = item[3] if item[3] is not None else ''

                                file_output.write('%s,%s,%s,%s,%s,%s,%s,%s,%s\n' % (user_id, fuzzy_user_id, country, region, city, item[0], item[1], item[2], item[3]))

                                # 使用商品的位置修复用户位置
                                if item_region != '' or item_city != '':
                                    # 修复redis和数据库中的数据:以商品中得地理位置为主,生成
                                    r.hmset('user:%s' % user_id, {'region': item_region, 'city': item_city})
                                    User.objects.filter(id=user_id).update(region=item_region, city=item_city)
                                    print user_id, fuzzy_user_id, country, region, city, fuzzy_item_id, item_country, item_region, item_city

                            else:
                                file_output.write('%s,%s,%s,%s,%s\n' % (user_id, fuzzy_user_id, country, region, city))
                                print user_id, fuzzy_user_id, country, region, city

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)
        finally:
            if file_output is not None:
                file_output.close()