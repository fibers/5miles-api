# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService
from common.es_dao import es_dao_item
from entity.models import Item

class Command(BaseCommand):
    args = ''
    help = 'Repair zero item original price in elasticsearch'

    def handle(self, *args, **options):
        srs = SrService()
        es_dao = es_dao_item()
        try:
            #ids = Item.objects.filter(original_price=0, price__gt=0, state__in=[0,1]).values('id')
            ids = Item.objects.filter(original_price__isnull=True, price__gt=0, state__in=[0,1]).values('id')
            i = 0
            for id in ids:
                print 'seq: %d, id: %d' % (i, id['id'])
                try:
                    es_dao.update(id['id'], {'script': 'ctx._source.remove("original_price")'})
                except Exception, e:
                    print e
                i = i + 1
        except Exception, e:
            raise CommandError(e)



