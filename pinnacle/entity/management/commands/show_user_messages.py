__author__ = 'wangguanglei'

import redis
from django.core.management.base import BaseCommand, CommandError
from entity.service import UserService
from django.conf import settings
from entity.models import User

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = '<user_id>'
    help = 'enable user'

    def handle(self, *args, **options):
        if len(args) < 2:
            raise CommandError('Invalid argument')

        user_id = int(args[0])
        msg_type = args[1]

        if msg_type not in ['buying', 'selling', 'system']:
            print 'msg_type not match'
            raise Exception

        if not UserService().user_exists(user_id):
            print 'User %s Not Exist' % user_id
            raise Exception

        try:
            inbox_key = 'user:%s:inbox:%s' % (user_id, msg_type)
            new_count = r.get('%s:new_count' % inbox_key)
            print 'new_count:%s' % new_count
            msg_list = r.zrevrange(inbox_key, 0, -1)
            for msg_id in msg_list:
                msg = r.hgetall('message:%s' % msg_id)
                print msg['from'], msg['to'], msg['item_id'], msg['unread'], msg['count'] if 'count' in msg else None, msg['thread_id']
                owner = r.get('item:%s:owner' % msg['item_id'])
                offer_ids = r.zrange('offerline:%s:entries' % msg['thread_id'], 0, -1)
                for offer_id in offer_ids:
                    offer = r.hgetall('offer:%s' % offer_id)
                    print msg['item_id'], owner, msg['from'], msg['to'], offer['payload'] if 'payload' in offer else offer['text'], offer['msg_type'] if 'msg_type' in offer else 0, msg['timestamp']
        except Exception, e:
            raise CommandError(e)

