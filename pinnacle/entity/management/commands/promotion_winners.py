# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import os
import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.models import FacebookProfile

class Command(BaseCommand):
    help = 'Clear all data in redis, elasticsearch and database'

    def handle(self, *args, **options):
        try:
            print 'Connecting to redis'
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)

            # 获取获奖人员名单(name+email/facebook_url)
            # 1.获取前三名获奖人员名单
            top_users = []
            top_key = 'promotion:post_item_num:charts'
            post_item_infos = r.zrevrange(top_key, 0, 2, withscores=True)
            if post_item_infos:
                for post_item_info in post_item_infos:
                    user_id = post_item_info[0]
                    # 上传的商品数
                    post_item_num = post_item_info[1]
                    # 获奖用户信息
                    user_info = r.hmget('user:%s' % user_id, 'id', 'email', 'fb_user_id', 'nickname')
                    fuzzy_user_id = user_info[0]
                    email = user_info[1]
                    fb_user_id = user_info[2]
                    nickname = user_info[3]
                    if email != '' and email is not None:
                        top_users.append('%s\t%s\t%s\t%s' % (user_id, fuzzy_user_id, nickname, email))
                    elif fb_user_id != '' and fb_user_id is not None:
                        facebook_user = FacebookProfile.objects.filter(id=fb_user_id).values('link')
                        if facebook_user and len(facebook_user) > 0:
                            link = facebook_user[0]['link']
                        top_users.append('%s\t%s\t%s\t%s' % (user_id, fuzzy_user_id, nickname, link))

            # 2.获取每天红包获奖人员名单
            red_package_users = []
            red_package_key = 'promotion:red_package_five:charts'
            user_ids = r.zrevrange(red_package_key, 0, -1)
            if user_ids:
                for user_id in user_ids:
                    # 获奖用户信息
                    user_info = r.hmget('user:%s' % user_id, 'id', 'email', 'fb_user_id', 'nickname')
                    fuzzy_user_id = user_info[0]
                    email = user_info[1]
                    fb_user_id = user_info[2]
                    nickname = user_info[3]
                    if email != '' and email is not None:
                        red_package_users.append('%s\t%s\t%s\t%s' % (user_id, fuzzy_user_id, nickname, email))
                    elif fb_user_id != '' and fb_user_id is not None:
                        facebook_user = FacebookProfile.objects.filter(id=fb_user_id).values('link')
                        if facebook_user and len(facebook_user) > 0:
                            link = facebook_user[0]['link']
                        red_package_users.append('%s\t%s\t%s\t%s' % (user_id, fuzzy_user_id, nickname, link))

            file_output = open('/tmp/promotion_winners.txt', 'w+')
            file_output.write('前三名:\n')
            file_output.write('%s\n' % '\n'.join(str(x) for x in top_users))

            file_output.write('红包获奖名单:\n')
            file_output.write('%s\n' % '\n'.join(str(x) for x in red_package_users))
            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)
        finally:
            if file_output is not None:
                file_output.close()