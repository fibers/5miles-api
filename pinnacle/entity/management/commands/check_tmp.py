# -*- coding: utf-8 -*-
__author__ = 'wangguanglei'


import redis
import logging

from django.core.management.base import BaseCommand
from django.conf import settings
from common.es_dao import es_dao_item
from django.db import connection
import time

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            print 'start...'
            sql = 'insert into item_diff_wgl1(id,db_state,r_state,es_state) values'
            sql_p = ''
            for item_id in range(0, 590000):
                item_key = 'item:%s' % item_id
                state = {'r': -1, 'es': -1}
                if es_dao_item().exists(item_id):
                    state['es'] = 1
                if r.exists(item_key):
                    rs = r.hget(item_key, 'state')
                    state['r'] = int(rs if rs else '0')
                if (state['es'] + state['r']) > -2:
                    if len(sql_p) > 0:
                        sql_p += ','
                    sql_p += '(%s, %s, %s, %s)' % (item_id, -1, state['r'], state['es'])
                if (item_id + 1) % 1000 == 0:
                    if len(sql_p) > 0:
                        cur = connection.cursor()
                        try:
                            cur.execute(sql + sql_p)
                        finally:
                            cur.close()
                        sql_p = ''
                    print 'item_id:%s completed.' % item_id
                    time.sleep(0.01)
            if len(sql_p) > 0:
                cur = connection.cursor()
                try:
                    cur.execute(sql + sql_p)
                finally:
                    cur.close()
                print 'item_id:%s completed.' % item_id
        except Exception, e:
            print e