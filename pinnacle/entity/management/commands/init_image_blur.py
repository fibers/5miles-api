# -*- coding: utf-8 -*-
__author__ = 'dragon'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_field_in_searchengine, search_item_by_id
# from entity.tasks import blur_detect
import logging


logger = logging.getLogger('pinnacle')

class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                    doc = search_item_by_id(item_id)
                    hits = doc['hits']['hits']
                    if len(hits) == 0:
                        continue

                    print 'item: %s, owner: %s' % (item_id, hits[0]['_source']['owner'])
                    if 'is_robot' in hits[0]['_source']['owner'] and hits[0]['_source']['owner']['is_robot'] == 0:
                        # images = hits[0]['_source']['images']
                        # if images:
                        #     print 'human, detect blur'
                        #     blur_detect.delay(item_id, images[0]['imageLink'])
                        pass
                    else:
                        print 'robot, set blur to 10'
                        update_field_in_searchengine(item_id, 'image_blur', 10)

                    # 初始化商品的权重
                    if 'weight' not in hits[0]['_source']:
                        update_field_in_searchengine(item_id, 'weight', 0)

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)