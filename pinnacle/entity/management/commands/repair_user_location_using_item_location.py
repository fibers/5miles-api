# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db import connection
from entity.models import User
from common.es_dao import es_dao_user
from entity.search_base import SrService


class Command(BaseCommand):

    def handle(self, *args, **options):
        file_output = None
        try:
            ext_sql = ''
            if len(args) >= 1:
                user_id = args[0]
                ext_sql = ' and t.user_id=%s' % user_id

            r = SrService().get_redis()
            print 'Connecting to redis'
            print settings.REDIS_HOST

            es_users = es_dao_user()

            # 从数据库中获取所有发过商品的真实用户列表 - 466687用户的位置是准确的
            cursor = connection.cursor()
            user_location_sql = 'select t.user_id, t.item_id, tt.country, tt.region, tt.city, tt.lat, tt.lon, ttt.country, ttt.region, ttt.city, ttt.latitude, ttt.longitude from (' \
                  'select a.id user_id,max(b.id) item_id ' \
                  'from entity_user a, entity_item b ' \
                  'where a.id=b.user_id and b.state!=5 and a.is_robot=0 ' \
                  'and b.country!="" and b.region!="" and b.city!="" ' \
                  'group by a.id) t, entity_item tt, entity_user ttt ' \
                  'where t.item_id=tt.id and tt.user_id=ttt.id ' \
                  'and (ttt.country!=tt.country or ttt.region!=tt.region or ttt.city!=tt.city) and t.user_id != 466687 %s' % ext_sql
            cursor.execute(user_location_sql)
            results = cursor.fetchall()
            if results:
                _index = 1
                file_output = open('/tmp/users_original_position_20150505.txt', 'w+')

                for result in results:
                    user_id = int(result[0])
                    item_id = int(result[1])
                    item_country = result[2].encode('utf-8')
                    item_region = result[3].encode('utf-8')
                    item_city = result[4].encode('utf-8')
                    item_lat = float(result[5])
                    item_lon = float(result[6])
                    user_country = result[7].encode('utf-8') if result[7] else ''
                    user_region = result[8].encode('utf-8') if result[8] else ''
                    user_city = result[9].encode('utf-8') if result[9] else ''
                    user_lat = result[10]
                    user_lon = result[11]

                    print '%s. user_id=%s, user_country=%s, user_region=%s, user_city=%s, user_lat=%s, user_lon=%s, ' \
                          'item_id=%s, item_country=%s, item_region=%s, item_city=%s, item_lat=%s, item_lon=%s, ' \
                          '' % (_index, user_id, item_id, user_country, user_region, user_city, user_lat, user_lon,
                                item_country, item_region, item_city, item_lat, item_lon)
                    file_output.write('%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n' % (user_id, user_country, user_region, user_city, user_lat, user_lon, item_id, item_country, item_region, item_city, item_lat, item_lon))

                    # 修复redis中用户的位置信息
                    user_key = 'user:%s' % user_id
                    if r.exists(user_key):
                        r.hmset(user_key, {'country': item_country, 'region': item_region, 'city': item_city, 'lat': item_lat, 'lon': item_lon})
                    else:
                        print 'redis中不存在该用户信息 - [%s]' % user_id
                    # 修复es中用户的位置信息
                    location_dict = {"doc": {"location": {"lat": item_lat, "lon": item_lon}} }
                    es_users.update(user_id, location_dict)
                    # 修复数据库中用户位置信息
                    User.objects.filter(id=user_id).update(country=item_country, region=item_region, city=item_city, latitude=item_lat, longitude=item_lon)

                    _index += 1
            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)
        finally:
            if file_output is not None:
                file_output.close()
