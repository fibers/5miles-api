# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import re
import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_field_in_searchengine
from elasticsearch import Elasticsearch
from entity.models import Item

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            items = Item.objects.filter(state=0).values_list('id', 'weight')
            for item_info in items:
                item_id = item_info[0]
                weight = (item_info[1] if item_info[1] else 0) + 5

                # 更新数据库信息
                Item.objects.filter(id=item_info[0]).update(weight=weight)

                # 更新redis信息
                count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})
                if count['count'] > 0:
                    update_field_in_searchengine(item_id, 'weight', weight)

                print item_id, weight
            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)