# -*- coding: utf-8 -*-
__author__ = 'wangguanglei'


import redis
import logging

from django.core.management.base import BaseCommand
from django.conf import settings
from common.utils import get_now_timestamp, get_timestamp_str
from entity.models import Item
import time

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            f = open('/tmp/diff_%s' % get_timestamp_str(get_now_timestamp())[:10], 'w')
            # 检查用户一致性
            offset = 0
            batch = 1000
            while True:
                print 'offset:%s' % offset
                items = Item.objects.values('id', 'state')[offset:offset+batch]
                for item in items:
                    if r.exists('item:%s' % item['id']):
                        state = r.hget('item:%s' % item['id'], 'state')
                        state = '0' if not state else state
                        if state != str(item['state']):
                            f.write('id:%s\tstate:%s\trstate:%s\n' % (item['id'], item['state'], state))
                    else:
                        f.write('id:%s\tstate:%s\n' % (item['id'], item['state']))
                f.flush()
                if len(items) < batch:
                    break
                offset += batch
                time.sleep(0.01)
            f.close()
            # 检查商品一致性
        except Exception, e:
            print e