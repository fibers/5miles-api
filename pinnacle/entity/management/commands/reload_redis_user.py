__author__ = 'wangguanglei'

import redis
from django.core.management.base import BaseCommand, CommandError
from entity.models import User
from common.biz import *
from common.utils import get_timestamp, lat_or_lon_format
from entity.utils import generate_access_token

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        try:

            for user_id in range(711989, 711990):
                user = User.objects.get(pk=user_id)

                if user.fb_user_id != '':
                    user_id_key = 'login:facebook:%s:user_id' % user.fb_user_id
                elif user.is_anonymous == 1:
                    print 'anonymous user %s ignored' % user.id
                    continue
                elif user.email:
                    user_id_key = 'login:email:%s:user_id' % user.email

                fuzzy_user_id = user.fuzzy_user_id
                r_attributes = {'id': user.fuzzy_user_id, 'nickname': user.nickname, 'email': user.email,
                                'portrait': user.portrait, 'created_at': get_timestamp(user.created_at),
                                'last_login_at': get_timestamp(user.last_login_at), 'is_robot': 0, 'state': user.state,
                                'lat': lat_or_lon_format(user.latitude), 'lon': lat_or_lon_format(user.longitude),
                                'timezone': user.timezone}
                if user.appsflyer_user_id:
                    r_attributes['appsflyer_user_id'] = user.appsflyer_user_id

                if user.fb_user_id:
                    r_attributes['fb_user_id'] = user.fb_user_id
                    r_attributes['email_verified'] = True
                    r_attributes['is_anonymous'] = 0
                else:
                    r_attributes['is_anonymous'] = 0

                with self.r.pipeline() as pipe:
                    pipe.set(user_id_key, user_id)
                    pipe.hmset('user:%s' % user_id, r_attributes)
                    pipe.set('fuzzy_id:%s:user_id' % fuzzy_user_id, user_id)
                    if not user.fb_user_id:
                        pipe.set('user:%s:password' % user_id, user.password)
                    pipe.execute()
        except Exception, e:
            raise CommandError(e)



