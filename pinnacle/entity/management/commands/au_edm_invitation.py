# -*- coding: utf-8 -*-

__author__ = 'dragon'

import logging

from django.core.management.base import BaseCommand, CommandError
from entity.models import User, MailTemplate
from entity.tasks import send_mail

logger = logging.getLogger('pinnacle')

class Command(BaseCommand):
    args = '<mail_template>'
    help = 'Send EDM invitation to Australia users'

    def handle(self, *args, **options):
        try:
            mail_template_name = args[0]
            mail_template = MailTemplate.objects.filter(name=mail_template_name)
            if not mail_template:
                logger.error('mail template: %s not found' % mail_template_name)

            users = User.objects.filter(country='Australia', is_robot=0)
            for user in users:
                send_mail.delay(target_user=str(user.id), template=mail_template_name, data={'nickname': user.nickname})
                logger.info('Sending mail to %s <%s>' % (user.nickname, user.email))

            logger.info('Done!')
        except Exception, e:
            logger.exception()
            raise CommandError(e)