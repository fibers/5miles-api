__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from entity.tasks import sync_redis_to_db

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'entity: connect to redis'

class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            current_user_id = r.get("global:next_user_id")

            for user_id in range(1, int(current_user_id) + 1):
                print "sync_redis_to_db('user', 'C', %s)" % (user_id)
                sync_redis_to_db('user', 'C', user_id)

            print 'Complete synchronization!'
        except Exception, e:
            print e
            raise CommandError(e)
