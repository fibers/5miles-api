# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis
import json
import logging

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from elasticsearch import Elasticsearch

from entity.models import Item, User
from common.utils import lat_or_lon_format

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

es = Elasticsearch(settings.ES_HOSTS)
print 'entity: connect to es'

import geoip2.database
reader = geoip2.database.Reader(settings.GEOIP['DB_URL'])
print 'load geoip2'

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):

    def init_lat_lon(self, start, limit):
        is_finished = False
        try:
            items = Item.objects.all().order_by('-id')[start:limit]
            if items:
                for item in items:
                    item_id = item.id
                    lat = lat_or_lon_format(item.lat)
                    lon = lat_or_lon_format(item.lon)

                    positions = self.get_positions(item.user_id)
                    lat = positions['lat']
                    lon = positions['lon']
                    country = positions['country']
                    region = positions['region']
                    city = positions['city']
                    print '%s,%s%s,%s,%s,%s,%s' % (item.user_id, item_id, lat, lon, country, region, city)

                    if country != '':
                        try:
                            update_item = Item.objects.get(pk=item.id)
                            update_item.lat = lat
                            update_item.lon = lon
                            update_item.country = country
                            update_item.city = city
                            update_item.region = region
                            update_item.save()
                        except Exception, e:
                            logger.error('fail to update item(%s).' % item.id)
                            print e

                        if lat != 0 or lon != 0:
                            # 更新redis信息
                            if r.exists('item:%s' % item_id):
                                update_redis = {}
                                update_redis['location'] = json.dumps({'lat': lat, 'lon': lon})
                                update_redis['country'] = country
                                update_redis['region'] = region
                                update_redis['city'] = city
                                r.hmset('item:%s' % item_id, update_redis)

                            # 更新es信息
                            count = es.count(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, body={'query': {'term': {'_id': str(item_id)}}})
                            if count['count'] > 0:
                                update_es = {}
                                update_es['location'] = {'lat': lat, 'lon': lon}
                                update_es['country'] = country
                                update_es['region'] = region
                                update_es['city'] = city

                                es.update(index=settings.ES_INDEX_MAIN, doc_type=settings.ES_DOCTYPE_ITEM, id=item_id,
                                          body={'doc': update_es})
                            else:
                                print '[warn]: %s is not exits in es' % item_id
            else:
                is_finished = True
        except Exception, e1:
            print e1

        return is_finished

    def get_positions(self, user_id):
        country = ''
        region = ''
        city = ''
        lat = 0
        lon = 0
        try:
            user = User.objects.get(pk=user_id)
            ip_address = user.ip_address
            if ip_address != '' and ip_address is not None:
                response = reader.city(ip_address)
                lat = lat_or_lon_format(float(response.location.latitude))
                lon = lat_or_lon_format(float(response.location.longitude))
                country = response.country.name
                city = response.city.name
                subdivisions = response.subdivisions
                if subdivisions:
                    for sub in subdivisions:
                        region = sub.iso_code
        except Exception, e:
            print e

        return {'country': country, 'region': region, 'city': city, 'lat': lat, 'lon': lon}

    def handle(self, *args, **options):

        page = 1
        page_size = 2000
        while True:
            start = (page - 1) * page_size
            limit = page * page_size

            is_finished = self.init_lat_lon(start, limit)

            page += 1
            if is_finished:
                break

        print 'Complete synchronization!'
