# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService
from entity.models import Item
from entity.utils import correct_country_name
from common.es_dao import es_dao_item

class Command(BaseCommand):
    args = ''
    help = '<Fix incorrect country name, run everyday, until bug was fixed>'

    def handle(self, *args, **options):
        srs = SrService()
        r = srs.get_redis()
        es_item = es_dao_item()
        try:
            items = Item.objects.raw("select * from entity_item where length(country) < 3 and country != ''")
            for i in items:
                print 'country: %s' % i.country
                country_name = correct_country_name(i.country)
                print 'change to: %s' % country_name
                i.country = country_name
                i.save()
                r.hset('item:%s' % i.id, 'country', country_name)
                try:
                    es_item.update_field(i.id, 'country', country_name)
                except Exception, e:
                    print e
        except Exception, e:
            raise CommandError(e)


