# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from commerce.utils import get_now_timestamp

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
print 'Connecting to redis'

class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                likers = r.smembers('item:%s:likers' % item_id)
                if likers:
                    for liker in likers:
                        print '%s. %s' % (item_id, liker)
                        r.zadd('item:%s:new_likers' % item_id, liker, get_now_timestamp())

            print 'Complete synchronization!'
        except Exception, e:
            print e