__author__ = 'dragon'

import redis
import json
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class Command(BaseCommand):
    help = 'Identify robot'

    def handle(self, *args, **options):
        try:
            print 'Connecting to redis'
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                location = r.hget('item:%s' % item_id, 'location')
                if location is None:
                    print 'item: %s, location is null' % item_id
                else:
                    obj = json.loads(location)
                    if not obj['lat'] or not obj['lon']:
                        print 'item: %s, location: %s' % (item_id, location)

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)