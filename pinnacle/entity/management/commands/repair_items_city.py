# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import re
import redis

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from common.utils_es import update_field_in_searchengine
from elasticsearch import Elasticsearch
from entity.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            print 'Connecting to redis'
            print settings.REDIS_HOST

            current_item_id = int(r.get('global:next_item_id'))
            for item_id in range(1, current_item_id + 1):
                item_key = 'item:%s' % item_id
                if r.exists(item_key):
                    item = r.hmget('item:%s' % item_id, 'id', 'city')
                    fuzzy_item_id = item[0]
                    city = item[1]
                    if city == 'None':
                        r.hset(item_key, 'city', '')
                        # r.hdel(item_key, 'city')
                        print item_id, fuzzy_item_id, city

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)