# -*- coding: utf-8 -*-

__author__ = 'dragon'

import logging
import redis

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from entity.models import User, FacebookProfile
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):
    help = u'根据facebook性别初始化User性别'

    def handle(self, *args, **options):
        try:
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
            for user in User.objects.raw("select id, fb_user_id from entity_user where length(fb_user_id) > 0"):
                logger.debug('fb_user_id: %s' % user.fb_user_id)
                try:
                    fb_user = FacebookProfile.objects.get(pk=user.fb_user_id)
                    if fb_user.gender != '':
                        if fb_user.gender.lower() == 'female':
                            i_gender = -1
                        elif fb_user.gender.lower() == 'male':
                            i_gender = 1
                        r.hset('user:%s' % user.id, 'gender', i_gender)
                        user = User.objects.get(pk=user.id)
                        user.gender = i_gender
                        user.save()
                except ObjectDoesNotExist, e:
                    logger.error('facebook profile missed: %s, %s' % (user.fb_user_id, e))

            logger.info('Done!')
        except Exception, e:
            logger.exception(e)
            raise CommandError(e)
