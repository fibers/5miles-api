# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings


class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    fuzzy_item_id = models.CharField(max_length=50, db_index=True)
    user_id = models.IntegerField(blank=False, db_index=True)
    title = models.CharField(max_length=500)
    desc = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=30, default=settings.DEFAULT_CURRENCY_CODE)
    local_price = models.CharField(max_length=50, blank=True)
    lat = models.DecimalField(max_digits=10, decimal_places=6)
    lon = models.DecimalField(max_digits=10, decimal_places=6)
    mediaLink = models.CharField(max_length=200, blank=True)
    state = models.SmallIntegerField(db_index=True, default=0)
    internal_status = models.SmallIntegerField(null=True, db_index=True, default=0)
    num_in_stock = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)    # list time
    update_time = models.DateTimeField(null=True)    # boss update time
    renewed_at = models.DateTimeField(null=True)           # renew time
    modified_at = models.DateTimeField(null=True)         # user modify time
    weight = models.IntegerField(null=True)
    risk = models.CharField(blank=True, max_length=200, default='')
    original_price = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    brand_id = models.IntegerField(null=True, db_index=True)
    brand_name = models.CharField(max_length=50, blank=True, default='')
    shipping_method = models.IntegerField(null=True, db_index=True, default=3)
    country = models.CharField(max_length=30, blank=True, default='')
    city = models.CharField(max_length=50, blank=True, default='')
    region = models.CharField(max_length=20, blank=True, default='')
    share = models.IntegerField(null=True, default=1)
    weight_updated_at = models.DateTimeField(null=True)
    likes = models.IntegerField(default=0)
    pv = models.IntegerField(default=0)
    uv = models.IntegerField(default=0)


class Image(models.Model):
    id = models.AutoField(primary_key=True)
    item_id = models.IntegerField(null=False, db_index=True)
    image_path = models.CharField(max_length=200)
    image_width = models.IntegerField(null=True)
    image_height = models.IntegerField(null=True)
    # 1: first image, 0: non first image
    is_first = models.SmallIntegerField(default=0)


class Brand(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True, default='', db_index=True)
    # 0表示系统品牌,1表示用户自定义品牌
    brand_type = models.IntegerField(default=0, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Category(models.Model):
    id = models.IntegerField(primary_key=True)
    parent_id = models.IntegerField(null=False, db_index=True, default=-1)
    title = models.CharField(max_length=50)
    slug = models.CharField(max_length=100)
    short_url = models.CharField(max_length=200, blank=True)
    description = models.CharField(max_length=200, blank=True)
    # status 0: Invalid, 1: Draft, 2: Published
    status = models.SmallIntegerField(db_index=True, default=0)
    order_num = models.SmallIntegerField(default=0)
    publish_date = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    # gender 1: male, -1: female, 0: ambivert
    gender = models.SmallIntegerField(default=0)
    weight = models.IntegerField(null=True, default=0)
    weight_updated_at = models.DateTimeField(null=True)
    icon = models.CharField(max_length=255, default='')
    map_to_id = models.IntegerField(null=True, blank=True)
    title_i18n_msg_id = models.IntegerField(null=True, default=-1)


class ItemCategory(models.Model):
    cat_id = models.IntegerField(db_index=True)
    item_id = models.IntegerField(db_index=True)


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    fuzzy_user_id = models.CharField(max_length=50, blank=True, db_index=True)
    appsflyer_user_id = models.CharField(max_length=50, blank=True, db_index=True)
    fb_user_id = models.CharField(max_length=50, blank=True, db_index=True)
    nickname = models.CharField(max_length=255, blank=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=50, blank=True, db_index=True)
    password = models.CharField(max_length=100, blank=True)
    state = models.SmallIntegerField(default=1, db_index=True)
    last_login_at = models.DateTimeField(null=True, auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    account_type_id = models.IntegerField(null=True)
    upgrade_type_id = models.IntegerField(null=True)
    trade_status = models.SmallIntegerField(default=0)
    accumulate_credits = models.IntegerField(null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    expire_date = models.DateField(null=True, blank=True)
    portrait = models.CharField(max_length=350, blank=True)
    about = models.TextField(blank=True)
    mobile_phone = models.CharField(max_length=30, blank=True)
    mobile_phone_country = models.CharField(max_length=30, blank=True)
    language = models.CharField(max_length=30, default="EN", blank=True)
    country = models.CharField(max_length=30, blank=True)
    city = models.CharField(max_length=50, blank=True)
    seller_positive_rating = models.IntegerField(default=0)
    seller_total_rating = models.IntegerField(default=0)
    buyer_positive_rating = models.IntegerField(default=0)
    buyer_total_rating = models.IntegerField(default=0)
    seller_rating_score = models.DecimalField(max_digits=7, decimal_places=5, null=True)
    rating_update_time = models.DateTimeField(null=True)
    buyer_rating_score = models.DecimalField(max_digits=7, decimal_places=5, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, null=True)
    address = models.CharField(max_length=1000, blank=True)
    from_channel = models.CharField(max_length=100, blank=True, db_index=True)
    last_heartbeat_at = models.DateTimeField(null=True, blank=True, auto_now=True)
    is_robot = models.SmallIntegerField(default=0)
    ip_address = models.CharField(max_length=20, blank=True)
    region = models.CharField(max_length=50, blank=True, default='')
    # gender 1: male, -1: female, 0: ambivert
    gender = models.SmallIntegerField(null=True, blank=True, default=0)
    os = models.CharField(max_length=20, blank=True, default='')
    os_version = models.CharField(max_length=20, blank=True, default='')
    app_version = models.CharField(max_length=20, blank=True, default='')
    device = models.CharField(max_length=255, default='')
    current_os = models.CharField(max_length=20, blank=True, default='')
    current_os_version = models.CharField(max_length=20, blank=True, default='')
    current_app_version = models.CharField(max_length=20, blank=True, default='')
    current_device = models.CharField(max_length=255, blank=True, default='')
    weight = models.IntegerField(null=True, default=0)
    weight_updated_at = models.DateTimeField(null=True)
    # email_verified 1: verified, 0: not be verified
    email_verified = models.BooleanField(default=False)
    # is_anonymous 1: anonymous user, 0: non anonymous user
    is_anonymous = models.SmallIntegerField(default=0)
    timezone = models.CharField(max_length=64, default='')
    zipcode = models.CharField(max_length=50, default='')
    deactive_comment = models.CharField(max_length=1024, default='')
    last_active_at = models.DateTimeField(auto_now_add=True, null=True)


class AppsFlyer(models.Model):
    app_id = models.CharField(max_length=50)
    platform = models.CharField(max_length=50, blank=True, db_index=True)
    ip = models.CharField(max_length=20)
    customer_user_id = models.CharField(max_length=50, db_index=True)
    click_time = models.DateTimeField(null=True)
    install_time = models.DateTimeField(null=True, db_index=True)
    agency = models.CharField(max_length=50, blank=True)
    media_source = models.CharField(max_length=50, blank=True, db_index=True)
    campaign = models.CharField(max_length=50, blank=True, db_index=True)
    campaign_id = models.CharField(max_length=50, blank=True, db_index=True)
    os_version = models.CharField(max_length=50, blank=True)
    sdk_version = models.CharField(max_length=50, blank=True)
    app_version = models.CharField(max_length=50, blank=True)
    click_url = models.CharField(max_length=255, blank=True)
    mac = models.CharField(max_length=50, blank=True)
    cost_per_install = models.CharField(max_length=50, blank=True)
    country_code = models.CharField(max_length=50, blank=True)
    city = models.CharField(max_length=50, blank=True)
    language = models.CharField(max_length=50, blank=True)
    adset = models.CharField(max_length=150, blank=True)
    adset_id = models.CharField(max_length=50, blank=True)
    adgroup = models.CharField(max_length=100, blank=True)
    adgroup_id = models.CharField(max_length=50, blank=True)
    af_siteid = models.CharField(max_length=50, blank=True)
    wifi = models.CharField(max_length=50, blank=True)
    appsflyer_device_id = models.CharField(max_length=50, blank=True)
    device_type = models.CharField(max_length=50, blank=True)
    af_sub1 = models.CharField(max_length=50, blank=True)
    af_sub2 = models.CharField(max_length=50, blank=True)
    af_sub3 = models.CharField(max_length=50, blank=True)
    af_sub4 = models.CharField(max_length=50, blank=True)
    af_sub5 = models.CharField(max_length=50, blank=True)
    contributer_1 = models.CharField(max_length=50, blank=True)
    contributer_2 = models.CharField(max_length=50, blank=True)
    contributer_3 = models.CharField(max_length=50, blank=True)
    android_id = models.CharField(max_length=50, blank=True)

    event_time = models.DateTimeField(null=True)
    event_name = models.CharField(max_length=50, blank=True)
    event_value = models.CharField(max_length=50, blank=True)
    currency = models.CharField(max_length=50, blank=True)
    event_type = models.CharField(max_length=50, blank=True, db_index=True)
    attribution_type = models.CharField(max_length=50, blank=True)
    fb_campaign_name = models.CharField(max_length=50, blank=True)
    device_name = models.CharField(max_length=64, blank=True)
    device_brand = models.CharField(max_length=64, blank=True)
    device_model = models.CharField(max_length=50, blank=True)
    idfa = models.CharField(max_length=50, blank=True)
    advertising_id = models.CharField(max_length=50, blank=True)
    imei = models.CharField(max_length=50, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)


class FacebookProfile(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    name = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=50, blank=True)
    gender = models.CharField(max_length=20, blank=True)
    verified = models.BooleanField(default=False)
    locale = models.CharField(max_length=10, blank=True)
    timezone = models.IntegerField(null=True)
    link = models.CharField(max_length=255, blank=True)
    updated_time = models.DateTimeField(null=True)
    age_range_min = models.IntegerField(null=True)
    age_range_max = models.IntegerField(null=True)
    user_currency = models.CharField(max_length=20, blank=True)
    usd_exchange_inverse = models.FloatField(null=True)
    usd_exchange = models.FloatField(null=True)
    currency_offset = models.IntegerField(null=True)
    fb_token = models.TextField(blank=True, default='')
    fb_token_expires = models.IntegerField(null=True, default=0)


class ViewItem(models.Model):
    user_id = models.IntegerField(db_index=True)
    item_id = models.IntegerField()
    created_at = models.DateField(auto_now_add=True)


class FeaturedItems(models.Model):
    """
    运营推荐商品集
    """
    title = models.CharField(max_length=50)
    banner_url = models.CharField(max_length=255)
    banner_width = models.IntegerField()
    banner_height = models.IntegerField()
    items = models.CharField(max_length=255)    # 逗号隔开
    comment = models.CharField(max_length=255)


class PushTemplate(models.Model):
    class Meta:
        db_table = 'fmmc_push_template'

    name = models.CharField(max_length=50, unique=True)
    title = models.CharField(max_length=50, blank=True, default='')
    content = models.TextField()
    status = models.SmallIntegerField(default=1)
    payload = models.CharField(max_length=255, default='')
    flag = models.SmallIntegerField(null=True)
    notification_type = models.SmallIntegerField(default=4)
    comment = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    daily_quota = models.IntegerField(default=-1, null=True, blank=True)


class MailTemplate(models.Model):
    class Meta:
        db_table = 'fmmc_mail_template'

    name = models.CharField(max_length=50, unique=True, db_index=True)
    content = models.TextField()
    subject = models.CharField(max_length=100)
    from_address = models.CharField(max_length=50, null=True, blank=True, default='')
    status = models.SmallIntegerField(default=1)
    notification_type = models.SmallIntegerField(default=4)
    comment = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class SystemMessageTemplate(models.Model):
    class Meta:
        db_table = 'fmmc_system_message_template'

    name = models.CharField(max_length=50, unique=True)
    content = models.TextField()
    image = models.CharField(max_length=255, blank=True, default='')
    action = models.CharField(max_length=100, blank=True, default='')
    desc = models.CharField(max_length=255, blank=True, default='')
    status = models.SmallIntegerField(default=1)
    # notification type, 0: normal, 1: review...
    sm_type = models.SmallIntegerField(null=True, blank=True, default=0)
    comment = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class SmsTemplate(models.Model):
    """
    短信模板
    """
    class Meta:
        db_table = 'fmmc_sms_template'

    name = models.CharField(max_length=50, unique=True)
    content = models.TextField()
    status = models.SmallIntegerField(default=1)
    comment = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class FrequencyPeriod(models.Model):
    class Meta:
        db_table = 'fmmc_frequency_period'

    name = models.CharField(max_length=50, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Task(models.Model):
    class Meta:
        db_table = 'fmmc_task'

    name = models.CharField(max_length=50, unique=True)
    clazz = models.CharField(max_length=50)
    mail_template = models.ForeignKey(MailTemplate, null=True, blank=True)
    push_template = models.ForeignKey(PushTemplate, null=True, blank=True)
    system_message_template = models.ForeignKey(SystemMessageTemplate, null=True, blank=True)
    comment = models.CharField(max_length=255, blank=True)
    status = models.SmallIntegerField(default=1)
    target_country = models.TextField(default='')
    target_region = models.TextField(default='')
    target_city = models.TextField(default='')
    target_gender = models.CharField(max_length=20)
    target_register_interval_period = models.ForeignKey(FrequencyPeriod, null=True, blank=True)
    target_register_interval_quantity = models.IntegerField(default=1, null=True, blank=True)
    # target_register_interval_type 0: ago, 1: in
    target_register_interval_type = models.SmallIntegerField(default=0, null=True, blank=True)
    target_uid_tail = models.CharField(max_length=20, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Cron(models.Model):
    class Meta:
        db_table = 'fmmc_cron'

    name = models.CharField(max_length=50, unique=True)
    task = models.ForeignKey(Task, null=True, blank=True)
    status = models.SmallIntegerField(default=1)
    comment = models.CharField(max_length=255, blank=True)
    frequency_period = models.ForeignKey(FrequencyPeriod, null=True, blank=True)
    frequency_quantity = models.SmallIntegerField(default=1, null=True)
    at = models.CharField(max_length=100, null=True, default='00:00')
    tz = models.CharField(max_length=100, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Trigger(models.Model):
    class Meta:
        db_table = 'fmmc_trigger'

    name = models.CharField(max_length=50, unique=True, db_index=True)
    task = models.ForeignKey(Task, null=True, blank=True)
    status = models.SmallIntegerField(default=1)
    comment = models.CharField(max_length=255, blank=True)
    interval_period = models.ForeignKey(FrequencyPeriod, null=True, blank=True)
    interval_quantity = models.IntegerField(default=1, null=True, blank=True)
    at = models.CharField(max_length=100)
    # trigger type 1: instant, 2: delayed
    trigger_type = models.SmallIntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class I18nMessage(models.Model):
    comment = models.CharField(max_length=255)
    # message_type 0: category, 1: report reason
    message_type = models.IntegerField()


class I18nTranslation(models.Model):
    message = models.ForeignKey(I18nMessage)
    language_code = models.CharField(max_length=10)
    translation = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


