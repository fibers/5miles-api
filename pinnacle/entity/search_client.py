# -*- coding: utf-8 -*-

import json
import logging
import random
from common import utils_func
from common.es_dao import es_dao_item
from common.utils_es import suggest_items_by_keyword_decay
from entity import search
from entity.exceptions import NotFoundError
from entity.search import SearchItems, SearchHomeService
from entity import search_base
from entity.service_base import SrService

__author__ = 'admin'

logger = logging.getLogger('search')

def shake_item_client(request, cat_id, item_id, title):
    # gender = _get_gender(request)
    req_params = SearchItems.init_params_by_request(request)
    req_params.limit = 100
    req_params.offset = 0
    if len(title.decode('utf-8')) > 150:
        title = title.decode('utf-8')[:150].encode('utf-8')
    results = suggest_items_by_keyword_decay(title, cat_id, req_params)
    recommended_item_map = {}
    recommended_item_ids = []
    for hit in results['hits']['hits']:
        item = hit['_source']
        if hit['_id'] != item_id:
            recommended_item_ids.append(hit['_id'])

            recommended_item_map[hit['_id']] = item
    if not recommended_item_ids:
        refind_params = SearchItems.init_params_by_request(request)
        es_body = SearchItems.build_query_body(refind_params)
        results = es_dao_item().es_search_exec(es_body)

        for hit in results['hits']['hits']:
            if hit['_id'] != item_id:
                recommended_item_ids.append(hit['_id'])
    if not recommended_item_ids:
        raise NotFoundError()
    shake_item_id = recommended_item_ids[random.randint(0, len(recommended_item_ids) - 1)]
    return recommended_item_map, shake_item_id


def home_item_client(request):
    # 接收refind 参数
    search_params = SearchItems.init_params_by_request(request)
    # logger.debug("req offset=%s, limit=%s" % (search_params.offset, search_params.limit))
    # 用户对比测试页面洗脸的效果
    dist = request.GET.get('dist', "y")
    items, hits_count, total_count, offset_plus = SearchHomeService().search_items_by_location_decay(search_params,
                                                                                                     dist=dist)
    # build list 返回格式数据
    ret = search._build_list_json(hits_count, items, search_params.limit, search_params.offset,
                                  total_count, offset_plus=offset_plus, list_type='home')
    return items, ret


def keyword_items_client(request):
    # 接收refind 参数
    refind_params = SearchItems.init_params_by_request(request)
    es_body = SearchItems.build_query_body(refind_params)
    # 执行搜索
    results = es_dao_item().es_search_exec(es_body)
    items, hits_count, total_count = search_base.es_data_parse(results)
    # 构造refind返回数据
    res_ext = SearchItems.build_refind_res(refind_params, results)
    ret = search._build_list_json(hits_count, items, refind_params.limit, refind_params.offset,
                                  total_count, res_ext=res_ext, list_type='search', query_params=refind_params)
    return items, ret

def category_items_client(request, cat_id):
    # 接收refind 参数
    refind_params = SearchItems.init_params_by_request(request)
    if cat_id:
        refind_params.cat_id = cat_id

    es_body = SearchItems.build_query_body(refind_params)
    # 执行搜索
    results = es_dao_item().es_search_exec(es_body)
    items, hits_count, total_count = search_base.es_data_parse(results)
    # 构造refind返回数据
    res_ext = SearchItems.build_refind_res(refind_params, results, query_type='category')
    ret = search._build_list_json(hits_count, items, refind_params.limit, refind_params.offset,
                                  total_count, res_ext=res_ext, list_type='category')
    return items, ret



def brand_items_client(request ,cat_id):

    # 接收refind 参数
    refind_params = SearchItems.init_params_by_request(request)

    es_body = SearchItems.build_query_body(refind_params)
    # 执行搜索
    results = es_dao_item().es_search_exec(es_body)
    items, hits_count, total_count = search_base.es_data_parse(results)
    # 构造refind返回数据
    res_ext = SearchItems.build_refind_res(refind_params, results)
    ret = search._build_list_json(hits_count, items, refind_params.limit, refind_params.offset,
                                  total_count, res_ext=res_ext, list_type='brand')
    return items, ret



def suggest_items_client(request ,item_id, fuzzy_item_id,title, cat_id):
    refind_params = SearchItems.init_params_by_request(request)
    # logger.debug("sg step 0.5 %s"% fuzzy_item_id)
    def load_suggest():
        #gender = _get_gender(request)
        #logger.debug("sg step 1.5 %s"% fuzzy_item_id)

        results = suggest_items_by_keyword_decay(title, cat_id, refind_params, is_filter_distance=True)
        #print "results=%s"%results
        items, hits_count, total_count = search_base.es_data_parse(results)
        #logger.debug("load_suggest items=%s" % len(items))

        # 排除当前的数据
        for item in items:
            id = item['id']
            if id == fuzzy_item_id:
                #logger.debug("1 del item=%s" %id)
                items.remove(item)

        #logger.debug("sg step 2-- %s"% fuzzy_item_id)

        # 如果没有相关的数据，将这类的数据取出来
        if len(items) <= 5:
            item_ids = utils_func.get_list_ids(items, 5)

            #results = search_items_by_category_decay(cat_id, lat, lon, gender, offset, limit, ip_address)
            results_2 = suggest_items_by_keyword_decay(title, cat_id, refind_params,
                                                       is_query_keyword=False, is_filter_distance=True)
            #items_2, hits_count, total_count = search_base.es_data_parse(results_2)
            item_2 = results_2['hits']['hits']
            for hit in item_2:
                item = hit['_source']
                id = item['id']
                if id not in item_ids:
                    items.append(item)

                if len(items) > 10:
                    break

        #logger.debug("sg step 3.1 %s"% fuzzy_item_id)
        utils_func.list_trace_tag(items, list_type="suggest", offset=0, query_params=refind_params)

        #logger.debug("sg step 4-- %s"% fuzzy_item_id)
        return json.dumps(items)

    try:
        r_ex = SrService().get_redis_ex()
        #logger.debug("sg step 1-- %s"% fuzzy_item_id)

        suggest_key = 'item:%s:sgt' % item_id
        suggest_cache = r_ex.get(suggest_key)
        #logger.debug("sg step 1.1 %s"% fuzzy_item_id)

        if not suggest_cache:
            suggest_cache = load_suggest()

            # 缓存 15 分钟
            ex = 8 * 60
            cache_res = r_ex.set(suggest_key, suggest_cache, ex=ex)
            #logger.debug('suggest_key=%s,res=%s,ex=%s,len=%s' , suggest_key,cache_res,ex,len(suggest_cache))
            #logger.debug("sg step 5-- %s"% fuzzy_item_id)
    except Exception, e:
        logger.error("suggest cache error:%s" % e)
        suggest_cache = load_suggest()

    return suggest_cache


def followings_items_client(request,following_fuzzy_ids):
    search_params = SearchItems.init_params_by_request(request)
    kwargs = {'following_ids': following_fuzzy_ids}
    es_body = SearchItems.search_followings_items(search_params, **kwargs)
    results = es_dao_item().es_search_exec(es_body)
    items, hits_count, total_count = search_base.es_data_parse(results)
    # build list 返回格式数据
    ret = search._build_list_json(hits_count, items, search_params.limit, search_params.offset,
                                  total_count, list_type='followings')
    return items, ret