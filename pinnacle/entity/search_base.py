# -*- coding: utf-8 -*-
import logging
from elasticsearch import Elasticsearch
#from common import search
from common.es_dao import es_dao_item
from entity.service_base import BaseService, singleton, SrService
from pinnacle import settings

__author__ = 'lvyi'

logger = logging.getLogger("pinnacle")

def es_data_parse(results):
    hits_count = len(results['hits']['hits'])
    total_count = results['hits']['total']
    items = []
    for hit in results['hits']['hits']:
        item = hit['_source']
        items.append(item)

    return items, hits_count, total_count
