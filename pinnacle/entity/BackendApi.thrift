enum USER_ACTIVE {
    ACTIVED=1,
    DELETED=0
}

enum PRODUCT_STATE {
    HGItemStateListing = 0,
    HGItemStateOfferAccepted,
    HGItemStateDoneDelivery,
    HGItemStateRatedSeller,
    HGItemStateOffShelf,
    HGItemStateDeleted,
}
/**
 *  update the user's information
 *  userId: user's id
 *  paramsMap: we need update the content
 *  return 0-success;-1-fail
 **/
service IThriftBackendApiService {
    /**
     *  modify product's information
     **/
    i32 updateProductById(1: i32 productId, 2: map<string,string> paramsMap)
    /**
     *  modify category's information
     **/
    i32 updateCategoryById(1: i32 categoryId, 2: map<string,string> paramsMap)
    /**
     *  change product's category
     **/
    i32 updateProductCategory(1: i32 productId, 2: i32 categoryId)
    /**
     *  comments list
     **/
    string get_comments_by_item_id(1: i32 item_id)
    /**
     *  offers list
     **/
    string get_offers_by_item_id(1: i32 item_id)
    /**
     *  get offer count
     **/
    string get_offer_count_by_item_ids(1: list<string> item_ids)
    /**
     *  batch update the items' weight
     **/
    string batch_update_item_weight(1: list<string> item_ids, 2: i32 weight)
    /**
     *  get user statistic data
     **/
    string get_user_statistic_data(1: i32 user_id)
    /**
     *  batch update the user' weight
     **/
    string batch_update_user_weight(1: list<i32> user_ids, 2: i32 weight)
    /**
     *  batch update the items' image blur weight
     **/
    string batch_update_image_blur_weight(1: list<i32> item_ids, 2: i32 weight)
    /**
     *  batch update the category's weight
     **/
    string batch_update_category_weight(1: i32 cat_id, 2: i32 weight)
}
