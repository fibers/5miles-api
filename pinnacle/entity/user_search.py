# -*- coding: utf-8 -*-
import logging
import json
from common import utils
from common import utils_func
from common.biz import get_user_from_id

from entity.service_base import singleton, SrService
from common.es_dao import es_dao_user
from entity import search_base
from entity.service import UserService

__author__ = 'dragon'

logger = logging.getLogger('pinnacle')


def init_params_by_request(request):
    lat = utils.lat_or_lon_format(float(request.GET.get('lat', 0)))
    lon = utils.lat_or_lon_format(float(request.GET.get('lon', 0)))
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    if lat == 0 and lon == 0:
        lat, lon = utils.try_build_location(request.user_id, utils.get_real_ip(request))

    params = RequestParams()
    params.lat = lat
    params.lon = lon
    params.offset = offset
    params.limit = limit
    params.user_id = request.user_id

    return params


def build_query_body(request_params):

    now_ts=utils_func.get_now_ts()

    es_body = {'sort': [], 'query': {"function_score": {"query": {"filtered": {"filter": {}}}}},
               'from': request_params.offset, 'size': request_params.limit}

    filter = es_body['query']['function_score']['query']["filtered"]["filter"]
    #function_score['query'] = {"filtered": {"filter": {}}}

    # ==============================
    and_arr = list()
    # and_arr.append({'term': {'verified': True}})

    # 排除机器人
    and_arr.append({"exists": {"field": "last_posted_at" } })
    and_arr.append({'term': {'is_robot': 0}})
    #and_arr.append({'range': {'location.lon': {'gt': 0}}})
    and_arr.append({'not': {'term': {'_id': request_params.user_id}}})
    and_arr.append({'range': {'item_num': {'gte': 1}}})
    and_arr.append({'not': {'ids': {'values': request_params.followings}}})
    and_arr.append({'not': {'script': {'script': "doc['location'][0]['lon'].value == 0 && doc['location'][0]['lat'].value == 0"}}})

    if and_arr:
        filter["and"] = and_arr

    #es_body['sort'].append({"item_num": {"order": "desc"}})

    # ==============================
    funcs = list()
    # 附近的人
    location_scale = "20"
    funcs.append({'gauss': {'location': {'origin': '%s,%s' % (request_params.lat, request_params.lon),
                            'scale': '%smi' % location_scale, 'offset': 0}}})
    # 最近发商品的人
    DEFAULT_POST_DECAY_DATE = 3600*24*3  # 3 days
    funcs.append({'gauss': {'last_posted_at': {'origin': now_ts, 'scale': DEFAULT_POST_DECAY_DATE}}})

    # 用户的发送的商品数量
    #funcs.append({'gauss': {'item_num': {'origin': 100, 'scale': 50}}})

    es_body['query']['function_score']['functions'] = funcs

    return es_body


class RequestParams(object):
    lat = 0
    lon = 0
    offset = 0
    limit = 10
    user_id = None
    followings = []


@singleton
class SearchUserService(object):
    def __init__(self):
        self.es_users = es_dao_user()
        self.redis = SrService().get_redis()

    def search_users_nearby(self, request_params):
        es_body = build_query_body(request_params)
        results = self.es_users.es_search_exec(es_body)
        users, hits_count, total_count = search_base.es_data_parse(results)

        #logger.debug("search 1 results=%s" % users)
        #users.sort(lambda x,y: cmp(y['item_num'],x['item_num']))
        users = sorted(users, key=lambda x:x['item_num'],reverse = True)
        #logger.debug("search 2 results=%s" % users)

        logger.debug("search neighbours: hits_count=%s, total_count=%s" % (hits_count, total_count))
        results = []
        for user in users:
            entry = get_user_from_id(user['id'])
            item_num = self.redis.zcard('user:%s:user_selling_list' % user['id'])
            entry['item_num'] = item_num
            entry['item_images'] = list()
            item_ids = self.redis.zrevrange('user:%s:user_selling_list' % user['id'], 0, min(6, item_num))
            for item_id in item_ids:
                images = json.loads(self.redis.hget('item:%s' % item_id, 'images'))
                entry['item_images'].append(images[0])
            entry['following'] = UserService().is_following(request_params.user_id, user['id'])
            results.append(entry)

        return results, hits_count, total_count
