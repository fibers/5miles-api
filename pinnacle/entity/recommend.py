# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import shared_task
import logging
from common import utils_func
from entity.service_base import BaseService, singleton, SrService

__author__ = 'lvyi'

logger = logging.getLogger('pinnacle')


class UserFollowService(BaseService):

    def get_follow_items(self, req_params):

        pass

    def get_follow_users(self, req_params):

        pass


@shared_task(queue='celery.misc')
def task_item_ctr(item_id, item, owner):
    ItemCtrService().set_ctr(item_id, item, owner)

@singleton
class ItemCtrService(BaseService):

    ctr_trace_max = 4*3600
    ctr_trace_ex = 5*3600

    def set_ctr(self, item_id , item ,user_id):
        try:
            created_at = int(item['created_at'])
            interval = utils_func.get_now_ts() - created_at

            # 记录4小时以内的item ctr
            #logger.debug("interval=%s,id=%s,uid=%s" % (interval, item_id, user_id))
            if interval < self.ctr_trace_max:

                key_user = "it:%s:u:%s:ctr" % (item_id, user_id)
                user_ctr = SrService().get_redis_ex().get(key_user)
                #logger.debug("user_ctr=%s" % user_ctr)
                if not user_ctr=='ctr':
                    # ctr+1
                    # 保存到redis ，设置5小时过期
                    key = "item:%s:ctr" % item_id
                    ctr = SrService().get_redis_ex().incr(key)
                    SrService().get_redis_ex().expire(key , self.ctr_trace_ex)

                    # 保存到es
                    from common import utils_es
                    utils_es.update_field_in_searchengine(item_id, "user_ctr", ctr)

                    # 记录用户排重信息
                    SrService().get_redis_ex().set(key_user, "ctr" , self.ctr_trace_ex)

                    logger.debug("save key=%s key_user=%s ctr=%s interval=%s catid=%s" %
                                 (key,key_user,ctr,interval ,item['cat_id']))
                #else:
                    #logger.debug("key_user=%s" % (key_user))
            else:
                #logger.debug("interval>%s" % (self.ctr_trace_max))
                pass
        except Exception,ex:
            logger.warn("ctr err=%s",ex)


class UserItemsDistribution(object):

    @staticmethod
    def items_index_zoom(orginal,n):
        '''
        将现有的商品位置进行重新计算
        :param orginal: 原始位置
        :param n: 同一买家的商品数量
        :return: 返回重新计算后的位置
        '''
        #n=n+1
        return orginal + pow(2,n)

    @staticmethod
    def build_sort_key(new_goods, sort_key):
        if not new_goods.get(sort_key,None):
            return sort_key
        else:
            #print 'repeat'
            sort_key = sort_key +"_01"
            return UserItemsDistribution.build_sort_key(new_goods, sort_key)
    
    @staticmethod
    def get_owner_id( item):
        return item['owner']['id']
    
    @staticmethod
    def do_parse(items , fnc_get_owner_id=None):
        try:
            # 每个卖家的商品数量
            owner_goods = {}

            # 待排序字典
            new_goods = {}

            # 遍历数据，依次将数据分离，间距放大
            orginal_index = 0
            for it in items:

                if fnc_get_owner_id==None:
                    owner_id = UserItemsDistribution.get_owner_id(it)
                else:
                    owner_id = fnc_get_owner_id(it)
                orginal_index = orginal_index+1

                #记录每个卖家的商品数量
                good_num = owner_goods.get(owner_id,1)
                owner_goods[owner_id] = good_num+1;

                sort_key = "n%015d" % UserItemsDistribution.items_index_zoom(orginal_index , good_num)   #.zfill(5)
                sort_key = UserItemsDistribution.build_sort_key(new_goods ,sort_key)

                #logger.debug("owner_id=%s, orginal_index=%s, sort_key=%s" % (owner_id ,orginal_index ,sort_key))
                #print "owner_id=%s, orginal_index=%s, sort_key=%s" % (owner_id ,orginal_index ,sort_key)
                new_goods[sort_key] = it

            # 排序
            new_items = sorted(new_goods.iteritems(), key=lambda d:d[0], reverse = False )
            res = []
            for it in new_items:
                res.append(it[1])

            #logger.debug("UserItemsDistribution res len=%s" % (len(res)))
            return res
        except Exception,e:
            #print "ERROR=%s",e
            logger.error("search_items_distribution error=%s" % e)
            return items


if __name__ == '__main__':

    #for i in xrange(50):
    #    nn = UserItemsDistribution.items_index_zoom(0,i)
    #    print "%s--%s--%s" % (i,UserItemsDistribution.items_index_zoom(0,i),len("%s"%nn))
    #for it in xrange(1,10):
        #print it
    #    aaa = SrService().get_redis_ex().incr("123")
    #    print aaa

    import pinnacle.settings.defaults

    item_id = 1111
    owner = 2222
    item = {'created_at':utils_func.get_now_ts()}
    ItemCtrService().set_ctr(item_id, item, owner)
