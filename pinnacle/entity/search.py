# -*- coding: utf-8 -*-

from __future__ import absolute_import
from celery import shared_task
import copy
import json
import logging
import random

from common import utils_func
from common import utils
from common import search_cnf
from common.es_dao import es_dao_item
from entity.recommend import UserItemsDistribution
from entity import search_base
from entity.search_base import SrService
from entity.service_base import singleton
from pinnacle import settings

__author__ = 'lvyi'

logger = logging.getLogger("pinnacle")

def _build_list_json(hits_count, items, limit, offset, total_count, res_ext=None , list_type='home',
                     query_params=None, offset_plus=0):

    #logger.debug("hits_count=%s,limit=%s,offset=%s,total_count=%s,res_ext=%s,offset_plus=%s" %
    #             (hits_count, limit, offset, total_count, res_ext , offset_plus))
        
    utils_func.list_trace_tag(items ,list_type=list_type, offset=offset , query_params=query_params)

    pagging_string = '?offset=%d&limit=%d'
    ret = {'meta': {'total_count': total_count, 'offset': offset, 'limit': hits_count,
                    'previous': pagging_string % (offset - limit + offset_plus, limit) if offset >= limit else None,
                    'next': pagging_string % (offset + limit + offset_plus, limit) if offset + limit + offset_plus < total_count else None},
           'objects': items}
    if res_ext:
        ret['res_ext']=res_ext

    #logger.debug("res meta = %s" % ret.get("meta",""))
    return ret

def _keyword_query(keyword):
    return {'multi_match': {'query': keyword, 'type': 'best_fields',
                            #'fields': ['title^10', 'brand_name'],
                            'fields': ["title.en^20", 'title^10', 'brand_name^20', 'city', 'desc'],
                            'tie_breaker': 0.3, 'minimum_should_match': '75%'}}

def _gauss_function_new(search_params, now_ts=utils_func.get_now_ts(),is_location=True,is_renew=True):

    lat = search_params.lat
    lon = search_params.lon

    return _gauss_function(lat, lon, now_ts=now_ts,is_location=is_location,is_renew=is_renew ,search_params=search_params)

def _gauss_function(lat, lon, now_ts=utils_func.get_now_ts(),is_location=True,is_renew=True ,search_params=None):

    # todo 添加新用户的1，2，3件商品进行不同程度的加权
    #   {"exp": {"owner.post_index": {"origin":0, "offset":"1", "scale": 2}}},

    # todo 添加对用户回复及时性的加权处理（平均时间，分钟）
    #   {"exp": {"user_chat": {"origin":0, "offset":"1", "scale": 2}}},

    # todo 根据4小时的ctr记录对商品进行加权
    #   {"gauss": {"ctr": {"origin":30, "scale": 20, "decay":0.3}}},

    # todo 添加按用户的评级衰减
    #   {"gauss": {"owner.weight": {"origin":10, "scale": 5, "decay":0.3}}},
    fnc=[
        # 停用机器人降权，同时需要在search阻止机器人
        #{'gauss': {'owner.is_robot': {'origin': 0, 'scale': 1, 'decay': 0.25}}},

        # 图片清晰度，暂时停用
        # {'gauss': {'image_blur': {'origin': 5, 'scale': 13}}},

        # lvyi，按商品的评级衰减改为 5-10 不变 ， 5-0 降权
        {"gauss": {"weight": {"origin":10, 'offset': 5, "scale": 3, "decay":0.2}}},

        # 按性别匹配分类，暂时停用
        # {'gauss': {'cat.gender': {'origin': gender if gender is not None else 0, 'scale': 1}}}
    ]

    if is_renew:
        # 2015-07-28 暂停 last_active_at 的使用
        #if search_cnf.ab_test_ctrl(search_cnf.ab_test_type_home_active_at,search_params):
            # 添加根据用户的最后在线时间提升商品的显示权限
        #    fnc.append({'gauss': {'last_active_at': {'origin': now_ts,'offset':3600,'scale': settings.DEFAULT_DECAY_DATE}}})
        #else:
            fnc.append({'gauss': {'updated_at': {'origin': now_ts, 'scale': settings.DEFAULT_DECAY_DATE}}})

    if is_location:
        #todo 暂时停用 location_offset = AutoSetDistanceOffset().get_max_distance(search_params)
        #todo location_offset = AutoSetDistanceOffset().max_distance_default
        #.debug("location_offset=%s"%location_offset)
        location_offset = 20
        location_scale = location_offset
        fnc.append({'gauss': {'location': {'origin': '%s,%s' % (lat, lon),
                                   'scale': "%smi"%location_scale, 'offset': "%smi"%location_offset, 'decay':0.22}}})

    return fnc


@singleton
class SearchHomeService(object):

    def first_5_ids(self, first_comp_number, items):
        return utils_func.get_list_ids(items, first_comp_number)

    def top_recommend(self, search_params, total_count, hits_count, items ):

        user_id = search_params.user_id
        offset = search_params.offset
        limit = search_params.limit

        max_recommend = 20
        first_comp_number = 5
        cache_ex = 5 * 60

        offset_plus = 0
        items_random = []
        # print items[0]
        # 如果没有用户ID ，不进行自动推荐?
        if not user_id:
            user_id = "tp_ur_id"

        max_offset = offset+limit

        if offset==0 and user_id:

            home_top_recommend = 'home:%s:top_list' % user_id
            home_top_offset_key = 'home:%s:top_offset' % user_id
            home_max_offset_key = 'home:%s:max_offset' % user_id

            # 获得上一次列表的偏移量
            last_offset = SrService().get_redis_ex().get(home_top_offset_key)
            last_offset = int(last_offset) if last_offset != None else 0
            logger.debug("last offet=%s" % last_offset)
            if last_offset<=max_recommend:

                # todo 将个人缓存的数据与直接搜索的数据进行比较，如果一致，进行推荐n条，已经推荐的数据，将不再推荐

                first_ids = self.first_5_ids(first_comp_number, items)
                logger.debug("first_ids =%s" % first_ids)

                # load 缓存数据
                last_home_key = 'home:%s:last' % user_id
                logger.debug("last_home_key=%s" % last_home_key)
                last_ids = SrService().get_redis_ex().smembers(last_home_key)
                last_ids = list(last_ids)
                logger.debug("last_ids =%s" % last_ids)

                comp_cnt = len([val for val in first_ids if val in last_ids])
                logger.debug("comp_cnt=%s" % comp_cnt)
                if comp_cnt == first_comp_number:
                    random_limit = random.randint(2, 5)
                    logger.debug("random load= %s" % random_limit)
                    # 两次数据没有变化
                    # 加载下一页，加载数目随机（2-5）

                    this_limit = last_offset + random_limit
                    this_offset = max_offset
                    logger.debug("this_limit=%s,this_offset=%s" % (this_limit,this_offset))

                    new_params = copy.deepcopy(search_params)
                    new_params.offset = this_offset
                    new_params.limit = this_limit

                    es_body_random = SearchItems.search_items_by_location(new_params)
                    results_random = es_dao_item().es_search_exec(es_body_random)
                    items_random, hits_count_random, total_count_random = search_base.es_data_parse(results_random)

                    offset_plus = len(items_random)

                    # 倒序
                    items_random.reverse()

                    #数据合并
                    #items = items_random + items
                    hits_count = max_offset + hits_count_random
                    total_count = total_count_random

                    SrService().get_redis_ex().set(home_top_offset_key, this_limit, ex=cache_ex)
                    # 保存数据
                    SrService().get_redis_ex().set(home_top_recommend, json.dumps(items_random) ,ex=cache_ex)
                else:
                    # 缓存数据
                    logger.debug("cache first_ids" % first_ids)
                    SrService().get_redis_ex().sadd(last_home_key, *first_ids)
                    SrService().get_redis_ex().expire(last_home_key, cache_ex)

                # 每次列表加载都需要保存最大的列表偏移量
                logger.debug("cache max_offset=%s" % max_offset)
                SrService().get_redis_ex().set(home_max_offset_key, max_offset, ex=cache_ex)

            else:
                logger.debug("top recommend max!")
                items_random_temp = SrService().get_redis_ex().get(home_top_recommend)
                items_random = json.loads(items_random_temp)

                offset_plus = len(items_random)
                hits_count = hits_count + offset_plus

            logger.debug("hits_count=%s, total_count=%s, offset_plus=%s" % (hits_count, total_count, offset_plus))
        else:
            logger.debug("home user_id=null or offset=0")

        if len(items_random)>0:
            items = items_random +items

        return items, hits_count, total_count, offset_plus

    def search_items_by_test(self, search_params=None):

        offset_plus = 0

        es_body= {
            "sort": [
                {
                  "user_ctr": {
                    "order": "desc"
                  }
                }
              ],
              "filter": {
                "and": [
                  {
                    "exists": {
                      "field": "user_ctr"
                    }
                  }
                ]
              },
              "aggs": {
                "group_by_user_ctr": {
                  "range": {
                    "field": "user_ctr",
                    "ranges": [
                      {
                        "to": 5
                      },
                      {
                        "from": 5,
                        "to": 10
                      },
                      {
                        "from": 10,
                        "to": 15
                      },
                      {
                        "from": 15,
                        "to": 20
                      },
                      {
                        "from": 20,
                        "to": 25
                      },
                      {
                        "from": 25,
                        "to": 30
                      },
                      {
                        "from": 30,
                        "to": 35
                      },
                      {
                        "from": 35,
                        "to": 40
                      },
                      {
                        "from": 40,
                        "to": 45
                      },
                      {
                        "from": 45,
                        "to": 50
                      },
                      {
                        "from": 50
                      }
                    ]
                  }
                }
              },
              "size": search_params.limit,
              "from": search_params.offset
            }

        results =  es_dao_item().es_search_exec(es_body)
        items, hits_count, total_count = search_base.es_data_parse(results)

        return items, hits_count, total_count, offset_plus

    def search_items_by_location_decay(self, search_params=None, dist='y'):

        # 基本查询
        es_body= SearchItems.search_items_by_location(search_params)
        results = es_dao_item().es_search_exec(es_body)
        items, hits_count, total_count = search_base.es_data_parse(results)
        #logger.debug("first query hits_count=%s, total_count=%s" % (hits_count, total_count))

        offset_plus = 0

        # todo 下拉推荐, 目前不确定效果是否足够好，可以用简单的随机代替 
        #items, hits_count, total_count , offset_plus = \ 
        #    self.top_recommend(search_params, total_count, hits_count, items)

        if "y"==dist:
            # 对数据进行洗脸：第一步将该次请求的结果中，相同用户的商品进行间隔分布
            items = UserItemsDistribution.do_parse(items)

        return items, hits_count, total_count, offset_plus



class ReqestParams(object):
    lat = 0
    lon = 0
    user_id = None

class SearchParams(ReqestParams):
    query = None
    offset = 0
    limit = 10

class RefindParams(SearchParams):
    # 搜索条件
    distance = -2
    cat_id = -2
    brand_id = -2
    price_min = -2
    price_max = -2
    user_verifed = -2
    # 搜索排序
    orderby = -2
    # 二次搜索标记
    refind = 0

class SearchItems(object):

    aggregations_field = "cat_id"
    aggregations_key = "group_by_refind"

    @staticmethod
    def build_aggs_body(es_body):
        # 聚合参数
        es_body['aggs'] = {SearchItems.aggregations_key: {"terms": {"field": SearchItems.aggregations_field}}}

    @staticmethod
    def init_params_by_request(request):

        lat = utils.lat_or_lon_format(float(request.GET.get('lat', 0)))
        lon = utils.lat_or_lon_format(float(request.GET.get('lon', 0)))

        offset = int(request.GET.get('offset', 0))
        limit = int(request.GET.get('limit', 10))

        brand_id = int(request.GET.get('brand_id', -2))
        query = request.GET.get('q', '')

        distance = int(request.GET.get('distance', '-2'))
        cat_id = int(request.GET.get('cat_id', '-2'))
        orderby = int(request.GET.get('orderby', '-2'))
        price_min = int(request.GET.get('price_min', '-2'))
        price_max = int(request.GET.get('price_max', '-2'))
        user_verifed = int(request.GET.get('user_verifed', '-2'))

        refind = int(request.GET.get('refind', '0'))
        rf_tag = request.GET.get('rf_tag', None)
        if "null"==rf_tag:
            rf_tag = None

        from common import utils_web
        user_id = utils_web.get_user_id(request)

        #logger.warn("refind=%s" % refind)

        ip_address = utils.get_real_ip(request)

        params = SearchItems._init_params(refind=refind, query=query,cat_id=cat_id,brand_id=brand_id,price_max=price_max,
                                        price_min=price_min,distance=distance,user_verifed=user_verifed,orderby=orderby)
        params.offset= offset
        params.limit= limit
        params.user_id= user_id

        if lat == 0 and lon == 0:
            lat, lon = utils.try_build_location(user_id, ip_address)

        params.lat = lat
        params.lon = lon
        params.ip_address = ip_address

        params.rf_tag = rf_tag

        return params

    @staticmethod
    def _init_params(lat=0, lon=0, refind=0, query='',cat_id=-2,brand_id=-2,price_max=0,price_min=0,distance=-2,
                    user_verifed=-2,orderby=-2):

        rp = RefindParams()
        rp.lat = lat
        rp.lon = lon

        rp.refind = refind
        rp.query = query
        rp.cat_id = cat_id
        rp.brand_id = brand_id

        rp.price_max = price_max
        rp.price_min = price_min
        rp.distance = distance
        rp.user_verifed = user_verifed

        rp.orderby = orderby

        return rp

    @staticmethod
    def build_query_body(refind_params):

        es_body = {'sort':{}, 'query':{"function_score":{}}, 'from':refind_params.offset, 'size':refind_params.limit}

        function_score = es_body['query']['function_score']
        function_score['query'] = {"filtered":{"filter":{}}}

        #sort
        SearchItems.build_sort_body_new(es_body, refind_params)

        #keyword
        if refind_params.query:
            function_score['query']["filtered"]["query"] = _keyword_query(refind_params.query)

        and_arr = []
        #brand_id
        if refind_params.brand_id>-1:
            and_arr.append({'term':{'brand_id':str(refind_params.brand_id)}})

        #user_verifed
        #logger.debug("user_verifed=%s" % refind_params.user_verifed)
        if refind_params.user_verifed>0:
            and_arr.append({"term":{"owner.verified":True}})

        and_arr.append({"range": {"weight": {"gt": 0}}})

        # category
        cat_id = refind_params.cat_id
        if cat_id>-1:
            and_arr.append({"term":{"cat_id":cat_id}})
        #logger.info("cat_id=%s" % cat_id)

        # price
        price_min = refind_params.price_min
        price_max = refind_params.price_max
        if price_min>0 or price_max>0:
            price_scope = {
                "range": {
                  "price": {}
                }
              }
            if price_min>price_max and price_max>0:
                # 最大值最小值位置交换
                price_swap = price_min
                price_min = price_max
                price_max = price_swap

            if price_min>0:
                price_scope["range"]["price"]["from"] = price_min
            if price_max>0:
                price_scope["range"]["price"]["to"] = price_max

            and_arr.append(price_scope)

        # distance
        distance = refind_params.distance
        if distance!=0:
            #logger.debug( "distance=%s" % distance)
            distance_mi = search_cnf.distance_confs.get(distance,None)
            #logger.debug( "distance_mi=%s" % distance_mi)
            if distance_mi:
                distance_filter = {
                    "geo_distance": {
                      "distance": "%smi" % distance_mi['val'],
                      "location": {
                        "lat": refind_params.lat,
                        "lon": refind_params.lon
                      }
                    }
                  }
                and_arr.append(distance_filter)

        # 机器人商品不用出现在search的结果列表中
        and_arr.append({"term":{"owner.is_robot":0}})

        if len(and_arr):
            function_score['query']["filtered"]["filter"]["and"] = and_arr

        # 权重
        function_score['functions'] = _gauss_function_new(refind_params, now_ts=utils_func.get_now_ts())

        # 聚合参数
        SearchItems.build_aggs_body(es_body)

        #print ('%s' % es_body).replace("'","\"")
        return es_body


    @staticmethod
    def build_sort_body_new(es_body, refind_params):

        lat = refind_params.lat
        lon = refind_params.lon

        orderby = refind_params.orderby
        #logger.debug("orderby=%s" % orderby)
        sort_conf = search_cnf.sort_confs.get(orderby, None)
        es_body['sort'] = []
        if orderby > 0:
            #logger.debug("sort_conf=%s" % sort_conf)
            if sort_conf:
                es_body['sort'] = SearchItems.sort_item( lat, lon, sort_conf)

        if orderby == 0:
            #sort_conf_0 = sort_conf[0]
            #sort_conf_1 = sort_conf[1]

            #es_body['sort'].append(SearchItems.sort_item( lat, lon, sort_conf_0))
            #es_body['sort'].append(SearchRefind.sort_item( lat, lon, orderby, sort_conf_1))
            pass
        #logger.debug("es_body['sort']=%s" % es_body['sort'])


    @staticmethod
    def sort_item(lat, lon, sort_conf):
        sort_field = sort_conf['field']
        sort_dir = sort_conf['dir']
        item = {}
        if sort_field == 'location':
            # 按距离排序
            item["_geo_distance"] = {
                sort_field: "%s,%s" % (lat, lon),
                "order": sort_dir
            }
        else:
            item = {sort_field: {"order": sort_dir}}
        return item

    @staticmethod
    def build_refind_res(refind_params ,results ,query_type=''):

        #if refind_params:
            ext = {}
            if refind_params:
                for param in dir(refind_params):
                    #print param
                    val =  getattr(refind_params, param)
                    if param=='cat_id' and val==-2:
                        setattr(refind_params, param ,-1)
                    elif val==-2:
                        setattr(refind_params, param ,0)

                ext = {
                    "refind":{"distance":refind_params.distance,"cat_id":refind_params.cat_id,
                              "orderby":refind_params.orderby,
                              "price_min":refind_params.price_min,"price_max":refind_params.price_max,
                              "user_verifed":refind_params.user_verifed},
                }
            if results:
                '''
                文档格式
                aggregations: {
                group_by_cat_id: {
                buckets: [
                {
                key: 0
                doc_count: 6
                },{},{}]}}
                '''
                from entity.service import CategoryService
                items = []
                if query_type=='category':
                    # 从分类进入，返回所有分类
                    # categories_tuple = CategoryService().get_categories()
                    # for it in categories_tuple:
                    #     key = it['id']
                    #     txt = it['title']
                    #     items.append({"id":"%s"%key, "cnt":0, "title":txt})
                    pass
                else:
                    logger.debug("refind_params.refind=%s" % refind_params.refind)
                    #if refind_params.refind==0:
                    aggregations = results.get('aggregations',None)
                    #logger.debug("aggregations=%s" % aggregations)
                    if aggregations:
                        # 按结果返回
                        #categories_map = CategoryService().get_categories_map()
                        for list in aggregations[SearchItems.aggregations_key]['buckets']:
                            cat_id = list['key']
                            # 从接口中获得
                            items.append({"id":"%s"%cat_id, "cnt":list['doc_count']})
                            #title = categories_map.get(int(cat_id),None)
                            # if title:
                            #     items.append({"id":"%s"%cat_id, "cnt":list['doc_count'], "title":title})
                            # else:
                            #     logger.warn("cat_id = %s, the title is no found!" % cat_id)

                #if len(items)>0:
                #    ext['refind_category'] = [{"id":-1, "cnt":0, "title":"All (default)"}] + items

                ext['refind_category'] = items
            return ext
        #else:
        #    return None

    @staticmethod
    def renew_func(es_body, now_ts):
        # 控制renew 的商品衰减速度
        renew_scale = 2 * 24 * 3600  # 1DAY
        '''
                renew_offset = 7200
                # 这是停用的renew衰减算法：在renew后一小时是新建商品的一半
                "script_score": {
                    "params": {
                      "now": now_ts,
                      "offset": renew_offset
                    },
                    "script": "((now-doc['updated_at'].value)<=offset?1:((doc['updated_at'].value-doc['created_at'].value)>offset?0.5:1))"
                }
                '''
        renew_decay = {
            "script_score": {
                "params": {
                    "now": now_ts,
                    "mix_boost": 0.0001,
                    "ratio": 0.5,
                    "scale": renew_scale,
                }, "script": "renew", "lang": "native"
                # "script":"Math.max(Math.exp(-1*ratio* Math.pow(((System.currentTimeMillis()/1000-doc['updated_at'].value)/(double)scale) * ((System.currentTimeMillis()/1000-doc['created_at'].value)/(double)scale +1), 2)),mix_boost)"
            }
        }
        es_body["query"]['function_score']["functions"].append(renew_decay)

    @staticmethod
    def home_by_distance(es_body, search_params):
        max_scope = AutoCalMaxDistance().get_max_distance(search_params)
        # max_scope = 0
        #if max_scope>0 and max_scope<100:
        #    max_scope = 100
        if max_scope > 0:
            es_body["filter"]["and"].append({
                "geo_distance": {
                    "distance": "%smi" % max_scope,
                    "location": {
                        "lat": search_params.lat,
                        "lon": search_params.lon
                    }
                }
            })

    @staticmethod
    def weight_ctrl():
        return {"range": {"weight": {"gt": 0}}}

    @staticmethod
    def search_items_by_location(search_params):

        offset = search_params.offset
        limit = search_params.limit

        now_ts = utils_func.get_now_ts()

        es_body = {"filter":{"and":[]},"query":{'function_score':{}},"from":offset,"size":limit}
        es_body["query"]['function_score']["functions"] = _gauss_function_new(search_params, now_ts=now_ts)

        # 权重=0 不显示
        es_body["filter"]["and"].append(SearchItems.weight_ctrl())

        # 根据可见的商品数，可视半径距离
        SearchItems.home_by_distance(es_body, search_params)

        # todo 只显示一月以内的商品
        is_show_one_month = False
        if is_show_one_month:
            age_60 = now_ts - 30*24*3600
            es_body["filter"]["and"].append({"range": {"updated_at": {"gt": age_60}}})

        '''
          将数据进行地理位置，显示本国家的商品
          {"term": {"country": "china"}}
        '''

        # 加入renew的衰减算法
        SearchItems.renew_func(es_body, now_ts)
        return es_body

    @staticmethod
    def suggest_items_by_keyword(keyword, cat_id, refind_params,
                                 is_query_keyword=True, is_filter_distance=False):

        lat = refind_params.lat
        lon = refind_params.lon
        #gender = refind_params.gender
        offset = refind_params.offset
        limit = refind_params.limit

        #logger.debug("suggest_items_by_keyword is_query_keyword=%s, is_filter_distance=%s" %
        #             (is_query_keyword, is_filter_distance))

        filter = {"and":[
            {
                "term": {
                  "cat_id": cat_id
                }
            },
            ]}

        # 只列出在售的商品
        filter['and'].append(
            #{"term": {"state": common.biz.ITEM_STATE_LISTING}}
            {"not":{"exists": {"field": "state"}}}
        )

        filter['and'].append({"range": {"weight": {"gt": 0}}})

        # 需要将推荐的商品控制在 50miles
        if is_filter_distance:
            filter['and'].append({
                "geo_distance": {
                  "distance": "50km",
                  "location": {
                    "lat": lat,
                    "lon": lon
                  }
                }
              })

        es_body = {'filter': filter, 'query': {'function_score': {
                    #'query': _keyword_query(keyword),
                    'functions': _gauss_function(lat, lon)}}, 'from':offset, 'size':limit}
        if is_query_keyword:
            es_body['query']['function_score']['query'] = _keyword_query(keyword)

        return es_body

    @staticmethod
    def search_followings_items(search_params, **kwargs):
        now_ts = utils_func.get_now_ts()
        es_body = {"query": {'function_score': {}}, "from": search_params.offset, "size": search_params.limit}
        function_score = es_body['query']['function_score']
        function_score['functions'] = _gauss_function_new(search_params, now_ts=now_ts)
        function_score['query'] = {"filtered": {"filter": {}}}

        and_arr = list()
        and_arr.append({"terms": {"owner.id": list(e.lower() for e in kwargs['following_ids'])}})

        if and_arr:
            function_score['query']["filtered"]["filter"]["and"] = and_arr

        # es_query = {"filtered": {"filter": {"range": {"weight": { "gte": 0 }}}}}
        # es_body["query"]['function_score']["query"] = es_query  

        return es_body

@singleton
class AutoSetDistanceOffset(object):
    # 边界商品数量
    max_item_number = 5000
    # 最大边界距离
    max_distance_default = 40.0
    # 缓存 1 天
    ex = 36 * 3600

    def measure_distance(self , lat, lon, max_distance):

        now = utils_func.get_now_ts()
        thiredays_before = now - 3600*24*3;

        max_distance_0 = max_distance

        # 查询3天内的在售新商品
        query_body = {
          "filter": {
            "and": [
              {
                "range": {"updated_at": {"lte": thiredays_before}}
              },
              {
                "not": {"exists": {"field": "state"}}
              }
            ]
          },
          "aggs": {
            "group_by_distance": {
              "geo_distance": {
                "unit": "mi", "field": "location",
                "origin": "%s,%s" % (lat,lon),
                "ranges": [
                  {"to": 5},{"from": 5,"to": 10},
                  {"from": 10,"to": 15},{"from": 15,"to": 20},{"from": 20,"to": 25},
                  {"from": 25,"to": 30},{"from": 30,"to": 35},
                  {"from": 35,"to": 40},{"from": 40,"to": 45},
                  {"from": 45,"to": 50},{"from": 50}
                ]
              }
            }
          }, "size": 0, "from": 0}

        query_res = es_dao_item().es_search_exec(query_body)
        '''
        返回数据的样例:
        aggregations: {
            group_by_distance: {
            buckets: [
                {
                    key: *-5.0
                    from: 0
                    to: 5
                    doc_count: 0
                }
                {
                    key: 5.0-10.0
                    from: 5
                    to: 10
                    doc_count: 5
                }
        '''

        # 数据累加

        nearby_items_sum = 0
        #max_distance = self.max_distance_default
        group = query_res['aggregations']['group_by_distance']['buckets']
        for it in group:
            nearby_items_sum += it['doc_count']
            if nearby_items_sum >=self.max_item_number:
                # 到达数据比较边界
                max_distance = it.get('to', max_distance_0)
                break

        # 保存该用户的数据边界
        return max_distance

    def get_max_distance(self ,search_params):  #
        '''
        检查缓存中是否存在上一次的计算结果
        如果没有，发起异步的加载，同时返回默认值
        '''
        max_distance = float(self.max_distance_default)
        if not search_params:
            return max_distance

        user_id=search_params.user_id
        if not user_id:
            return max_distance

        lat=search_params.lat
        lon=search_params.lon
        try:
            r_ex = SrService().get_redis_ex()

            max_distance_key = 'usr:%s:maxdst' % user_id
            max_distance_0 = r_ex.get(max_distance_key)
            #logger.debug("1 max_distance_0=%s" % max_distance_0)
            if not max_distance_0:
                logger.debug("max_distance_key=%s" , max_distance_key)
                # 异步加载，下次使用
                #task_load_max_distance(max_distance_key, lat, lon, max_distance)
                task_cal_max_offset.apply_async((max_distance_key, lat, lon, max_distance), queue='celery.misc')
            else:
                #logger.debug("max_distance_cache=%s" , max_distance_cache)
                max_distance = float(max_distance_0)

                pass
        except Exception,e:
            logger.error("get_max_distance cache error:%s" % e)

        logger.info("stop uid=%s max_distance=%s" % (user_id,max_distance))

        return float(self.max_distance_default)


    def load_max_distance(self ,max_distance_key, lat, lon, max_distance):
        try:
            # 计算
            max_distance = self.measure_distance(lat, lon , max_distance)

            # 设置缓存
            r_ex = SrService().get_redis_ex()
            cache_res = r_ex.set(max_distance_key , max_distance ,ex=self.ex)
            logger.info('load_max_distance key=%s res=%s ex=%s val=%s', max_distance_key, cache_res, self.ex, max_distance)
        except Exception,e:
            logger.error("load_max_distance error:%s" % e)

class AutoCalMaxDistanceBase(object):
    # 边界商品数量
    max_item_number = 50000

    first_max_default = 2000.0
    # 最大边界距离
    max_distance_default = 1000.0
    # 最小检查距离
    check_scope = 100.0
    # 缓存 10 小时
    ex = 10 * 3600
    cache_key = 'usr:%s:radius'
    # 商品的有效期
    before = 3600*24*3

    def measure_distance(self , lat, lon, max_distance):

        now = utils_func.get_now_ts()
        thiredays_before = now - self.before;

        max_distance_0 = max_distance

        # 查询3天内的在售新商品
        query_body = {
          "filter": {
            "and": [
              {"range": {"updated_at": {"lte": thiredays_before}}},
              {"not": {"exists": {"field": "state"}}}
            ]
          },
          "aggs": {
            "group_by_distance": {
              "geo_distance": {
                "unit": "mi", "field": "location",
                "origin": "%s,%s" % (lat,lon),
                "ranges": []
              }
            }
          }, "size": 0, "from": 0}

        #{"to": 5},{"from": 5,"to": 10},{"from": 50}
        ranges = query_body['aggs']['group_by_distance']['geo_distance']['ranges']
        ranges.append({"to": self.check_scope})

        steps_one = int(300/self.check_scope)
        #logger.debug("max_step_one=%s"%steps_one)
        last_step = 300
        for step in xrange(steps_one):
            #logger.info(step+1)
            last_step = self.check_scope*(step+2)
            ranges.append({"from": self.check_scope*(step+1),"to":last_step})

            last_step = self.check_scope*(step+2)

        steps_two = int((self.max_distance_default)/self.check_scope_2)
        #logger.debug("max_step_two=%s"%steps_two)
        for step in xrange(steps_two):
            #logger.info(step+1)
            last_step_2 = last_step+self.check_scope_2*(step+1)
            ranges.append({"from": last_step+self.check_scope_2*(step),"to":last_step_2})

        ranges.append({"from": last_step_2})
        logger.debug("max_last_step =%s" % last_step_2)

        # todo query_body 可以使用缓存
        query_res = es_dao_item().es_search_exec(query_body)
        '''
        返回数据的样例:
        aggregations: {
            group_by_distance: {
            buckets: [
                {
                    key: *-5.0
                    from: 0
                    to: 5
                    doc_count: 0
                },
                {
                    key: 5.0-10.0
                    from: 5
                    to: 10
                    doc_count: 5
                }
        '''

        # 数据累加
        nearby_items_sum = 0
        group = query_res['aggregations']['group_by_distance']['buckets']
        #logger.info("max_ aggregations=%s" % group)
        for it in group:
            nearby_items_sum += it['doc_count']

            if nearby_items_sum >=self.max_item_number:
                # 到达数据比较边界
                max_distance = it.get('to', max_distance_0)
                break
        if nearby_items_sum < self.max_item_number:
            max_distance = 0

        logger.info("nearby_items_sum=%s,max_distance=%s" % (nearby_items_sum,max_distance))

        # 保存该用户的数据边界
        return max_distance

    def get_max_distance(self ,search_params):
        #logger.info("get_max_distance=%s" % search_params)
        '''
        检查缓存中是否存在上一次的计算结果
        如果没有，发起异步的加载，同时返回默认值
        '''
        max_distance = float(self.first_max_default)
        if not search_params:
            return max_distance

        user_id=search_params.user_id
        #logger.info("max_ user_id=%s" % user_id)
        if not user_id:
            return max_distance
            pass

        lat=search_params.lat
        lon=search_params.lon
        try:
            max_distance_key = self.cache_key % user_id
            max_distance_0 = SrService().get_redis_ex().get(max_distance_key)
            logger.debug("key=%s , max_distance_0=%s" % (max_distance_key,max_distance_0))
            #max_distance_0 = None
            if max_distance_0:
                max_distance = float(max_distance_0)
            else:
                #logger.debug("max_radius_key=%s" , max_distance_key)
                # 异步加载，下次使用
                #task_cal_max_distance(max_distance_key, lat, lon, max_distance)
                task_cal_max_distance.apply_async((max_distance_key, lat, lon, max_distance), queue='celery.misc')
                pass

        except Exception,e:
            logger.error("get_max_distance error:%s" % e)

        logger.info("uid=%s max_radius=%s" % (user_id, max_distance))
        return float(max_distance)


    def load_max_distance(self ,max_distance_key, lat, lon, max_distance):
        try:
            # 计算
            max_distance = self.measure_distance(lat, lon , max_distance)
            # 设置缓存
            cache_res = SrService().get_redis_ex().set(max_distance_key , max_distance ,ex=self.ex)
            logger.info('load_max_distance key=%s res=%s ex=%s val=%s', max_distance_key, cache_res, self.ex, max_distance)
        except Exception,e:
            logger.error("load_max_distance error:%s" % e)

@singleton
class AutoCalMaxOffset(AutoCalMaxDistanceBase):
    # 边界商品数量
    max_item_number = 2000

    first_max_default = 50.0
    # 最大边界距离
    max_distance_default = 50.0
    # 最小检查距离
    check_scope = 5.0
    # 缓存 10 小时
    ex = 10 * 3600
    cache_key = 'usr:%s:maxdst'
    # 商品的有效期
    before = 3600*24*3

@singleton
class AutoCalMaxDistance(AutoCalMaxDistanceBase):
    # 边界商品数量
    max_item_number = 20000

    first_max_default = 0
    # 最大边界距离
    max_distance_default = 7000.0
    # 最小检查距离
    check_scope = 50.0
    check_scope_2 = 200.0
    # 缓存 10 小时
    ex = 10 * 3600
    cache_key = 'usr:%s:radius'
    # 商品的有效期
    before = 3600*24*3

@shared_task(queue='celery.misc')
def task_cal_max_distance(max_distance_key, lat, lon, max_distance):
    AutoCalMaxDistance().load_max_distance(max_distance_key, lat, lon, max_distance)
    pass

@shared_task(queue='celery.misc')
def task_cal_max_offset(max_distance_key, lat, lon, max_distance):
    AutoSetDistanceOffset().load_max_distance(max_distance_key, lat, lon, max_distance)
    pass

if __name__ == '__main__':

    query = 'hello'
    lat = 30.615566
    lon = -95.59191
    offset = 30
    limit = 30


    AutoCalMaxDistance().load_max_distance("qqqqqq", lat, lon, 110)

''''
    refind_params = SearchItems._init_params(lat=lat , lon=lon, query=query,refind=1,distance=-2 , cat_id=-2 ,orderby=-2,
                                             price_min=-2, price_max=-2, user_verifed=-2)
    SearchItems.build_query_body(refind_params)


    refind_params = SearchItems._init_params(lat=lat , lon=lon, refind=1,distance=0 , cat_id='4' ,orderby=0,
                                             price_min=0, price_max=0, user_verifed=0)
    SearchItems.build_query_body(refind_params)


    refind_params = SearchItems._init_params(lat=lat , lon=lon, refind=1,distance=2 , cat_id=-1,orderby=1,
                                             price_min=90, price_max=10, user_verifed=1)
    SearchItems.build_query_body(refind_params)


    refind_params = SearchItems._init_params(distance=1 , cat_id=2 ,orderby=3, price_min=0, price_max=0,
                            user_verifed=0)
    SearchItems.build_query_body(refind_params)
'''
'''
    refind_params = SearchRefind._init_refind_params(distance=10 , cat_id=0 ,orderby=1, price_min=10, price_max=20,
                            user_verifed=0)
    _test_es_body(refind_params)

    refind_params = SearchRefind._init_refind_params(distance=10 , cat_id=0 ,orderby=0, price_min=0, price_max=20,
                            user_verifed=1)
    _test_es_body(refind_params)
'''