# -*- coding: utf-8 -*-

import random
import logging
import jwt

from django.conf import settings
from django.utils.translation import ugettext as _
from twilio.rest import TwilioRestClient
from hashids import Hashids
from datetime import datetime, timedelta
from iso3166 import countries

twilioClient = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_ACCOUNT_TOKEN)
hashider = Hashids(min_length=settings.HASH_ID_MIN_LENGTH, salt=settings.HASH_ID_SALT)
hashider4item = Hashids(min_length=settings.ITEM_HASH_ID_MIN_LENGTH, salt=settings.ITEM_HASH_ID_SALT)

logger = logging.getLogger("pinnacle")


def generate_temp_passcode(digits=6):
    return str("%0." + str(digits) + "d") % random.randint(0, 10**digits-1)


def generate_access_token(user_id):
    # now = datetime.utcnow()
    # future = now + timedelta(days=settings.JWT_TOKEN_EXPIRATION_DAYS)
    # token = jwt.encode({'user_id':user_id, 'exp':future}, settings.JWT_SECRET_KEY)
    # iOS V2.5开始引入匿名注册，token设置为永不过期
    token = jwt.encode({'user_id': user_id}, settings.JWT_SECRET_KEY)
    return token


def generate_temp_password(length=6):
    """
    Generate the temporary password.
    :return: The password
    """
    return _random_str(length)

def send_sms_to_number(number, passcode):
    msg = _('*5miles* Your login passcode: %s') % passcode
    try:
        message = twilioClient.messages.create(from_=settings.TWILIO_FROM_NUMBER, to=number, body=msg)
    except twilio.TwilioRestException as e:
        logger = logging.getLogger('pinnacle')
        logger.exception('twilio error')

def obfuscate_id(raw_id):
    return hashider.encrypt(raw_id)

def obfuscate_item_id(raw_id):
    return hashider4item.encrypt(raw_id)

def tiny_url(url):
    import urllib

    try:
        apiurl = "http://tinyurl.com/api-create.php?url="
        tinyurl = urllib.urlopen(apiurl+url).read()
        return tinyurl
    except Exception, e:
        return url



def _random_str(length=6):
    seed = '23456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'
    str = []
    for i in range(length):
        str.append(random.choice(seed))
    return ''.join(str)


def decrypt_user_id(fluzzy_user_id):
    return hashider.decrypt(fluzzy_user_id)


def decrypt_item_id(fluzzy_item_id):
    return hashider4item.decrypt(fluzzy_item_id)

def correct_country_name(country_name):
    if len(country_name) > 2:
        return country_name

    country = countries.get(country_name)
    return country.name if country else country_name

