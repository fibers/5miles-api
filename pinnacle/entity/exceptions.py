# -*- coding: utf-8 -*-
__author__ = 'dragon'

from django.utils.translation import ugettext as _

ERR_CODE_TOKEN_EXPIRED = 1000
ERR_CODE_UNAUTHENTICATED = 1001
ERR_CODE_BAD_REQUEST = 1002
ERR_CODE_INVALID_PARAMETER = 1003
ERR_CODE_USER_NOT_FOUND = 1004
ERR_CODE_ITEM_NOT_FOUND = 1005
ERR_CODE_NOT_FOUND = 1006
ERR_CODE_INVALID_PASSWORD = 1007
ERR_CODE_EMAIL_EXISTS = 1008
ERR_CODE_INVALID_CREDENTIALS = 1009
ERR_CODE_RESET_PASSWORD_FAILED = 1010
ERR_CODE_INVALID_STATE = 1011
ERR_CODE_INVALID_OPERATION = 1012
ERR_CODE_PHONE_NUMBER_BOUND = 1013
ERR_CODE_INVALID_PASSCODE = 1014
ERR_CODE_EMAIL_NOT_EXISTS = 1015
ERR_CODE_ITEM_CATEGORY_NOT_FOUND = 1016


ERR_CODE_SERVER_INTERNAL_ERROR = 1999


ERR_CODE_UNAUTHORIZED = 9000
ERR_CODE_USER_BLOCKED = 9001

COMMON_ERROR_MSG = _('Sorry, 5miles is experiencing a technical issue. Please try again later.')

ERR_MSG = {
    ERR_CODE_UNAUTHORIZED: _('Your account has been suspended for violating our policies. Please contact support@5milesapp.com for more information.'),
    # ERR_CODE_UNAUTHORIZED: COMMON_ERROR_MSG,
    ERR_CODE_USER_BLOCKED: _('You have been blocked by this person.'),
    ERR_CODE_UNAUTHENTICATED: COMMON_ERROR_MSG,
    ERR_CODE_TOKEN_EXPIRED: COMMON_ERROR_MSG,
    ERR_CODE_BAD_REQUEST: COMMON_ERROR_MSG,
    ERR_CODE_INVALID_PARAMETER: COMMON_ERROR_MSG,
    ERR_CODE_NOT_FOUND: COMMON_ERROR_MSG,
    ERR_CODE_INVALID_PASSWORD: _('Your password should contain 6~16 characters.'),
    ERR_CODE_EMAIL_EXISTS: _('That email address has already been used.'),
    ERR_CODE_INVALID_CREDENTIALS: _('Incorrect email or password.'),
    ERR_CODE_USER_NOT_FOUND: COMMON_ERROR_MSG,
    ERR_CODE_ITEM_NOT_FOUND: COMMON_ERROR_MSG,
    ERR_CODE_ITEM_CATEGORY_NOT_FOUND: COMMON_ERROR_MSG,
    ERR_CODE_SERVER_INTERNAL_ERROR: COMMON_ERROR_MSG,
    ERR_CODE_RESET_PASSWORD_FAILED: _('Unable to reset password. Please try again.'),
    ERR_CODE_INVALID_STATE: COMMON_ERROR_MSG,
    ERR_CODE_INVALID_OPERATION: _('Disallowed operation, please wait a while.'),
    ERR_CODE_PHONE_NUMBER_BOUND: _('Your phone number has already been bound with another 5miles account, please check.'),
    ERR_CODE_INVALID_PASSCODE: _('Passcode is incorrect.'),
    ERR_CODE_EMAIL_NOT_EXISTS: _('This email doesn\'t exist, please check.'),
}


class PinnacleApiError(Exception):
    def __init__(self, err_code, err_msg=None):
        self.err_code = err_code
        self.err_msg = err_msg if err_msg else ERR_MSG[self.err_code]


class BadRequestError(PinnacleApiError):
    def __init__(self, err_code=ERR_CODE_BAD_REQUEST, err_msg=None):
        PinnacleApiError.__init__(self, err_code, err_msg)


class NotFoundError(PinnacleApiError):
    def __init__(self, err_code=ERR_CODE_NOT_FOUND):
        PinnacleApiError.__init__(self, err_code)


class ItemNotFoundError(NotFoundError):
    def __init__(self, item_id):
        NotFoundError.__init__(self, ERR_CODE_ITEM_NOT_FOUND)
        self.item_id = item_id


class ItemCategoryNotFoundError(NotFoundError):
    def __init__(self, item_id, category_id):
        NotFoundError.__init__(self, ERR_CODE_ITEM_CATEGORY_NOT_FOUND)
        self.item_id = item_id
        self.category_id = category_id


class UserNotFoundError(NotFoundError):
    def __init__(self, user_id):
        NotFoundError.__init__(self, ERR_CODE_USER_NOT_FOUND)
        self.user_id = user_id


class InvalidParameterError(PinnacleApiError):
    def __init__(self, err_msg=None):
        PinnacleApiError.__init__(self, ERR_CODE_INVALID_PARAMETER, err_msg)


class UnauthorizedError(PinnacleApiError):
    def __init__(self):
        PinnacleApiError.__init__(self, ERR_CODE_UNAUTHORIZED)


class UserBlockedError(PinnacleApiError):
    def __init__(self):
        PinnacleApiError.__init__(self, ERR_CODE_USER_BLOCKED)


class UnauthenticatedError(PinnacleApiError):
    def __init__(self):
        PinnacleApiError.__init__(self, ERR_CODE_UNAUTHENTICATED)


class InvalidCredentialsError(PinnacleApiError):
    def __init__(self):
        PinnacleApiError.__init__(self, ERR_CODE_INVALID_CREDENTIALS)


class InvalidStateError(PinnacleApiError):
    def __init__(self):
        PinnacleApiError.__init__(self, ERR_CODE_INVALID_STATE)


class InvalidOperationError(PinnacleApiError):
    def __init__(self):
        PinnacleApiError.__init__(self, ERR_CODE_INVALID_OPERATION)