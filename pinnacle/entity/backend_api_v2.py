# -*- coding: utf-8 -*-
from common import utils_web

from entity.exceptions import UserNotFoundError, BadRequestError, ItemNotFoundError, \
    UnauthenticatedError, ItemCategoryNotFoundError, PinnacleApiError, \
    ERR_CODE_SERVER_INTERNAL_ERROR, InvalidParameterError

__author__ = 'dragon'

from django.http import HttpResponse
from django.views.decorators.http import require_GET, require_POST
from tastypie.authentication import ApiKeyAuthentication
from .service import ItemImageService, UserService, ItemService
from common.biz import *

from management.commands.runbackendapiserver import BackendApiHandler

authentication = ApiKeyAuthentication()
user_service = UserService()
item_service = ItemService()
item_image_service = ItemImageService()
backendApiHandler = BackendApiHandler()


@require_POST
def set_first_image(request):
    auth_check(request)

    item_id = request.POST.get('item_id', '')
    image_id = request.POST.get('image_id', '')

    if item_id == '' or image_id == '':
        raise BadRequestError(err_msg='Item ID or Image ID is required.')

    if not item_service.item_exists(item_id):
        raise ItemNotFoundError(item_id)

    item_image_service.set_first_image(item_id, image_id)
    return HttpResponse()


@require_GET
def get_user_devices(request):
    auth_check(request)

    user_id = request.GET.get('user_id', '')
    if user_id == '':
        raise BadRequestError(err_msg='User ID is required.')

    devices = user_service.get_devices(user_id)
    return HttpResponse(json.dumps(devices), content_type='application/json')


@require_POST
def disable_user(request):
    auth_check(request)

    user_id = request.POST.get('user_id', '')
    action = request.POST.get('action', '')

    if not user_service.user_exists(user_id):
        raise UserNotFoundError(user_id)

    user_state = user_service.get_user_state(user_id)
    if user_state == USER_STATE_FORBIDDEN:
        raise BadRequestError(err_msg='User already disabled')

    if action == 'disable':
        user_service.disable_user(user_id)
    else:
        raise BadRequestError(err_msg='Unsupported action, only support disable')

    return HttpResponse()


@require_POST
def unlist_item(request):
    auth_check(request)
    item_id = request.POST.get('item_id', '')
    ItemService().unlist_item(item_id, is_check_owner=False)
    return HttpResponse()


def auth_check(request):
    auth_result = authentication.is_authenticated(request)
    if isinstance(auth_result, HttpResponse) or not auth_result:
        raise UnauthenticatedError()


@require_POST
def review_item(request):
    auth_check(request)

    item_id = request.POST.get('item_id', '')
    action = request.POST.get('action', '')
    target_state = int(request.POST.get('state', 0))

    if not item_service.item_exists(item_id):
        raise ItemNotFoundError(item_id)

    item_state = item_service.get_item_state(item_id)
    if action in ['approve', 'approve_risk']:
        if item_state == ITEM_STATE_UNAPPROVED:
            # 从运营下架状态恢复到之前的状态
            if target_state == ITEM_STATE_LISTING:
                # 恢复上架状态
                item_service.recover_list(item_id)
            elif target_state == ITEM_STATE_UNLISTED:
                # 恢复用户下架状态
                item_service.recover_unlist(item_id)
            elif target_state == ITEM_STATE_SOLD:
                # 恢复sold状态
                item_service.recover_sold(item_id)
    elif action == 'disapprove':
        if item_state in [ITEM_STATE_LISTING, ITEM_STATE_UNLISTED, ITEM_STATE_SOLD]:
            item_service.disapprove(item_id)
    else:
        raise BadRequestError(err_msg='Unsupported action, only support approve/approve_risk/disapprove')

    return HttpResponse()


@require_POST
def lbs_item_update(request):
    auth_check(request)

    item_id = request.POST.get('item_id', '')
    item_fz_id = request.POST.get('item_fz_id', None)
    lat = utils_web.lab_format(request, 'lat')
    lon = utils_web.lab_format(request, 'lon')
    is_update_owner = request.POST.get('is_update_owner', False)

    # 修改商品位置信息
    if item_id or item_fz_id:
        ItemService().lbs_update(item_id, lat, lon, fz_id=item_fz_id, is_update_owner=is_update_owner)
    else:
        logger.warn("id is empty=%s,%s" % (item_id,item_fz_id))
        raise ItemNotFoundError(item_id)

    return HttpResponse()


@require_POST
def lbs_user_update(request):

    auth_result = authentication.is_authenticated(request)
    if isinstance(auth_result, HttpResponse) or not auth_result:
        raise UnauthenticatedError()

    user_id = request.POST.get('user_id', '')
    user_fz_id = request.POST.get('user_fz_id', None)
    lat = utils_web.lab_format(request, 'lat')
    lon = utils_web.lab_format(request, 'lon')

    # 修改个人的位置信息
    if user_id or user_fz_id:
        UserService().lbs_update(user_id , lat ,lon,fz_id = user_fz_id)
    else:
        logger.warn("id is empty=%s,%s" % (user_id,user_fz_id))
        raise ItemNotFoundError(user_id)

    return HttpResponse()

@require_POST
def update_product_category(request):
    auth_check(request)

    product_id = request.POST.get('product_id', '')
    category_id = request.POST.get('category_id', '')
    logger.info("update_product_category(product_id:%s,category_id:%s)" % (product_id, category_id))

    if not product_id or not category_id:
        raise InvalidParameterError()

    item_id = int(product_id)
    cat_id = int(category_id)

    result = backendApiHandler.updateProductCategory(item_id, cat_id)

    if result == -2:
        raise ItemCategoryNotFoundError(product_id, category_id)
    elif result < 0:
        raise PinnacleApiError(ERR_CODE_SERVER_INTERNAL_ERROR)


    return HttpResponse()


@require_POST
def get_offers_by_item_id(request):
    auth_check(request)
    item_id = request.POST.get('item_id', '')
    if not item_id:
        raise ItemNotFoundError(item_id)

    json_dumps = backendApiHandler.get_offers_by_item_id(item_id)

    return HttpResponse(json_dumps, content_type='application/json')


@require_POST
def get_offer_count_by_item_ids(request):
    auth_check(request)
    item_ids = json.loads(request.POST.get('item_ids', '[]'))
    if len(item_ids) == 0:
        raise InvalidParameterError()

    json_dumps = backendApiHandler.get_offer_count_by_item_ids(item_ids)

    return HttpResponse(json_dumps, content_type='application/json')


@require_POST
def batch_update_item_weight(request):
    auth_check(request)
    item_ids = json.loads(request.POST.get('item_ids', '[]'))
    weight = request.POST.get('weight', '')
    if len(item_ids) == 0 or not weight:
        raise InvalidParameterError()

    json_dumps = backendApiHandler.batch_update_item_weight(item_ids, weight)

    return HttpResponse(json_dumps, content_type='application/json')


@require_POST
def get_user_statistic_data(request):
    auth_check(request)
    user_id = request.POST.get('user_id', '')
    if not user_id:
       raise InvalidParameterError()

    json_dumps = backendApiHandler.get_user_statistic_data(user_id)

    return HttpResponse(json_dumps, content_type='application/json')


@require_POST
def batch_update_user_weight(request):
    auth_check(request)
    user_ids = json.loads(request.POST.get('user_ids', '[]'))
    weight = request.POST.get('weight', '')
    if len(user_ids) == 0 or not weight:
        raise InvalidParameterError()

    json_dumps = backendApiHandler.batch_update_user_weight(user_ids, weight)

    return HttpResponse(json_dumps, content_type='application/json')


@require_POST
def batch_update_image_blur_weight(request):
    auth_check(request)
    item_ids = json.loads(request.POST.get('item_ids', '[]'))
    weight = request.POST.get('weight', '')
    if len(item_ids) == 0 or not weight:
        raise InvalidParameterError()

    json_dumps = backendApiHandler.batch_update_image_blur_weight(item_ids, weight)

    return HttpResponse(json_dumps, content_type='application/json')


@require_POST
def batch_update_category_weight(request):
    auth_check(request)
    cat_id = request.POST.get('cat_id', '')
    weight = request.POST.get('weight', '')
    if not cat_id or not weight:
        raise InvalidParameterError()

    json_dumps = backendApiHandler.batch_update_category_weight(cat_id, weight)

    return HttpResponse(json_dumps, content_type='application/json')

@require_POST
def clear_portrait(request):
    auth_result = authentication.is_authenticated(request)
    if isinstance(auth_result, HttpResponse) or not auth_result:
        raise UnauthenticatedError()

    user_id = request.POST.get('user_id', '')
    if user_id == '':
        raise BadRequestError()

    user_service.clear_portrait(user_id)

    return HttpResponse()


@require_POST
def get_offered_item_count_by_user_id(request):
    auth_check(request)

    user_id = request.POST.get('user_id', '')
    if not user_id:
        raise InvalidParameterError()

    inbox_key = 'user:%s:inbox:buying' % user_id

    msg_count = r.zcard(inbox_key)
    return HttpResponse(json.dumps({'count': msg_count}), content_type='application/json')


@require_POST
def get_offered_items_by_user_id(request):
    auth_check(request)

    user_id = request.POST.get('user_id', '')
    if not user_id:
        raise InvalidParameterError()

    inbox_key = 'user:%s:inbox:buying' % user_id

    msg_ids = r.zrange(inbox_key, 0, -1)
    items = []
    for msg_id in msg_ids:
        info = r.hmget('message:%s' % msg_id, 'item_id', 'timestamp')
        if not info[0] or not info[1]:
            continue
        items.append({'item_id': info[0], 'last_timestamp': info[1]})

    return HttpResponse(json.dumps(items), content_type='application/json')


@require_POST
def get_offer_count_by_user_id(request):
    auth_check(request)

    user_id = request.POST.get('user_id', '')
    if not user_id:
        raise InvalidParameterError()

    offer_count = 0
    for msg_type in ['buying', 'selling']:
        inbox_key = 'user:%s:inbox:%s' % (user_id, msg_type)
        msg_ids = r.zrange(inbox_key, 0, -1)
        for msg_id in msg_ids:
            offerline_id = r.hget('message:%s' % msg_id, 'thread_id')
            if not offerline_id:
                continue
            offer_count += r.zcard('offerline:%s:entries' % offerline_id)
    return HttpResponse(json.dumps({'count': offer_count}), content_type='application/json')


@require_POST
def get_offers_by_user_id(request):
    auth_check(request)

    user_id = request.POST.get('user_id', '')
    if not user_id:
        raise InvalidParameterError()

    items = []
    for msg_type in ['buying', 'selling']:
        inbox_key = 'user:%s:inbox:%s' % (user_id, msg_type)
        msg_ids = r.zrange(inbox_key, 0, -1)
        for msg_id in msg_ids:
            msg = r.hgetall('message:%s' % msg_id)
            if not msg:
                continue
            owner = r.get('item:%s:owner' % msg['item_id'])
            offer_ids = r.zrange('offerline:%s:entries' % msg['thread_id'], 0, -1)
            for offer_id in offer_ids:
                offer = r.hgetall('offer:%s' % offer_id)
                items.append({'item_id': msg['item_id'], 'owner': owner, 'from': offer['from'], 'to': offer['to'],
                              'payload': offer['payload'] if 'payload' in offer else offer['text'],
                              'offer_type': offer['msg_type'] if 'msg_type' in offer else 0,
                              'timestamp': offer['timestamp']})
    return HttpResponse(json.dumps(items), content_type='application/json')

