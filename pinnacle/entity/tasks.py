# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import shared_task
from common.es_dao import es_dao_user, es_dao_item
from entity.service_base import SrService
from geo.services import GeoService

__author__ = 'chuyouxin'

from django.conf import settings
import json
import logging
import urllib
import urllib2
import time
import datetime
import pytz
import os
# import blur_detection

from re import compile
from raven import Client
client = Client(settings.RAVEN_CONFIG['dsn'])

from commerce.utils import get_now_timestamp, debug, get_timestamp_str
from message.tasks import send_mail, notify, welcome
from entity.models import AppsFlyer, FacebookProfile, User, Item, ItemCategory, Image
from common.utils import lat_or_lon_format, capture_exception, capture_message, logger
from common import utils_es
from geo.services import GeoService
from entity.exceptions import UserNotFoundError


logger = logging.getLogger("pinnacle")
IGNORE_URLS = [compile(expr) for expr in settings.RECORD_EXEMPT_URLS]
r = SrService().get_redis()
reader = SrService().get_geoip2()
geo_service = GeoService()
r_ex = SrService().get_redis_ex()


@shared_task
def appsflyer_job(appsflyerBody):

    try:
        appsflyerJson = appsflyerBody.replace('\'', '"').replace('\t', '').replace('\n', '')
        appsflyerData = json.loads(appsflyerJson)

        app_id = appsflyerData["app_id"]
        platform = appsflyerData["platform"]
        ip = appsflyerData["ip"]
        customer_user_id = appsflyerData["customer_user_id"]
        click_time = appsflyerData["click_time"]
        if click_time:
            click_time = datetime.datetime.strptime(click_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
        install_time = appsflyerData["install_time"]
        if install_time:
            install_time = datetime.datetime.strptime(install_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
        agency = appsflyerData["agency"]
        media_source = appsflyerData["media_source"]
        campaign = appsflyerData["campaign"]
        campaign_id = appsflyerData["campaign_id"]
        os_version = appsflyerData["os_version"]
        sdk_version = appsflyerData["sdk_version"]
        app_version = appsflyerData["app_version"]
        click_url = appsflyerData["click_url"]
        mac = appsflyerData["mac"]
        cost_per_install = appsflyerData["cost_per_install"]
        country_code = appsflyerData["country_code"]
        city = appsflyerData["city"]
        adset = appsflyerData["adset"]
        adset_id = appsflyerData["adset_id"]
        adgroup = appsflyerData["adgroup"]
        adgroup_id = appsflyerData["adgroup_id"]
        af_siteid = appsflyerData["af_siteid"]
        wifi = appsflyerData["wifi"]
        language = appsflyerData["language"]
        appsflyer_device_id = appsflyerData["appsflyer_device_id"]
        device_type = appsflyerData["device_type"]
        af_sub1 = appsflyerData["af_sub1"]
        af_sub2 = appsflyerData["af_sub2"]
        af_sub3 = appsflyerData["af_sub3"]
        af_sub4 = appsflyerData["af_sub4"]
        af_sub5 = appsflyerData["af_sub5"]
        contributer_1 = appsflyerData["contributer_1"]
        contributer_2 = appsflyerData["contributer_2"]
        contributer_3 = appsflyerData["contributer_3"]

        android_id = appsflyerData["android_id"]

        appsFlyer = AppsFlyer(app_id=app_id, platform=platform, ip=ip, customer_user_id=customer_user_id, click_time=click_time, install_time=install_time,
            agency=agency, media_source=media_source, campaign=campaign, campaign_id=campaign_id, os_version=os_version, sdk_version=sdk_version,
            app_version=app_version, click_url=click_url, mac=mac, cost_per_install=cost_per_install, country_code=country_code, city=city,
            adset=adset, adset_id=adset_id, adgroup=adgroup, adgroup_id=adgroup_id, af_siteid=af_siteid, wifi=wifi, language=language,
            appsflyer_device_id=appsflyer_device_id, device_type=device_type, af_sub1=af_sub1, af_sub2=af_sub2, af_sub3=af_sub3, af_sub4=af_sub4, af_sub5=af_sub5,
            contributer_1=contributer_1, contributer_2=contributer_2, contributer_3=contributer_3, android_id=android_id)
        appsFlyer.save()
    except Exception, e:
        debug(e)
        logging.error(appsflyerBody)
        if not settings.DEBUG:
            client.captureException(extra={'ctx': appsflyerBody})

@shared_task
def appsflyer_two_job(appsflyerBody):
    try:
        appsflyerJson = appsflyerBody.replace('\t', '').replace('\n', '').replace('\\', '\\\\')
        appsflyerData = json.loads(appsflyerJson)

        app_id = appsflyerData["app_id"] if 'app_id' in appsflyerData and appsflyerData['app_id'] is not None else ''
        platform = appsflyerData["platform"] if 'platform' in appsflyerData and appsflyerData['platform'] is not None else ''
        ip = appsflyerData["ip"] if 'ip' in appsflyerData and appsflyerData['ip'] is not None else ''
        customer_user_id = appsflyerData["customer_user_id"]
        click_time = appsflyerData["click_time"]
        if click_time:
            click_time = datetime.datetime.strptime(click_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
        install_time = appsflyerData["install_time"]
        if install_time:
            install_time = datetime.datetime.strptime(install_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
        agency = appsflyerData["agency"] if 'agency' in appsflyerData and appsflyerData['agency'] is not None else ''
        media_source = appsflyerData["media_source"] if 'media_source' in appsflyerData and appsflyerData['media_source'] is not None else ''
        campaign = appsflyerData["campaign"] if 'campaign' in appsflyerData and appsflyerData['campaign'] is not None else ''
        campaign_id = appsflyerData["fb_campaign_id"] if 'fb_campaign_id' in appsflyerData and appsflyerData['fb_campaign_id'] is not None else ''
        os_version = appsflyerData["os_version"] if 'os_version' in appsflyerData and appsflyerData['os_version'] is not None else ''
        sdk_version = appsflyerData["sdk_version"] if 'sdk_version' in appsflyerData and appsflyerData['sdk_version'] is not None else ''
        app_version = appsflyerData["app_version"] if 'app_version' in appsflyerData and appsflyerData['app_version'] is not None else ''
        click_url = appsflyerData["click_url"] if 'click_url' in appsflyerData and appsflyerData['click_url'] is not None else ''
        mac = appsflyerData["mac"] if 'mac' in appsflyerData and appsflyerData['mac'] is not None else ''
        cost_per_install = appsflyerData["cost_per_install"] if 'cost_per_install' in appsflyerData and appsflyerData['cost_per_install'] is not None else ''
        country_code = appsflyerData["country_code"] if 'country_code' in appsflyerData and appsflyerData['country_code'] is not None else ''
        city = appsflyerData["city"] if 'city' in appsflyerData and appsflyerData['city'] is not None else ''
        adset = appsflyerData["fb_adset_name"] if 'fb_adset_name' in appsflyerData and appsflyerData['fb_adset_name'] is not None else ''
        adset_id = appsflyerData["fb_adset_id"] if 'fb_adset_id' in appsflyerData and appsflyerData['fb_adset_id'] is not None else ''
        adgroup = appsflyerData["fb_adgroup_name"] if 'fb_adgroup_name' in appsflyerData and appsflyerData['fb_adgroup_name'] is not None else ''
        adgroup_id = appsflyerData["fb_adgroup_id"] if 'fb_adgroup_id' in appsflyerData and appsflyerData['fb_adgroup_id'] is not None else ''
        af_siteid = appsflyerData["af_siteid"] if 'af_siteid' in appsflyerData and appsflyerData['af_siteid'] is not None else ''
        wifi = appsflyerData["wifi"] if 'wifi' in appsflyerData and appsflyerData['wifi'] is not None else ''
        language = appsflyerData["language"] if 'language' in appsflyerData and appsflyerData['language'] is not None else ''
        appsflyer_device_id = appsflyerData["appsflyer_device_id"] if 'appsflyer_device_id' in appsflyerData and appsflyerData['appsflyer_device_id'] is not None else ''
        device_type = appsflyerData["device_type"] if 'device_type' in appsflyerData and appsflyerData['device_type'] is not None else ''
        af_sub1 = appsflyerData["af_sub1"] if 'af_sub1' in appsflyerData and appsflyerData['af_sub1'] is not None else ''
        af_sub2 = appsflyerData["af_sub2"] if 'af_sub2' in appsflyerData and appsflyerData['af_sub2'] is not None else ''
        af_sub3 = appsflyerData["af_sub3"] if 'af_sub3' in appsflyerData and appsflyerData['af_sub3'] is not None else ''
        af_sub4 = appsflyerData["af_sub4"] if 'af_sub4' in appsflyerData and appsflyerData['af_sub4'] is not None else ''
        af_sub5 = appsflyerData["af_sub5"] if 'af_sub5' in appsflyerData and appsflyerData['af_sub5'] is not None else ''
        contributer_1 = appsflyerData["contributer_1"] if 'contributer_1' in appsflyerData and appsflyerData['contributer_1'] is not None else ''
        contributer_2 = appsflyerData["contributer_2"] if 'contributer_2' in appsflyerData and appsflyerData['contributer_2'] is not None else ''
        contributer_3 = appsflyerData["contributer_3"] if 'contributer_3' in appsflyerData and appsflyerData['contributer_3'] is not None else ''

        event_time = appsflyerData["event_time"]
        if event_time:
            event_time = datetime.datetime.strptime(event_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
        event_name = appsflyerData["event_name"] if 'event_name' in appsflyerData and appsflyerData['event_name'] is not None else ''
        event_value = appsflyerData["event_value"] if 'event_value' in appsflyerData and appsflyerData['event_value'] is not None else ''
        currency = appsflyerData["currency"] if 'currency' in appsflyerData and appsflyerData['currency'] is not None else ''
        event_type = appsflyerData["event_type"] if 'event_type' in appsflyerData and appsflyerData['event_type'] is not None else ''
        attribution_type = appsflyerData["attribution_type"]if 'attribution_type' in appsflyerData and appsflyerData['attribution_type'] is not None else ''
        fb_campaign_name = appsflyerData["fb_campaign_name"]if 'fb_campaign_name' in appsflyerData and appsflyerData['fb_campaign_name'] is not None else ''
        device_name = appsflyerData["device_name"] if 'device_name' in appsflyerData and appsflyerData['device_name'] is not None else ''
        device_brand = appsflyerData["device_brand"] if 'device_brand' in appsflyerData and appsflyerData['device_brand'] is not None else ''
        device_model = appsflyerData["device_model"] if 'device_model' in appsflyerData and appsflyerData['device_model'] is not None else ''
        idfa = appsflyerData["idfa"] if 'idfa' in appsflyerData and appsflyerData['idfa'] is not None else ''

        android_id = appsflyerData["android_id"] if 'android_id' in appsflyerData and appsflyerData['android_id'] is not None else ''
        advertising_id = appsflyerData["advertising_id"] if 'advertising_id' in appsflyerData and appsflyerData['advertising_id'] is not None else ''
        imei = appsflyerData["imei"] if 'imei' in appsflyerData and appsflyerData['imei'] is not None else ''

        appsFlyer = AppsFlyer(app_id=app_id, platform=platform, ip=ip, customer_user_id=customer_user_id, click_time=click_time, install_time=install_time,
            agency=agency, media_source=media_source, campaign=campaign, campaign_id=campaign_id, os_version=os_version, sdk_version=sdk_version,
            app_version=app_version, click_url=click_url, mac=mac, cost_per_install=cost_per_install, country_code=country_code, city=city,
            adset=adset, adset_id=adset_id, adgroup=adgroup, adgroup_id=adgroup_id, af_siteid=af_siteid, wifi=wifi, language=language,
            appsflyer_device_id=appsflyer_device_id, device_type=device_type, af_sub1=af_sub1, af_sub2=af_sub2, af_sub3=af_sub3, af_sub4=af_sub4, af_sub5=af_sub5,
            contributer_1=contributer_1, contributer_2=contributer_2, contributer_3=contributer_3, android_id=android_id,
            event_time=event_time, event_name=event_name, event_value=event_value, currency=currency, event_type=event_type,
            attribution_type=attribution_type, fb_campaign_name=fb_campaign_name, device_name=device_name,device_brand=device_brand,
            device_model=device_model, idfa=idfa, advertising_id=advertising_id, imei=imei)
        appsFlyer.save()
    except Exception, e:
        debug(e)
        logging.error(appsflyerBody)
        if not settings.DEBUG:
            client.captureException(extra={'ctx': appsflyerBody})

@shared_task
def facebook_me_job(user_id, op_type, nickname, fb_token, fb_token_expires):

    try:
        if len(fb_token) > 0:
            fields = settings.FACEBOOK['ME_FIELDS']
            base_url = settings.FACEBOOK['API_URL'] + ("me?fields=%s&access_token=%s" % (fields, fb_token))
            result = urllib2.urlopen(base_url, timeout=6).read() # if can't connect facebook in 6 seconds, the error message will display
            #result = '{"id":"1234567890","first_name":"chuyouxin","email":"chuyouxin2005@gmail.com"}'
            s = json.loads(result)

            fb_user_id = s['id']
            first_name = s['first_name'] if 'first_name' in s else ''
            last_name = s['last_name'] if 'last_name' in s else ''
            name = s['name'] if 'name' in s else ''
            email = s['email'] if 'email' in s else ''
            gender = s['gender'] if 'gender' in s else ''
            verified = s['verified'] if 'verified' in s else 0
            locale = s['locale'] if 'locale' in s else ''
            timezone = s['timezone'] if 'timezone' in s else None
            link = s['link'] if 'link' in s else ''
            updated_time = s['updated_time'] if 'updated_time' in s else None
            age_range_min = None
            age_range_max = None
            if 'age_range' in s:
                age_range = s['age_range']
                if 'min' in age_range:
                    age_range_min = age_range['min']
                if 'max' in age_range:
                    age_range_max = age_range['max']

            user_currency = ''
            usd_exchange_inverse = None
            usd_exchange = None
            currency_offset = None
            if 'currency' in s:
                currency = s['currency']

                user_currency = currency['user_currency'] if 'user_currency' in currency else ''
                usd_exchange_inverse = currency['usd_exchange_inverse'] if 'usd_exchange_inverse' in currency else None
                usd_exchange = currency['usd_exchange'] if 'usd_exchange' in currency else None
                currency_offset = currency['currency_offset'] if 'currency_offset' in currency else None

            user = User.objects.get(pk=user_id)
            if email != '':
                r.hset('user:%s' % user_id, 'email', email)
                user.email = email
                user.email_verified = True

            if gender != '':
                if gender.lower() == 'female':
                    i_gender = -1
                elif gender.lower() == 'male':
                    i_gender = 1
                else:
                    i_gender = 0
                r.hset('user:%s' % user_id, 'gender', i_gender)
                user.gender = i_gender

            portrait = 'http://res.cloudinary.com/fivemiles/image/facebook/%s.jpg' % fb_user_id
            r.hmset('user:%s' % user_id, {'portrait': portrait, 'email_verified': True})
            user.portrait = portrait
            user.save()

            facebook_profile = FacebookProfile(
                id=fb_user_id, first_name=first_name, last_name=last_name, name=name, email=email, gender=gender,
                verified=verified, locale=locale, timezone=timezone, link=link, updated_time=updated_time,
                age_range_min=age_range_min, age_range_max=age_range_max, user_currency=user_currency,
                usd_exchange=usd_exchange, usd_exchange_inverse=usd_exchange_inverse, currency_offset=currency_offset,
                fb_token=fb_token, fb_token_expires=fb_token_expires)
            facebook_profile.save()

            if not settings.DEBUG:
                if op_type == 'SIGNUP' and email != '' and email is not None:
                    # 发送欢迎邮件
                    welcome.delay(str(user_id), email, {'nickname': nickname.encode('utf-8')}, False, True)

                    # 48小时之后提醒用户发商品，系统消息与push同时发送
                    notify.delay(str(user_id), 'TRI_REMIND_TO_LIST', {'nickname': nickname.encode('utf-8')})
                else:
                    print "SEND_MAIL:%s,email is empty" % op_type
        else:
            if not settings.DEBUG:
                client.captureMessage('Having not token with facebook(%s)' % op_type)
    except Exception, e:
        logger.error(e)
        if not settings.DEBUG:
            client.captureException()


@shared_task
def sync_redis_to_db(model, action, object_id):
    if not settings.SYNCHRONIZE_SWITCH:
        debug("the switch of synchronizing data from redis to database don't open")
        return

    params = 'model: %s, action: %s, object_id: %s' % (model, action, object_id)

    debug('Synchronizing: %s' % params)

    try:
        if model == 'user':
            if action == 'C':
                d = r.hgetall('user:%s' % object_id)

                d["fuzzy_user_id"] = d['id']
                d['id'] = object_id
                d['created_at'] = get_timestamp_str(long(d['created_at']))
                d['state'] = 0
                if 'last_login_at' in d:
                    d['last_login_at'] = get_timestamp_str(long(d['last_login_at']))

                pwd = r.get('user:%s:password' % object_id)
                d["password"] = pwd if pwd is not None else ''

                if 'last_heartbeat_at' in d:
                    d['last_heartbeat_at'] = get_timestamp_str(long(d['last_heartbeat_at']))

                user = User(**d)
                user.save()
            elif action == 'D':
                user = User.objects.get(pk=object_id)
                if user:
                    user.state = -1
                    user.save()
        elif model == 'item':
            if action == 'C':
                d = r.hgetall('item:%s' % object_id)
                if len(d) > 0:
                    images_list = []
                    if "images" in d and d["images"] != '':
                        images_list = json.loads(d["images"])

                    cat_id = d["cat_id"]
                    d['fuzzy_item_id'] = d['id']
                    d['id'] = object_id
                    d['created_at'] = get_timestamp_str(long(d['created_at']))
                    d['state'] = 0
                    d['lat'] = json.loads(d["location"])['lat']
                    d['lon'] = json.loads(d["location"])['lon']
                    if 'num_in_stock' not in d:
                        d['num_in_stock'] = 0

                    del d["cat_id"]
                    if "images" in d:
                        del d["images"]
                    del d["location"]
                    item = Item(**d)
                    item.save()
                    #sync itemcategory
                    if 'cat_id' not in d:
                        item_count = ItemCategory.objects.filter(cat_id=cat_id, item_id=object_id).count()
                        if item_count == 0:
                            ic = {}
                            ic['item_id'] = object_id
                            ic['cat_id'] = cat_id
                            itemCategory = ItemCategory(**ic)
                            itemCategory.save()
                    else:
                        debug("SyncItem:There are no item's category")

                    #save images
                    img_list = Image.objects.filter(item_id=object_id)

                    image_len = len(images_list)
                    images = []
                    for i in range(0, image_len):
                        image_path = None
                        if 'imageLink' in images_list[i]:
                            image_path = images_list[i]["imageLink"]
                        image_width = None
                        if 'width' in images_list[i]:
                            image_width = images_list[i]["width"]
                        image_height = None
                        if 'height' in images_list[i]:
                            image_height = images_list[i]["height"]

                        is_add = True
                        for img in img_list:
                            if img.image_path == image_path and img.image_width == image_width and img.image_height == image_height:
                                is_add = False
                                break

                        if is_add:
                            images.append(Image(item_id=object_id, image_path=image_path, image_width=image_width, image_height=image_height))

                    if len(images) > 0:
                        Image.objects.bulk_create(images)
                    else:
                        debug("SyncItem:There are no images")

            elif action == 'D':
                item = Item.objects.get(pk=object_id)
                if item:
                    item.state = -1
                    item.save()
        else:
            raise ValueError("Unsupported model: %s" % model)

        debug('Synchronized: %s' % params)
    except BaseException, e:
        logger.error(e)
        debug(e)
        if not settings.DEBUG:
            client.captureException(extra={'ctx': params})


@shared_task
def lookup_geoip(user_id, ip_address):
    if not ip_address:
        return

    country = ''
    city = ''
    try:
        geo_ip_address = geo_service.lookup_geoip(ip_address)
        country = geo_ip_address['country_name']
        city = geo_ip_address['city_name']

        user = User.objects.get(pk=user_id)
        if user:
            user.country = country
            user.city = city
            user.save()
    except Exception, e:
        logger.exception(e)

    return {'country': country, 'city': city}


@shared_task(queue='celery.position')
def lookup_user_position(user_id, lat, lon, ip_address):
    # lat和lon保留六位小数的精度
    lat = lat_or_lon_format(lat)
    lon = lat_or_lon_format(lon)
    infos = geocode(lat, lon, ip_address)
    country = infos['country']
    city = infos['city']
    lat = infos['lat']
    lon = infos['lon']
    region = infos['region']
    if country != '' or city != '':
        user = User.objects.get(pk=user_id)
        if user:
            user.country = country
            user.city = city
            user.latitude = lat
            user.longitude = lon
            user.region = region
            user.save()

        r.hmset('user:%s' % user_id, {'country': country, 'region': region, 'city': city, 'lat': lat, 'lon': lon})
        es_dao_user().update(user_id, {"doc": {"location": {"lat": lat, "lon": lon}}})


@shared_task(name='celery.position')
def lookup_item_position(user_id, item_id, lat, lon, ip_address):
    lat = lat_or_lon_format(lat)
    lon = lat_or_lon_format(lon)
    #logger.debug('lookup_item_position: lat: %s, lon: %s' % (lat, lon))

    # 获取位置信息逻辑:
    # 从google map api获取位置信息(国家、城市、经度、纬度),如果获取不到则从geoip获取,获取不到则使用用户的位置信息覆盖
    # 如果经纬度信息不为空则始终使用app传过来的经纬度信息(app的经纬度信息比较正确)
    infos = geocode(lat, lon, ip_address)
    country = infos['country']
    city = infos['city']
    lat = infos['lat']
    lon = infos['lon']
    region = infos['region']

    try:
        if country == '' and city == '':
            user = User.objects.get(pk=user_id)
            if user:
                country = user.country
                city = user.city
                lat = user.latitude if lat == 0 else lat
                lon = user.longitude if lon == 0 else lon

        db_parameters = {}
        es_parameters = {}
        redis_parameters = {}
        if country != '':
            db_parameters['country'] = country
            es_parameters['country'] = country
            redis_parameters['country'] = country
        if city != '':
            db_parameters['city'] = city
            es_parameters['city'] = city
            redis_parameters['city'] = city
        if lat != 0 and lon != 0:
            es_parameters['location'] = {'lat': lat, 'lon': lon}
            redis_parameters['location'] = json.dumps({'lat': lat, 'lon': lon})
            db_parameters['lat'] = lat
            db_parameters['lon'] = lon
        if region != '':
            db_parameters['region'] = region
            if not region.isdigit():
                redis_parameters['region'] = region
                es_parameters['region'] = region

        if len(db_parameters) > 0:
            # 保存redis
            r.hmset('item:%s' % item_id, redis_parameters)
            # 保存数据
            Item.objects.filter(id=item_id).update(**db_parameters)
            # es增加国家和城市
            utils_es.update_fields_in_searchengine(item_id, **es_parameters)

    except Exception, e:
        logger.exception(e)


def geoip(lat, lon, ip_address):
    if not ip_address:
        return {'country': '', 'region': '', 'city': '', 'lat': lat_or_lon_format(lat), 'lon': lat_or_lon_format(lon)}

    country = ''
    city = ''
    region = ''
    try:
        geo_ip_address = geo_service.lookup_geoip(ip_address)
        country = geo_ip_address['country_name'] if geo_ip_address['country_name'] is not None else ''
        city = geo_ip_address['city_name'] if geo_ip_address['city_name'] is not None else ''
        lat = lat_or_lon_format(float(geo_ip_address['location_latitude'])) if lat == 0 else lat
        lon = lat_or_lon_format(float(geo_ip_address['location_longitude'])) if lon == 0 else lon
        region = geo_ip_address['subdivisions_most_specific_iso_code']
    except Exception, e:
        logger.exception(e)

    return {'country': country, 'region': region, 'city': city, 'lat': lat, 'lon': lon}


def geocode(lat, lon, ip_address):
    country = ''
    city = ''
    region = ''
    # logger.debug('geocode: lat: %s, lon: %s' % (lat, lon))
    # 首先按照经纬度来获取位置信息,如果不存在的话,再按照ip地址获取
    if lat == 0 and lon == 0:
        logger.debug('latitude & longitude not provided, call geoip')
        return geoip(lat, lon, ip_address)
    else:
        try:
            # 从缓存中获取位置信息
            fmt_lat = float('%.3f' % lat)
            fmt_lon = float('%.3f' % lon)
            redis_info = r_ex.hgetall('geo:%s:%s:position' % (fmt_lat, fmt_lon))
            if redis_info:
                redis_info['lat'] = lat
                redis_info['lon'] = lon
                return redis_info
            else:
                # logger.debug('call Google Geocode API: lat: %s, lon: %s' % (lat, lon))
                geo_args = {'latlng': '%s,%s' % (fmt_lat, fmt_lon), 'sensor': 'false'}
                url = '%s?%s' % (settings.GEOCODE_BASE_URL, urllib.urlencode(geo_args))
                resp = json.load(urllib2.urlopen(url, timeout=6))
                results = resp['results']
                status = resp['status']
                # logger.debug(results)
                if status != 'OK':
                    capture_message('WARNING: Google Geocoding API error: %s ' % status)

                if results and len(results) > 0:
                    detailed_address = results[0]['address_components']
                    for ac in detailed_address:
                        if 'country' in ac['types'] and country == '':
                            country = ac['long_name']
                        if 'administrative_area_level_1' in ac['types'] and region == '':
                            region = ac['short_name']
                        if 'locality' in ac['types'] and city == '':
                            city = ac['long_name']

                        if country and region and city:
                            break

                if country == '':
                    logger.debug('country not found, call geoip')
                    return geoip(lat, lon, ip_address)
                else:
                    # 把经纬度对应的国家和城市保存到缓存中
                    key = 'geo:%s:%s:position' % (fmt_lat, fmt_lon)
                    r_ex.hmset(key, {'country': country, 'region': region, 'city': city})
                    r_ex.expire(key, 7 * 24 * 3600)
        except Exception, e:
            logger.exception(e)
            return geoip(lat, lon, ip_address)

    return {'country': country, 'city': city, 'region': region, 'lat': lat, 'lon': lon}


@shared_task(bind=True, is_eager=False)
def record_analytic_data(user_id, api_path, query_str, referer, lang_code, ip_address, user_agent, utm_date, ext_params):
    if any(m.match(api_path) for m in IGNORE_URLS):
        return

    country = ''
    city = ''
    try:
        response = reader.city(ip_address)
        country = response.country.name
        city = response.city.name
    except Exception, e:
        logger.error('[record_analytic_data] - error_message:%s,params:%s,%s,%s,%s' % (e.message, user_id, api_path, user_agent, ip_address))

    new_query_str = ''
    json_query_str = ''
    try:
        for dict in query_str.dicts:
            if len(dict) > 0:
                for key in dict:
                    fields = settings.ANALYTIC_QUERY_FILTER_FIELDS
                    if key not in fields:
                        new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
                        json_query_str = '%s,' % json_query_str if json_query_str != '' else ''
                        value = dict[key]
                        if key == 'item_id':
                            item_id = r.get('fuzzy_item_id:%s:item_id' % value)
                            json_query_str = '%s"%s":"%s"' % (json_query_str, key, item_id if item_id else value)
                            new_query_str = '%s%s:%s' % (new_query_str, key, item_id if item_id else value)
                        elif key == 'user_id' or key == 'to_user' or key == 'seller_id':
                            f_user_id = r.get('fuzzy_id:%s:user_id' % value)
                            json_query_str = '%s"%s":"%s"' % (json_query_str, key, f_user_id if f_user_id else value)
                            new_query_str = '%s%s:%s' % (new_query_str, key, f_user_id if f_user_id else value)
                        else:
                            json_query_str = '%s"%s":"%s"' % (json_query_str, key, value.replace('|', '&#124;').replace('\n', ' '))
                            new_query_str = '%s%s:%s' % (new_query_str, key, value.replace('|', '&#124;').replace('\n', ' '))

        new_query_str = new_query_str.encode('utf-8')
        if api_path == 'api/v2/accept_offer/':
            # 获取item_id存入日志中
            json_data = json.loads('{%s}' % json_query_str)
            ref_offer_id = json_data['ref_offer_id']

            # 获取offer的价格、本地价格
            offers = r.hmget('offer:%s' % ref_offer_id, 'price', 'local_price')
            price = offers[0]
            local_price = offers[1]

            offerline_id = r.get('offer:%s:offerline_id' % ref_offer_id)
            item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')

            new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
            new_query_str = '%sitem_id:%s,price:%s,local_price:%s' % (new_query_str, item_id, price, local_price)

        elif api_path == 'api/v2/login_facebook/':
            # 获取item_id存入日志中
            json_data = json.loads('{%s}' % json_query_str)
            fb_user_id = json_data['fb_userid']
            fbl_user_id = r.get('login:facebook:%s:user_id' % fb_user_id)
            new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
            # type为0时表示新注册用户,1表示facebook用户登录,30秒之内,系统认为是新用户注册
            op_type = 0
            if fbl_user_id:
                current_time = get_now_timestamp()
                created_at = r.hget('user:%s' % fbl_user_id, 'created_at')
                op_type = 0 if current_time - long(created_at) < 30 else 1

            new_query_str = '%s%s:%s,%s:%s' % (new_query_str, 'user_id', fbl_user_id, 'type', op_type)
            user_id = fbl_user_id

        elif api_path == 'api/v2/signup_email/':
            json_data = json.loads('{%s}' % json_query_str)
            email = json_data['email']
            s_user_id = r.get('login:email:%s:user_id' % email)
            new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
            new_query_str = '%s%s:%s' % (new_query_str, 'user_id', s_user_id)
            user_id = s_user_id

        elif api_path == 'api/v2/confirm_delivery/' or api_path == 'api/v2/offerline/':
            # 获取item_id存入日志中
            json_data = json.loads('{%s}' % json_query_str)
            offerline_id = json_data['offerline_id']

            item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')

            new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
            new_query_str = '%sitem_id:%s' % (new_query_str, item_id)

    except Exception, e1:
        logger.error(e1)

    try:
        referer = referer if referer else ''
        lang_code = lang_code if lang_code else ''
        ip_address = ip_address if ip_address else ''
        v_hour = time.strptime(utm_date, '%Y-%m-%d %H:%M:%SZ').tm_hour
        api_path = api_path.encode('utf-8')
        user_id = str(user_id).encode('utf-8') if user_id is not None else ''
        country = country.encode('utf-8') if country is not None else ''
        city = city.encode('utf-8') if city is not None else ''

        log_datas = "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s" % (utm_date, user_id, api_path, new_query_str, referer, user_agent, lang_code, ip_address, v_hour, country, city)
        logging.getLogger("access").info(log_datas)
    except Exception, e:
        logger.error(e)


@shared_task(bind=True, is_eager=False)
def record_item_exposure(user_id, api_path, ip_address, items, offset, user_agent, utm_date, ext_params):
    # 具体的实现在日志服务器上实现,在本地不处理
    logger.warn((user_id, api_path, ip_address, items, offset, user_agent, utm_date, ext_params))


@shared_task
def blur_detect(item_id, image_url):

    import cv2
    if image_url is None or len(image_url) == 0:
        return

    logger.debug('blur detect: item_id: %s, image_url: %s' % (item_id, image_url))
    resp = urllib2.urlopen(image_url)
    filename = image_url[image_url.rfind('/') + 1:]
    image_path = '/tmp/%s' % filename
    try:
        f = open(image_path, 'w')
        f.write(resp.read())
        img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        if img is None:
            return

        row_count, col_count = img.shape
        resized_img = cv2.resize(img, (500, 500 * row_count / col_count))
        blur_output = blur_detection.blur_detect(resized_img, AREA_SIZE=100, BLUR_WIDTH_THRESHOLD=13)
        utils_es.update_field_in_searchengine(item_id, 'image_blur', blur_output[0] if blur_output[0] is not None else 13)
    finally:
        f.close()
        os.remove(image_path)
        utils_es.update_field_in_searchengine(item_id, 'image_blur', 13)


@shared_task(queue='celery.schedule')
def remove_new_tag(item_id):
    """
    新商品发布一段时间之后移除new标签
    :param item_id:
    :return:
    """
    # connection.close()
    r.hdel('item:%s' % item_id, 'is_new')
    utils_es.update_field_in_searchengine(item_id, 'is_new', 0)


@shared_task(queue='celery.misc')
def user_async_checkin(user_id, nts, db_ts):
    items = r.zrange('user:%s:user_selling_list' % user_id, 0, -1)
    if items:
        for item_id in items:
            try:
                if es_dao_item().exists(item_id):
                    es_dao_item().update(item_id, {"doc": {"last_active_at": nts}})
            except Exception, e:
                logger.warning('user_async_checkin es_dao_item().update error,msg:%s' % e.message)

    user = User.objects.get(pk=user_id)
    if not user:
        raise UserNotFoundError(user_id)

    user.last_active_at = db_ts
    user.save()

