__author__ = 'chuyouxin'

import sys, os, time

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift import Thrift

thrift_path = os.path.join(os.path.dirname(__file__), 'gen-py')
sys.path.append(thrift_path)
from BackendApi import IThriftBackendApiService
from BackendApi.ttypes import USER_ACTIVE
from BackendApi.ttypes import PRODUCT_STATE

LONGTIME_FORMAT="%Y-%m-%d %H:%M:%S"

def getThriftClient():
    transport = TSocket.TSocket('localhost', 11000)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    client = IThriftBackendApiService.Client(protocol)

    transport.open()
    return client

def updateProductById(productId, state, internal_status):
    try:
        productMap = {}
        productMap.__setitem__("state", str(state))
        productMap.__setitem__("internal_status", str(internal_status))
        productMap.__setitem__("update_time", time.strftime(LONGTIME_FORMAT))
        result = getThriftClient().updateProductById(productId, productMap)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def updateCategoryById(categoryId):
    try:
        categoryMap = {}
        categoryMap.__setitem__("title", "11")
        categoryMap.__setitem__("description", "11")
        categoryMap.__setitem__("slug", "11")
        categoryMap.__setitem__("status", "11")
        categoryMap.__setitem__("order_num", "110")
        categoryMap.__setitem__("parent_id", "1010")
        categoryMap.__setitem__("publish_date", "2014-10-11 11:00:00")
        result = getThriftClient().updateCategoryById(categoryId, categoryMap)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def addProductsForCategory(categoryId, productIds):
    try:
        products = []
        productIdsStr = productIds.split(',')
        pLen = len(productIdsStr)
        for i in range(0, pLen):
            products.append(int(productIdsStr[i]))

        result = getThriftClient().addProductsForCategory(categoryId, products)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def addCategoriesForProduct(productId, categoryIds):
    try:
        categories = []
        categoryIdsStr = categoryIds.split(',')
        catLen = len(categoryIdsStr)
        for i in range(0, catLen):
            categories.append(int(categoryIdsStr[i]))

        result = getThriftClient().addCategoriesForProduct(productId, categories)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def updateProductCategory(productId, categoryId):
    try:
        result = getThriftClient().updateProductCategory(productId, categoryId)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def updateProductCategory(productId, categoryId):
    try:
        result = getThriftClient().updateProductCategory(productId, categoryId)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def get_comments_by_item_id(item_id):
    try:
        result = getThriftClient().get_comments_by_item_id(item_id)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def post_comment_by_item_id(user_id, item_id, text, reply_to):
    try:
        result = getThriftClient().post_comment_by_item_id(user_id, item_id, text, reply_to)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def get_comment_count_by_item_ids(item_ids):
    try:
        result = getThriftClient().get_comment_count_by_item_ids(item_ids)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def get_offers_by_item_id(item_id):
    try:
        result = getThriftClient().get_offers_by_item_id(item_id)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def get_offer_count_by_item_ids(item_ids):
    try:
        result = getThriftClient().get_offer_count_by_item_ids(item_ids)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def batch_update_item_weight(item_ids, weight):
    try:
        result = getThriftClient().batch_update_item_weight(item_ids, weight)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)

def batch_update_item_risk(item_ids, risk):
    try:
        result = getThriftClient().batch_update_item_risk(item_ids, risk)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)


def get_user_statistic_data(user_id):
    try:
        result = getThriftClient().get_user_statistic_data(user_id)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)


def batch_update_user_weight(user_ids, weight):
    try:
        result = getThriftClient().batch_update_user_weight(user_ids, weight)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)


def batch_update_image_blur_weight(item_ids, weight):
    try:
        result = getThriftClient().batch_update_image_blur_weight(item_ids, weight)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)


def batch_update_category_weight(cat_id, weight):
    try:
        result = getThriftClient().batch_update_category_weight(cat_id, weight)
        print result

    except Thrift.TException, tx:
        print '%s' % (tx.message)


if __name__ == '__main__':

    op_type = int(sys.argv[1])

    if op_type == 2:
        product_id = sys.argv[2]
        state = sys.argv[3]
        internal_status = sys.argv[4]
        updateProductById(int(product_id), state, internal_status)
    elif op_type == 3:
        category_id = sys.argv[2]
        updateCategoryById(int(category_id))
    elif op_type == 4:
        product_id = sys.argv[2]
        category_id = sys.argv[3]
        updateProductCategory(int(product_id), int(category_id))
    elif op_type == 6:
        item_id = sys.argv[2]
        get_comments_by_item_id(int(item_id))
    elif op_type == 7:
        user_id = int(sys.argv[2]) if len(sys.argv) >= 3 else 0
        item_id = int(sys.argv[3]) if len(sys.argv) >= 4 else 0
        text = sys.argv[4] if len(sys.argv) >= 5 else ''
        reply_to = int(sys.argv[5]) if len(sys.argv) >= 6 else None

        print "%s,%s,%s,%s" % (user_id, item_id, text, reply_to)
        post_comment_by_item_id(user_id, item_id, text, reply_to)
    elif op_type == 8:
        item_ids = sys.argv[2].split(',')
        get_comment_count_by_item_ids(item_ids)
    elif op_type == 9:
        item_id = sys.argv[2]
        get_offers_by_item_id(int(item_id))
    elif op_type == 10:
        item_ids = sys.argv[2].split(',')
        get_offer_count_by_item_ids(item_ids)
    elif op_type == 11:
        item_ids = sys.argv[2].split(',')
        weight = int(sys.argv[3])
        batch_update_item_weight(item_ids, weight)
    elif op_type == 12:
        item_ids = sys.argv[2].split(',')
        risk = sys.argv[3]
        batch_update_item_risk(item_ids, risk)
    elif op_type == 13:
        user_id = sys.argv[2]
        get_user_statistic_data(int(user_id))
    elif op_type == 14:
        user_list = []
        user_ids = sys.argv[2].split(',')
        for user_id in user_ids:
            user_list.append(int(user_id))
        weight = int(sys.argv[3])
        batch_update_user_weight(user_list, weight)
    elif op_type == 15:
        item_list = []
        item_ids = sys.argv[2].split(',')
        for item_id in item_ids:
            item_list.append(int(item_id))
        weight = int(sys.argv[3])
        batch_update_image_blur_weight(item_list, weight)
    elif op_type == 16:
        cat_id = int(sys.argv[2])
        weight = int(sys.argv[3])
        batch_update_category_weight(cat_id, weight)