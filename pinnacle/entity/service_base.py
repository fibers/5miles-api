# -*- coding: utf-8 -*-

import logging
from elasticsearch import Elasticsearch
import redis
from pinnacle import settings

__author__ = 'lvyi'

logger = logging.getLogger('pinnacle')


def singleton(cls, *args, **kw):
    instances = {}

    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]
    return _singleton


class BaseService(object):
    # r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
    pool = redis.ConnectionPool(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB, max_connections=settings.REDIS_MAX_CONNS)
    r = redis.Redis(connection_pool=pool)
    logger.info("BaseService redis init db=%s" % settings.V2_REDIS_DB)

    def __init__(self):
        pass


@singleton
class SrService(BaseService):
    #Server Resouce Service

    #es = Elasticsearch(settings.ES_HOSTS)
    #logger.info("SrService es init db=%s" % settings.ES_HOSTS)

    # r_ex 创建一个新的 RedisDB 用户存放临时的数据(设置过期时间)，这些数据可以在需要的时候清空，系统会自动重建，不会影响中断服务
    # lvyi 2015-2-5
    r_ex = redis.Redis(host=settings.V2_REDIS_HOST, port=settings.V2_REDIS_PORT, db=settings.V2_REDIS_DB_EX)
    logger.info("SrService redis ex=%s db=%s" % (settings.V2_REDIS_HOST,settings.V2_REDIS_DB_EX))

    import geoip2.database
    reader = geoip2.database.Reader(settings.GEOIP['DB_URL'])
    #print 'load geoip2'
    logger.info("SrService geoip2 init DB_URL=%s" % settings.GEOIP['DB_URL'])

    def __init__(self):
        pass

    def get_redis(self):
        return self.r

    def get_redis_ex(self):
        return self.r_ex

    #def get_es(self):
    #    return self.es

    def get_geoip2(self):
        return self.reader
