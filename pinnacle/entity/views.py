# -*- coding: utf-8 -*-

import sys
import os
import bitly_api
from requests.exceptions import RequestException

from django.http import HttpResponse, HttpResponseNotAllowed
from django.views.decorators.http import require_GET, require_POST
from django.utils.translation import ugettext as _
from django.contrib.auth.hashers import make_password, check_password
from babel import numbers
from cloudinary.utils import sign_request
from common import utils_web
from common.es_dao import es_dao_user
from common.utils_web import check_blacklist
from entity import search
from entity import search_client

from common.utils import _get_latlng_by_geoip
from entity import search_base
from entity.exceptions import UserBlockedError, UserNotFoundError, InvalidParameterError, PinnacleApiError, \
    ERR_CODE_INVALID_PASSWORD, ERR_CODE_EMAIL_EXISTS, NotFoundError, InvalidCredentialsError, UnauthorizedError, \
    ERR_CODE_RESET_PASSWORD_FAILED, ItemNotFoundError, InvalidStateError, InvalidOperationError, \
    ERR_CODE_PHONE_NUMBER_BOUND, ERR_CODE_INVALID_PASSCODE, ERR_CODE_EMAIL_NOT_EXISTS

from entity.models import User, Image, ItemCategory, Brand, FeaturedItems, Item
from entity.recommend import task_item_ctr
from entity.utils import *
from message.tasks import send_mail, notify, prompt_to_upload_portrait, prompt_price_change, \
    prompt_followers_has_new_items, send_sms, remind_to_renew, send_seller_guide, update_user_current_device
from entity.tasks import appsflyer_job, lookup_item_position, \
    appsflyer_two_job, remove_new_tag, record_item_exposure
from common.biz import *
from common.utils import *
from common.utils_es import *
from message.utils import send_passcode_to_phone
from review.services import ReviewService
from sociallink.tasks import share_item_to_facebook
from offertalk.tasks import push_message_to_user_inbox, record_message_in_user_inbox
from service import UserService, CategoryService, ItemService
from boss.tasks import send_item_to_boss
from apilog.service import AnalyticsService


default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

logger = logging.getLogger('pinnacle')

r = SrService().get_redis()
user_service = UserService()
thrift_path = os.path.join(os.path.dirname(__file__), 'gen-py')
sys.path.append(thrift_path)

@require_POST
def signup_sms(request):
    phone = request.POST.get('phone', 0)
    if phone == 0:
        raise InvalidParameterError()

    pass_code = r.get('auth:phone:%s:pass_code')
    if not pass_code:
        pass_code = generate_temp_passcode()
        r.set('auth:phone:%s:pass_code' % phone, pass_code, 30*60)
        r.set('auth:pass_code:%s:phone' % pass_code, phone, 30*60)

    send_sms_to_number(phone, pass_code)
    return HttpResponse()
    #return HttpResponse(json.dumps({'code':pass_code}), content_type='application/json')

@require_POST
def verify_code(request):
    phone = request.POST.get('phone', '')
    pass_code = request.POST.get('pass_code', '')
    password = request.POST.get('password', '')

    if len(password) < 6:
        raise InvalidParameterError()
    else:
        password = make_password(password, settings.PASSWORD_SALT, settings.PASSWORD_ALGORITHM)

    if phone == r.get('auth:pass_code:%s:phone' % pass_code) and pass_code == r.get('auth:phone:%s:pass_code' % phone):
        timestamp = get_now_timestamp()
        user_id = r.get('login:phone:%s:user' % phone)
        if not user_id:
            user_id = r.incr('global:next_user_id')
            fuzzy_user_id = obfuscate_id(user_id)
            token = generate_access_token(user_id)

            with r.pipeline() as pipe:
                pipe.set('login:phone:%s:user_id' % phone, user_id)
                pipe.hmset('user:%s' % user_id, {'id':fuzzy_user_id, 'phone':phone, 'created_at':timestamp, 'last_login_at':timestamp})

                pipe.set('fuzzy_id:%s:user_id' % fuzzy_user_id, user_id)

                pipe.set('user:%s:password' % user_id, password)

                pipe.delete('auth:pass_code:%s:phone' % pass_code)
                pipe.delete('auth:phone:%s:pass_code' % phone)

                pipe.execute()

            user = User(id=user_id, fuzzy_user_id=fuzzy_user_id, mobile_phone=phone, password=password)
            user.save()
        else:
            token = r.get('user:%s:token' % user_id)
            r.hset('user:%s' % user_id, 'last_login_at', timestamp)
            user = User.objects.get(pk=user_id)
            user.last_login_at = datetime.datetime.utcnow().replace(tzinfo=utc)
            user.save()

        # sync_redis_to_db.delay('user', 'C', user_id)

        result = r.hgetall('user:%s' % user_id)
        result['token'] = token

        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        raise PinnacleApiError(ERR_CODE_INVALID_PASSCODE)


@require_POST
def signup_email(request):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    email = request.POST.get('email', '')
    email = email.lower()
    password = request.POST.get('password', '')
    nickname = request.POST.get('nickname', '')
    appsflyer_user_id = request.POST.get('appsflyer_user_id', '')
    ip_address = get_real_ip(request)
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    fuzzy_anonymous_user_id = request.POST.get('anonymous_user_id', '')
    device_id = request.POST.get('device_id', '')
    time_zone = request.POST.get('timezone', '')
    anonymous_user_id = unfuzzy_user_id(fuzzy_anonymous_user_id) if fuzzy_anonymous_user_id != '' else None

    try:
        validate_email(email)
    except ValidationError:
        raise InvalidParameterError()

    if len(password) < 6 or len(password) > 16:
        raise PinnacleApiError(ERR_CODE_INVALID_PASSWORD)
    else:
        password = make_password(password, settings.PASSWORD_SALT, settings.PASSWORD_ALGORITHM)

    if nickname == '':
        raise InvalidParameterError()

    user_id = r.get('login:email:%s:user_id' % email)
    if not user_id:
        result = UserService().signup(signup_type='email', nickname=nickname, lat=lat, lon=lon, ip_address=ip_address,
                                      anonymous_user_id=anonymous_user_id, appsflyer_user_id=appsflyer_user_id,
                                      email=email, password=password, os=request.os, os_version=request.os_version,
                                      app_version=request.app_version, device=request.device, device_id=device_id,
                                      time_zone=time_zone)
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        raise PinnacleApiError(ERR_CODE_EMAIL_EXISTS)


@require_POST
def signup_anonymous(request):
    device_id = request.POST.get('device_id', '')
    appsflyer_user_id = request.POST.get('appsflyer_user_id', '')
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    time_zone = request.POST.get('timezone', '')
    ip_address = get_real_ip(request)
    if lat == 0 and lon == 0:
        lat, lon = _get_latlng_by_geoip(ip_address)

    user_id = r.get('login:anonymous:%s:user_id' % device_id)
    if not user_id:
        result = UserService().signup(signup_type='anonymous', nickname='Anonymous', lat=lat, lon=lon, ip_address=ip_address,
                                      appsflyer_user_id=appsflyer_user_id, email='', device_id=device_id, os=request.os,
                                      os_version=request.os_version, app_version=request.app_version, device=request.device,
                                      time_zone=time_zone)
    else:
        result = UserService().login(login_type='anonymous', user_id=user_id, appsflyer_user_id=appsflyer_user_id,
                                     lat=lat, lon=lon, ip_address=ip_address, os=request.os, os_version=request.os_version,
                                     app_version=request.app_version, device=request.device)

    return HttpResponse(json.dumps(result), content_type='application/json')


@require_GET
def validate_email(request):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    email = request.GET.get('email', '')
    email = email.lower()

    # Validate email address
    try:
        validate_email(email)
    except ValidationError:
        raise InvalidParameterError()

    if not mailgun_validate_email(email):
        raise InvalidParameterError()

    user_id = r.get('login:email:%s:user_id' % email)
    if user_id:
        raise PinnacleApiError(ERR_CODE_EMAIL_EXISTS)

    return HttpResponse()


@require_GET
def verify_email(request):
    email = request.GET.get('email', '')
    fb_user_id = None
    if email == '':
        raise InvalidParameterError()

    try:
        email_json = jwt.decode(email, settings.JWT_SECRET_KEY)
        email = email_json['email']
        if 'fb_user_id' in email_json and email_json['fb_user_id'] is not None and email_json['fb_user_id'] != '':
            fb_user_id = email_json['fb_user_id']
    except:
        raise InvalidParameterError()

    user_id = r.get('login:email:%s:user_id' % email) \
        if not fb_user_id else r.get('login:facebook:%s:user_id' % fb_user_id)
    if not user_id:
        raise NotFoundError()

    r.hset('user:%s' % user_id, 'email_verified', True)
    user = User.objects.get(pk=user_id)
    user.email_verified = True
    user.save()

    return HttpResponse()


@require_POST
@validate_user
def send_verify_email(request):
    user_info = r.hmget('user:%s' % request.user_id, 'nickname', 'email', 'fb_user_id')
    encoded_email = jwt.encode({'email': user_info[1], 'fb_user_id': user_info[2]}, settings.JWT_SECRET_KEY)
    send_mail(request.user_id, 'TMPL_VERIFY_EMAIL', data={'nickname': user_info[0].encode('utf-8'), 'encoded_email': encoded_email})
    return HttpResponse()


@require_POST
@validate_user
def fill_profile(request):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    nickname = request.POST.get('nickname', '')
    portrait = request.POST.get('portrait', '')
    email = request.POST.get('email', '')

    zipcode = request.POST.get('zipcode', '')
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    country = request.POST.get('country', '')
    region = request.POST.get('region', '')
    city = request.POST.get('city', '')

    # FIXME: [https://github.com/3rdStone/fmmc/issues/99#issuecomment-121182913]
    # 设置zipcode时iOS版传输了国家简称而应该传全称，V3.2版会修复，修复之前后端先转换一下
    if country != '':
        country = correct_country_name(country)

    # [issue#93 wangguanglei 2015-05-20]预处理:判断是否合法,是否重复
    if email != '':
        # 判断email合法性
        try:
            validate_email(email)
        except ValidationError:
            raise InvalidParameterError()

        # 判断email是否已存在
        user_id = r.get('login:email:%s:user_id' % email)
        if user_id:
            raise PinnacleApiError(ERR_CODE_EMAIL_EXISTS)

    kwargs = {'nickname': nickname, 'portrait': portrait, 'email': email,
              'zipcode': zipcode, 'lat': lat, 'lon': lon,
              'country': country, 'region': region, 'city': city}
    result = UserService().fill_user_detail(request.user_id, **kwargs)

    return HttpResponse(json.dumps(result), content_type='application/json', status=200)


@require_POST
def login_sms(request):
    phone = request.POST.get('phone', 0)
    password = request.POST.get('password', '')

    if phone == 0 or len(password) < 6:
        raise InvalidParameterError()

    user_id = r.get('login:phone:%s:user_id' % phone)
    encrypted_pwd = r.get('user:%s:password' % user_id)
    if not check_password(password, encrypted_pwd):
        raise InvalidCredentialsError()

    user = r.hget('user:%s' % request.user_id)
    return HttpResponse(json.dumps(user), content_type='application/json')

@require_POST
def login_facebook(request):
    fb_user_id = request.POST.get('fb_userid', '')
    nickname = request.POST.get('fb_username', '')
    portrait = request.POST.get('fb_headimage', '')
    email = request.POST.get('fb_email', '')
    appsflyer_user_id = request.POST.get('appsflyer_user_id', '')
    fb_token = request.POST.get('fb_token', '')
    fb_token_expires = int(request.POST.get('fb_token_expires', 0))
    ip_address = get_real_ip(request)
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    fuzzy_anonymous_user_id = request.POST.get('anonymous_user_id', '')
    device_id = request.POST.get('device_id', '')
    time_zone = request.POST.get('timezone', '')
    anonymous_user_id = unfuzzy_user_id(fuzzy_anonymous_user_id) if fuzzy_anonymous_user_id != '' else None

    if lat == 0 and lon == 0:
        lat, lon = _get_latlng_by_geoip(ip_address)

    kwargs = {'fb_token': fb_token, 'fb_token_expires': fb_token_expires}
    user_id = r.get('login:facebook:%s:user_id' % fb_user_id)
    if user_id:
        if get_user_state(user_id) == USER_STATE_FORBIDDEN:
            raise UnauthorizedError()

        result = UserService().login(login_type='facebook', user_id=user_id, appsflyer_user_id=appsflyer_user_id,
                                     lat=lat, lon=lon, ip_address=ip_address, nickname=nickname, email=email, portrait=portrait,
                                     fb_user_id=fb_user_id, os=request.os, os_version=request.os_version,
                                     app_version=request.app_version, device=request.device, **kwargs)
    else:
        result = UserService().signup(signup_type='facebook', nickname=nickname, email=email, lat=lat, lon=lon,
                                      ip_address=ip_address, fb_user_id=fb_user_id, anonymous_user_id=anonymous_user_id,
                                      portrait=portrait, appsflyer_user_id=appsflyer_user_id, os=request.os,
                                      os_version=request.os_version, app_version=request.app_version,
                                      device_id=device_id, device=request.device, time_zone=time_zone, **kwargs)

    return HttpResponse(json.dumps(result), content_type='application/json')


@require_POST
def login_email(request):
    email = request.POST.get('email', '')
    email = email.lower()
    password = request.POST.get('password', '')
    appsflyer_user_id = request.POST.get('appsflyer_user_id', '')
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    ip_address = get_real_ip(request)
    if lat == 0 and lon == 0:
        lat, lon = _get_latlng_by_geoip(ip_address)

    user_id = r.get('login:email:%s:user_id' % email)
    if not user_id:
        raise InvalidCredentialsError()

    if get_user_state(user_id) == USER_STATE_FORBIDDEN:
        raise UnauthorizedError()

    encrypted_pwd = r.get('user:%s:password' % user_id)
    if not check_password(password, encrypted_pwd):
        raise InvalidCredentialsError()

    result = UserService().login(login_type='email', user_id=user_id, appsflyer_user_id=appsflyer_user_id, lat=lat,
                                 lon=lon, ip_address=ip_address, os=request.os, os_version=request.os_version,
                                 app_version=request.app_version, device=request.device)
    return HttpResponse(json.dumps(result), content_type='application/json')



@require_GET
def forget_password(request):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    email = request.GET.get('email', '')
    email = email.lower()
    # nickname = request.GET.get('nickname', '')

    # Validate email address
    try:
        validate_email(email)
    except ValidationError:
        raise PinnacleApiError(ERR_CODE_EMAIL_NOT_EXISTS)

    user_id = r.get('login:email:%s:user_id' % email)
    if not user_id:
        raise PinnacleApiError(ERR_CODE_EMAIL_NOT_EXISTS)

    email = jwt.encode({'email': email}, settings.JWT_SECRET_KEY)
    # send email
    send_mail.delay(str(user_id), 'TMPL_RESET_PASSWORD', None, {'url': settings.URL_RESET_PASSWORD, 'email': email})

    return HttpResponse()

@require_POST
def reset_password(request):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    password = request.POST.get('password', '')
    email = request.POST.get('email', '')

    jsonEmail = jwt.decode(email, settings.JWT_SECRET_KEY)
    email = jsonEmail['email']

    try:
        validate_email(email)
    except ValidationError:
        raise PinnacleApiError(ERR_CODE_EMAIL_NOT_EXISTS)

    if len(password) < 6 or len(password) > 16:
        raise PinnacleApiError(ERR_CODE_INVALID_PASSWORD)
    else:
        password = make_password(password, settings.PASSWORD_SALT, settings.PASSWORD_ALGORITHM)

    update_cache_success = False
    user_id = None
    try:
        user_id = r.get('login:email:%s:user_id' % email)
        if user_id:
            user_id = int(user_id)
            old_password = r.get('user:%d:password' % user_id)
            # update password in redis
            r.set('user:%d:password' % user_id, password)
            update_cache_success = True

            # update password in db
            user = User.objects.get(pk=user_id)
            user.password = password
            user.save()
        else:
            raise NotFoundError()
    except Exception, e:
        print e
        if update_cache_success and user_id:
            old_password = make_password(old_password, settings.PASSWORD_SALT, settings.PASSWORD_ALGORITHM)
            r.set('user:%d:password' % user_id, old_password)

        raise PinnacleApiError(ERR_CODE_RESET_PASSWORD_FAILED)

    return HttpResponse()


@require_GET
def share_store(request):
    fuzzy_user_id = request.GET.get('user_id', '')
    user_id = unfuzzy_user_id(fuzzy_user_id)
    if not user_id:
        raise UserNotFoundError(fuzzy_user_id)

    store_object = get_user_from_id(user_id)
    store_object['store_url'] = '%s%s' % (settings.STORE_URL_PREFIX, fuzzy_user_id)

    return HttpResponse(json.dumps(store_object), content_type='application/json')


@require_POST
def sign_cloudinary_upload(request):
    options = {'api_key': settings.CLOUDINARY_APIKEY, 'api_secret': settings.CLOUDINARY_SECRET}
    timestamp = get_now_timestamp()
    callback = request.POST.get('callback', '')
    params = {'timestamp': timestamp, 'callback': callback}
    result = sign_request(params, options)
    return HttpResponse(json.dumps(result), content_type='application/json')


@require_POST
@validate_user
def post_item(request):
    title = request.POST.get('title', '')
    description = request.POST.get('desc', '')
    category_id = int(request.POST.get('category', 0))
    category_id = CategoryService().get_map_to_id(category_id)
    try:
        price = float(request.POST.get('price', 0))
    except Exception:
        price = 0

    ip_address = get_real_ip(request)
    original_price = request.POST.get('original_price')
    # 1: Exchange in person
    # 2: Send by post
    # 3: Either by post or in person
    shipping_method = request.POST.get('shipping_method', 3)
    brand_name = request.POST.get('brand', '')
    brand_id = 0
    try:
        if brand_name != '':
            brand = Brand.objects.filter(name=brand_name)
            if brand and len(brand) > 0:
                brand_id = brand[0].id
            else:
                brand = Brand(name=brand_name, brand_type=1)
                brand.save()
                brand_id = brand.id
    except Exception, e:
        print e
        brand_name = ''

    currency_code = request.POST.get('currency', settings.DEFAULT_CURRENCY_CODE)
    images = json.loads(request.POST.get('images', '[]'))
    mediaLink = request.POST.get('mediaLink', '')
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    # V3.2开始客户端会传递位置信息
    country = request.POST.get('country', '')
    region = request.POST.get('region', '')
    city = request.POST.get('city', '')
    share = int(request.POST.get('share', 0))    # 1表示后台自动分享,0表示不自动分享

    if len(images) == 0:
        raise InvalidParameterError()

    local_price = numbers.format_currency(price, currency_code, locale='en_US')

    timestamp = get_now_timestamp()
    item_id = r.incr('global:next_item_id')
    fuzzy_item_id = obfuscate_item_id(item_id)
    with r.pipeline() as pipe:
        pipe.set('fuzzy_item_id:%s:item_id' % fuzzy_item_id, item_id)
        pipe.hmset('item:%s' % item_id,
                   get_item_redis_json(fuzzy_item_id, 0, title, description, category_id, price, currency_code,
                                       local_price, lat, lon, request.POST['images'], mediaLink, shipping_method,
                                       request.user_id, original_price, brand_id,
                                       brand_name, 1, timestamp, timestamp, timestamp,
                                       country, region, city))

        pipe.set('item:%d:owner' % item_id, request.user_id)

        # 存放后端审核通过的商品编号列表用于app端查看
        pipe.zadd('user:%s:my_selling_list' % request.user_id, item_id, timestamp)
        pipe.zadd('user:%s:user_selling_list' % request.user_id, item_id, timestamp)
        pipe.execute()

    default_weight = 5
    now = get_utcnow()
    item = Item(id=item_id, fuzzy_item_id=fuzzy_item_id, state=ITEM_STATE_LISTING, title=title, desc=description, price=price,
                currency=currency_code, local_price=local_price, lat=lat, lon=lon, mediaLink=mediaLink,
                user_id=request.user_id, weight=default_weight, original_price=original_price,
                shipping_method=shipping_method, brand_id=brand_id, brand_name=brand_name, share=share,
                modified_at=now, renewed_at=now, country=country, region=region, city=city)
    item.save()
    for i in images:
        image = Image(item_id=item_id, image_path=i['imageLink'], image_height=i['height'], image_width=i['width'])
        image.save()
    cat = ItemCategory(item_id=item_id, cat_id=category_id)
    cat.save()

    user_items_num = UserService().get_listing_num(request.user_id)
    owner = get_user_from_id(request.user_id,item_num=user_items_num+1)
    doc = get_item_es_json(fuzzy_item_id, title, description, category_id, images, mediaLink, price, currency_code,
                           local_price, original_price, brand_id, brand_name, lat, lon, owner, default_weight,
                           get_category_as_json(category_id), timestamp, timestamp, timestamp, 1, country, region, city)

    #index_item_in_searchengine(item_id, doc)
    es_dao_item().index(item_id, doc)

    # if not settings.DEBUG:
    #     blur_detect.delay(item_id, images[0]['imageLink'])

    prompt_to_upload_portrait.delay(request.user_id, 'ITEM_POST')
    prompt_followers_has_new_items.delay(request.user_id, item_id)

    # 处理位置信息
    if country == '' and region == '' and city == '':
        lookup_item_position.apply_async((request.user_id, item_id, lat, lon, ip_address), queue='celery.position', countdown=3)

    # 自动分享商品到facebook
    if settings.FACEBOOK['AUTO_SHARE_ITEM_SWITCH'] and share == 1:
        if settings.DEBUG:
            logger.debug('share item to facebook: user_id:%s, item_id:%s' % (request.user_id, item_id))
        share_item_to_facebook.delay(request.user_id, item_id)

    # call web service to do bot follow and bot like
    payload = {'user_id':request.user_id, 'item_id':item_id}
    url = 'http://%s/botfollow/' % '10.0.2.156'
    try:
        requests.post(url, data=payload, timeout=0.6)
    except RequestException:
        logger.error('bot follow request failed')

    # 一段时间后自动移除new tag
    remove_new_tag.apply_async((int(item_id),),
                               eta=datetime.datetime.utcnow() + timedelta(days=settings.ITEM_NEW_TAG_HOLD_DAYS),
                               queue='celery.schedule')

    send_seller_guide.delay(request.user_id, item_id)
    remind_to_renew.delay(request.user_id, item_id)

    renew_ttl = _renew_countdown(item_id)
    send_item_to_boss.delay(item_id, item_action='New')

    es_dao_user().update_fields(request.user_id, **{'item_num': user_items_num,
                                                    'last_posted_at': timestamp})

    return HttpResponse(json.dumps({'new_id': fuzzy_item_id, 'renew_ttl': renew_ttl}), content_type='application/json',
                        status=201)


@require_POST
@validate_user
def edit_item(request):
    fuzzy_item_id = request.POST.get('item_id', '')
    item_id = unfuzzy_item_id(fuzzy_item_id)
    if not item_id:
        raise ItemNotFoundError(fuzzy_item_id)

    if get_item_state(item_id) not in [ITEM_STATE_LISTING, ITEM_STATE_UNLISTED]:
        raise InvalidParameterError()

    title = request.POST.get('title', '')
    description = request.POST.get('desc', '')
    category_id = int(request.POST.get('category', 0))
    category_id = CategoryService().get_map_to_id(category_id)
    try:
        price = float(request.POST.get('price', 0))
    except Exception:
        price = 0

    original_price = request.POST.get('original_price')
    shipping_method = request.POST.get('shipping_method')
    brand_name = request.POST.get('brand', '')
    brand_id = None
    try:
        if brand_name != '':
            brand = Brand.objects.filter(name=brand_name)
            if brand and len(brand) > 0:
                brand_id = brand[0].id
            else:
                brand = Brand(name=brand_name, brand_type=1)
                brand.save()
                brand_id = brand.id
    except:
        brand_name = ''

    currency_code = request.POST.get('currency', settings.DEFAULT_CURRENCY_CODE)
    images = json.loads(request.POST.get('images', '[]'))
    mediaLink = request.POST.get('mediaLink', '')

    if len(images) == 0:
        raise InvalidParameterError()

    local_price = numbers.format_currency(price, currency_code, locale='en_US')

    price_changed = False
    if price != float(r.hget('item:%s' % item_id, 'price')):
        price_changed = True

    # V3.2开始客户端会传递位置信息
    country = request.POST.get('country', '')
    region = request.POST.get('region', '')
    city = request.POST.get('city', '')

    timestamp = get_now_timestamp()
    r.hmset('item:%s' % item_id,
            get_update_item_redis_json(title, description, category_id, price, currency_code, local_price,
                                       request.POST['images'], mediaLink, original_price, shipping_method, brand_id,
                                       brand_name, timestamp, country, region, city))
    item = Item.objects.get(pk=item_id)
    item.title = title
    item.desc = description
    item.price = price
    item.currency = currency_code
    item.local_price = local_price
    item.mediaLink = mediaLink
    item.internal_status = ITEM_INTERNAL_STATUS_PENDING_REVIEW
    item.original_price = original_price
    item.shipping_method = shipping_method
    item.brand_id = brand_id
    item.brand_name = brand_name
    item.modified_at = datetime.datetime.utcnow().replace(tzinfo=utc)
    if country:
        item.country = country
    if region:
        item.region = region
    if city:
        item.city = city
    item.save()

    Image.objects.filter(item_id=item_id).delete()
    for i in images:
        image = Image(item_id=item_id, image_path=i['imageLink'], image_height=i['height'], image_width=i['width'])
        image.save()

    ItemCategory.objects.filter(item_id=item_id).delete()
    cat = ItemCategory(item_id=item_id, cat_id=category_id)
    cat.save()

    partial_doc = get_update_item_es_json(title, description, category_id, images, mediaLink, price, currency_code,
                                          local_price, original_price, brand_id, brand_name,
                                          get_category_as_json(category_id), timestamp, country, region, city)

    update_fields_in_searchengine(item_id, **partial_doc)

    if price_changed:
        prompt_price_change.delay(item_id, fuzzy_item_id, request.user_id)

    send_item_to_boss.delay(item_id ,item_action="Edit")

    return HttpResponse()


@require_GET
def get_item_detail(request):
    item_id = unfuzzy_item_id(request.GET.get('item_id', ''))
    if not item_id:
        raise NotFoundError()

    owner = r.get('item:%s:owner' % item_id)
    if hasattr(request, 'user_id') and UserService().is_blocked(owner, request.user_id):
        raise UserBlockedError()

    item = r.hgetall('item:%s' % item_id)
    item['state'] = 0 if 'state' not in item or item['state'] is None else int(item['state'])

    try:
        item['liked'] = r.sismember('item:%s:likers' % item_id, request.user_id)
        score = r.zscore('item:%s:new_likers' % item_id, request.user_id)
        item['liked'] = True if score else False
    except Exception:
        item['liked'] = False

    item['owner'] = get_user_from_id(owner)
    item['images'] = json.loads(item['images'])
    item['location'] = json.loads(item['location'])
    item['isMine'] = False if not hasattr(request, 'user_id') else owner == str(request.user_id)
    if 'user_id' in item:
        item.pop('user_id')

    item['offerLines'] = _assemble_offerlines(request.user_id, item_id, item['isMine']) if hasattr(request, 'user_id') \
        else _assemble_offerlines('-1', item_id, item['isMine'])
    item['comment_count'] = r.llen('item:%s:comments' % item_id)
    item['has_more_comments'] = item['comment_count'] > 5

    comment_ids = r.lrange('item:%s:comments' % item_id, 0, 4)
    comments = []
    for comment_id in comment_ids:
        comment = r.hgetall('comment:%s' % comment_id)

        plain_author_id = comment['author'] if 'author' in comment else comment['authorID']
        comment['author'] = get_user_from_id(plain_author_id)

        if 'authorID' in comment:
            comment.pop('authorID')

        comments.append(comment)

    item['comments'] = comments
    # item['deletable'] = False if (r.llen('item:%s:offerlines' % item_id) > 0 or get_item_state(item_id) not in [ITEM_STATE_LISTING, ITEM_STATE_UNLISTED]) else True
    item['deletable'] = True    # 暂定为所有商品均可删除

    if hasattr(request, 'user_id') and owner == str(request.user_id):
        renew_ttl = _get_renew_ttl(item_id)
        if renew_ttl:
            item['renew_ttl'] = renew_ttl

    if hasattr(request, 'user_id'):
        # lvyi 记录用户的ctr 点击数
        task_item_ctr.apply_async((item_id, item, request.user_id), queue='celery.misc')

    review_num, review_score = ReviewService().get_avg_score(owner)
    if review_num >= 3:
        item['review_num'], item['review_score'] = review_num, review_score

    item['user_blocked'] = UserService().is_blocked(request.user_id, owner) if hasattr(request, 'user_id') else False

    if hasattr(request, 'user_id'):
        UserService().user_daily_checkin(request.user_id)

    return HttpResponse(json.dumps(item), content_type='application/json')

@require_GET
def get_item_offerlines(request):
    item_id = unfuzzy_item_id(request.GET.get('item_id', ''))
    if not item_id:
        raise NotFoundError()

    owner = r.get('item:%s:owner' % item_id)
    item_is_mine = (owner == str(request.user_id))

    offerlines = _assemble_offerlines(request.user_id, item_id, item_is_mine)

    return HttpResponse(json.dumps({'objects':offerlines}), content_type='application/json')


@require_GET
def get_item_short_url(request):
    fuzzy_item_id = request.GET.get('item_id', '')
    item_id = unfuzzy_item_id(fuzzy_item_id)
    if not item_id:
        raise ItemNotFoundError(fuzzy_item_id)

    bitly = bitly_api.Connection(access_token=settings.BITLY_ACCESS_TOKEN)
    orig_url = 'http://5milesapp.com/item/%s' % fuzzy_item_id
    data = bitly.shorten(orig_url)
    if data is not None:
        short_url = data['url']
    else:
        short_url = orig_url

    return HttpResponse(json.dumps({'short_url': short_url}), content_type='application/json')


@require_POST
def appsflyer_callback_two(request):
    appsflyer_two_job.delay(request.body)
    return HttpResponse()


def _assemble_offerlines(user_id, item_id, item_is_mine):

    def _assember_text(offer):
        if 'msg_type' not in offer:
            text = last_offer['text']
        else:
            msg_type = int(offer['msg_type'])
            if msg_type == 0:
                text = json.loads(offer['payload'])['text']
            elif msg_type == 1:
                text = '[Photo]'
            elif msg_type == 2:
                text = '[Location]'

        return text

    item_state = r.hget('item:%s' % item_id, 'state')
    item_state = 0 if item_state is None else int(item_state)

    offerline_ids = r.lrange('item:%s:offerlines' % item_id, 0, -1)
    offerlines = []
    if item_is_mine:
        #Seller's viewpoint

        for offerline_id in offerline_ids:
            last_records = r.zrange('offerline:%s:entries' % offerline_id, -1, -1)
            if len(last_records) == 0:
                continue

            last_offer_id = last_records[-1]
            last_offer = r.hgetall('offer:%s' % last_offer_id)

            buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')
            buyer = get_user_from_id(buyer_id)

            if item_state >= 1 and buyer_id == r.get('item:%s:sold_to' % item_id):
                offerline = {'id':offerline_id, 'description':_('You sold the item to %s' % buyer['nickname']), 'buyer':buyer, 'diggable':True}
            else:
                if str(user_id) == last_offer['from']:
                    if float(last_offer['price']) > 0:
                        offerline = {'id':offerline_id, 'description':_('You offered %(local_price)s to %(nickname)s') % {'local_price': last_offer['local_price'], 'nickname': buyer['nickname']}, 'buyer':buyer, 'diggable':True}
                    else:
                        text = _assember_text(last_offer)
                        offerline = {'id':offerline_id, 'description':text, 'buyer':buyer, 'diggable':True}
                else:
                    if float(last_offer['price']) > 0:
                        offerline = {'id':offerline_id, 'description':_('%(nickname)s offered %(local_price)s') % {'nickname': r.hget('user:%s' % last_offer['from'], 'nickname'), 'local_price': last_offer['local_price']}, 'buyer':buyer, 'diggable':True}
                    else:
                        text = _assember_text(last_offer)
                        offerline = {'id': offerline_id, 'description': _('%(nickname)s: %(text)s') % {'nickname': r.hget('user:%s' % last_offer['from'], 'nickname'), 'text': text}, 'buyer': buyer, 'diggable': True}

            offerlines.append(offerline)
    else:
        #Buyer's viewpoint

        my_offerline_id = r.get('item:%s:buyer:%s:offerline_id' % (item_id, user_id))
        if my_offerline_id is None:
            if len(offerline_ids) > 0 and item_state == ITEM_STATE_LISTING:
                offerlines.append({'id':0, 'buyer':None, 'description':'%d offers made' % len(offerline_ids) if len(offerline_ids) > 1 else '1 offer made'})
            elif item_state > ITEM_STATE_LISTING:
                offerlines.append({'id':0, 'buyer':None, 'description':_('This item has been sold')})
        else:
            if item_state == ITEM_STATE_LISTING:
                if len(offerline_ids) >= 2:
                    num_others = len(offerline_ids) - 1
                    offerlines.append({'id':0, 'buyer':None, 'description':'%d other people also made offer' % num_others, 'diggable':False})

                last_records = r.zrange('offerline:%s:entries' % my_offerline_id, -1, -1)
                if len(last_records) > 0:
                    last_offer_id = last_records[-1]
                    last_offer = r.hgetall('offer:%s' % last_offer_id)
                    subject = _('You') if str(user_id) == last_offer['from'] else _('The seller')

                    if float(last_offer['price']) > 0:
                        offerline = {'id':my_offerline_id, 'description':'%s offered %s' % (subject, last_offer['local_price']), 'buyer': get_user_from_id(user_id), 'diggable':True}
                    else:
                        text = _assember_text(last_offer)
                        offerline = {'id':my_offerline_id, 'description':'%s: %s' % (subject, text), 'buyer': get_user_from_id(user_id), 'diggable':True}

                    offerlines.append(offerline)
            else:
                if str(user_id) == r.get('item:%s:sold_to' % item_id):
                    offerline = {'id':my_offerline_id, 'description':_('You got the item'), 'buyer': get_user_from_id(user_id), 'diggable':True}
                else:
                    offerline = {'id':my_offerline_id, 'description':_('This item has been sold'), 'buyer':None, 'diggable':False}
                offerlines.append(offerline)

    return offerlines


@require_GET
def load_comments(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))

    item_id = unfuzzy_item_id(request.GET.get('item_id', ''))
    if not item_id:
        raise NotFoundError()

    comment_ids = r.lrange('item:%s:comments' % item_id, offset, offset+limit-1)
    comments = []
    for comment_id in comment_ids:
        comment = r.hgetall('comment:%s' % comment_id)

        plain_author_id = comment['author'] if 'author' in comment else comment['authorID']
        comment['author'] = get_user_from_id(plain_author_id)

        if 'authorID' in comment:
            comment.pop('authorID')

        comments.append(comment)

    meta = {'next':'?offset=%d&limit=%d' % (offset+limit, limit) if len(comment_ids) == limit else None}

    return HttpResponse(json.dumps({'meta':meta, 'objects':comments}), content_type='application/json')

@require_GET
def dummy_get_item_detail(request, item_id):
    result = {'liked':False}

    offerline0 = {'id':0, 'buyer':{'id':1, 'nickname':'johndoe', 'portrait':'http://tp4.sinaimg.cn/1835342091/180/5636259460/1'}, 'description':'5 other people also made offer', 'diggable':False}
    offerline1 = {'id':1, 'buyer':{'id':1, 'nickname':'johndoe', 'portrait':'http://tp4.sinaimg.cn/1835342091/180/5636259460/1'}, 'description':'you offered $20', 'diggable':True}
    result['offerLines'] = [offerline0, offerline1]

    timestamp = get_now_timestamp_str()
    comment1 = {'id':1, 'authorID':1, 'author':{'id':'1', 'nickname':'Hector', 'portrait':'http://tp4.sinaimg.cn/1835342091/180/5636259460/1'}, 'text':'look very nice', 'timestamp':timestamp}
    result['comments'] = [comment1]

    return HttpResponse(json.dumps(result), content_type='application/json')


@require_GET
def get_user_detail(request):
    raw_user_id = unfuzzy_user_id(request.GET.get('user_id', ''))
    if not raw_user_id:
        raise UserNotFoundError(raw_user_id)

    if hasattr(request, 'user_id') and UserService().is_blocked(raw_user_id, request.user_id):
        raise UserBlockedError()

    # FIXME: is_me字段后续版本会删除掉，判断逻辑改为由客户端实现
    is_me = False if not hasattr(request, 'user_id') else str(request.user_id) == raw_user_id

    # 卖家列表中不能看见审核不通过的商品
    items_count = r.zcard('user:%s:user_selling_list' % raw_user_id)

    followers_count = r.zcard('user:%s:followers' % raw_user_id)
    following_count = r.zcard('user:%s:following' % raw_user_id)
    if not hasattr(request, 'user_id'):
        followed_by_request_user = False
    else:
        followed_by_request_user = is_me or r.zrank('user:%s:following' % request.user_id, raw_user_id) >= 0

    seller_feedback_count = r.llen('user:%s:seller_ratings' % raw_user_id)
    try:
        total_score = int(r.get('user:%s:seller_ratings:total_score' % raw_user_id))
    except Exception:
        total_score = 0
        r.set('user:%s:seller_ratings:total_score' % raw_user_id, 0)

    seller_feedback_score = 0 if (total_score == 0 or seller_feedback_count == 0) else float(total_score) / seller_feedback_count
    user_info = r.hmget('user:%s' % raw_user_id, 'nickname', 'portrait', 'verified', 'email_verified', 'mobile_phone', 'fb_user_id', 'country', 'region', 'city', 'lat', 'lon')
    facebook_verified = r.exists('user:%s:linked_facebook' % raw_user_id) or (True if user_info[5] is not None else False)
    review_num, review_score = ReviewService().get_avg_score(raw_user_id)
    user = {'isMe': is_me, 'followed': followed_by_request_user, 'count_sellings': items_count,
            'count_followers': followers_count, 'count_following': following_count,
            'seller_rating_score': seller_feedback_score, 'count_seller_ratings': seller_feedback_count,
            'nickname': user_info[0], 'portrait': user_info[1], 'verified': bool(user_info[2]) if user_info[2] is not None else False,
            'email_verified': bool(user_info[3]) if user_info[3] is not None else False,
            'facebook_verified': facebook_verified,
            'mobile_phone_verified': True if user_info[4] is not None else False,
            'country': user_info[6] if user_info[6] is not None else '',
            'region': user_info[7] if user_info[7] and not user_info[7].isdigit() else '',
            'city': user_info[8] if user_info[8] is not None else '',
            'lat': user_info[9], 'lon': user_info[10],
            'place': get_place(user_info[6], user_info[7], user_info[8]),
            'review_num': review_num, 'review_score': review_score,
            'user_blocked': UserService().is_blocked(request.user_id, raw_user_id) if hasattr(request, 'user_id') else False}

    prompt_to_upload_portrait.delay(raw_user_id, 'PROFILE_VISITED')

    if hasattr(request, 'user_id'):
        UserService().user_daily_checkin(request.user_id)

    return HttpResponse(json.dumps(user), content_type='application/json')

@require_GET
def get_me_detail(request):
    if not r.exists('user:%s' % request.user_id):
        raise UserNotFoundError(request.user_id)

    items_count = r.zcard('user:%s:my_selling_list' % request.user_id)
    followers_count = r.zcard('user:%s:followers' % request.user_id)
    following_count = r.zcard('user:%s:following' % request.user_id)
    likes_count = r.zcard('user:%s:likes' % request.user_id)
    purchases_count = r.zcard('user:%s:purchases' % request.user_id)

    seller_feedback_count = r.llen('user:%s:seller_ratings' % request.user_id)
    buyer_feedback_count = 10

    try:
        total_score = int(r.get('user:%s:seller_ratings:total_score' % request.user_id))
    except Exception:
        total_score = 0
        r.set('user:%s:seller_ratings:total_score' % request.user_id, 0)

    seller_feedback_score = 0 if (total_score == 0 or seller_feedback_count == 0) \
        else float(total_score) / seller_feedback_count
    buyer_feedback_score = 4.5

    user_info = r.hmget('user:%s' % request.user_id, 'nickname', 'portrait', 'email',
                        'verified', 'email_verified', 'mobile_phone', 'fb_user_id',
                        'zipcode', 'country', 'region', 'city')
    facebook_verified = r.exists('user:%s:linked_facebook' % request.user_id) \
                        or (True if user_info[6] is not None else False)
    review_num, review_score = ReviewService().get_avg_score(request.user_id)
    ret = {'items_count': items_count, 'followers_count': followers_count, 'following_count': following_count,
           'likes_count': likes_count, 'purchases_count': purchases_count,
           'seller_feedback_count': seller_feedback_count,
           'buyer_feedback_count': buyer_feedback_count, 'seller_feedback_score': seller_feedback_score,
           'buyer_feedback_score': buyer_feedback_score, 'nickname': user_info[0], 'portrait': user_info[1],
           'email': user_info[2], 'verified': bool(user_info[3]) if user_info[3] is not None else False,
           'email_verified': bool(user_info[4]) if user_info[4] is not None else False,
           'facebook_verified': facebook_verified,
           'mobile_phone_verified': True if user_info[5] is not None else False,
           'mobile_phone': user_info[5] if user_info[5] else '',
           'review_num': review_num, 'review_score': review_score,
           'zipcode': user_info[7] if user_info[7] else '', 'country': user_info[8] if user_info[8] else '',
           'region': user_info[9] if user_info[9] else '', 'city': user_info[10] if user_info[10] else ''}

    return HttpResponse(json.dumps(ret), content_type='application/json')

@require_GET
def dummy_get_user_detail(request):
    user = {'isMe':False, 'followed':False, 'count_sellings':8, 'count_followers':10, 'count_following':23}
    return HttpResponse(json.dumps(user), content_type='application/json')

@require_GET
def get_user_sellings(request):
    seller_id = request.GET.get('seller_id', '')
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))

    raw_seller_id = r.get('fuzzy_id:%s:user_id' % seller_id)
    if not raw_seller_id:
        raise InvalidParameterError()

    try:
        # 在卖家列表中不显示审核不通过的商品
        item_ids = r.zrevrange('user:%s:user_selling_list' % raw_seller_id, offset, offset+limit-1)
        total_count = r.zcard('user:%s:user_selling_list' % raw_seller_id)
    except Exception:
        return HttpResponse(json.dumps([]), content_type='application/json')

    owner = get_user_from_id(raw_seller_id)
    items = []
    for item_id in item_ids:
        if not r.exists('item:%s' % item_id):
            continue

        try:
            item = r.hgetall('item:%s' % item_id)
            item['owner'] = owner
            item['images'] = json.loads(item['images'])
            item['location'] = json.loads(item['location'])

            items.append(item)
        except Exception:
            continue

    hits_count = len(items)
    ret = search._build_list_json(hits_count, items, limit, offset, total_count, list_type='sellings')

    return HttpResponse(json.dumps(ret), content_type='application/json')

@require_GET
def get_my_sellings(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))

    try:
        item_ids = r.zrevrange('user:%s:my_selling_list' % request.user_id, offset, offset+limit-1)
        total_count = r.zcard('user:%s:my_selling_list' % request.user_id)
    except Exception:
        return HttpResponse(json.dumps([]), content_type='application/json')

    owner = get_user_from_id(request.user_id)
    items = []
    for item_id in item_ids:
        try:
            item = r.hgetall('item:%s' % item_id)
            item['owner'] = owner
            item['images'] = json.loads(item['images'])
            item['location'] = json.loads(item['location'])
            # item['deletable'] = False if (r.llen('item:%s:offerlines' % item_id) > 0 or get_item_state(item_id) not in [ITEM_STATE_LISTING, ITEM_STATE_UNLISTED]) else True
            item['deletable'] = True    # 暂定为所有商品均可删除
            renew_ttl = _get_renew_ttl(item_id)
            if renew_ttl:
                item['renew_ttl'] = renew_ttl
            items.append(item)
        except Exception,e:
            logger.error('get_my_sellings error, msg:%s' % e)

    hits_count = len(items)
    ret = search._build_list_json(hits_count, items, limit, offset, total_count, list_type='category')

    return HttpResponse(json.dumps(ret), content_type='application/json')



@require_GET
def home_items(request):

    offset = int(request.GET.get('offset', 0))
    user_id = utils_web.get_user_id(request)

    # todo 需要切换到search 微服务
    items, ret = search_client.home_item_client(request)

    # 记录被搜索商品的曝光率
    _send_item_exposure(request, items, offset)

    if user_id:
        UserService().user_daily_checkin(user_id)

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def followings_items(request):

    following_ids = r.zrange('user:%s:following' % request.user_id, 0, -1)
    following_fuzzy_ids = []
    for following_id in following_ids:
        following_fuzzy_ids.append(r.hget('user:%s' % following_id, 'id'))

    # 获取用户following的用户的商品列表
    # 执行搜索
    items, ret = search_client.followings_items_client(request,following_fuzzy_ids)

    # 记录被搜索商品的曝光率
    offset = int(request.GET.get('offset', 0))
    _send_item_exposure(request, items, offset)

    return HttpResponse(json.dumps(ret), content_type='application/json')

@require_GET
def keyword_search(request):

    user_id = utils_web.get_user_id(request)

    keyword = request.GET.get('q', '')
    if keyword == '':
        raise InvalidParameterError()

    # todo 需要切换到search 微服务
    items, ret = search_client.keyword_items_client(request)

    merge_cats_name(ret)

    # 记录被搜索商品的曝光率
    offset = int(request.GET.get('offset', 0))
    _send_item_exposure(request, items, offset)

    if user_id:
        UserService().user_daily_checkin(user_id)

    return HttpResponse(json.dumps(ret), content_type='application/json')


def merge_cats_name(ret, query_type='' ,cat_id=-1):

    if ret['res_ext']:
        #logger.debug(ret['res_ext'])

        category_list = ret['res_ext'].get('refind_category',[])

        if len(category_list)==0 and query_type=="category":
            # 从分类进入，返回所有分类
            categories_tuple = CategoryService().get_categories()
            for it in categories_tuple:
                key = it['id']
                txt = it['title']
                category_list.append({"id":"%s"%key, "cnt":0, "title":txt})

        if len(category_list)==0 and cat_id>=0:
            category_list.append({"id":cat_id, "cnt":0})

        # merge 分类title
        categories_map = CategoryService().get_categories_map()
        for it in category_list:
            it['title'] = categories_map.get(int(it['id']),None)
        #if len(category_list) > 0:
        ret['res_ext']['refind_category'] = [{"id": -1, "cnt": 0, "title": "All (default)"}] + category_list
        #else:
        #    ret['res_ext']['refind_category'] = category_list

        #logger.debug(ret['res_ext'])

@require_GET
def category_items(request):

    cat_id = int(request.GET.get('cat_id', '-2'))
    if cat_id>-1:
        # 新旧版本的category_id映射，向下兼容
        cat_id = CategoryService().get_map_to_id(cat_id)

    # todo 需要切换到search 微服务
    items, ret = search_client.category_items_client(request, cat_id)

    # todo refind 中的 category list 在这里添加

    query_type='category'

    #logger.info(ret)
    merge_cats_name(ret ,query_type=query_type ,cat_id=cat_id)

    # 记录被搜索商品的曝光率
    offset = int(request.GET.get('offset', 0))
    _send_item_exposure(request, items, offset)

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def brand_items(request):

    cat_id = int(request.GET.get('cat_id', '-2'))
    if cat_id>-1:
        # 新旧版本的category_id映射，向下兼容
        cat_id = CategoryService().get_map_to_id(cat_id)

    # todo 需要切换到search 微服务
    items, ret = search_client.brand_items_client(request ,cat_id)

    merge_cats_name(ret,cat_id=cat_id)

    # 记录被搜索商品的曝光率
    offset = int(request.GET.get('offset', 0))
    _send_item_exposure(request, items, offset)

    return HttpResponse(json.dumps(ret), content_type='application/json')

'''
# lvyi 2015-3-4 该接口已经不使用
@require_GET
def search_items(request):
    total_count = 0
    is_num = True
    items = []
    v_timestamp = request.GET.get('timestamp')

    if v_timestamp is not None and v_timestamp != '':
        nums = string.digits
        for i in v_timestamp:
            if i not in nums:
                is_num = False
                break

        if is_num:
            created_at = long(v_timestamp)

            results = search_items_by_created_at(created_at)

            hits_count = len(results['hits']['hits'])
            total_count = results['hits']['total']
            items = []
            for hit in results['hits']['hits']:
                item = hit['_source']
                items.append(item)

    ret = {'meta':{'total_count':total_count}, 'objects':items}

    return HttpResponse(json.dumps(ret), content_type='application/json')
'''


@require_GET
def suggest_items(request):

    fuzzy_item_id = request.GET.get('item_id', '')
    #logger.debug("sg step 0== %s"% fuzzy_item_id)

    item_id = r.get('fuzzy_item_id:%s:item_id' % fuzzy_item_id)
    if not item_id:
        raise ItemNotFoundError(fuzzy_item_id)

    # title = r.hget('item:%s' % item_id, 'title')
    # cat_id = r.hget('item:%s' % item_id, 'cat_id')
    title, cat_id = r.hmget('item:%s' % item_id, 'title', 'cat_id')
    if len(title.decode('utf-8')) > 150:
        title = title.decode('utf-8')[:150].encode('utf-8')

    #print "title=%s" % title
    #print "cat_id=%s" % cat_id
    if not cat_id:
        cat_id = 0

    suggest_cache = search_client.suggest_items_client(request,item_id, fuzzy_item_id, title, cat_id)
    items = json.loads(suggest_cache)

    # 记录被搜索商品的曝光率
    offset = int(request.GET.get('offset', 0))
    _send_item_exposure(request, items, offset)

    #print "suggest_cache=%s" % suggest_cache
    return HttpResponse(suggest_cache, content_type='application/json')


@require_GET
def shake_item(request):

    fuzzy_item_id = request.GET.get('item_id', '')
    item_id = unfuzzy_item_id(fuzzy_item_id)

    if not item_id:
        raise ItemNotFoundError(fuzzy_item_id)

    title = r.hget('item:%s' % item_id, 'title')
    cat_id = r.hget('item:%s' % item_id, 'cat_id')
    if not cat_id:
        cat_id = 0

    recommended_item_map, shake_item_id = search_client.shake_item_client(request, cat_id, item_id, title)
    # print 'shake_item_id: %s' % shake_item_id

    # 记录被搜索商品的曝光率
    _send_item_exposure(request, [recommended_item_map[shake_item_id]] if shake_item_id in recommended_item_map else [])

    item = r.hgetall('item:%s' % shake_item_id)
    item['state'] = 0 if 'state' not in item or item['state'] is None else int(item['state'])

    try:
        item['liked'] = r.sismember('item:%s:likers' % shake_item_id, request.user_id)
        score = r.zscore('item:%s:new_likers' % item_id, request.user_id)
        item['liked'] = True if score else False
    except Exception:
        item['liked'] = False

    owner = r.get('item:%s:owner' % shake_item_id)
    item['owner'] = get_user_from_id(owner)
    item['images'] = json.loads(item['images'])
    item['location'] = json.loads(item['location'])
    item['isMine'] = owner == str(request.user_id)
    if 'user_id' in item:
        item.pop('user_id')

    item['offerLines'] = _assemble_offerlines(request.user_id, shake_item_id, item['isMine'])
    item['comment_count'] = r.llen('item:%s:comments' % shake_item_id)
    item['has_more_comments'] = item['comment_count'] > 5

    comment_ids = r.lrange('item:%s:comments' % shake_item_id, 0, 4)
    comments = []
    for comment_id in comment_ids:
        comment = r.hgetall('comment:%s' % comment_id)

        plain_author_id = comment['author'] if 'author' in comment else comment['authorID']
        comment['author'] = get_user_from_id(plain_author_id)

        if 'authorID' in comment:
            comment.pop('authorID')

        comments.append(comment)

    item['comments'] = comments

    return HttpResponse(json.dumps(item), content_type='application/json')

# 该方法已经废弃 lvyi
"""@require_POST
def post_comment(request):
    fuzzy_item_id = request.POST.get('item_id', '')
    item_id = unfuzzy_item_id(fuzzy_item_id)
    if not item_id:
        return HttpResponseBadRequest(_('item not exist'))

    text = request.POST.get('text', '')
    reply_to = request.POST.get('reply_to', '')

    comment_id = r.incr('global:next_comment_id')
    reply_to_comment = r.hgetall('comment:%s' % reply_to)
    if len(reply_to_comment) > 0:
        reply_to_author_id = reply_to_comment['author'] if 'author' in reply_to_comment else reply_to_comment['authorID']
        reply_to_author = r.hget('user:%s' % reply_to_author_id, 'nickname')
        text = '%s%s: %s' % (_('@'), reply_to_author, text)

    timestamp = get_now_timestamp_str()
    with r.pipeline() as pipe:
        pipe.hmset('comment:%d' % comment_id, {'id':comment_id, 'text':text, 'author':request.user_id, 'reply_to':reply_to, 'timestamp':timestamp})
        pipe.lpush('item:%s:comments' % item_id, comment_id)
        pipe.execute()

    from_name = r.hget('user:%s' % request.user_id, 'nickname')
    push_message = '%s: %s' % (from_name, text)
    if len(reply_to_comment) > 0:
        send_message_to_user(reply_to_author_id, push_message, fuzzy_item_id)
    else:
        item_seller = r.get('item:%s:owner' % item_id)
        send_message_to_user(item_seller, push_message, fuzzy_item_id)

    return HttpResponse(json.dumps({}), content_type='application/json', status=201)
"""


@require_POST
def delete_item(request):
    """
    用户删除商品
    :param request:
    :return:
    """
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))

    if not item_id:
        raise NotFoundError()
    elif not r.exists('item:%s' % item_id):
        #delete_item_in_searchengine(int(item_id))
        raise NotFoundError()

    owner_id = r.get('item:%s:owner' % item_id)
    if owner_id != str(request.user_id):
        raise InvalidOperationError()

    # if get_item_state(item_id) not in [ITEM_STATE_LISTING, ITEM_STATE_UNLISTED]:
    #     return HttpResponseBadRequest(json.dumps({'message': _('This item must be listing or unlisted')}), content_type='application/json')

    # if r.llen('item:%s:offerlines' % item_id) > 0:
    #     return HttpResponseBadRequest(json.dumps({'message': _('You are not able to delete this item.')}), content_type='application/json')

    _delete_item(request.user_id, item_id)

    send_item_to_boss.delay(item_id, item_action='Delete')

    return HttpResponse()


def unlist_item_byid(item_id, user_id):
    if not r.exists('item:%s' % item_id):
        raise NotFoundError()


    owner_id = r.get('item:%s:owner' % item_id)
    if owner_id != str(user_id):
        raise InvalidOperationError()

    with r.pipeline() as pipe:
        pipe.hmset('item:%s' % item_id, {'state': ITEM_STATE_UNLISTED, 'modified_at': get_now_timestamp()})
        pipe.zrem('user:%s:user_selling_list' % user_id, item_id)
        pipe.execute()
    delete_item_in_searchengine(item_id)
    UserService().update_item_num(user_id)
    items = Item.objects.filter(pk=item_id)
    if items:
        items[0].state = ITEM_STATE_UNLISTED
        items[0].modified_at = get_utcnow()
        items[0].save()


@require_POST
def unlist_item(request):
    """
    用户下架商品
    :param request:
    :return:
    """
    fuzzy_item_id = request.POST.get('item_id', '')
    user_id = request.user_id

    item_id = unfuzzy_item_id(fuzzy_item_id)
    if not item_id:
        raise NotFoundError()

    if get_item_state(item_id) != ITEM_STATE_LISTING:
        raise InvalidStateError()

    ItemService().unlist_item(item_id, user_id=user_id, is_check_owner=True)

    send_item_to_boss.delay(item_id, item_action='Unlist')

    return HttpResponse()


@require_POST
def relist_item(request):
    """
    重新上架商品
    :param request: item_id
    :return:
    """
    fuzzy_item_id = request.POST.get('item_id', '')
    item_id = unfuzzy_item_id(fuzzy_item_id)

    if not item_id:
        raise NotFoundError()
    elif not r.exists('item:%s' % item_id):
        delete_item_in_searchengine(int(item_id))
        raise NotFoundError()

    owner_id = r.get('item:%s:owner' % item_id)
    if owner_id != str(request.user_id):
        raise InvalidOperationError()

    prev_state = get_item_state(item_id)
    if prev_state not in [ITEM_STATE_UNLISTED, ITEM_STATE_SOLD]:
        raise InvalidStateError()

    if prev_state == ITEM_STATE_SOLD:
        # 模拟卖家给所有买家发送内容为商品重新上架的假聊天消息
        # item_title = r.hget('item:%s' % item_id, 'title')
        # text = 'I have relisted my %s, are you still interested in it?' % item_title.decode('utf-8')
        text = _('This item is avaliable again! Are you interested? Let me know.')
        _send_msg_to_buyers(item_id, request.user_id, text, request.app_version)

    ItemService().reindex_item(item_id)
    with r.pipeline() as pipe:
        pipe.hmset('item:%s' % item_id, {'state': ITEM_STATE_LISTING, 'modified_at': get_now_timestamp()})
        pipe.zadd('user:%s:user_selling_list' % request.user_id, item_id, get_now_timestamp())
        pipe.execute()

    UserService().update_item_num(request.user_id)

    item = Item.objects.get(pk=item_id)
    if item:
        item.state = ITEM_STATE_LISTING
        item.modified_at = get_utcnow()
        item.save()

    send_item_to_boss.delay(item_id, item_action='Relist')

    return HttpResponse()


@require_POST
def mark_sold(request):
    """
    标记商品为已卖出
    :param request: item_id - fuzzy item ID
    :return:
    """
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))

    if not item_id:
        raise NotFoundError()
    elif not r.exists('item:%s' % item_id):
        delete_item_in_searchengine(int(item_id))
        raise NotFoundError()

    owner_id = r.get('item:%s:owner' % item_id)
    if owner_id != str(request.user_id):
        raise InvalidOperationError()

    item_state = get_item_state(item_id)
    if item_state not in [ITEM_STATE_LISTING, ITEM_STATE_UNLISTED]:
        raise InvalidStateError()

    # [issue#193 wangguanglei 2015-05-18]下架产品标记为Sold时需要在搜索引擎重建索引,一边Home和Seller Page可以看到该商品
    now_ts = get_now_timestamp()
    r.hmset('item:%s' % item_id, {'state': ITEM_STATE_SOLD, 'modified_at': now_ts})

    # 如果是下架产品,在MarkSold时,
    if item_state == ITEM_STATE_UNLISTED:
        # 1.加入Seller Page列表
        r.zadd('user:%s:user_selling_list' % owner_id, item_id, now_ts)
        UserService().update_item_num(owner_id)
        # 2.ES加入搜索索引,使其可以在Home页搜索到
        ItemService().reindex_item(item_id)

    # [issue#193 end]

    update_field_in_searchengine(int(item_id), 'state', ITEM_STATE_SOLD)
    delete_item_in_searchengine.apply_async((int(item_id),), eta=datetime.datetime.utcnow() + timedelta(days=settings.SOLD_ITEMS_HOLD_DAYS), queue='celery.schedule')

    UserService().update_item_num(request.user_id)

    item = Item.objects.get(pk=item_id)
    if item:
        item.state = ITEM_STATE_SOLD
        item.modified_at = get_utcnow()
        item.save()

    # 模拟卖家给所有买家发送内容为商品已卖出的假聊天消息
    item_title = r.hget('item:%s' % item_id, 'title')
    text = _('I have sold my %s. Thank you for your interest.' % item_title.decode('utf-8'))
    _send_msg_to_buyers(item_id, request.user_id, text, request.app_version)

    # 通知喜欢的买家该商品已卖出
    notify.delay(None, 'TRI_SOLD_LIKER', {'item_id': str(item_id)}, owner_id)

    send_item_to_boss.delay(item_id, item_action='MarkasSold')

    return HttpResponse()


@require_POST
def renew_item(request):
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))
    owner_id = r.get('item:%s:owner' % item_id)
    if owner_id != str(request.user_id):
        raise InvalidOperationError()

    if r.exists('item:%s:renew_countdown' % item_id) and _get_renew_ttl(item_id) > 60:
        # renew倒计时尚未到期（为解决客户端计时器可能与服务器端不同步的问题，如果剩余时间不到60秒，也允许renew）
        raise InvalidOperationError()

    renew_ttl = _renew_countdown(item_id)
    renew_ts = get_now_timestamp()
    update_field_in_searchengine(item_id, 'updated_at', renew_ts)
    r.hset('item:%s' % item_id, 'updated_at', renew_ts)
    Item.objects.filter(pk=item_id).update(renewed_at=get_utcnow())

    return HttpResponse(json.dumps({'renew_ttl': renew_ttl, 'updated_at': renew_ts}), content_type='application/json')


@require_GET
def heartbeat(request):
    timestamp = get_now_timestamp()
    r.hset('user:%s' % request.user_id, 'last_heartbeat_at', timestamp)
    # user = User.objects.get(pk=request.user_id)
    # user.save()
    User.objects.filter(id=request.user_id).update(last_heartbeat_at=datetime.datetime.utcnow().replace(tzinfo=utc))
    return HttpResponse()


@require_GET
def get_promotion_ranking(request):
    key = 'promotion:post_item_num:charts'
    promotion_item_num_timestamp_key = 'promotion:post_item_num:time:charts'
    post_item_infos = r.zrevrange(key, 0, settings.PROMOTION_POST_ITEM_USER_DISPLAY_NUM - 1, withscores=True)

    top_datas = []
    if post_item_infos:
        # 找出上传商品数量一样的用户
        user_same_map = {}
        prev_user_post_item_num = 0
        prev_user_post_item_user_id = 0

        for post_item_info in post_item_infos:
            user_id = post_item_info[0]
            # 上传的商品数
            post_item_num = post_item_info[1]
            if prev_user_post_item_num == post_item_num:
                prev_score_timestamp = r.zscore(promotion_item_num_timestamp_key, prev_user_post_item_user_id)
                score_timestamp = r.zscore(promotion_item_num_timestamp_key, user_id)
                user_same_map[prev_user_post_item_user_id] = prev_score_timestamp
                user_same_map[user_id] = score_timestamp
            else:
                prev_user_post_item_num = post_item_num
                prev_user_post_item_user_id = user_id

        for post_item_info in post_item_infos:
            user_id = post_item_info[0]
            # 上传的商品数
            post_item_num = post_item_info[1]
            # 头像
            user_info = r.hmget('user:%s' % user_id, 'id', 'nickname', 'portrait')
            timestamp = 0
            #
            if user_id in user_same_map:
                timestamp = user_same_map[user_id]

            top_datas.append({'fuzzy_user_id': user_info[0], 'nickname': user_info[1], 'portrait': user_info[2], 'item_num': post_item_num, 'timestamp': timestamp})

        # 如果存在上传商品数量相同的用户,则需要对用户按照上传时间重新进行排序
        if len(user_same_map.keys()) > 0:
            loop = len(top_datas)

            for i in range(0, loop):
                i_rank_data = top_datas[i]
                i_timestamp = i_rank_data['timestamp']
                i_item_num = i_rank_data['item_num']

                current_timestamp = i_timestamp
                current_item_num = i_item_num
                for j in range(i+1, loop):
                    j_rank_data = top_datas[j]
                    j_timestamp = j_rank_data['timestamp']
                    j_item_num = j_rank_data['item_num']

                    if current_timestamp != 0 and j_timestamp != 0 and current_item_num == j_item_num:
                        if j_timestamp < current_timestamp:

                            current_timestamp = top_datas[j]['timestamp']
                            current_item_num = top_datas[j]['item_num']
                            top_data_tmp = top_datas[i]
                            top_datas[i] = top_datas[j]
                            top_datas[j] = top_data_tmp

    return HttpResponse(json.dumps({'total': len(top_datas), 'users': top_datas}), content_type='application/json', status=200)

@require_GET
def get_red_package(request):

    current_time_stamp = get_now_timestamp()
    current_time = get_timestamp_str2(long(current_time_stamp), '%Y-%m-%d %H:%M:%S')
    zone_current_time = get_zone_date(current_time, settings.PROMOTION_POST_ITEM_ZONE_TIME, '%Y-%m-%d %H:%M:%S')
    yesterday = datetime.datetime.strptime(zone_current_time, '%Y-%m-%d %H:%M:%S') - datetime.timedelta(days=1)
    yesterday_date = yesterday.strftime('%Y-%m-%d')

    red_package_key = 'promotion:red_package_five:charts'
    red_package_day_key = 'promotion:red_package_five:%s:charts' % yesterday_date

    user_num = r.zcard(red_package_key)
    user_ids = r.zrevrange(red_package_day_key, 0, -1)
    users = []
    if user_ids:
        for user_id in user_ids:
            user_infos = r.hmget('user:%s' % user_id, 'id', 'nickname', 'portrait')

            users.append({'fuzzy_user_id': user_infos[0], 'nickname': user_infos[1], 'portrait': user_infos[2]})

    data = {'total': settings.PROMOTION_POST_ITEM_FIVE_RED_PACKAGE_TOTAL_NUM, 'user_num': user_num,
            'surplus_num': settings.PROMOTION_POST_ITEM_FIVE_RED_PACKAGE_TOTAL_NUM - user_num, 'users': users}

    return HttpResponse(json.dumps(data), content_type='application/json', status=200)

@require_GET
def get_promotion_items(request):

    five_key = 'promotion:post_item:charts'

    item_ids = r.zrevrange(five_key, 0, 30)
    items = []
    if item_ids:
        for item_id in item_ids:
            item_infos = r.hmget('item:%s' % item_id, 'images', 'id')
            images = item_infos[0]
            image = ''
            if images:
                image_json = json.loads(images)
                if image_json and len(image_json) > 0:
                    image = image_json[0]['imageLink'] if 'imageLink' in image_json[0] else ''
            items.append({'fuzzy_item_id': item_infos[1], 'image': image})

    data = {'total': len(item_ids), 'items': items}

    return HttpResponse(json.dumps(data), content_type='application/json', status=200)


def _delete_item(user_id, item_id):
    with r.pipeline() as pipe:
        pipe.hset('item:%s' % item_id, 'state', ITEM_STATE_UNAVAILABLE)
        pipe.zrem('user:%s:my_selling_list' % user_id, item_id)
        pipe.zrem('user:%s:user_selling_list' % user_id, item_id)
        pipe.execute()

    delete_item_in_searchengine(item_id)
    UserService().update_item_num(user_id)

    try:
        item = Item.objects.get(pk=item_id)
        if item:
            item.state = ITEM_STATE_UNAVAILABLE
            item.modified_at = get_utcnow()
            item.save()
    except Item.DoesNotExist, e:
        logger.error('Item not found: %s , excepiton: %s' % (item_id, e.message))


@require_POST
def web_signup_email(request):
    """
    For synchronize the messages and offers between web and app.
    Create the user by the email with a random generated password.
    This api also creates a new 'offerline' for the communication between the saler and the buyer.
    :param request:
    :return: The user detail in json format
    """

    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    email = request.POST.get('email', '').lower()
    password = generate_temp_password()
    obfuscated_password = make_password(password, settings.PASSWORD_SALT, settings.PASSWORD_ALGORITHM)
    nickname = request.POST.get('nickname', '')
    time_zone = request.POST.get('timezone', '')
    ip_address = get_real_ip(request)

    try:
        validate_email(email)
    except ValidationError:
        raise InvalidParameterError()

    if nickname == '':
        raise InvalidParameterError()

    user_id = r.get('login:email:%s:user_id' % email)
    if not user_id:
        result = UserService().signup(signup_type='email', nickname=nickname, lat=0, lon=0, ip_address=ip_address,
                                      email=email, password=obfuscated_password, send_password=True,
                                      os=request.os, os_version=request.os_version, app_version=request.app_version,
                                      device=request.device, time_zone=time_zone)
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        raise PinnacleApiError(ERR_CODE_EMAIL_EXISTS)


@require_GET
def get_featured_items(request):
    featured_items_id = int(request.GET.get('featured_items_id', 0))
    if featured_items_id == 0:
        raise InvalidParameterError()

    try:
        featured_items = FeaturedItems.objects.get(pk=featured_items_id)
    except FeaturedItems.DoesNotExist:
        raise InvalidParameterError()

    items = []
    for item_id in featured_items.items.split(','):
        doc = search_item_by_id(item_id)
        hits = doc['hits']['hits']
        if len(hits) == 0:
            continue

        items.append(hits[0]['_source'])

    result = {'title': featured_items.title, 'banner_url': featured_items.banner_url,
              'banner_width': featured_items.banner_width, 'banner_height': featured_items.banner_height,
              'items': items}

    return HttpResponse(json.dumps(result), content_type='application/json')


@require_POST
@validate_user
def send_passcode(request):
    """
    认证手机号：发送验证码
    :param request:
    :return:
    """
    phone = request.POST.get('phone', 0)
    if phone == 0:
        raise InvalidParameterError()

    if User.objects.filter(mobile_phone=phone).exists():
        raise PinnacleApiError(ERR_CODE_PHONE_NUMBER_BOUND)

    pass_code = r.get('verification:phone:%s:pass_code')
    if not pass_code:
        pass_code = generate_temp_passcode(4)
        r.set('verification:phone:%s:pass_code' % phone, pass_code, 30*60)
        r.set('verification:pass_code:%s:phone' % pass_code, phone, 30*60)

    send_passcode_to_phone(phone, pass_code)
    return HttpResponse()


@require_POST
def verify_passcode(request):
    """
    认证手机号：验证验证码
    :param request:
    :return:
    """
    phone = request.POST.get('phone', '')
    pass_code = request.POST.get('pass_code', '')

    if phone == r.get('verification:pass_code:%s:phone' % pass_code) \
            and pass_code == r.get('verification:phone:%s:pass_code' % phone):
        user = User.objects.get(pk=request.user_id)
        user.mobile_phone = phone
        user.save()
        r.hmset('user:%s' % request.user_id, {'mobile_phone': phone, 'verified': True})
        ItemService().update_es_verification_by_user(request.user_id)

        # 用户认证成功手机时，如果没有使用过App(Web用户)，发短信
        if not is_app_user(request.user_id):
            send_sms.delay(target_user=request.user_id, template='TMPL_SMS_WELCOME')

        return HttpResponse(json.dumps({'message': _('Thanks for verifying your phone number')}), content_type='application/json', status=200)
    else:
        raise PinnacleApiError(ERR_CODE_INVALID_PASSCODE)


@require_GET
def get_categories(request):
    """
    获取品类列表
    :param request:
    :return:
    """
    categories = CategoryService().get_categories(language_code=request.LANGUAGE_CODE)
    ret = {'meta': {'total_count': len(categories)},
           'objects': categories}

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def init(request):
    """
    app启动或从后台切换到前台运行时调用，获取全局配置
    :param request:
    :return:
    """
    ret = {'switchers': settings.SWITCHERS, 'api_version': settings.API_VERSION}
    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_POST
def update_location(request):
    lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
    lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
    country = request.POST.get('country', '')
    region = request.POST.get('region', '')
    city = request.POST.get('city', '')

    # 如果用户设置了zipcode,直接返回成功
    zipcode = r.hget('user:%s' % request.user_id, 'zipcode')
    if zipcode:
        logger.info('User %s set zipcode to %s, ignore location update...' % (request.user_id, zipcode))
        return HttpResponse()

    if request.os.lower() == 'android':
        update_user_current_device.delay(request.user_id, request.os, request.os_version,
                                         request.app_version, request.device)

    # 实现更新位置接口
    #logger.debug('update_location: lat: %f, lon: %f, country: %s, region: %s, city: %s' %
    #             (lat, lon, country, region, city))
    kwargs = {'zipcode': '', 'lat': lat, 'lon': lon, 'country': country, 'region': region, 'city': city}
    UserService().fill_user_detail(request.user_id, **kwargs)

    return HttpResponse()

@require_GET
def search_poi(request):
    q = request.GET.get('q', '')
    logger.debug('search_poi: %s' % q)
    return HttpResponse()


def _send_item_exposure(request, items, offset=0):

    try:
        api_path = request.path_info.lstrip('/')
        ip_address = get_real_ip(request)
        user_agent = request.META.get('HTTP_USER_AGENT')
        utc_date = get_now_timestamp_str2()
        ext_params = None
        if items and len(items) > 0:
            try:
                user_id = request.user_id
            except:
                user_id = ''

            if settings.ANALYTICS_NEW_API_LOG_SWITCH:
                AnalyticsService().record_item_exposure(request, items, offset)
            else:
                record_item_exposure.apply_async((user_id, api_path, ip_address, items, offset, user_agent, utc_date, ext_params), countdown=2, queue='celery.log.item_exposure')
        else:
            logger.error('[%s] - No exposure items' % api_path)
    except Exception, e:
        logger.error(e)


def _format_currency(value, currency_code):
    return numbers.format_currency(value, currency_code, locale='en_US')


def _get_gender(request):
    gender = 0

    #lvyi 15-2-7 该数据库查询逻辑现在不用, 注释
    '''
    if hasattr(request, 'user_id'):
        try:
            user = User.objects.get(pk=request.user_id)
            gender = user.gender
        except ObjectDoesNotExist, e:
            logger.warn('user not found: %s, exception: %s' % (request.user_id, e.message))
    '''
    return gender


def _renew_countdown(item_id):
    key = 'item:%s:renew_countdown' % item_id
    r.set(key, '', timedelta(minutes=settings.ITEM_RENEW_INTERVAL))
    return r.ttl(key)


def _get_renew_ttl(item_id):
    return r.ttl('item:%s:renew_countdown' % item_id)

def _send_msg_to_buyers(item_id, user_id, text, app_version=None):
    """
    模拟卖家给所有买家发送假聊天消息
    :param item_id:
    :param user_id:
    :param text:
    :return:
    """
    timestamp = get_now_timestamp()
    offerline_ids = r.lrange('item:%s:offerlines' % item_id, 0, -1)
    for offerline_id in offerline_ids:
        to_user = r.hget("offerline:%s:meta" % offerline_id, 'buyer_id')

        if user_service.is_blocked(to_user, user_id):
            continue

        offer_id = r.incr('global:next_offer_id')
        with r.pipeline() as pipe:
            pipe.hmset('offer:%s' % offer_id,
                       {'id': offer_id, 'type': 2, 'from': user_id, 'to': to_user, 'price': 0,
                        'local_price': '', 'text': text, 'timestamp': get_now_timestamp_str()})
            pipe.set('offer:%s:offerline_id' % offer_id, offerline_id)
            pipe.zadd('offerline:%s:entries' % offerline_id, offer_id, timestamp)
            pipe.execute()

        target_inbox_key = 'user:%s:inbox:buying' % to_user if str(user_id) == r.get(
            'item:%s:owner' % item_id) else 'user:%s:inbox:selling' % to_user
        me_inbox_key = 'user:%s:inbox:selling' % user_id if str(user_id) == r.get(
            'item:%s:owner' % item_id) else 'user:%s:inbox:buying' % user_id

        push_message_to_user_inbox.delay(target_inbox_key, text, user_id, to_user, item_id, offerline_id,
                                         timestamp, text, text)
        record_message_in_user_inbox.delay(me_inbox_key, text, user_id, to_user, item_id, offerline_id,
                                           timestamp, app_version)


@require_POST
def deactive_account(request):
    """
    用户执行deactive操作后,
    1.不再以任何方式向用户推送信息;
    2.用户所有在售(listing)商品全部下架;
    3.用户不再出现在卖家推荐里面(商品下架后自然满足);
    4.用户可以用原账号密码再次登录,则用户被重新激活
    :param request:
    :return:
    """
    deactive_comment = request.POST.get('comment', '')
    # 当前用户所有Listing商品下架
    try:
        item_ids = r.zrange('user:%s:user_selling_list' % request.user_id, 0, -1)
    except Exception:
        raise InvalidOperationError()

    for item_id in item_ids:
        if not r.exists('item:%s' % item_id):
            continue

        if get_item_state(item_id) != ITEM_STATE_LISTING:
            continue

        ItemService().unlist_item(item_id, user_id=request.user_id, is_check_owner=True)

    # 用户去活操作(用户下次登录之前不再有任何活跃数据)
    UserService().deactive_user(request.user_id, deactive_comment)
    return HttpResponse()
