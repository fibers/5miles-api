"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test import Client
from django.conf import settings
from .models import Category

import json
import redis

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

class SimpleTest(TestCase):
    def setUp(self):
        super(SimpleTest, self).setUp()
        self.client = Client()

    def test_login_sms(self):
        response = self.client.post('/api/v2/login_sms/', {'cell_number':'+8613552472705'})
        self.assertEqual(response.status_code, 200)

    def test_verify_code(self):
        cell_number = '+8613552472705'
        pass_code = r.get('auth:cellphone:%s:pass_code' % cell_number)

        response = self.client.post('/api/v2/verify_code/', {'cell_number':cell_number, 'pass_code':pass_code})
        #print response
        self.assertEqual(response.status_code, 200)

        results = json.loads(response.content)
        self.assertEqual('token' in results, True)

    #def test_login_facebook(self):
        #response = self.client.post('/api/v2/login_facebook/', {'cell_number':'+8613552472705'})
        #self.assertEqual(response.status_code, 200)


class CategoryTest(TestCase):
    def setUp(self):
        Category.objects.create(id=1, title='Furniture', slug='', order_num=0, status=2)
        Category.objects.create(id=2, title='Household', slug='', order_num=1, status=2)
        Category.objects.create(id=3, title='Electronics & Computers', slug='', order_num=2, status=2)
        self.client = Client()

    def tearDown(self):
        pass

    def test_get_categories(self):
        response = self.client.get('/api/v2/categories/', HTTP_X_FIVEMILES_USER_ID=1, HTTP_X_FIVEMILES_USER_TOKEN='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMSJ9.iVfT97xBZE7fY-ehhezCpK3KYifXAjvQ-o9gOmhkhps')
        self.assertEquals(response.status_code, 200)
        results = json.loads(response.content)
        self.assertEquals(len(results['objects']), 3)
