# -*-coding: utf-8 -*-

from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.cache import cache_page
from django.utils.translation import ugettext as _
from common.utils import get_i18n_message
from entity.exceptions import InvalidParameterError, NotFoundError
from .models import *
from review.models import Review
from .utils import *

import json

from common.biz import *
from boss.tasks import send_report_to_boss

@require_POST
def report_bad_item(request):
    item_id = request.POST.get('item_id', '')
    if len(item_id) == 0:
        raise NotFoundError()

    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))
    if not item_id:
        raise NotFoundError()

    reason = int(request.POST.get('reason', -1))
    reason_content = request.POST.get('reason_content', '')

    record = ItemReportRecord(reporter_id=request.user_id, item_id=item_id, reason=reason, reason_content=reason_content)
    record.save()
    send_report_to_boss.delay(report_type='item', report_id=record.id)

    return HttpResponse()


@require_POST
def report_bad_user(request):
    user_id = request.POST.get('user_id', '')
    if len(user_id) == 0:
        raise NotFoundError()

    user_id = unfuzzy_user_id(request.POST.get('user_id', ''))
    if not user_id:
        raise NotFoundError()

    reason = int(request.POST.get('reason', -1))
    reason_content = request.POST.get('reason_content', '')

    record = UserReportRecord(reporter_id=request.user_id, user_id=user_id, reason=reason, reason_content=reason_content)
    record.save()
    send_report_to_boss.delay(report_type='user', report_id=record.id)

    return HttpResponse()


@require_POST
def report_review(request):
    review_id = int(request.POST.get('review_id', 0))
    if review_id == 0:
        raise InvalidParameterError()

    review = Review.objects.get(pk=review_id)
    reason = int(request.POST.get('reason', -1))
    reason_content = request.POST.get('reason_content', '')
    record = ReviewReportRecord(reporter_id=request.user_id, reviewer_id=review.reviewer_id, review_id=review.id,
                                reason=reason, reason_content=reason_content)
    record.save()
    send_report_to_boss.delay(report_type='review', report_id=record.id)

    return HttpResponse()


@require_GET
@cache_page(60 * 60)
def get_report_reasons(request):
    """
    获取举报原因列表
    :param request: report_type: 0 -> report item, 1 -> report user, 2 -> report review
    :return:
    """
    report_type = int(request.GET.get('type', -1))
    if report_type == -1:
        objects = ReportReason.objects.all()
    else:
        objects = ReportReason.objects.filter(report_type=report_type)

    results = []
    for obj in objects:
        reason_content = obj.reason_content
        msg_id = obj.report_content_i18n_msg_id
        if msg_id and msg_id != -1:
            reason_content = get_i18n_message(msg_id, request.LANGUAGE_CODE)

        for entry in results:
            if entry['type'] == obj.report_type:
                entry['objects'].append({'id': obj.id, 'reason_content': reason_content})
                break
        else:
            results.append({'type': obj.report_type, 'objects': [{'id': obj.id, 'reason_content': reason_content}]})

    return HttpResponse(json.dumps(results), content_type='application/json')

