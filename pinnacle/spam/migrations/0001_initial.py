# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ItemReport'
        db.create_table(u'spam_itemreport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('reporter_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('reason', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'spam', ['ItemReport'])

        # Adding model 'PersonReport'
        db.create_table(u'spam_personreport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('person_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('reporter_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('reason', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'spam', ['PersonReport'])


    def backwards(self, orm):
        # Deleting model 'ItemReport'
        db.delete_table(u'spam_itemreport')

        # Deleting model 'PersonReport'
        db.delete_table(u'spam_personreport')


    models = {
        u'spam.itemreport': {
            'Meta': {'object_name': 'ItemReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.personreport': {
            'Meta': {'object_name': 'PersonReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['spam']