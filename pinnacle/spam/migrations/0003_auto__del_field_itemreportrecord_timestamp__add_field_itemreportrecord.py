# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ItemReportRecord.timestamp'
        db.delete_column(u'spam_itemreportrecord', 'timestamp')

        # Adding field 'ItemReportRecord.is_processed'
        db.add_column(u'spam_itemreportrecord', 'is_processed',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'ItemReportRecord.updated_at'
        db.add_column(u'spam_itemreportrecord', 'updated_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ItemReportRecord.created_at'
        db.add_column(u'spam_itemreportrecord', 'created_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'ItemReportRecord.timestamp'
        raise RuntimeError("Cannot reverse this migration. 'ItemReportRecord.timestamp' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'ItemReportRecord.timestamp'
        db.add_column(u'spam_itemreportrecord', 'timestamp',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True),
                      keep_default=False)

        # Deleting field 'ItemReportRecord.is_processed'
        db.delete_column(u'spam_itemreportrecord', 'is_processed')

        # Deleting field 'ItemReportRecord.updated_at'
        db.delete_column(u'spam_itemreportrecord', 'updated_at')

        # Deleting field 'ItemReportRecord.created_at'
        db.delete_column(u'spam_itemreportrecord', 'created_at')


    models = {
        u'spam.itemreport': {
            'Meta': {'object_name': 'ItemReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.itemreportrecord': {
            'Meta': {'object_name': 'ItemReportRecord'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'item_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'spam.personreport': {
            'Meta': {'object_name': 'PersonReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.userreportrecord': {
            'Meta': {'object_name': 'UserReportRecord'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['spam']