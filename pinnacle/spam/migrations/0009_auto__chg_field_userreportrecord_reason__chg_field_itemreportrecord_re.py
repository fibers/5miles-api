# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'UserReportRecord.reason'
        db.alter_column(u'spam_userreportrecord', 'reason', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'ItemReportRecord.reason'
        db.alter_column(u'spam_itemreportrecord', 'reason', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'ReviewReportRecord.reason'
        db.alter_column(u'spam_reviewreportrecord', 'reason', self.gf('django.db.models.fields.IntegerField')())

    def backwards(self, orm):

        # Changing field 'UserReportRecord.reason'
        db.alter_column(u'spam_userreportrecord', 'reason', self.gf('django.db.models.fields.PositiveIntegerField')())

        # Changing field 'ItemReportRecord.reason'
        db.alter_column(u'spam_itemreportrecord', 'reason', self.gf('django.db.models.fields.PositiveIntegerField')())

        # Changing field 'ReviewReportRecord.reason'
        db.alter_column(u'spam_reviewreportrecord', 'reason', self.gf('django.db.models.fields.PositiveIntegerField')())

    models = {
        u'spam.itemreport': {
            'Meta': {'object_name': 'ItemReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.itemreportrecord': {
            'Meta': {'object_name': 'ItemReportRecord'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'item_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'reason': ('django.db.models.fields.IntegerField', [], {}),
            'reason_content': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'reply_content': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'blank': 'True'}),
            'reporter_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'spam.personreport': {
            'Meta': {'object_name': 'PersonReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reason': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reporter_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.reportreason': {
            'Meta': {'object_name': 'ReportReason'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason_content': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report_type': ('django.db.models.fields.SmallIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.reviewreportrecord': {
            'Meta': {'object_name': 'ReviewReportRecord'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reason': ('django.db.models.fields.IntegerField', [], {}),
            'reason_content': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'reply_content': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'blank': 'True'}),
            'reporter_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'review_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reviewer_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'spam.userreportrecord': {
            'Meta': {'object_name': 'UserReportRecord'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reason': ('django.db.models.fields.IntegerField', [], {}),
            'reason_content': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'reply_content': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'blank': 'True'}),
            'reporter_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['spam']