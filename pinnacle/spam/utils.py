
from tornado import gen, httpclient
from tornado.httputil import HTTPHeaders
import urllib
import json

from commerce.utils import get_now_timestamp_str

CMS_SERVER = '54.79.125.152:3000'

def record_item_report(reporter_id, item_id, reason_id):
    client = httpclient.HTTPClient()
    ts = _get_now_timestamp_str()
    args = {'reporter_id':reporter_id, 'product_id':item_id, 'is_processed':0, 'reason':reason_id, 'created_at':ts, 'updated_at':ts}
    try:
        request = httpclient.HTTPRequest(
                url="http://%s/api/product_reports/" % CMS_SERVER,
                method="POST",
                headers=HTTPHeaders({"Content-Type":'application/json'}),
                body=json.dumps(args))

        response = client.fetch(request)
    except Exception, e:
        print e

    client.close()

def _get_now_timestamp_str():
    from datetime import datetime

    now = datetime.utcnow()
    timestamp = now.strftime('%Y-%m-%d %H:%M:%S')

    return timestamp

def record_person_report(reporter_id, person_id, reason_id):
    client = httpclient.HTTPClient()
    ts = _get_now_timestamp_str()
    args = {'reporter_id':reporter_id, 'user_id':person_id, 'is_processed':0, 'reason':reason_id, 'created_at':ts, 'updated_at':ts}
    try:
        request = httpclient.HTTPRequest(
                url="http://%s/api/user_reports/" % CMS_SERVER,
                method="POST",
                headers=HTTPHeaders({"Content-Type":'application/json'}),
                body=json.dumps(args))

        response = client.fetch(request)
    except Exception, e:
        print e

    client.close()

def _get_now_timestamp_str():
    from datetime import datetime

    now = datetime.utcnow()
    timestamp = now.strftime('%Y-%m-%d %H:%M:%S')

    return timestamp
