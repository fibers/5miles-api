from django.db import models


class ItemReport(models.Model):
    item_id = models.PositiveIntegerField()
    reporter_uid = models.PositiveIntegerField()
    reason = models.PositiveIntegerField()
    timestamp = models.DateTimeField(auto_now=True)


class PersonReport(models.Model):
    person_uid = models.PositiveIntegerField()
    reporter_uid = models.PositiveIntegerField()
    reason = models.PositiveIntegerField()
    timestamp = models.DateTimeField(auto_now=True)


class ItemReportRecord(models.Model):
    reporter_id = models.PositiveIntegerField()
    item_id = models.CharField(max_length=20)
    reason = models.IntegerField()
    is_processed = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    reason_content = models.CharField(max_length=255, blank=True, default='')
    reply_content = models.CharField(max_length=512, blank=True, default='')


class UserReportRecord(models.Model):
    reporter_id = models.PositiveIntegerField()
    user_id = models.CharField(max_length=20)
    reason = models.IntegerField()
    is_processed = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    reason_content = models.CharField(max_length=255, blank=True, default='')
    reply_content = models.CharField(max_length=512, blank=True, default='')


class ReportReason(models.Model):
    reason_content = models.CharField(max_length=255)
    # report type 0: report item, 1: report user, 2: report review
    report_type = models.SmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    report_content_i18n_msg_id = models.IntegerField(null=True, default=-1)


class ReviewReportRecord(models.Model):
    reporter_id = models.PositiveIntegerField()
    review_id = models.PositiveIntegerField()
    reviewer_id = models.CharField(max_length=20)
    reason = models.IntegerField()
    is_processed = models.BooleanField(default=False)
    reason_content = models.CharField(max_length=255, blank=True, default='')
    reply_content = models.CharField(max_length=512, blank=True, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
