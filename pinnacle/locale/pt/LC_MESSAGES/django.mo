��    $      <  5   \      0  $   1     V      m     �     �  *   �     �  
   �     �          +     9     J     a     m     �  	   �     �  �   �  H   ]  &   �  )   �  
   �       +     J   F     �     �  %   �  +   �     �  v     -   �  S   �  V   	  j  `  '   �	     �	     

     *
     B
  7   V
     �
  	   �
     �
     �
     �
     �
                ,     D  	   T     ^  �   u  b   /  .   �  +   �  
   �     �  @     P   P     �     �  $   �  1   �       �   .  +   �  Z   �  b   ;            $                                                      	      "      !      #                                                   
                                             %(nickname)s offered %(local_price)s %(nickname)s: %(text)s *5miles* Your login passcode: %s All Notifications Courtesy Calls Disallowed operation, please wait a while. Email Notifications Functional Incorrect email or password. Marketing Activities New Followers Offer/Message(s) Passcode is incorrect. Phone calls Push Notifications Recommendations Reminders SMS Notifications Sorry! 5miles couldn't find enough sellers close to you. Here are some recommended top sellers from other nearby cities. 
Start listing items and you too can be featured below. Sorry, 5miles is experiencing a technical issue. Please try again later. Thanks for verifying your phone number That email address has already been used. The seller This item has been sold Unable to reset password. Please try again. Want to know what your neighbors just listed?
Follow them now to find out! You You got the item You have been blocked by this person. You offered %(local_price)s to %(nickname)s You sold the item to %s Your account has been suspended for violating our policies. Please contact support@5milesapp.com for more information. Your password should contain 6~16 characters. Your phone number has already been bound with another 5miles account, please check. [5miles] Your verification code is: %s. Enter this in 5miles to confirm your identity. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-07 17:56+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 %(nickname)s ofereceram %(local_price)s %(nickname)s: %(text)s [5miles] Sua senha de login: %s Todas as Notificações Visitas de Cortesia Operação não permitida, por favor, aguarde um pouco. Notificações por E-mail Funcional E-mail ou senha incorreta. Atividades de Marketing Novos Seguidores Oferta/Mensagens A senha está incorreta. Telefonemas Notificações por Push Recomendações Lembretes Notificações por SMS Desculpe! O 5miles não tem vendedores suficientes próximos a você, mas recomenda alguns bons vendedores de outras cidades. 
Você será apresentado a anúncios de itens com estrelas. Desculpe, o 5miles está passando por um problema técnico. Por favor, tente novamente mais tarde. Obrigado por verificar seu número de telefone Este endereço de e-mail já foi utilizado. O vendedor Este item foi vendido. Não é possível redefinir a senha. Por favor, tente novamente. Quer saber o que seus vizinhos acabaram de listar?
Siga-os agora para descobrir! Você Você obteve o item Você foi bloqueado por essa pessoa. Você ofereceu %(local_price)s para %(nickname)s. Você vendeu o item para %s A sua conta foi suspensa por violar nossas políticas. Por favor, entre em contato com support@5milesapp.com para mais informações. Sua senha deve conter de 6 a 16 caracteres. Seu número de telefone já foi vinculado com outra conta do 5miles, por favor, verifique. [5miles] Seu código de verificação é: %s. Digite isso no 5miles para confirmar sua identidade. 