��    $      <  5   \      0  $   1     V      m     �     �  *   �     �  
   �     �          +     9     J     a     m     �  	   �     �  �   �  H   ]  &   �  )   �  
   �       +     J   F     �     �  %   �  +   �     �  v     -   �  S   �  V   	  j  `  %   �	     �	      
     )
     B
  1   X
  &   �
  	   �
  -   �
     �
               &     B     X     t     �  #   �  �   �  e   }  -   �  ;        M     Y  B   y  U   �            $   -  )   R     |  ~   �  7     V   N  `   �            $                                                      	      "      !      #                                                   
                                             %(nickname)s offered %(local_price)s %(nickname)s: %(text)s *5miles* Your login passcode: %s All Notifications Courtesy Calls Disallowed operation, please wait a while. Email Notifications Functional Incorrect email or password. Marketing Activities New Followers Offer/Message(s) Passcode is incorrect. Phone calls Push Notifications Recommendations Reminders SMS Notifications Sorry! 5miles couldn't find enough sellers close to you. Here are some recommended top sellers from other nearby cities. 
Start listing items and you too can be featured below. Sorry, 5miles is experiencing a technical issue. Please try again later. Thanks for verifying your phone number That email address has already been used. The seller This item has been sold Unable to reset password. Please try again. Want to know what your neighbors just listed?
Follow them now to find out! You You got the item You have been blocked by this person. You offered %(local_price)s to %(nickname)s You sold the item to %s Your account has been suspended for violating our policies. Please contact support@5milesapp.com for more information. Your password should contain 6~16 characters. Your phone number has already been bound with another 5miles account, please check. [5miles] Your verification code is: %s. Enter this in 5miles to confirm your identity. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-07 17:56+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 %(nickname)s ofreció %(local_price)s %(nickname)s: %(text)s [5miles] Tu clave de acceso:  %s Todas las notificaciones Llamadas de cortesía Operación denegada, por favor espera un momento. Notificaciones por correo electrónico Funcional Correo electrónico o contraseña incorrecta. Actividades de marketing Nuevos seguidores Oferta/Mensaje(s) Clave de acceso incorrecta. Llamadas telefónicas Notificaciones automáticas Recomendaciones Recordatorios Notificaciones por mensaje de texto ¡Lo sentimos! 5miles no tiene suficientes vendedores cerca para ti, pero te recomienda algunos de los mejores vendedores de otras ciudades. 
Comienza a publicar artículos y aparecerás más abajo. Lo sentimos, 5miles está experimentando un problema técnico. Por favor intenta de nuevo más tarde. Gracias por verificar tu número de teléfono Esa dirección de correo electrónico ya ha sido utilizada. El vendedor Este artículo ha sido vendido. No se pudo restablecer la contraseña. Por favor intenta de nuevo. ¿Quieres saber que acaban de listar tus vecinos?
¡Síguelos ahora para descubrirlo! Tú Obtuviste el artículo Has sido bloqueado por esta persona. Ofreciste %(local_price)s a %(nickname)s. Vendiste el artículo a %s Tu cuenta ha sido suspendida por violar nuestras políticas. Por favor contacta a support@5milesapp.com por más información. Tu contraseña debería contener entre 6~16 caracteres. Tu número de teléfono ya se ha vinculado con otra cuenta 5miles, por favor verifica. [5miles] Tu código de verificación es: %s. Ingresa esto en 5miles para confirmar tu identidad. 