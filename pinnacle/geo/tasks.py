# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import shared_task
import logging
from django.core.cache import cache
from common import utils_func
from entity.service_base import SrService
from geo.models import GeoIpAddress
from common.utils import capture_message

__author__ = 'dragon'

logger = logging.getLogger('pinnacle')

@shared_task
def lookup_geoip_remote(client, ip_address):
    try:
        #logger.debug('lookup GEOIP remote: %s' % ip_address)
        response = client.city(ip_address)

        if response.maxmind.queries_remaining < 15000:
            capture_message('WARNING: maxmind queries remaining: %d, recharge ASAP' % response.maxmind.queries_remaining)

        return resolve_response(ip_address, response)
    except Exception, e:
        logger.exception(e)


@shared_task
def lookup_geoip_local(client, ip_address):
    try:
        #logger.debug('lookup GEOIP local: %s' % ip_address)
        response = client.city(ip_address)
        return resolve_response(ip_address, response)
    except Exception, e:
        logger.exception(e)


def resolve_response(ip_address, response):
    city_name = response.city.name if response.city.name else ''
    city_geoname_id = response.city.geoname_id
    country_iso_code = response.country.iso_code
    country_geoname_id = response.country.geoname_id
    country_name = response.country.name if response.country.name else ''
    location_latitude = response.location.latitude
    location_longitude = response.location.longitude
    location_metro_code = response.location.metro_code
    subdivisions_most_specific_geoname_id = response.subdivisions.most_specific.geoname_id
    subdivisions_most_specific_iso_code = response.subdivisions.most_specific.iso_code if response.subdivisions.most_specific.iso_code else ''
    subdivisions_most_specific_name = response.subdivisions.most_specific.name if response.subdivisions.most_specific.name else ''

#   暂时用不到IP数据，所以暂不存放数据库，以后看情况
#    geo_ip_addresses = GeoIpAddress.objects.filter(ip_address=ip_address)
#    if geo_ip_addresses:
#        geo_ip_address = geo_ip_addresses[0]
#        geo_ip_address.city_name = city_name
#        geo_ip_address.city_geoname_id = city_geoname_id
#        geo_ip_address.country_iso_code = country_iso_code
#        geo_ip_address.country_geoname_id = country_geoname_id
#        geo_ip_address.country_name = country_name
#        geo_ip_address.location_latitude = location_latitude
#        geo_ip_address.location_longitude = location_longitude
#        geo_ip_address.location_metro_code = location_metro_code
#        geo_ip_address.subdivisions_most_specific_geoname_id = subdivisions_most_specific_geoname_id
#        geo_ip_address.subdivisions_most_specific_iso_code = subdivisions_most_specific_iso_code
#        geo_ip_address.subdivisions_most_specific_name = subdivisions_most_specific_name
#    else:
#        geo_ip_address = GeoIpAddress(
#            ip_address=ip_address,
#            city_name=city_name,
#            city_geoname_id=city_geoname_id,
#            country_iso_code=country_iso_code,
#            country_geoname_id=country_geoname_id,
#            country_name=country_name,
#            location_latitude=location_latitude,
#            location_longitude=location_longitude,
#            location_metro_code=location_metro_code,
#            subdivisions_most_specific_geoname_id=subdivisions_most_specific_geoname_id,
#            subdivisions_most_specific_iso_code=subdivisions_most_specific_iso_code,
#            subdivisions_most_specific_name=subdivisions_most_specific_name)
#
#    geo_ip_address.save()
    # logger.debug('set key: %s' % ('ip_%s' % ip_address))
    # cache.set('ip_%s' % ip_address, geo_ip_address, None)
    ret_val = {'city_name': city_name, 'country_name': country_name, 'country_iso_code': country_iso_code,
               'location_latitude': location_latitude, 'location_longitude': location_longitude,
               'location_metro_code': location_metro_code if location_metro_code else '',
               'subdivisions_most_specific_iso_code': subdivisions_most_specific_iso_code,
               'created_at': utils_func.get_now_ts()}
    SrService().get_redis_ex().hmset('ip_%s' % ip_address, ret_val)

    return ret_val

