# -*- coding: utf-8 -*-
import geoip2
import geoip2.webservice
import datetime
import logging
from django.conf import settings
from django.utils.timezone import utc
from common import utils_func
from entity.service_base import singleton, BaseService, SrService
from geo.tasks import lookup_geoip_remote, lookup_geoip_local

__author__ = 'dragon'

logger = logging.getLogger('pinnacle')


@singleton
class GeoService(BaseService):

    def __init__(self):
        self.geoip_client = geoip2.webservice.Client(settings.GEOIP['USER_ID'], settings.GEOIP['LICENSE_KEY'])
        srs = SrService()
        self.geoip2 = srs.get_geoip2()
        self.cache = srs.get_redis_ex()

    def lookup_geoip(self, ip_address):
        """
        首先查memcached，如果没有命中，则查询本地geoip库，缓存到memcached，然后异步调用geoip web service进行查询，
        用远程返回的数据更新本地缓存中的数据，并更新本地数据库
        """
        if not ip_address:
            return {'city_name': '', 'country_name': '', 'country_iso_code': '',
                    'location_latitude': 0, 'location_longitude': 0,
                    'location_metro_code': -1, 'subdivisions_most_specific_iso_code': ''}

        geo_ip = self.cache.hgetall('ip_%s' % ip_address)
        if geo_ip:
            #logger.debug('lookup geoip cache hit: %s' % ip_address)

            # 如果memcached的数据超过1个月未更新，则更新
            if utils_func.get_now_ts() > int(geo_ip['created_at']) + datetime.timedelta(days=7).total_seconds():
                # 异步查询geoip web service
                lookup_geoip_remote.delay(self.geoip_client, ip_address)
        else:
            # 查询本地库并缓存
            # geo_ip = lookup_geoip_local(self.geoip2, ip_address)
            # if geo_ip:
            #     logger.debug('local hit: %s' % str(geo_ip))
            #     # 异步查询geoip web service并更新缓存
            #     lookup_geoip_remote.delay(self.geoip_client, ip_address)
            # else:
            #     # 本地库未命中，同步查询远程库
            #     geo_ip = lookup_geoip_remote(self.geoip_client, ip_address)
            #     logger.debug('remote hit: %s' % str(geo_ip))
            # 同步查询远程库
            geo_ip = lookup_geoip_remote(self.geoip_client, ip_address)
            #logger.debug('remote hit: %s' % str(geo_ip))

        return geo_ip


if __name__ == '__main__':
    geo_ip_address = GeoService().lookup_geoip('107.77.80.124')
    print geo_ip_address['created_at']
