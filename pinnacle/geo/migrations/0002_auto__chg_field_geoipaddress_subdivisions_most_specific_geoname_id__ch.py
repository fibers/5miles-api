# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'GeoIpAddress.subdivisions_most_specific_geoname_id'
        db.alter_column(u'geo_geoipaddress', 'subdivisions_most_specific_geoname_id', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'GeoIpAddress.city_geoname_id'
        db.alter_column(u'geo_geoipaddress', 'city_geoname_id', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'GeoIpAddress.country_geoname_id'
        db.alter_column(u'geo_geoipaddress', 'country_geoname_id', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'GeoIpAddress.location_metro_code'
        db.alter_column(u'geo_geoipaddress', 'location_metro_code', self.gf('django.db.models.fields.IntegerField')(null=True))

    def backwards(self, orm):

        # Changing field 'GeoIpAddress.subdivisions_most_specific_geoname_id'
        db.alter_column(u'geo_geoipaddress', 'subdivisions_most_specific_geoname_id', self.gf('django.db.models.fields.IntegerField')(default=-1))

        # Changing field 'GeoIpAddress.city_geoname_id'
        db.alter_column(u'geo_geoipaddress', 'city_geoname_id', self.gf('django.db.models.fields.IntegerField')(default=-1))

        # Changing field 'GeoIpAddress.country_geoname_id'
        db.alter_column(u'geo_geoipaddress', 'country_geoname_id', self.gf('django.db.models.fields.IntegerField')(default=-1))

        # Changing field 'GeoIpAddress.location_metro_code'
        db.alter_column(u'geo_geoipaddress', 'location_metro_code', self.gf('django.db.models.fields.IntegerField')(default=-1))

    models = {
        u'geo.geoipaddress': {
            'Meta': {'object_name': 'GeoIpAddress'},
            'city_geoname_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'city_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'country_geoname_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'country_iso_code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'country_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'location_latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '6'}),
            'location_longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '6'}),
            'location_metro_code': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'subdivisions_most_specific_geoname_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'subdivisions_most_specific_iso_code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'subdivisions_most_specific_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['geo']