# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GeoIpAddress'
        db.create_table(u'geo_geoipaddress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ip_address', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('city_geoname_id', self.gf('django.db.models.fields.IntegerField')()),
            ('city_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('country_iso_code', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('country_geoname_id', self.gf('django.db.models.fields.IntegerField')()),
            ('country_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('location_latitude', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=6)),
            ('location_longitude', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=6)),
            ('location_metro_code', self.gf('django.db.models.fields.IntegerField')()),
            ('subdivisions_most_specific_geoname_id', self.gf('django.db.models.fields.IntegerField')()),
            ('subdivisions_most_specific_iso_code', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('subdivisions_most_specific_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'geo', ['GeoIpAddress'])


    def backwards(self, orm):
        # Deleting model 'GeoIpAddress'
        db.delete_table(u'geo_geoipaddress')


    models = {
        u'geo.geoipaddress': {
            'Meta': {'object_name': 'GeoIpAddress'},
            'city_geoname_id': ('django.db.models.fields.IntegerField', [], {}),
            'city_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'country_geoname_id': ('django.db.models.fields.IntegerField', [], {}),
            'country_iso_code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'country_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'location_latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '6'}),
            'location_longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '6'}),
            'location_metro_code': ('django.db.models.fields.IntegerField', [], {}),
            'subdivisions_most_specific_geoname_id': ('django.db.models.fields.IntegerField', [], {}),
            'subdivisions_most_specific_iso_code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'subdivisions_most_specific_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['geo']