# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.
class GeoIpAddress(models.Model):
    ip_address = models.CharField(max_length=15)
    city_geoname_id = models.IntegerField(null=True)
    city_name = models.CharField(max_length=255)
    country_iso_code = models.CharField(max_length=2, null=True)
    country_geoname_id = models.IntegerField(null=True)
    country_name = models.CharField(max_length=255)
    location_latitude = models.DecimalField(max_digits=10, decimal_places=6)
    location_longitude = models.DecimalField(max_digits=10, decimal_places=6)
    location_metro_code = models.IntegerField(null=True)
    subdivisions_most_specific_geoname_id = models.IntegerField(null=True)
    subdivisions_most_specific_iso_code = models.CharField(max_length=2)
    subdivisions_most_specific_name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'ip_address: %s, country_name: %s, city_name: %s, subdivisions_most_specific_iso_code: %s,' \
               'location_latitude: %s, location_longitude: %s' % \
               (self.ip_address, self.country_name, self.city_name, self.subdivisions_most_specific_iso_code,
                self.location_latitude, self.location_longitude)


