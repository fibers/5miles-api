# -*- coding: utf-8 -*-

import time
import jwt
from django.http import HttpResponse, HttpResponseBadRequest, \
    HttpResponseServerError, HttpResponseForbidden
from re import compile
from user_agents import parse
from ipware.ip import get_real_ip
from raven import Client

from commerce.utils import *
from entity.exceptions import PinnacleApiError, UserBlockedError, UnauthenticatedError, ERR_CODE_SERVER_INTERNAL_ERROR, \
    ERR_MSG, ERR_CODE_UNAUTHENTICATED, ERR_CODE_TOKEN_EXPIRED
from entity.service_base import SrService
from entity.tasks import record_analytic_data
from django.utils.translation import ugettext as _
from apilog.service import AnalyticsService

client = Client(settings.RAVEN_CONFIG['dsn'])
logger = logging.getLogger('pinnacle')
EXEMPT_URLS = [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]


class CommonMiddleware(object):
    r = SrService().get_redis()

    def process_request(self, request):
        path = request.path_info.lstrip('/')
        if path.startswith('be_api/'):
            return

        user_agent = request.META.get('HTTP_USER_AGENT')
        if not user_agent:
            request.os = request.os_version = request.app_version = request.device = ''
        elif user_agent.find('app_version') > -1:
            ua_dict = {option.split(':')[0].strip(): option.split(':')[1].strip() for option in user_agent.split(',') if ':' in option}
            request.os = ua_dict['os'].lower()
            request.os_version = ua_dict['os_version']
            request.app_version = ua_dict['app_version']
            request.device = ua_dict['device']
        else:
            try:
                p_user_agent = parse(user_agent)
                request.os = p_user_agent.os.family
                request.os_version = p_user_agent.os.version_string
                request.app_version = 'Web_%s' % p_user_agent.browser.family
                request.device = p_user_agent.device.family
            except:
                request.os = request.os_version = request.app_version = request.device = ''
                logger.error('user_agent:%s' % user_agent)
                if not settings.DEBUG:
                    client.captureException()

        if request.META.get('HTTP_X_FIVEMILES_USER_ID') and request.META.get('HTTP_X_FIVEMILES_USER_TOKEN'):
            user_token = request.META['HTTP_X_FIVEMILES_USER_TOKEN']

            try:
                payload = jwt.decode(user_token, settings.JWT_SECRET_KEY)
            except jwt.ExpiredSignature:
                return HttpResponse(json.dumps({'err_code': ERR_CODE_TOKEN_EXPIRED,
                                                'err_msg': _(ERR_MSG[ERR_CODE_TOKEN_EXPIRED])}),
                                    content_type='application/json',
                                    status=401)
            except:
                return HttpResponse(json.dumps({'err_code': ERR_CODE_UNAUTHENTICATED,
                                                'err_msg': _(ERR_MSG[ERR_CODE_UNAUTHENTICATED])}),
                                    content_type='application/json',
                                    status=401)

            request.user_id = payload['user_id']
            # 以下代码处理6.6日事故造成的部分用户ID重复问题，对所有这些用户提示重新登录
            user_id = int(request.user_id)
            if ((user_id in range(691756, 692552) or user_id in [1, 434012, 486139])
                and not self.r.get('relogin:%s' % user_id)):
                return HttpResponse(json.dumps({'err_code': ERR_CODE_TOKEN_EXPIRED,
                                                'err_msg': _('We upgraded our system yesterday and your account may have been affected. Please log out and login again now. Go to Profile > Settings > Logout. Sorry for the inconvenience! ')}),
                                    content_type='application/json',
                                    status=401)

        elif any(m.match(path) for m in EXEMPT_URLS):
            user_id = ''
        else:
            return HttpResponse(json.dumps({'err_code': ERR_CODE_UNAUTHENTICATED,
                                            'err_msg': _(ERR_MSG[ERR_CODE_UNAUTHENTICATED])}),
                                content_type='application/json',
                                status=401)


    def process_exception(self, request, exception):
        if isinstance(exception, UnauthenticatedError):
            return HttpResponse(json.dumps({'err_code': exception.err_code, 'err_msg': _(exception.err_msg)}),
                                content_type='application/json', status=401)
        elif isinstance(exception, UserBlockedError):
            return HttpResponseForbidden(json.dumps({'err_code': exception.err_code, 'err_msg': _(exception.err_msg)}),
                                         content_type='application/json')
        elif isinstance(exception, PinnacleApiError):
            return HttpResponseBadRequest(json.dumps({'err_code': exception.err_code, 'err_msg': _(exception.err_msg)}),
                                          content_type='application/json')
        
        logger.exception('WRONG!!!!')

        if not settings.DEBUG:
            logger.exception(exception)
            client.captureException()

        return HttpResponseServerError(json.dumps({'err_code': ERR_CODE_SERVER_INTERNAL_ERROR,
                                                   'err_msg': _(ERR_MSG[ERR_CODE_SERVER_INTERNAL_ERROR])}),
                                       content_type='application/json')


class ReqExecTimeMiddleware(object):

    interval_conf = 0.2

    def process_request(self, request):
        #logger.info("----req start----------")
        # 记录请求的开始执行时间，用于性能监控
        request._exec_start_time = time.time()

    def process_response(self, request, response):
        try:
            # 性能检测
            ext_params = {}
            ext_params['status_code'] = response.status_code
            if request._exec_start_time:
                interval = time.time() - request._exec_start_time

                ext_params['exec_time'] = interval
                #logger.warn("ReqExecTimeMiddleware interval= %s" % (interval));
                # 大于 0.2 秒 输出日志
                if interval > self.interval_conf:
                    logger.warn("req_exec=%s > %s uri=%s" % (interval ,self.interval_conf , request.get_full_path()))
                else:
                    if is_gray():
                        logger.debug("access url = %s" % request.get_full_path())
                    pass

            if settings.ANALYTICS_NEW_API_LOG_SWITCH:
                AnalyticsService().record_analytic_data(request, ext_params)
            else:
                path = request.path_info.lstrip('/')
                ip_address = get_real_ip(request)
                user_agent = request.META.get('HTTP_USER_AGENT')
                referer = request.META.get('HTTP_REFERER')
                query_str = request.REQUEST
                lang_code = request.META.get('HTTP_ACCEPT_LANGUAGE', 'en-us')
                utc_date = get_now_timestamp_str2()
                user_id = request.user_id if hasattr(request, 'user_id') else ''
                record_analytic_data.apply_async((user_id, path, query_str, referer, lang_code, ip_address, user_agent,
                                              utc_date, None), queue='celery.log.analysis')
        except Exception, e:
            logger.warn("ReqExecTimeMiddleware error=%s ,uri=%s" , e,request.get_full_path())

        return response