# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import shared_task

__author__ = 'dragon'

import logging
import redis
import json
import urllib2, urllib
from django.conf import settings
import requests
from common.biz import is_app_user, has_mobile_phone, is_craigslist_user, ITEM_STATE_LISTING, ITEM_STATE_SOLD
from entity.utils import generate_access_token
from message.tasks import push, send_sms, start_reply_countdown, send_mail, mark_read_message
from entity.service_base import SrService

logger = logging.getLogger('pinnacle')
r = SrService().get_redis()

@shared_task
def push_message_to_user_inbox(inbox_key, msg_text, from_uid, to_uid, item_id, offerline_id, timestamp, push_text=None,
                               mail_text=None, content_type=0):
    # Put a message into the target user's inbox
    if msg_text is None:
        return

    if inbox_key.endswith('buying'):
        msg_type = 'buying'
    elif inbox_key.endswith('selling'):
        msg_type = 'selling'

    r.incr('%s:new_count' % inbox_key, 1)

    msg_id = r.get('from:%s:to:%s:item:%s:message_id' % (from_uid, to_uid, item_id))
    if msg_id is not None:
        with r.pipeline() as pipe:
            pipe.hincrby('message:%s' % msg_id, 'count', 1)
            pipe.hmset('message:%s' % msg_id,
                       {'from': from_uid, 'to': to_uid, 'text': msg_text, 'timestamp': timestamp, 'unread': True,
                        'type': msg_type})
            pipe.zadd(inbox_key, msg_id, timestamp)
            pipe.execute()
    else:
        # Create new message object
        msg_id = r.incr('global:next_message_id')
        with r.pipeline() as pipe:
            pipe.hmset('message:%s' % msg_id,
                       {'id': msg_id, 'from': from_uid, 'to': to_uid, 'text': msg_text, 'item_id': item_id,
                        'thread_id': offerline_id, 'unread': True, 'timestamp': timestamp, 'type': msg_type,
                        'count': 1})
            pipe.set('from:%s:to:%s:item:%s:message_id' % (from_uid, to_uid, item_id), msg_id)
            pipe.zadd(inbox_key, msg_id, timestamp)
            pipe.execute()

    sender = r.hgetall('user:%s' % from_uid)
    item = r.hgetall('item:%s' % item_id)
    owner_id = int(r.get('item:%s:owner' % item_id))

    try:
        sender_id = sender['id']
        sender_nickname = sender['nickname'].decode('utf-8')
        sender_portrait = sender['portrait']

        fuzzy_item_id = item['id']
        item_title = item['title'].decode('utf-8')
        item_price = item['local_price']
        item_images = json.loads(item['images'])

    except KeyError, e:
        logger.exception(e)
        return

    item_title = item_title[:50]
    item_image = item_images[0]['imageLink']

    push_text = '%s: %s' % (sender_nickname, push_text)
    push_text = push_text[:37] + '...' if len(push_text) > 40 else push_text
    if is_app_user(to_uid):
        push(to_uid,
             text=push_text,
             payload={'t': 'o', 'olid': str(offerline_id), 'mid': str(msg_id), 'img': sender['portrait']},
             source_user=from_uid)
    elif has_mobile_phone(to_uid) and not is_craigslist_user(to_uid):
        sms_text = '[5miles] %s %s' % (push_text, '(To reply or get more information, please use the 5miles app http://bit.ly/5miles_download)')
        send_sms.delay(target_user=to_uid, text=sms_text)

    # offer/chat 24小时未回复，且用户有手机号，发短信提醒用户回复
    #if has_mobile_phone(to_uid) and not is_craigslist_user(to_uid) \
    #        and (item['state'] == str(ITEM_STATE_LISTING) or item['state'] == str(ITEM_STATE_SOLD)):
    #    start_reply_countdown(to_uid, offerline_id, owner_id)

    if settings.SEND_OFFER_MAIL:
        if sender_portrait.find('http://res.cloudinary.com/fivemiles/image/upload/') >= 0\
                and sender_portrait != settings.DEFAULT_AVATAR_LINK:
            sender_portrait = sender_portrait.split('http://res.cloudinary.com/fivemiles/image/upload/')[1]

        send_mail.delay(to_uid, 'TMPL_MAIL_OFFER', None, {
            'text': mail_text,
            'content_type': content_type,
            'item_id': fuzzy_item_id,
            'item_title': item_title,
            'item_price': item_price,
            'item_image': item_image.split('http://res.cloudinary.com/fivemiles/image/upload/')[1],
            'sender_id': sender_id,
            'sender_nickname': sender_nickname,
            'sender_portrait': sender_portrait,
            'receiver_token': generate_access_token(to_uid)
        }, from_address='"5miles" <discuss-%s@5milesapp.me>' % offerline_id)


@shared_task
def record_message_in_user_inbox(inbox_key, msg_text, from_uid, to_uid, item_id, offerline_id, timestamp,
                                 app_version=None):
    if inbox_key.endswith('buying'):
        msg_type = 'buying'
    elif inbox_key.endswith('selling'):
        msg_type = 'selling'

    msg_id = r.get('from:%s:to:%s:item:%s:message_id' % (to_uid, from_uid, item_id))
    if msg_id is not None:
        with r.pipeline() as pipe:
            mark_read_message.apply_async((from_uid, msg_id, offerline_id), queue='celery.misc')
            pipe.hmset('message:%s' % msg_id,
                       {'from': from_uid, 'to': to_uid, 'text': msg_text, 'timestamp': timestamp,
                        'type': msg_type})
            pipe.zadd(inbox_key, msg_id, timestamp)
            pipe.execute()
    else:
        # Create new message object
        msg_id = r.incr('global:next_message_id')
        with r.pipeline() as pipe:
            pipe.hmset('message:%s' % msg_id,
                       {'id': msg_id, 'from': from_uid, 'to': to_uid, 'text': msg_text, 'item_id': item_id,
                        'thread_id': offerline_id, 'unread': False, 'timestamp': timestamp, 'type': msg_type,
                        'count': 0})
            pipe.set('from:%s:to:%s:item:%s:message_id' % (to_uid, from_uid, item_id), msg_id)
            pipe.zadd(inbox_key, msg_id, timestamp)
            pipe.execute()

