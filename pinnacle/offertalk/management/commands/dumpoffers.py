__author__ = 'hugang'

import redis
import csv

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class Command(BaseCommand):
    help = 'Dump offers in Redis to a csv file'

    def handle(self, *args, **options):
        try:
            print 'Connecting to redis'
            r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)

            max_offer_id = int(r.get('global:next_offer_id'))

            print 'Dumping...'
            with open('./offers.csv', 'wb') as csvfile:
                offerwriter = csv.writer(csvfile, dialect='excel')

                for i in range(1, max_offer_id):
                    offer = r.hgetall('offer:%s' % i)
                    if not offer:
                        continue

                    offerwriter.writerow([offer['id'], offer['from'], offer['to'], offer['local_price'], offer['text'], offer['timestamp']])

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)

