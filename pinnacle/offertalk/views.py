# -*- coding: utf-8 -*-

from datetime import timedelta

import redis
from django.views.decorators.http import require_GET, require_POST
from django.http import HttpResponse
from django.utils.translation import ugettext as _

from offertalk.services import build_chat_params, TextChatParams, PictureChatParams, LocationChatParams
from commerce.utils import *
from common import utils_es
from common.utils_web import check_blacklist
from entity.exceptions import NotFoundError, InvalidParameterError, InvalidOperationError
from entity.service import UserService
from entity.utils import *
from entity.views import _format_currency
from message.tasks import notify, notify_other_buyers_and_likers, encourage_listing_when_first_deal, \
    stop_reply_countdown, mark_read_message
from common.biz import *
from common.utils import capture_exception, capture_message
from entity.models import Item, User
from offertalk.tasks import push_message_to_user_inbox, record_message_in_user_inbox
from offertalk.utils import parse_email_text
from common.utils import mailgun_verify

logger = logging.getLogger('pinnacle')
r = SrService().get_redis()

@require_POST
@mailgun_verify
def mailgun_notify(request):
    """
    This api is for receiving the forward mail from mailgun.
    :param request:
    :return: The same with api 'make_offer'
    """

    from django.core.validators import validate_email

    try:

        from_address = request.POST.get('from', '')
        from_address = from_address if from_address.find('<') < 0 and from_address.find('>') < 0 \
            else from_address[from_address.rfind('<') + 1: from_address.rfind('>')]

        recipient = request.POST.get('recipient', '')
        stripped_text = request.POST.get('stripped-text', '')

        if not all([from_address, recipient]):
            error_msg = 'Wrong arguments (from, recipient): %s, %s' % (from_address, recipient)
            logger.warn(error_msg)
            capture_message(error_msg)
            return HttpResponse('OK')

        validate_email(from_address)

        offerline_id_str = recipient[len('discuss-'):-len('@5milesapp.me')]
        offerline_id = int(offerline_id_str)

        offerline_meta = r.hgetall('offerline:%s:meta' % offerline_id)

        item_id = int(offerline_meta['item_id'])
        owner_id = int(r.get('item:%d:owner' % item_id))
        buyer_id = int(offerline_meta['buyer_id'])

        if not all([item_id, owner_id, buyer_id]):
            error_msg = 'Wrong data (item_id, owner_id, buyer_id): %d, %d, %d' % (item_id, owner_id, buyer_id)
            logger.warn(error_msg)
            capture_message(error_msg)
            return HttpResponse('OK')

        from_uid = r.get('login:email:%s:user_id' % from_address)
        if from_uid:
            from_uid = int(from_uid)

        if from_uid not in [owner_id, buyer_id]:
            # 可能是facebook用户
            fb_users = User.objects.filter(email=from_address).exclude(fb_user_id='')
            if fb_users:
                from_uid = fb_users[0].id
            else:
                from_uid = 0

        if from_uid not in [owner_id, buyer_id]:
            # error_msg = '[mailgun_notify] The user cannot use this service: user_id => %d, email => %s' % (from_uid, from_address)
            # logger.warn(error_msg)
            # capture_message(error_msg)
            return HttpResponse('OK')

        to_uid = buyer_id if from_uid == owner_id else owner_id

        # Parse price and text from the email and set it to request
        comment, price = parse_email_text(stripped_text)

        # Set the new parameters into request.POST
        clone_post = request.POST.copy()
        clone_post.setdefault('item_id', obfuscate_item_id(item_id))
        clone_post.setdefault('to_user', obfuscate_id(to_uid))
        clone_post.setdefault('text', comment)
        request.user_id = from_uid

        item_state = r.hget('item:%s' % item_id, 'state')
        item_state = int(item_state) if item_state is not None else 0
        if item_state == ITEM_STATE_LISTING:
            clone_post.setdefault('price', price)
            request.POST = clone_post
            make_offer(request)
        else:
            # clone_post.setdefault('offerline_id', offerline_id)
            request.POST = clone_post
            make_offer(request)
    except Exception:
        logger.exception()
        capture_exception()
    finally:
        return HttpResponse('OK')


@require_POST
@validate_user
@check_blacklist
def make_offer(request):
    chat_params = build_chat_params(request)

    item_id = chat_params.item_id
    to_user = chat_params.to_user
    item = r.hgetall('item:%s' % item_id)
    # item_state = item['state']
    # item_state = int(item_state) if item_state is not None else 0

    # if item_state in [ITEM_STATE_UNAVAILABLE, ITEM_STATE_UNLISTED, ITEM_STATE_UNAPPROVED]:
    #     return HttpResponseBadRequest(_('This item is unavailable. Discover more items now!'))

    role = 1 if str(request.user_id) == r.get('item:%s:owner' % item_id) else 0
    if role == 1:
        # request user is the seller
        offerline_id = r.get('item:%s:buyer:%s:offerline_id' % (item_id, to_user))
    else:
        # request user is the buyer
        offerline_id = r.get('item:%s:buyer:%s:offerline_id' % (item_id, request.user_id))

    # 一旦用户发起offer或者chat，不管三七二十一，清除offer倒计时器
    #stop_reply_countdown(request.user_id, offerline_id)

    timestamp = get_now_timestamp()

    # Create new offerline object if necessary
    if not offerline_id:
        offerline_id = r.incr('global:next_offerline_id')
        buyer_id = request.user_id if role == 0 else to_user
        with r.pipeline() as pipe:
            pipe.set('item:%s:buyer:%s:offerline_id' % (item_id, buyer_id), offerline_id)
            pipe.lpush('item:%s:offerlines' % item_id, offerline_id)
            pipe.hmset('offerline:%s:meta' % offerline_id,
                       {'id': offerline_id, 'item_id': item_id, 'buyer_id': request.user_id})
            pipe.zadd('user:%s:purchases' % buyer_id, item_id, timestamp)
            pipe.execute()

    msg_text = push_text = ''
    offer_hash = {'type': 0, 'from': request.user_id, 'to': to_user, 'timestamp': get_now_timestamp_str(),
                  'price': -1, 'local_price': '', 'text': '', 'msg_type': chat_params.content_type}
    mail_text = ''
    if isinstance(chat_params, TextChatParams):
        price = chat_params.price
        local_price = _format_currency(price, item['currency'])
        if price >= 0:
            if role == 1:
                # request user is the seller
                mail_text = push_text = _('offered %s') % local_price
            else:
                # request user is the buyer
                mail_text = push_text = _('I would like to buy your %(title)s for %(local_price)s.') % {'title': item['title'], 'local_price': local_price}
            msg_text = _('Offered: %s') % local_price
            offer_hash['payload'] = json.dumps({'text': push_text})
            offer_hash['text'] = '' if price > 0 else push_text
        else:
            mail_text = msg_text = push_text = chat_params.text
            offer_hash['payload'] = json.dumps({'text': chat_params.text})
            offer_hash['text'] = chat_params.text

        offer_hash['price'] = price
        offer_hash['local_price'] = local_price
    elif isinstance(chat_params, PictureChatParams):
        msg_text = push_text = _('[Photo]')
        offer_hash['payload'] = json.dumps({'picture_url': chat_params.picture_url})
        # offer_hash['text'] = _('I sent you a photo, please update to the latest version to view. http://5milesapp.com/update ')
        offer_hash['text'] = _('I sent you a photo, please click here view it %s' % chat_params.picture_url)
    elif isinstance(chat_params, LocationChatParams):
        msg_text = push_text = _('[Location]')
        offer_hash['payload'] = json.dumps({'address_name': chat_params.address_name, 'lat': chat_params.lat, 'lon': chat_params.lon,
                                            'address_map_thumb': chat_params.address_map_thumb, 'place_name': chat_params.place_name})
        # offer_hash['text'] = _('I sent you a meeting location, please update to the latest version to view. http://5milesapp.com/update ')
        offer_hash['text'] = _('Location: %s' % chat_params.address_name)

    # Create new offer object
    offer_id = r.incr('global:next_offer_id')
    offer_hash['id'] = offer_id
    with r.pipeline() as pipe:
        pipe.hmset('offer:%s' % offer_id, offer_hash)
        pipe.set('offer:%s:offerline_id' % offer_id, offerline_id)
        pipe.zadd('offerline:%s:entries' % offerline_id, offer_id, timestamp)
        pipe.execute()

    # Deliver related message to each side's inbox
    target_inbox_key = 'user:%s:inbox:buying' % to_user if role == 1 else 'user:%s:inbox:selling' % to_user
    me_inbox_key = 'user:%s:inbox:selling' % request.user_id if role == 1 else 'user:%s:inbox:buying' % request.user_id

    push_message_to_user_inbox(target_inbox_key, msg_text, request.user_id, to_user, item_id, offerline_id, timestamp,
                               push_text, mail_text, chat_params.content_type)
    record_message_in_user_inbox(me_inbox_key, msg_text, request.user_id, to_user, item_id, offerline_id,
                                 timestamp, request.app_version)

    # 卖家收到第一个offer时触发提醒
    if r.llen('item:%s:offerlines' % item_id) == 1 \
            and role == 0 \
            and r.zcard('offerline:%s:entries' % offerline_id) == 1\
            and r.zcard(target_inbox_key) == 1:
        nickname = r.hget('user:%s' % to_user, 'nickname')
        notify.delay(str(to_user), 'TRI_ENCOURAGE_LISTING_1',
                     {'nickname': nickname.encode('utf-8'), 'item_title': item['title'], 'item_id': item_id})

    # 新买家第一次发offer给有风险的商品时触发提醒
    if role == 0 and r.zcard('offerline:%s:entries' % offerline_id) == 1:
        m_item = Item.objects.get(id=int(item_id))
        if m_item.internal_status == 2:
            nickname = r.hget('user:%s' % to_user, 'nickname')
            notify.delay(str(request.user_id), 'TRI_ITEM_RISK_BUYER_1',
                         {'nickname': nickname.encode('utf-8'), 'item_title': item['title'].encode('utf-8'),
                          'item_id': item_id, 'message': m_item.risk, 'fuzzy_item_id': item['id'],
                          'item_image': json.loads(item['images'])[0]['imageLink']})

    if hasattr(request, 'user_id'):
        UserService().user_daily_checkin(request.user_id)

    return HttpResponse(json.dumps({'offerline_id': str(offerline_id), 'offer_id': str(offer_id)}), content_type='application/json', status=201)


@require_POST
@validate_user
def accept_offer(request):
    ref_offer_id = int(request.POST.get('ref_offer_id', 0))
    ref_offer = r.hgetall('offer:%s' % ref_offer_id)
    if str(request.user_id) != ref_offer['to']:
        raise InvalidParameterError()

    offerline_id = r.get('offer:%s:offerline_id' % ref_offer_id)
    item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')

    # 一旦用户发起offer或者chat，不管三七二十一，清除offer倒计时器
    #stop_reply_countdown(request.user_id, offerline_id)

    try:
        item_state = int(r.hget('item:%s' % item_id, 'state'))
    except:
        item_state = 0
    if item_state > 0:
        raise InvalidParameterError()

    buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')

    # Create new offer object
    timestamp = get_now_timestamp()
    offer_id = r.incr('global:next_offer_id')
    with r.pipeline() as pipe:
        pipe.hmset('offer:%s' % offer_id,
                   {'id': offer_id, 'type': 1, 'from': request.user_id, 'to': buyer_id, 'price': ref_offer['price'],
                    'local_price': ref_offer['local_price'], 'text': '', 'timestamp': get_now_timestamp_str()})
        pipe.set('offer:%s:offerline_id' % offer_id, offerline_id)
        pipe.zadd('offerline:%s:entries' % offerline_id, offer_id, timestamp)
        pipe.set('item:%s:sold_to' % item_id, buyer_id)
        pipe.hset('item:%s' % item_id, 'state', ITEM_STATE_SOLD)
        pipe.zadd('user:%s:purchases' % buyer_id, item_id, timestamp)

        pipe.execute()

    # Push a message into the target user's inbox
    target_inbox_key = 'user:%s:inbox:buying' % ref_offer['from'] if ref_offer[
                                                                         'from'] == buyer_id else 'user:%s:inbox:selling' % \
                                                                                                  ref_offer['from']
    me_inbox_key = 'user:%s:inbox:selling' % request.user_id if ref_offer[
                                                                    'from'] == buyer_id else 'user:%s:inbox:buying' % request.user_id
    msg_text = _('Accepted offer of %s') % ref_offer['local_price']

    push_message_to_user_inbox(target_inbox_key, msg_text, request.user_id, ref_offer['from'], item_id, offerline_id,
                               timestamp, msg_text, msg_text)
    record_message_in_user_inbox(me_inbox_key, msg_text, request.user_id, ref_offer['from'], item_id, offerline_id,
                                 timestamp)

    # 通知买家、卖家、其他买家和喜欢此商品的用户该商品已卖出
    notify_other_buyers_and_likers.delay(item_id, buyer_id, offerline_id)

    utils_es.update_field_in_searchengine(int(item_id), 'state', ITEM_STATE_SOLD)
    utils_es.delete_item_in_searchengine.apply_async(
        (int(item_id),),
        eta=datetime.datetime.utcnow() + timedelta(days=settings.SOLD_ITEMS_HOLD_DAYS),
        queue='celery.schedule')

    Item.objects.filter(id=int(item_id)).update(state=ITEM_STATE_SOLD)

    # 卖家第一笔成交时触发提醒
    encourage_listing_when_first_deal.delay(r.get('item:%s:owner' % item_id), item_id)

    return HttpResponse()


@require_POST
@validate_user
def make_chat(request):
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))
    if not item_id:
        raise InvalidParameterError()

    offerline_id = int(request.POST.get('offerline_id', 0))
    if not r.exists('offerline:%s:meta' % offerline_id):
        raise NotFoundError()

    target_user = unfuzzy_user_id(request.POST.get('to_user', ''))
    if target_user is None:
        raise NotFoundError()
    elif target_user == str(request.user_id):
        raise InvalidParameterError()

    text = request.POST.get('text', '')
    if text == '':
        raise InvalidParameterError()

    # 一旦用户发起offer或者chat，不管三七二十一，清除offer倒计时器
    #stop_reply_countdown(request.user_id, offerline_id)

    # item_state = int(r.hget('item:%s' % item_id, 'state'))
    # if item_state > 3:
    # return HttpResponseBadRequest(_('You\'re unable to message the user about this item. '))

    timestamp = get_now_timestamp()
    offer_id = r.incr('global:next_offer_id')
    with r.pipeline() as pipe:
        pipe.hmset('offer:%s' % offer_id,
                   {'id': offer_id, 'type': 2, 'from': request.user_id, 'to': target_user, 'price': 0,
                    'local_price': '', 'text': text, 'timestamp': get_now_timestamp_str()})
        pipe.set('offer:%s:offerline_id' % offer_id, offerline_id)
        #pipe.rpush('offerline:%s:offers' % offerline_id, offer_id)
        pipe.zadd('offerline:%s:entries' % offerline_id, offer_id, timestamp)

        pipe.execute()

    target_inbox_key = 'user:%s:inbox:buying' % target_user if str(request.user_id) == r.get(
        'item:%s:owner' % item_id) else 'user:%s:inbox:selling' % target_user
    me_inbox_key = 'user:%s:inbox:selling' % request.user_id if str(request.user_id) == r.get(
        'item:%s:owner' % item_id) else 'user:%s:inbox:buying' % request.user_id

    push_message_to_user_inbox(target_inbox_key, text, request.user_id, target_user, item_id, offerline_id, timestamp,
                               text, text)
    record_message_in_user_inbox(me_inbox_key, text, request.user_id, target_user, item_id, offerline_id, timestamp)

    return HttpResponse()


@require_GET
@validate_user
def get_offerline(request):
    offerline_id = request.GET.get('offerline_id', '')
    item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')
    if not item_id:
        raise NotFoundError()

    seller_id = r.get('item:%s:owner' % item_id)
    buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')

    if str(request.user_id) != seller_id and str(request.user_id) != buyer_id:
        raise InvalidOperationError()

    if r.exists('offerline:%s:entries' % offerline_id):
        entry_ids = r.zrange('offerline:%s:entries' % offerline_id, 0, -1)
        # last_records = r.zrange('offerline:%s:entries' % offerline_id, -1, -1, withscores=True)
        #(uid, ts) = last_records[-1]
        #latest_ts = int(ts)
    else:
        entry_ids = r.lrange('offerline:%s:offers' % offerline_id, 0, -1)

    latest_ts = get_now_timestamp()

    entries = []
    for uid in entry_ids:
        entry = r.hgetall('offer:%s' % uid)
        entry['from_buyer'] = entry['from'] == buyer_id
        entry['from_me'] = entry['from'] == str(request.user_id)
        entry['type'] = int(entry['type'])
        entry['price'] = float(entry['price'])
        entry.pop('from')
        entry.pop('to')
        if 'payload' in entry:
            entry['payload'] = json.loads(entry['payload'])
        else:
            r.hset('offer:%s' % uid, 'payload', json.dumps({'text': entry['text']}))
            entry['payload'] = {'text': entry['text']}

        entries.append(entry)

    item = r.hgetall('item:%s' % item_id)
    item['owner'] = get_user_from_id(seller_id)
    item['images'] = json.loads(item['images'])
    item['location'] = json.loads(item['location'])
    if 'state' not in item:
        item['state'] = 0

    if int(item['state']) >= 1:
        sold_to_user = r.get('item:%s:sold_to' % item_id)
        sold_to_others = (sold_to_user is not None and sold_to_user != buyer_id)
    else:
        sold_to_others = False

    target_user_id = seller_id if buyer_id == str(request.user_id) else buyer_id
    user_blocked = UserService().is_blocked(request.user_id, target_user_id)
    ret = {'meta': {'item': item, 'buyer': get_user_from_id(buyer_id), 'latest_ts': latest_ts,
                    'sold_to_others': sold_to_others, 'user_blocked': user_blocked}, 'objects': entries}

    mark_read_message.apply_async((request.user_id, '', offerline_id), queue='celery.misc')

    if hasattr(request, 'user_id'):
        UserService().user_daily_checkin(request.user_id)

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
@validate_user
def get_offerline_updates(request):
    from_timestamp = request.GET.get('from_timestamp', 0)
    offerline_id = request.GET.get('offerline_id', '')

    item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')
    if not item_id:
        raise NotFoundError()

    seller_id = r.get('item:%s:owner' % item_id)
    buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')

    try:
        state = int(r.hget('item:%s' % item_id, 'state'))
    except Exception:
        state = 0
        r.hset('item:%s' % item_id, 'state', state)

    if state >= 1:
        sold_to_user = r.get('item:%s:sold_to' % item_id)
        sold_to_others = (sold_to_user != buyer_id)
    else:
        sold_to_others = False

    if str(request.user_id) != seller_id and str(request.user_id) != buyer_id:
        raise InvalidOperationError()

    if r.exists('offerline:%s:entries' % offerline_id):
        from_ts = '(%s' % from_timestamp
        entry_ids = r.zrangebyscore('offerline:%s:entries' % offerline_id, from_ts, '+inf', withscores=True)
    else:
        entry_ids = []

    latest_ts = get_now_timestamp()
    entries = []
    for record in entry_ids:
        (uid, ts) = record
        ts = int(ts)
        entry = r.hgetall('offer:%s' % uid)
        entry['from_buyer'] = entry['from'] == buyer_id
        entry['from_me'] = entry['from'] == str(request.user_id)
        entry['type'] = int(entry['type'])
        entry['price'] = float(entry['price'])
        entry.pop('from')
        entry.pop('to')
        if 'payload' in entry:
            entry['payload'] = json.loads(entry['payload'])
        else:
            r.hset('offer:%s' % uid, 'payload', json.dumps({'text': entry['text']}))
            entry['payload'] = {'text': entry['text']}

        entries.append(entry)
        latest_ts = ts

    ret = {'meta': {'latest_ts': latest_ts, 'item_state': state, 'sold_to_others': sold_to_others}, 'objects': entries}

    mark_read_message.apply_async((request.user_id, '', offerline_id), queue='celery.misc')

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def get_my_purchases(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))

    try:
        item_ids = r.zrevrange('user:%s:purchases' % request.user_id, offset, offset + limit - 1)
        total_count = r.zcard('user:%s:purchases' % request.user_id)
    except Exception:
        item_ids = []

    items = []
    for item_id in item_ids:
        item = r.hgetall('item:%s' % item_id)

        try:
            owner_id = r.get('item:%s:owner' % item_id)
            item['owner'] = get_user_from_id(owner_id)
            item['images'] = json.loads(item['images'])
            item['location'] = json.loads(item['location'])

            items.append(item)
        except Exception:
            r.zrem('user:%s:purchases' % request.user_id, item_id)

    ret = {'meta': {'total_count': total_count, 'offset': offset, 'limit': len(items),
                    'previous': '?offset=%d&limit=%d' % (offset - limit, limit) if offset >= limit else None,
                    'next': '?offset=%d&limit=%d' % (offset + limit, limit) if offset + limit < total_count else None},
           'objects': items}

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_POST
def confirm_delivery(request):
    offerline_id = int(request.POST.get('offerline_id', 0))
    if not r.exists('offerline:%s:meta' % offerline_id):
        raise NotFoundError()

    if str(request.user_id) != r.hget('offerline:%s:meta' % offerline_id, 'buyer_id'):
        raise InvalidOperationError()

    item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')
    r.hset('item:%s' % item_id, 'state', ITEM_STATE_DONE_DELIVERY)

    # 一旦用户发起offer或者chat，不管三七二十一，清除offer倒计时器
    #stop_reply_countdown(request.user_id, offerline_id)

    timestamp = get_now_timestamp()
    offer_id = r.incr('global:next_offer_id')
    owner_id = r.get('item:%s:owner' % item_id)
    with r.pipeline() as pipe:
        pipe.hmset('offer:%s' % offer_id,
                   {'id': offer_id, 'type': 3, 'from': request.user_id, 'to': owner_id, 'price': 0, 'local_price': '',
                    'text': 'Delivery confirmed', 'timestamp': get_now_timestamp_str()})
        pipe.set('offer:%s:offerline_id' % offer_id, offerline_id)
        pipe.zadd('offerline:%s:entries' % offerline_id, offer_id, timestamp)

        pipe.execute()

    # the item has been delivery, so must change the state to ITEM_STATE_DONE_DELIVERY in db
    Item.objects.filter(id=int(item_id)).update(state=ITEM_STATE_DONE_DELIVERY)

    return HttpResponse()


@require_POST
def rate_seller(request):
    text = request.POST.get('text', '')
    score = int(request.POST.get('score', 0))
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))

    if not item_id:
        raise NotFoundError()

    item_state = int(r.hget('item:%s' % item_id, 'state'))
    if item_state < 1 or item_state >= 3:
        raise InvalidParameterError()

    rating_id = r.incr('global:next_rate_id')
    seller_id = r.get('item:%s:owner' % item_id)
    offer_id = r.incr('global:next_offer_id')
    offerline_id = r.get('item:%s:buyer:%s:offerline_id' % (item_id, request.user_id))

    # 一旦用户发起offer或者chat，不管三七二十一，清除offer倒计时器
    #stop_reply_countdown(request.user_id, offerline_id)

    timestamp = get_now_timestamp()
    with r.pipeline() as pipe:
        pipe.hset('item:%s' % item_id, 'state', ITEM_STATE_RATED_SELLER)
        pipe.hmset('rating:%s' % rating_id,
                   {'id': rating_id, 'from': request.user_id, 'score': score, 'text': text, 'created_at': timestamp})
        pipe.lpush('user:%s:seller_ratings' % seller_id, rating_id)

        pipe.hmset('offer:%s' % offer_id,
                   {'id': offer_id, 'type': 4, 'from': request.user_id, 'to': seller_id, 'price': 0, 'local_price': '',
                    'text': json.dumps({'text': text, 'score': score}), 'timestamp': get_now_timestamp_str()})
        pipe.set('offer:%s:offerline_id' % offer_id, offerline_id)
        pipe.zadd('offerline:%s:entries' % offerline_id, offer_id, timestamp)

        pipe.execute()

    # the item has been rated, so must change the state to ITEM_STATE_RATED_SELLER in db
    Item.objects.filter(id=int(item_id)).update(state=ITEM_STATE_RATED_SELLER)

    r.incrby('user:%s:seller_ratings:total_score' % seller_id, score)

    # Push a message into the target user's inbox
    target_inbox_key = 'user:%s:inbox:selling' % seller_id
    me_inbox_key = 'user:%s:inbox:buying' % request.user_id
    msg_text = _('Feedback: %d stars') % score
    offerline_id = r.get('item:%s:buyer:%s:offerline_id' % (item_id, request.user_id))

    push_message_to_user_inbox(target_inbox_key, msg_text, request.user_id, seller_id, item_id, offerline_id, timestamp,
                               msg_text, msg_text)
    record_message_in_user_inbox(me_inbox_key, msg_text, request.user_id, seller_id, item_id, offerline_id, timestamp)

    return HttpResponse(status=201)


@require_GET
def get_seller_ratings(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    seller_id = unfuzzy_user_id(request.GET.get('seller_id', ''))
    if not seller_id:
        seller_id = request.user_id

    rating_ids = r.lrange('user:%s:seller_ratings' % seller_id, offset, offset + limit - 1)
    total_count = r.llen('user:%s:seller_ratings' % seller_id)
    ratings = []
    for rating_id in rating_ids:
        rating = r.hgetall('rating:%s' % rating_id)
        rating['rater'] = get_user_from_id(rating['from'])
        rating['created_at'] = int(rating['created_at'])
        rating.pop('from')
        ratings.append(rating)

    ret = {'meta': {'total_count': total_count, 'offset': offset, 'limit': len(ratings),
                    'previous': '?offset=%d&limit=%d' % (offset - limit, limit) if offset >= limit else None,
                    'next': '?offset=%d&limit=%d' % (offset + limit, limit) if offset + limit < total_count else None},
           'objects': ratings}

    return HttpResponse(json.dumps(ret), content_type='application/json')
