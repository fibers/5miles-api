__author__ = 'dragon'

import redis
import re
from entity.models import PushTemplate
from annoying.functions import get_object_or_None
from entity.service_base import SrService

r = SrService().get_redis()


def push_system_message_to_user_inbox(to_uid, timestamp, msg_text=None, template=None, data=None):
    # Put a system message into the target user's inbox
    if msg_text is None and template is None:
        return

    inbox_key = 'user:%s:inbox:system' % to_uid
    if template is not None:
        push_template = get_object_or_None(PushTemplate, name=template)
        msg_text = push_template.content

        if data is not None:
            for (k, v) in data.items():
                msg_text = msg_text.replace('<%%= %s %%>' % k, v)

    r.incr('%s:new_count' % inbox_key, 1)

    # Create new message object
    msg_id = r.incr('global:next_message_id')
    with r.pipeline() as pipe:
        pipe.hmset('message:%s' % msg_id, {'id': msg_id, 'to': to_uid, 'text': msg_text, 'unread': True, 'timestamp': timestamp, 'type': 'system'})
        pipe.zadd(inbox_key, msg_id, timestamp)
        pipe.execute()


def parse_email_text(text):
    """
    Parse the email content.
    :param text: The email content
    :return: The tuple contains the text and price.
             Currently consider that all the email content is the text, so the price always be -1.
    """

    if not text:
        return '', -1

    content_array = re.split('-{100,}', text, maxsplit=1)

    return content_array[0], -1







