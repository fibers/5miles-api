# -*- coding: utf-8 -*-
from common.biz import unfuzzy_item_id, unfuzzy_user_id
from entity.exceptions import NotFoundError, InvalidParameterError
from common.utils import lat_or_lon_format


class ChatParams(object):
    item_id = None
    from_user = None
    to_user = None
    content_type = 0


class TextChatParams(ChatParams):
    content_type = 0
    text = None
    price = 0


class PictureChatParams(ChatParams):
    content_type = 1
    picture_url = None


class LocationChatParams(ChatParams):
    content_type = 2
    address_name = None
    lat = 0
    lon = 0
    address_map_thumb = None
    place_name = None


def build_chat_params(request):
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))
    if not item_id:
        raise NotFoundError()

    to_user = unfuzzy_user_id(request.POST.get('to_user', ''))
    if to_user is None:
        raise NotFoundError()
    elif to_user == str(request.user_id):
        raise InvalidParameterError()

    content_type = int(request.POST.get('msg_type', 0))     # 0: text, 1: photo, 2: location
    params = None
    if content_type == 0:
        params = TextChatParams()
        try:
            params.price = float(request.POST.get('price', -1))
        except ValueError:
            params.price = -1
        params.text = request.POST.get('text', '')
    elif content_type == 1:
        params = PictureChatParams()
        params.picture_url = request.POST.get('picture_url', '')
    elif content_type == 2:
        params = LocationChatParams()
        params.address_name = request.POST.get('address_name', '')
        params.lat = lat_or_lon_format(float(request.POST.get('lat', 0)))
        params.lon = lat_or_lon_format(float(request.POST.get('lon', 0)))
        params.address_map_thumb = request.POST.get('address_map_thumb', '')
        params.place_name = request.POST.get('place_name', '')
    else:
        params = ChatParams()

    params.item_id = item_id
    params.from_user = request.user_id
    params.to_user = to_user

    return params
