# -*- coding: utf-8 -*-
from django.db import models


class LinkAccount(models.Model):
    id = models.AutoField(primary_key=True)
    link_id = models.CharField(max_length=50, blank=True, default='')
    link_token = models.TextField(blank=True, default='')
    user_id = models.IntegerField()
    # state 1 => linked, 0 => unlinked
    state = models.IntegerField(default=1)
    # type link account type: 0 => facebook
    type = models.IntegerField(null=True, default=0)
    expire = models.IntegerField(null=True, default=0)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)