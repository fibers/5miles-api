# -*- coding: utf-8 -*-

from django.views.decorators.http import require_GET, require_POST
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.translation import ugettext as _
from ipware.ip import get_real_ip

import redis
import json

from commerce.utils import *
from common.biz import *
from common import biz
from common.utils_web import check_blacklist
from common.utils_func import build_list_json
from entity.exceptions import InvalidParameterError, UserNotFoundError
from entity.service import UserService
from common.utils import lat_or_lon_format, _get_latlng_by_geoip
from sociallink.models import LinkAccount
from message.tasks import notify
from entity import user_search

r = SrService().get_redis()
print 'sociallink: connect to redis'

user_service = UserService()

@require_POST
@check_blacklist
def follow(request):
    fuzzy_user_id = request.POST.get('user_id', '')
    fuzzy_user_ids = request.POST.get('user_ids', '')
    if fuzzy_user_id:
        target_user = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)
        UserService().follow(request.user_id, target_user)
    elif fuzzy_user_ids:
        try:
            for fuzzy_user_id in fuzzy_user_ids.split(','):
                target_user = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)
                UserService().follow(request.user_id, target_user)
        except InvalidParameterError:
            pass

    return HttpResponse(status=201)

@require_POST
def unfollow(request):
    fuzzy_user_id = request.POST.get('user_id', '')
    target_user = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)

    if target_user is None:
        raise UserNotFoundError(fuzzy_user_id)

    with r.pipeline() as pipe:
        pipe.zrem('user:%s:followers' % target_user, request.user_id)
        pipe.zrem('user:%s:following' % request.user_id, target_user)
        pipe.execute()

    return HttpResponse()

@require_GET
def get_followers(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    fuzzy_user_id = request.GET.get('user_id', '')
    user_id = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)

    if user_id is None:
        raise UserNotFoundError(fuzzy_user_id)

    follower_ids = r.zrevrange('user:%s:followers' % user_id, offset, offset+limit-1)
    user_service = UserService()
    users = []
    for uid in follower_ids:
        user = get_user_from_id(uid)
        user['following'] = user_service.is_following(request.user_id, uid)
        users.append(user)

    ret = {'meta':{'limit':limit, 'next':'?offset=%d&limit=%d' % (offset+limit, limit) if len(follower_ids) >= limit else None, 'offset':offset}, 'objects':users}

    return HttpResponse(json.dumps(ret), content_type='application/json', status=200)

@require_GET
def get_following(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    fuzzy_user_id = request.GET.get('user_id', '')
    user_id = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)

    if user_id is None:
        raise UserNotFoundError(fuzzy_user_id)

    following_ids = r.zrevrange('user:%s:following' % user_id, offset, offset+limit-1)
    user_service = UserService()
    users = []
    for uid in following_ids:
        user = get_user_from_id(uid)
        user['following'] = user_service.is_following(request.user_id, uid)
        users.append(user)

    ret = {'meta':{'limit':limit, 'next':'?offset=%d&limit=%d' % (offset+limit, limit) if len(following_ids) >= limit else None, 'offset':offset}, 'objects':users}

    return HttpResponse(json.dumps(ret), content_type='application/json', status=200)


@require_GET
def users_nearby(request):
    # 获取用户位置附近10个用户
    request_params = user_search.init_params_by_request(request)
    request_params.followings = user_service.get_followings(request.user_id)
    users, hits_count, total_count = user_search.SearchUserService().search_users_nearby(request_params)
    return HttpResponse(json.dumps(users), content_type='application/json', status=200)

@require_GET
def users_nearby_new(request):
    # 获取用户位置附近10个用户
    request_params = user_search.init_params_by_request(request)
    request_params.followings = user_service.get_followings(request.user_id)
    users, hits_count, total_count = user_search.SearchUserService().search_users_nearby(request_params)
    res_ext = {'sufficient_users_hint': _('Want to know what your neighbors just listed?\nFollow them now to find out!'),
               'insufficient_users_hint': _('Sorry! 5miles couldn\'t find enough sellers close to you. Here are some recommended top sellers from other nearby cities. \nStart listing items and you too can be featured below.')}
    ret = build_list_json(total_count, users, request_params.offset, request_params.limit, res_ext)
    return HttpResponse(json.dumps(ret), content_type='application/json', status=200)

@require_POST
@validate_user
def link_facebook(request):
    fb_user_id = request.POST.get('fb_userid', '')
    fb_token = request.POST.get('fb_token', '')
    fb_token_expires = int(request.POST.get('fb_expire', 0))

    # 判断数据库中是否已经绑定该类型的link
    link_accounts = LinkAccount.objects.filter(user_id=request.user_id, type=0)
    if len(link_accounts) > 0:
        # 更新数据库
        link_account = link_accounts[0]
        link_account.expire = fb_token_expires
        link_account.link_id = fb_user_id
        link_account.link_token = fb_token
        link_account.state = 1
        link_account.save()
    else:
        # 添加数据库
        link_account = LinkAccount(link_id=fb_user_id, link_token=fb_token, expire=fb_token_expires, user_id=request.user_id, state=1, type=0)
        link_account.save()

    biz.link_facebook(request.user_id, fb_user_id, fb_token, fb_token_expires)

    return HttpResponse(status=201)


@require_POST
def unlink_facebook(request):
    linked_accounts = LinkAccount.objects.filter(user_id=request.user_id, type=0)
    for linked_account in linked_accounts:
        linked_account.state = 0
        linked_account.save()

    biz.unlink_facebook(request.user_id)

    return HttpResponse()


@require_POST
def block(request):
    fuzzy_target_user = request.POST.get('target_user', '')
    if not fuzzy_target_user:
        raise InvalidParameterError()

    target_user = unfuzzy_user_id(fuzzy_target_user)
    user_service.block_user(request.user_id, target_user)

    return HttpResponse()


@require_POST
def unblock(request):
    fuzzy_target_user = request.POST.get('target_user', '')
    if not fuzzy_target_user:
        raise InvalidParameterError()

    target_user = unfuzzy_user_id(fuzzy_target_user)
    user_service.unblock_user(request.user_id, target_user)

    return HttpResponse()

