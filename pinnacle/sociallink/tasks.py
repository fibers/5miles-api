# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import shared_task
from entity.utils import obfuscate_id

__author__ = 'chuyouxin'

import logging
import redis

from django.conf import settings
from sociallink.models import LinkAccount
from entity.models import Item
from common.utils import capture_message, capture_exception
from common.deep_link import *
from entity.service_base import SrService

logger = logging.getLogger("pinnacle")
r = SrService().get_redis()

@shared_task
def share_item_to_facebook(user_id, item_id):
    base_url = settings.FACEBOOK['API_URL'] + "me/feed"
    token = r.hget('user:%s:linked_facebook' % user_id, 'fb_token')

    if not token or token == '':
        # logger.error('facebook token is empty or expired: user_id => %s, item_id => %s' % (user_id, item_id))
        # capture_message('facebook token is empty or expired: user_id => %s, item_id => %s' % (user_id, item_id))
        return

    # if settings.DEBUG:
    #     logger.debug('[auto_share_item]token: %s' % token)

    # 获取城市
    item = Item.objects.get(pk=item_id)
    city = item.city
    title = item.title
    local_price = item.local_price
    fuzzy_item_id = item.fuzzy_item_id

    city = 'my city' if city == '' else city
    fuzzy_user_id = obfuscate_id(user_id)
    share_type = 'item'
    event_id = fuzzy_item_id
    desktop_url = 'http://5milesapp.com/item/%s' % fuzzy_item_id
    branch_short_link = create_branch_short_link(
        data={'userId': fuzzy_user_id, 'shareType': share_type, 'eventId': event_id,
              'userid': fuzzy_user_id, 'itemid': event_id,
              'user_id': fuzzy_user_id, 'share_type': share_type, 'event_id': event_id,
              '$desktop_url': desktop_url, '$og_url': desktop_url},
        alias=create_branch_link_alias(fuzzy_user_id, share_type, event_id))
    share_content = settings.FACEBOOK['SHARE_CONTENT'] % (city, title, local_price, branch_short_link)
    # if settings.DEBUG:
    #     logger.debug('share content: %s' % share_content)
    data = dict()
    data['access_token'] = token
    data['message'] = share_content.encode('utf-8')
    data['link'] = branch_short_link
    try:
        resp = requests.post(base_url, data=data)
        text = resp.text
        status_code = resp.status_code
        if settings.DEBUG:
            logger.debug('status code: %s, result: %s' % (status_code, text))
        if status_code < 200 or status_code > 299:
            message = 'Failed to share item to facebook: user_id: %s, item_id: %s, token: %s, status_code: %s, text: %s'\
                      % (user_id, item_id, token, status_code, text)
            logger.warn(message)
            # 由于无效token越来越多，分享失败也越来越多，为避免过多报警，先注释掉。需要改为由app来分享
            # if status_code != 403:
            #     capture_message(message)
    except RequestException, e:
        logger.exception(e)


