# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'LinkAccount.link_token'
        db.alter_column(u'sociallink_linkaccount', 'link_token', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):

        # Changing field 'LinkAccount.link_token'
        db.alter_column(u'sociallink_linkaccount', 'link_token', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'sociallink.linkaccount': {
            'Meta': {'object_name': 'LinkAccount'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expire': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'link_token': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'state': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['sociallink']