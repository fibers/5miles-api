# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LinkAccount'
        db.create_table(u'sociallink_linkaccount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('link_id', self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True)),
            ('link_token', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')()),
            ('state', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('type', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
            ('expire', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'sociallink', ['LinkAccount'])


    def backwards(self, orm):
        # Deleting model 'LinkAccount'
        db.delete_table(u'sociallink_linkaccount')


    models = {
        u'sociallink.linkaccount': {
            'Meta': {'object_name': 'LinkAccount'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expire': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'link_token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'state': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['sociallink']