# -*- coding: utf-8 -*-

from django.db import models

CAMPAIGN_TYPE_LAUNCH = 1
CAMPAIGN_TYPE_BANNER = 2


class Campaign(models.Model):
    name = models.CharField(max_length=50, unique=True)
    # campaign type: 1 = launch, 2 = banner campaign, 3 = ad banner
    campaign_type = models.SmallIntegerField(default=1)
    image_url = models.CharField(max_length=255, default='')
    image_width = models.IntegerField()
    image_height = models.IntegerField()
    click_action = models.CharField(max_length=255, blank=True)
    begin_at = models.DateTimeField()   # UTC
    end_at = models.DateTimeField()
    comment = models.CharField(blank=True, max_length=255)