# -*- coding: utf-8 -*-

import logging
import datetime
import json

from django.utils.timezone import utc
from django.views.decorators.http import require_GET
from django.http import HttpResponse
from campaign.models import Campaign, CAMPAIGN_TYPE_LAUNCH
from common.utils import get_timestamp
from entity.exceptions import InvalidParameterError

logger = logging.getLogger('pinnacle')


@require_GET
def get_campaigns(request):
    campaign_type = int(request.GET.get('type', 0))
    if campaign_type == 0:
        raise InvalidParameterError()

    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    campaigns = Campaign.objects.filter(begin_at__lte=now, end_at__gte=now, campaign_type=campaign_type)
    objects = []
    for campaign in campaigns:
        camp_dict = dict()
        camp_dict['id'] = campaign.id
        camp_dict['name'] = campaign.name
        camp_dict['campaign_type'] = campaign_type
        camp_dict['image_url'] = campaign.image_url
        camp_dict['image_width'] = campaign.image_width
        camp_dict['image_height'] = campaign.image_height
        camp_dict['click_action'] = campaign.click_action
        camp_dict['begin_at'] = get_timestamp(campaign.begin_at)
        camp_dict['end_at'] = get_timestamp(campaign.end_at)
        objects.append(camp_dict)

    if campaign_type == CAMPAIGN_TYPE_LAUNCH and len(objects) > 1:
        objects = objects[:1]

    ret = {'meta': {'total_count': campaigns.count()},
           'objects': objects}

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def get_ad_banner(request):
    cid = int(request.GET.get('cid', 0))

    if cid == 0:
        raise InvalidParameterError('cid is required')

    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    campaigns = Campaign.objects.filter(id=cid, begin_at__lte=now, end_at__gte=now, campaign_type=3)
    ret = {}
    if campaigns:
        c = campaigns[0]
        ret = {'id': c.id, 'name': c.name, 'image_url': c.image_url, 'click_action': c.click_action,
               'image_width': c.image_width, 'image_height': c.image_height}

    return HttpResponse(json.dumps(ret), content_type='application/json')
