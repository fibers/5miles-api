# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Campaign.state'
        db.delete_column(u'campaign_campaign', 'state')

        # Deleting field 'Campaign.image'
        db.delete_column(u'campaign_campaign', 'image')

        # Adding field 'Campaign.image_url'
        db.add_column(u'campaign_campaign', 'image_url',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Adding field 'Campaign.image_width'
        db.add_column(u'campaign_campaign', 'image_width',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Campaign.image_height'
        db.add_column(u'campaign_campaign', 'image_height',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Campaign.state'
        db.add_column(u'campaign_campaign', 'state',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Campaign.image'
        db.add_column(u'campaign_campaign', 'image',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Deleting field 'Campaign.image_url'
        db.delete_column(u'campaign_campaign', 'image_url')

        # Deleting field 'Campaign.image_width'
        db.delete_column(u'campaign_campaign', 'image_width')

        # Deleting field 'Campaign.image_height'
        db.delete_column(u'campaign_campaign', 'image_height')


    models = {
        u'campaign.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'begin_at': ('django.db.models.fields.DateTimeField', [], {}),
            'campaign_type': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'click_action': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'end_at': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_height': ('django.db.models.fields.IntegerField', [], {}),
            'image_url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'image_width': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['campaign']