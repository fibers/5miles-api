# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse

from django.views.decorators.http import require_POST, require_GET
from common.biz import unfuzzy_user_id, get_user_from_id, unfuzzy_item_id
from common.utils import get_timestamp
from common.utils_func import build_list_json
from entity.exceptions import InvalidParameterError, UserNotFoundError
from entity.service_base import SrService
from review.models import Review
from .services import ReviewService

review_service = ReviewService()
r = SrService().get_redis()


@require_POST
def post_review(request):
    reviewer_id = request.user_id
    fuzzy_target_user = request.POST.get('target_user', '')
    score = int(request.POST.get('score', 0))
    comment = request.POST.get('comment', '')
    direction = int(request.POST.get('direction', 0))
    fuzzy_item_id = request.POST.get('item_id', '')

    if not fuzzy_target_user:
        raise InvalidParameterError()

    if not fuzzy_item_id:
        raise InvalidParameterError()

    if score not in [1, 2, 3, 4, 5]:
        raise InvalidParameterError()

    if direction not in [0, 1]:
        raise InvalidParameterError()

    target_user = unfuzzy_user_id(fuzzy_target_user)
    item_id = unfuzzy_item_id(fuzzy_item_id)
    review_service.post_review(reviewer_id, target_user, direction, score, comment, item_id)

    return HttpResponse()


@require_GET
def get_reviews(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    fuzzy_user_id = request.GET.get('user_id', '')

    if not fuzzy_user_id:
        raise InvalidParameterError()

    user_id = unfuzzy_user_id(fuzzy_user_id)
    total_count, reviews = review_service.get_reviews(user_id, offset, limit)
    objects = []

    for obj in reviews:
        objects.append({'id': obj.id, 'score': obj.score, 'comment': obj.comment,
                        'created_at': get_timestamp(obj.created_at),
                        'reviewer': get_user_from_id(obj.reviewer_id),
                        'reported': obj.has_reported(request.user_id)})

    review_num, review_score = review_service.get_avg_score(user_id)
    ret = build_list_json(total_count, objects, offset, limit, {'review_num': review_num, 'review_score': review_score})

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def get_review(request):
    fuzzy_target_user = request.GET.get('target_user', '')
    fuzzy_item_id = request.GET.get('item_id', '')
    if not fuzzy_target_user:
        raise InvalidParameterError()
    if not fuzzy_item_id:
        raise InvalidParameterError()

    target_user = unfuzzy_user_id(fuzzy_target_user)
    item_id = unfuzzy_item_id(fuzzy_item_id)

    reviews = Review.objects.filter(reviewer_id=request.user_id, target_user=target_user, item_id=item_id)
    ret = {}
    if reviews:
        ret = {'id': reviews[0].id, 'score': reviews[0].score, 'comment': reviews[0].comment,
               'created_at': get_timestamp(reviews[0].created_at),
               'reviewer': get_user_from_id(reviews[0].reviewer_id)}

    return HttpResponse(json.dumps(ret), content_type='application/json')

