# -*- coding: utf-8 -*-
from django.db.models import Sum
from common.utils import get_now_timestamp
from entity.service_base import singleton, BaseService, SrService
from message.tasks import notify
from boss.tasks import send_review_to_boss
from .models import Review
__author__ = 'dragon'


@singleton
class ReviewService(BaseService):
    def __init__(self):
        srs = SrService()
        self.cache = srs.get_redis_ex()
        self.redis = srs.get_redis()

    def post_review(self, reviewer_id, target_user, direction, score, comment, item_id):
        reviews = Review.objects.filter(reviewer_id=reviewer_id, target_user=target_user, direction=direction,
                                        item_id=item_id)
        if reviews:
            review = reviews[0]
            review.comment = comment
            review.score = score
            review.state = 0
            review.save()
            send_review_to_boss.delay(review.id)
        else:
            review = Review(reviewer_id=reviewer_id, target_user=target_user, direction=direction, score=score,
                            comment=comment, state=0, item_id=item_id)
            review.save()
            send_review_to_boss.delay(review.id)


    @staticmethod
    def get_avg_score(user_id):
        results = Review.objects.filter(target_user=user_id, state=1)
        count = results.count()
        sum_score = results.aggregate(sum_score=Sum('score'))['sum_score']
        return count, round(float(sum_score) / count * 2.0) / 2.0 if sum_score else 5

    def get_reviews(self, user_id, offset, limit):
        query_set = Review.objects.filter(target_user=user_id, state=1).order_by('-last_updated_at')
        total_count = query_set.count()
        objects = query_set[offset: offset + limit]
        return total_count, objects

