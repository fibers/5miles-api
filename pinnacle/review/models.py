# -*- coding: utf-8 -*-

from django.db import models
from spam.models import ReviewReportRecord

__author__ = 'dragon'


class Review(models.Model):
    reviewer_id = models.PositiveIntegerField()
    target_user = models.PositiveIntegerField(db_index=True)
    item_id = models.PositiveIntegerField(db_index=True)
    score = models.PositiveSmallIntegerField()
    comment = models.TextField(blank=True, default='')
    # state 1: approved, 0: pending, 2: disapproved
    state = models.SmallIntegerField(default=0)
    # direction 0: buyer => seller, 1: seller => buyer
    direction = models.SmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    last_updated_at = models.DateTimeField(auto_now=True)

    def has_reported(self, user_id):
        reports = ReviewReportRecord.objects.filter(reporter_id=user_id, review_id=self.pk)
        return True if reports.count() > 0 else False




