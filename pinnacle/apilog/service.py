# -*- coding: utf-8 -*-
__author__ = 'chuyouxin'

import time
import logging
import json

from re import compile
from raven import Client
from ipware.ip import get_real_ip
from entity.utils import decrypt_item_id, decrypt_user_id
from django.conf import settings
from common.utils import get_now_timestamp, get_now_timestamp_str2
from entity.service_base import SrService

client = Client(settings.RAVEN_CONFIG['dsn'])

IGNORE_URLS = [compile(expr) for expr in settings.RECORD_EXEMPT_URLS]

sr_service = SrService()
r = sr_service.get_redis()
print 'entity: connect to redis'

reader = sr_service.get_geoip2()
print 'load geoip2'

logger = logging.getLogger("pinnacle")


class AnalyticsService:

    def record_analytic_data(self, request, ext_params={}):

        api_path = request.path_info.lstrip('/')
        if any(m.match(api_path) for m in IGNORE_URLS):
            return

        user_id = request.user_id if hasattr(request, 'user_id') else ''
        ip_address = get_real_ip(request)
        user_agent = request.META.get('HTTP_USER_AGENT')
        referer = request.META.get('HTTP_REFERER')
        query_str = request.REQUEST
        lang_code = request.META.get('HTTP_ACCEPT_LANGUAGE', 'en-us')
        utm_date = get_now_timestamp_str2()

        country = ''
        city = ''
        try:
            if ip_address:
                response = reader.city(ip_address)
                country = response.country.name
                city = response.city.name
        except Exception, e:
            logger.warn('[record_analytic_data] - error_message:%s,params:%s,%s,%s,%s' % (e.message, user_id, api_path, user_agent, ip_address))

        new_query_str = ''
        json_query_str = ''
        try:
            for dict in query_str.dicts:
                if len(dict) > 0:
                    for key in dict:
                        fields = settings.ANALYTIC_QUERY_FILTER_FIELDS
                        if key not in fields:
                            new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
                            json_query_str = '%s,' % json_query_str if json_query_str != '' else ''
                            value = dict[key]
                            if key == 'item_id':
                                # item_id = r.get('fuzzy_item_id:%s:item_id' % value)
                                item_id_arr = decrypt_item_id(value)
                                item_id = item_id_arr[0] if len(item_id_arr) > 0 else ''
                                json_query_str = '%s"%s":"%s"' % (json_query_str, key, item_id if item_id else value)
                                new_query_str = '%s%s:%s' % (new_query_str, key, item_id if item_id else value)
                            elif key == 'user_id' or key == 'to_user' or key == 'seller_id' or key == 'target_user':
                                # f_user_id = r.get('fuzzy_id:%s:user_id' % value)
                                f_user_id_arr = decrypt_user_id(value)
                                f_user_id = f_user_id_arr[0] if len(f_user_id_arr) > 0 else ''
                                json_query_str = '%s"%s":"%s"' % (json_query_str, key, f_user_id if f_user_id else value)
                                new_query_str = '%s%s:%s' % (new_query_str, key, f_user_id if f_user_id else value)
                            elif key == 'user_ids':
                                users = value.split(',')
                                user_ids_str = ''
                                for user in users:
                                    f_user_id_arr = decrypt_user_id(user)
                                    f_user_id = str(f_user_id_arr[0]) if len(f_user_id_arr) > 0 else ''
                                    if f_user_id != '':
                                        user_ids_str += ',' if user_ids_str != '' else ''
                                        user_ids_str += f_user_id
                                    logging.getLogger("pinnacle").warn(user_ids_str)
                                json_query_str = '%s"%s":"%s"' % (json_query_str, key, user_ids_str)
                                new_query_str = '%s%s:%s' % (new_query_str, key, user_ids_str)

                            elif key == 'images':
                                json_query_str = '%s"%s":"%s"' % (json_query_str, key, value.replace('\/', '/').replace(' ','').replace('|', '&#124;').replace('\n', ' '))
                                new_query_str = '%s%s:%s' % (new_query_str, key, value.replace('\/', '/').replace(' ','').replace('|', '&#124;').replace('\n', ' '))

                            elif key != 'desc':
                                json_query_str = '%s"%s":"%s"' % (json_query_str, key, value.replace('|', '&#124;').replace('\n', ' '))
                                new_query_str = '%s%s:%s' % (new_query_str, key, value.replace('|', '&#124;').replace('\n', ' '))

            new_query_str = new_query_str.encode('utf-8')
            if api_path == 'api/v2/accept_offer/':
                # 获取item_id存入日志中
                json_data = json.loads('{%s}' % json_query_str)
                ref_offer_id = json_data['ref_offer_id']

                # 获取offer的价格、本地价格
                offers = r.hmget('offer:%s' % ref_offer_id, 'price', 'local_price')
                price = offers[0]
                local_price = offers[1]

                offerline_id = r.get('offer:%s:offerline_id' % ref_offer_id)
                item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')

                new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
                new_query_str = '%sitem_id:%s,price:%s,local_price:%s' % (new_query_str, item_id, price, local_price)

            elif api_path == 'api/v2/login_facebook/':
                # 获取item_id存入日志中
                json_data = json.loads('{%s}' % json_query_str)
                fb_user_id = json_data['fb_userid']
                fbl_user_id = r.get('login:facebook:%s:user_id' % fb_user_id)
                new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
                # type为0时表示新注册用户,1表示facebook用户登录,30秒之内,系统认为是新用户注册
                op_type = 0
                if fbl_user_id:
                    current_time = get_now_timestamp()
                    created_at = r.hget('user:%s' % fbl_user_id, 'created_at')
                    op_type = 0 if current_time - long(created_at) < 30 else 1

                new_query_str = '%s%s:%s,%s:%s' % (new_query_str, 'user_id', fbl_user_id, 'type', op_type)
                user_id = fbl_user_id

            elif api_path == 'api/v2/signup_email/':
                json_data = json.loads('{%s}' % json_query_str)
                email = json_data['email']
                s_user_id = r.get('login:email:%s:user_id' % email)
                new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
                new_query_str = '%s%s:%s' % (new_query_str, 'user_id', s_user_id)
                user_id = s_user_id

            elif api_path == 'api/v2/confirm_delivery/' or api_path == 'api/v2/offerline/':
                # 获取item_id存入日志中
                json_data = json.loads('{%s}' % json_query_str)
                offerline_id = json_data['offerline_id']

                item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')

                new_query_str = '%s,' % new_query_str if new_query_str != '' else ''
                new_query_str = '%sitem_id:%s' % (new_query_str, item_id)

        except Exception, e1:
            logger.error(e1)

        try:
            referer = referer if referer else ''
            lang_code = lang_code if lang_code else ''
            ip_address = ip_address if ip_address else ''
            v_hour = time.strptime(utm_date, '%Y-%m-%d %H:%M:%SZ').tm_hour
            api_path = api_path.encode('utf-8')
            user_id = str(user_id).encode('utf-8') if user_id is not None else ''
            country = country.encode('utf-8') if country is not None else ''
            city = city.encode('utf-8') if city is not None else ''
            exec_time = ext_params['exec_time'] if ext_params and 'exec_time' in ext_params else ''
            status = ext_params['status_code'] if ext_params and 'status_code' in ext_params else ''
            log_datas = "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s" % (user_id, api_path, new_query_str, referer,
                                                                    user_agent, lang_code, ip_address, v_hour, country, city, exec_time, status)
                
            logging.getLogger("analytics").info(log_datas)
        except Exception, e:
            logger.error(e)


    def record_item_exposure(self, request, items, offset, ext_params={}):

        user_id = request.user_id if hasattr(request, 'user_id') else ''
        api_path = request.path_info.lstrip('/')
        ip_address = get_real_ip(request)
        user_agent = request.META.get('HTTP_USER_AGENT')
        # utm_date = get_now_timestamp_str2()

        country = ''
        city = ''
        try:
            if ip_address:
                response = reader.city(ip_address)
                country = response.country.name
                city = response.city.name
        except Exception, e:
            logger.warn('[record_item_exposure] - error_message:%s,params:%s,%s,%s' % (e.message, user_id, api_path, ip_address))

        try:
            ip_address = ip_address if ip_address else ''
            api_path = api_path.encode('utf-8') if api_path is not None else ''
            user_agent = user_agent.encode('utf-8') if user_agent is not None else ''
            user_id = str(user_id).encode('utf-8') if user_id is not None else ''
            country = country.encode('utf-8') if country is not None else ''
            city = city.encode('utf-8') if city is not None else ''

            item_ids = []
            for item in items:
                # 计算商品在当前页面出现在第几个位置
                fuzzy_item_id = item['id'] if 'id' in item else ''

                fuzzy_item_id = str(fuzzy_item_id).encode('utf-8') if fuzzy_item_id else ''
                item_id = decrypt_item_id(fuzzy_item_id)[0] if fuzzy_item_id != '' else ''

                if item_id:
                    item_ids.append(item_id)

            item_ids_str = ','.join('%s' % x for x in item_ids)
            log_datas = "%s|%s|%s|%s|%s|%s|%s|%s" % (user_id, api_path, item_ids_str, offset + 1, user_agent, ip_address, country, city)
            logging.getLogger("analytics_exposure").info(log_datas)
        except Exception, e:
            logger.warn(items)
            logger.error(e)
