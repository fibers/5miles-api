# -*- coding: utf-8 -*-
__author__ = 'dragon'

import requests
import logging
import json
import hashlib

from django.conf import settings
from requests.exceptions import RequestException
from common.utils import capture_exception

logger = logging.getLogger('pinnacle')

#BRANCH_DOMAIN_NAME = 'https://bnc.lt/'
BRANCH_DOMAIN_NAME = 'https://app.5miles.us/'


def create_branch_short_link(data, **kwargs):
    try:
        params = {'app_id': settings.BRANCH_API_KEY, 'data': data}
        params.update(kwargs)
        resp = requests.post('%s/v1/url' % settings.BRANCH_API_BASE_URL,
                             json=params,
                             headers={'Content-Type': 'application/json'})
        if settings.DEBUG:
            logger.debug('Branch API status code: %s, text: %s' % (resp.status_code, resp.text))
        if resp.status_code == 200:
            return resp.json()['url']
        elif resp.status_code == 409:
            return '%s%s' % (BRANCH_DOMAIN_NAME, params['alias'])
    except RequestException, e:
        logger.exception(e)
        capture_exception()
        raise e


def create_branch_link_alias(user_id, share_type, event_id):
    m = hashlib.md5()
    m.update('%s%s%s' % (user_id, share_type, event_id))
    md5 = m.hexdigest()[:6]
    return '5miles-v2-%s' % md5


if __name__ == '__main__':
    print create_branch_short_link(data={'userId': 'test', 'shareType': 'item', 'eventId': '1234', '$desktop_url': 'http://5milesapp.com', '$og_url': 'http://5milesapp.com'}, alias='5miles-v2-8944f2')
