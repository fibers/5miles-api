import json
import logging
from common.biz import unfuzzy_user_id
from entity.exceptions import UserBlockedError
from entity.service import UserService
from common.utils import lat_or_lon_format

__author__ = 'admin'

logger = logging.getLogger('pinnacle')


def get_user_id(request):
    if hasattr(request, "user_id"):
        user_id = request.user_id
        return user_id
    return None


def lab_format(request, type):
    return lat_or_lon_format(float(request.POST.get(type, 0)))

def check_blacklist(func):
    def wrapper(request, *args, **kwargs):
        fuzzy_target_user = request.POST.get('to_user', '')
        if not fuzzy_target_user:
            fuzzy_target_user = request.POST.get('user_id', '')
        if not fuzzy_target_user:
            fuzzy_target_user = request.POST.get('target_user', '')

        if not fuzzy_target_user:
            return func(request, *args, **kwargs)

        target_user = unfuzzy_user_id(fuzzy_target_user)
        if hasattr(request, 'user_id') and UserService().is_blocked(target_user, request.user_id):
            raise UserBlockedError()
        else:
            return func(request, *args, **kwargs)
    return wrapper
