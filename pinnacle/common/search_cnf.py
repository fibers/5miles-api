import logging

__author__ = 'lvyi'


logger = logging.getLogger("pinnacle")


distance_confs = {
            1:{"val":5,"txt":"5 miles"},
            2:{"val":10,"txt":"10 miles"},
            3:{"val":20,"txt":"20 miles"},
            4:{"val":30,"txt":"30 miles"},
            5:{"val":50,"txt":"50 miles"}
        }

sort_confs = {1:{"field":"location","dir":"asc"},
             2:{"field":"created_at","dir":"desc"},
             3:{"field":"price","dir":"asc"},
             4:{"field":"price","dir":"desc"}}

#0:[{"field":"updated_at","dir":"dasc"},{"field":"location","dir":"asc"}],

list_type_tag_confs = {'home':'H',
                       'search':'S',
                       'category':'C',
                       'suggest':'G',
                       'sellings':'L',
                       'push':'P',
                       'brand':'B' ,
                       'system':'Y',
                       'none':'N'}


ab_test_type_home_active_at = 'home_active_at'

def ab_test_ctrl(type, search_params):
    try:
        if type=='home_active_at' and search_params:
            user_id = search_params.user_id
            if search_params and user_id and int(user_id)%2==0:
                logger.info("ab_%s:a:%s"% (ab_test_type_home_active_at, user_id))
                return True
            else:
                logger.info("ab_%s:b:%s"% (ab_test_type_home_active_at, user_id))
                return False
    except:
        logger.info("ab_%s:c:%s"% (ab_test_type_home_active_at, search_params))
        return False

if __name__ == '__main__':

    print ab_test_ctrl(ab_test_type_home_active_at, 2222)