import logging
import os
from elasticsearch import Elasticsearch
from entity.service_base import singleton
from pinnacle.settings import ES_INDEX_MAIN, ES_DOCTYPE_ITEM, ES_DOCTYPE_USER, ES_HOSTS

__author__ = 'lvyi'

logger = logging.getLogger("pinnacle")


@singleton
class EsConnections(object):
    index_doc_cache = {}

    def __init__(self):
        hosts = ES_HOSTS
        #logger.debug("------->EsConnections init=%s" % hosts)
        self.es = Elasticsearch(hosts)
        logger.info("EsConnections es init db=%s" % hosts)

        # Create main index on ES if the index is not exist yet.
        items_body = {'item': {'properties': {'title': {'type': 'string'}, 'location': {'type': 'geo_point'}}}}
        self.es_index(ES_INDEX_MAIN, ES_DOCTYPE_ITEM, items_body)

        users_body = {'user': {'properties': {'location': {'type': 'geo_point'},
                                              'verified': {'type': 'boolean'},
                                              'nickname': {'type': 'string'},
                                              'id': {'type': 'long'}
                                              }
                               }
                      }
        self.es_index(ES_INDEX_MAIN, ES_DOCTYPE_USER, users_body)

    def es_index(self, index_name, doc_type, index_body):
        #logger.info("es_index")
        key = "%s_%s" % (index_name, doc_type)
        if not self.index_doc_cache.get(key):
            if not self.get_es().indices.exists(index=index_name):
                # Create index
                self.get_es().indices.create(index=index_name)

            if not self.get_es().indices.exists_type(index=index_name, doc_type=doc_type):
                # Create field mapping on the index
                try:
                    self.get_es().indices.put_mapping(index=index_name, doc_type=doc_type, body=index_body)
                except Exception:
                    self.get_es().indices.delete_mapping(index=index_name, doc_type=doc_type)
                    self.get_es().indices.put_mapping(index=index_name, doc_type=doc_type, body=index_body)

            self.index_doc_cache[key] = EsDao(self.get_es(), index_name, doc_type)

    def get_es(self):
        return self.es

    def dao(self, index_name, doc_type):
        return self.index_doc_cache["%s_%s" % (index_name, doc_type)];


class EsDao():
    def __init__(self, es, index_name, doc_type):
        self.es = es
        self.index_name = index_name
        self.doc_type = doc_type
        self.gray_env = 'no'
        pass

    def get_es(self):
        return self.es

    def index(self, doc_id, doc):
        return self.es.index(index=self.index_name, doc_type=self.doc_type, id=doc_id, body=doc)

    def update_field(self, doc_id, field, value):
        try:
            logger.debug("ES: update_field=%s"% doc_id)
            ret = self.es.update(index=self.index_name, doc_type=self.doc_type, id=doc_id,
                                 body={"doc": {field: value}})
        except Exception, e:
            try:
                logger.exception("update_field:%s,%s,%s,%s" % (doc_id, field, value, e), exc_info=True)
            except Exception, e1:
                logger.exception(e1)
            return None
        return ret

    def update_fields(self, doc_id, **kwargs):
        try:
            logger.debug("ES: update_fields=%s"% doc_id)
            ret = self.es.update(index=self.index_name, doc_type=self.doc_type, id=doc_id, body={"doc": kwargs})
        except Exception, e:
            try:
                logger.exception("update_fields:%s,%s,%s" % (doc_id, kwargs, e), exc_info=True)
            except Exception, e1:
                logger.exception(e1)
            return None
        return ret

    def update(self, doc_id, body, params=None):
        try:
            logger.debug("ES: update=%s"% doc_id)
            ret = self.es.update(index=self.index_name, doc_type=self.doc_type, id=doc_id, body=body, params=params)
        except Exception, e:
            try:
                logger.exception("update:%s,%s,%s,%s" % (doc_id, body, params, e), exc_info=True)
            except Exception, e1:
                logger.exception(e1)
            return None
        return ret

    def delete(self, doc_id):
        try:
            logger.debug("ES: delete=%s"% doc_id)
            ret = self.es.delete(index=self.index_name, doc_type=self.doc_type, id=doc_id)
        except Exception, e:
            try:
                logger.exception("delete:%s,%s" % (doc_id, e), exc_info=True)
            except Exception, e1:
                logger.exception(e1)
            ret = None
        return ret

    def get(self, doc_id):
        return self.es.get(index=self.index_name, doc_type=self.doc_type, id=doc_id)

    def exists(self, doc_id):
        return self.es.exists(index=self.index_name, doc_type=self.doc_type, id=doc_id)

    def delete_mapping(self):
        try:
            self.es.indices.delete_mapping(index=self.index_name, doc_type=self.doc_type)
        except Exception, e:
            return None

    def is_gray(self):
        if self.gray_env=='no':
            gray_env = os.getenv('GRAY')
            logger.info("gray_env=%s" % gray_env)
            if "True" == gray_env:
                self.gray_env = True
                return True
            else:
                self.gray_env = False
                return False
        else:
            return self.gray_env

    def es_search_exec(self, es_body, fields=None):

        if self.is_gray():
            logger.debug("es_search_exec=%s" % ("%s" % es_body).replace("'", "\""))

        if fields:
            return self.es.search(index=self.index_name, doc_type=self.doc_type,
                                  body=es_body,
                                  fields=fields)
        else:
            return self.es.search(index=self.index_name, doc_type=self.doc_type,
                                  body=es_body)


def es_dao_user():
    index_name = ES_INDEX_MAIN
    doc_type = ES_DOCTYPE_USER
    return EsConnections().dao(index_name, doc_type)


def es_dao_item():
    index_name = ES_INDEX_MAIN
    doc_type = ES_DOCTYPE_ITEM
    return EsConnections().dao(index_name, doc_type)


if __name__ == "__main__":
    '''
    dct = {'aaa':'a1' ,'bbb':'b1', 'ccc':'c1'}
    class abc():
        aaa = 1
        bbb = 2
        def __init__(self, **kwg):
            self.__dict__ = dict(kwg)

        def get(self, key):
            return self.__dict__.get(key,None)

        def __str__(self):
            return "abc:%s" %(self.__dict__.keys())

    a= abc()
    print a
    a= abc(**dct)
    print a
    print a.get('ddd')
    '''
    id = 1
    es_dao_user().index(id, {"id": 123, "location": {"lat": 123, "lon": 87}})

    es_dao_user().update_field(id, "edu", 123)

    print es_dao_user().exists(id)

    print es_dao_user().get(id)

    es_dao_user().update(id, {"doc": {'owner': {'verified': True}}})

    es_dao_user().delete(id)

    print es_dao_user().get(id)



