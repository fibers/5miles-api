# -*- coding: utf-8 -*-
import calendar
import datetime
import logging
from math import radians, cos, sin, asin, sqrt
import os
import traceback
from common import search_cnf

__author__ = 'lvyi'
# 注意：该文件只提供独立的可供其他类使用的工具方法，与业务逻辑，与服务环境无关

logger = logging.getLogger("pinnacle")

def get_now_ts():
    now = datetime.datetime.utcnow()
    timestamp = calendar.timegm(now.timetuple())

    return timestamp


def get_list_ids(items, get_number):
    ids = []
    for it in items[:get_number]:
        id = it.get("id", "")
        ids.append(id)
    return ids


def list_type_tag(list_type, parent_rf_tag=None):
    try:
        if "search"==list_type and parent_rf_tag:
            list_type = "push"
            return "%s%s" % (search_cnf.list_type_tag_confs[list_type] ,parent_rf_tag)
        else:
            return search_cnf.list_type_tag_confs[list_type]
    except:
        return "N"

def list_trace_tag(items, list_type='none' , offset=0, query_params=None):
    # 添加商品位置监控的埋点  T>H!O>1!P>8!R>101!Q>haha
    # T_O_P_R_[Q]
    # T Home / Search / Categary / Suggest / seLlers / Push / Brand / sYstem/
    # Q 列表条件
    # P 数据在本页的位置
    # O 页号
    # R refind参数

    # 第一阶段
    # T_O_P_Q

    parent_rf_tag = None
    if query_params:
        #logger.debug("parent_rf_tag=%s" % query_params.rf_tag)
        parent_rf_tag = query_params.rf_tag

    t = list_type_tag(list_type ,parent_rf_tag=parent_rf_tag)
    for p,row in enumerate(items):
        tag = "%s_%s_%s"% (t,offset,p)
        row['rf_tag'] = tag

def cal_distance(lat1,lon1,   lat2,lon2):
    """
    根据经纬度，计算2个点之间的距离
    """
    # convert decimal degrees to radians
    try:
        if lon1==0 or lat1==0 or lon2==0 or lat2==0:
            return -1

        lon1, lat1, lon2, lat2 = map(radians, [float(lon1), float(lat1), float(lon2), float(lat2)])
        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        km = 6371 * c

        distance = int(km * 1000)
        logging.debug("lon1=%s, lat1=%s, lon2=%s, lat2=%s, distance=%s" % (lon1, lat1, lon2, lat2, distance))
        return distance
    except Exception,e:
        #logging.error(traceback.format_exc())
        logger.warn(traceback.format_exc())
        return -1


def build_list_json(total_count, objects, offset, limit, res_ext=None):
    paginate_str = '?offset=%d&limit=%d'
    ret = {'meta': {'total_count': total_count, 'offset': offset, 'limit': len(objects),
                    'previous': paginate_str % (offset - limit, limit) if offset >= limit else None,
                    'next': paginate_str % (offset + limit, limit) if offset + limit < total_count else None},
           'objects': objects}
    if res_ext:
        ret['res_ext'] = res_ext

    return ret


import os

gray_env_tag = 'no'
def is_gray():
    global gray_env_tag
    if gray_env_tag=='no':
        gray_env_tag = os.getenv('GRAY')
        logger.info("gray_env_tag=%s" % gray_env_tag)
        if "True" == gray_env_tag:
            gray_env_tag = True
            return True
        else:
            gray_env_tag = False
            return False
    else:
        return gray_env_tag

if __name__ == "__main__":
    print os.getenv("DJANGO_SETTINGS_MODULE")
    print os.getenv('HOME')

    print is_gray()