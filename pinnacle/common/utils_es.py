from __future__ import absolute_import
from celery import shared_task
from common.es_dao import es_dao_item
from entity.search import SearchItems

__author__ = 'lvyi'


#def index_item_in_searchengine(item_id, item_doc):
#    return es_dao_item().index(item_id, item_doc)

def update_field_in_searchengine(item_id, field, value):
    #return EsService().update_field(item_id, field, value)
    return es_dao_item().update_field(item_id, field, value)

def update_fields_in_searchengine(item_id, **kwargs):
    #return EsService().update_fields(item_id, **kwargs)
    return es_dao_item().update_fields(item_id, **kwargs)

@shared_task(queue='celery.schedule')
def delete_item_in_searchengine(doc_id):
    # connection.close()
    #return EsService().delete_item(doc_id)
    return es_dao_item().delete(doc_id)

#def remove_all_items_in_searchengine():
#    EsService().remove_all_items()

#def delete_item_mapping():
#    EsService().delete_item_mapping()

def search_items_by_category(cat_id, fields=None):
    es_body = {'query': {'bool': {'must': [{'term': {'cat_id': str(cat_id)}},
                                           {'term': {'owner.is_robot': 0}}]}}}

    return es_dao_item().es_search_exec(es_body, fields=fields)


def search_item_by_id(id):
    return es_dao_item().es_search_exec({'query': {'term': {'_id': str(id)}}})


def search_item_count_by_id(id):
    return es_dao_item().es_search_exec({'query':{'term':{'_id':str(id)}}})

#def search_items_by_created_at(created_at):
#    es_body = {'query': {'bool': {'must': [{'range': {'created_at': {'from': created_at}}}]}},
#                              'sort': [{'created_at': 'asc'}], 'from': 0, 'size': settings.DEFAULT_SEARCH_ITEMS}
#    return es_search_exec(es_body)

def suggest_items_by_keyword_decay(keyword, cat_id, refind_params,
                                   is_query_keyword=True, is_filter_distance=False):

    es_body = SearchItems.suggest_items_by_keyword(keyword, cat_id, refind_params,
                                   is_query_keyword=is_query_keyword , is_filter_distance=is_filter_distance)
    return es_dao_item().es_search_exec(es_body)