# -*- coding: utf-8 -*-
from django.http import HttpResponseForbidden
from common import utils_func
from common.es_dao import es_dao_item
from entity.exceptions import UnauthorizedError
from entity.service_base import SrService

__author__ = 'dragon'

#import redis
import json
import re
import logging
import datetime

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import utc
from common.utils import get_now_timestamp
from entity.models import Category, FacebookProfile

#r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB)
r = SrService().get_redis()
# r_ex 创建一个新的 RedisDB 用户存放临时的数据(设置过期时间)，这些数据可以在需要的时候清空，系统会自动重建，不会影响中断服务
# lvyi 2015-2-5
#r_ex = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.V2_REDIS_DB_EX)

# 商品流转状态
ITEM_STATE_LISTING = 0                          # 正常listing
ITEM_STATE_OFFER_ACCEPTED = 1                   # 旧状态（废弃），对应ITEM_STATE_SOLD
ITEM_STATE_DONE_DELIVERY = 2                    # 旧状态（废弃）
ITEM_STATE_RATED_SELLER = 3                     # 旧状态（废弃）
ITEM_STATE_OFF_SHELF = 4                        # 旧状态（废弃），对应ITEM_STATE_UNAPPROVED
ITEM_STATE_USER_DELISTING = 5                   # 旧状态（废弃），对应ITEM_STATE_UNAVAILABLE
ITEM_STATE_UNAVAILABLE = 5                      # 新状态：用户删除
ITEM_STATE_UNLISTED = 6                         # 新状态：用户下架
ITEM_STATE_SOLD = 1                             # 新状态：用户标记为卖出
ITEM_STATE_UNAPPROVED = 4                       # 新状态：运营下架

# 商品审核状态
ITEM_INTERNAL_STATUS_PENDING_REVIEW = 0         # 用户上传与编辑后待审核
ITEM_INTERNAL_STATUS_APPROVED = 1               # 审核通过
ITEM_INTERNAL_STATUS_APPROVED_RISKY = 2         # 审核通过有风险，买家发offer回收到警告
ITEM_INTERNAL_STATUS_UNAPPROVED = 3             # 审核下架
ITEM_INTERNAL_STATUS_UNAPPROVED_PENDING = 4     # 审核不通过待编辑，30分钟内不重新编辑则自动下架

# 用户状态
USER_STATE_FORBIDDEN = 0    # 用户被禁用
USER_STATE_VALID = 1        # 用户有效
USER_STATE_DEACTIVE = 2     # 用户主动申请deactive(禁止推送任何消息,重新登陆后恢复)

USER_CHECKIN_INTERVAL = 15*60  # 用户活跃检查间隔

logger = logging.getLogger('pinnacle')


def unfuzzy_item_id(fuzzy_item_id):
    return r.get('fuzzy_item_id:%s:item_id' % fuzzy_item_id)


def unfuzzy_user_id(fuzzy_user_id):
    user_id = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)
    return user_id


def get_user_from_id(plain_user_id ,item_num= 0):
    data = r.hmget('user:%s' % plain_user_id, 'id', 'nickname', 'portrait', 'is_robot', 'verified', 'lat', 'lon',
                   'country', 'region', 'city')
    return {'id': data[0], 'nickname': data[1], 'portrait': data[2],
            'is_robot': int(data[3] if data[3] else 0),
            'verified': bool(data[4]) if data[4] is not None else False,
            'location': {'lat': float(data[5]) if data[5] is not None and data[5] != '' else 0,
                         'lon': float(data[6]) if data[6] is not None and data[6] != '' else 0},
            'place': get_place(data[7], data[8], data[9]),
            'post_index':item_num}


def get_item_redis_json(fuzzy_item_id, state, title, description, category_id, price, currency_code, local_price, lat,
                        lon, images, media_link, shipping_method, user_id, original_price, brand_id, brand_name, is_new,
                        created_at, modified_at=None, updated_at=None, country=None, region=None, city=None):
    redis_json = {'id': fuzzy_item_id, 'state': state, 'title': title, 'desc': description, 'cat_id': category_id,
                  'price': price, 'currency': currency_code, 'local_price': local_price,
                  'location': json.dumps({'lat': lat, 'lon': lon}), 'images': images, 'mediaLink': media_link,
                  'shipping_method': shipping_method, 'created_at': created_at,
                  'user_id': user_id, 'brand_name': brand_name, 'is_new': is_new}

    if brand_id:
        redis_json['brand_id'] = brand_id

    if original_price:
        redis_json['original_price'] = float(original_price)

    if updated_at:
        redis_json['updated_at'] = updated_at

    if modified_at:
        redis_json['modified_at'] = modified_at

    if country:
        redis_json['country'] = country

    if region:
        redis_json['region'] = region

    if city:
        redis_json['city'] = city

    return redis_json


def get_update_item_redis_json(title, description, category_id, price, currency_code, local_price, images, media_link,
                               original_price, shipping_method, brand_id, brand_name, modified_at=None, updated_at=None,
                               country=None, region=None, city=None):

    redis_json = {'title': title, 'desc': description, 'cat_id': category_id, 'price': price, 'currency': currency_code,
                  'local_price': local_price, 'images': images, 'mediaLink': media_link,
                  'shipping_method': shipping_method, 'brand_name': brand_name}

    if brand_id:
        redis_json['brand_id'] = brand_id

    if original_price:
        redis_json['original_price'] = float(original_price)

    if updated_at:
        redis_json['updated_at'] = updated_at

    if modified_at:
        redis_json['modified_at'] = modified_at

    if country:
        redis_json['country'] = country

    if region:
        redis_json['region'] = region

    if city:
        redis_json['city'] = city

    return redis_json


def get_item_es_json(fuzzy_item_id, title, description, category_id, images, media_link, price, currency_code,
                     local_price, original_price, brand_id, brand_name, lat, lon, owner, weight, cat_json, created_at,
                     modified_at=None, updated_at=None, is_new=0, country=None, region=None, city=None):

    es_json = {'id': fuzzy_item_id, 'title': title, 'desc': description, 'cat_id': category_id,
               'images': images, 'mediaLink': media_link, 'price': price, 'currency': currency_code,
               'local_price': local_price, 'brand_id': brand_id,
               'brand_name': brand_name, 'location': {'lat': lat, 'lon': lon}, 'owner': owner,
               'created_at': int(created_at),'listing_at': int(created_at), 'weight': weight,
               'cat': cat_json, 'is_new': is_new, 'last_active_at': get_now_timestamp()}

    #logger.debug("is_new=%s" % is_new)
    if modified_at:
        es_json['listing_at'] = int(modified_at)

    if original_price:
        es_json['original_price'] = float(original_price)

    if updated_at:
        es_json['updated_at'] = int(updated_at)

    if modified_at:
        es_json['modified_at'] = int(modified_at)

    if country:
        es_json['country'] = country

    if region:
        es_json['region'] = region

    if city:
        es_json['city'] = city

    return es_json


def get_update_item_es_json(title, description, category_id, images, mediaLink, price, currency_code, local_price,
                            original_price, brand_id, brand_name, cat_json, modified_at=None, updated_at=None,
                            country=None, region=None, city=None):

    es_json = {'title': title, 'desc': description, 'cat_id': category_id, 'images': images,
               'mediaLink': mediaLink, 'price': price,
               'currency': currency_code, 'local_price': local_price, 'brand_id': brand_id,
               'brand_name': brand_name, 'cat': cat_json}

    if original_price:
        es_json['original_price'] = float(original_price)

    if updated_at:
        es_json['updated_at'] = int(updated_at)

    if modified_at:
        es_json['modified_at'] = int(modified_at)

    if country:
        es_json['country'] = country

    if region:
        es_json['region'] = region

    if city:
        es_json['city'] = city

    return es_json


def get_category_as_json(category_id):
    cat = Category.objects.get(pk=int(category_id))
    return {'id': category_id, 'gender': cat.gender}


def link_facebook(user_id, fb_user_id, fb_token, fb_token_expires):
    """
    关联facebook帐号
    :param fb_token:
    :param user_id: 5miles UserID
    :param fb_user_id: facebook UserID
    :param fb_token_expires: facebook user access token过期时间（unix timestamp）
    :return: None
    """
    FacebookProfile.objects.filter(id=fb_user_id).update(fb_token=fb_token,
                                                         fb_token_expires=fb_token_expires,
                                                         updated_time=datetime.datetime.utcnow().replace(tzinfo=utc))
    with r.pipeline() as pipe:
        pipe.hmset('user:%s:linked_facebook' % user_id,
                   {'fb_user_id': fb_user_id, 'fb_token': fb_token, 'fb_token_expires': fb_token_expires})
        pipe.expireat('user:%s:linked_facebook' % user_id, fb_token_expires)
        pipe.execute()


def unlink_facebook(user_id):
    """
    取消与facebook账号的关联
    :param user_id: 5miles UserID
    :return: None
    """
    r.delete('user:%s:linked_facebook' % user_id)


def is_app_user(user_id):
    """
    判断用户是否是APP用户，依据是该用户是否绑定过个推或Parse
    :param user_id: 用户ID
    :return: True / False
    """
    if r.scard('user:%s:installation_ids' % user_id) > 0 \
            or r.scard('user:%s:clients' % user_id) > 0 \
            or r.scard('push:getui:user:%s:insts' % user_id) > 0 \
            or r.scard('push:parse:user:%s:insts' % user_id) > 0:
        return True
    else:
        return False


def has_mobile_phone(user_id):
    """
    判断用户是否拥有手机号码
    :param user_id: 用户ID
    :return: True / False
    """
    mobile_phone = r.hget('user:%s' % user_id, 'mobile_phone')
    return True if mobile_phone else False


def is_craigslist_user(user_id):
    """
    判断用户是否Craigslist用户
    :param user_id: 用户ID
    :return: True / False
    """
    # Craigslist用户
    email = r.hget('user:%s' % user_id, 'email')
    pattern = re.compile('^az.*@worldbuz.com$')
    return True if pattern.match(email) else False


def validate_user(func):
    def wrapper(request, *args, **kwargs):
        user_state = get_user_state(request.user_id)
        if user_state == USER_STATE_FORBIDDEN:
            raise UnauthorizedError()
        else:
            return func(request, *args, **kwargs)
    return wrapper


def get_user_state(user_id):
    user_state = r.hget('user:%s' % user_id, 'state')
    if not user_state:
        user_state = USER_STATE_VALID
    else:
        user_state = int(user_state)

    return user_state


def get_item_state(item_id):
    item_state = r.hget('item:%s' % item_id, 'state')
    if not item_state or item_state == '':
        item_state = ITEM_STATE_LISTING
    else:
        item_state = int(item_state)

    return item_state



def reindex_item(item_id):
    item = r.hgetall('item:%s' % item_id)
    fuzzy_item_id = item['id']
    title = item['title']
    description = item['desc']
    category_id = item['cat_id']
    images = []
    if "images" in item and item["images"] != '':
        images = json.loads(item["images"])
    media_link = item['mediaLink']
    price = item['price']
    currency_code = item['currency']
    local_price = item['local_price']
    lat = json.loads(item['location'])['lat']
    lon = json.loads(item['location'])['lon']
    original_price = item['original_price'] if 'original_price' in item else ''
    brand_id = item['brand_id'] if 'brand_id' in item else ''
    brand_name = item['brand_name'] if 'brand_name' in item else ''
    weight = item['weight'] if 'weight' in item else 1
    owner = get_user_from_id(item['user_id'])
    created_at = item['created_at']
    updated_at = item['updated_at']
    is_new = int(item['is_new']) if 'is_new' in item else 0
    modified_at = utils_func.get_now_ts()
    country = item['country'] if 'country' in item else None
    region = item['region'] if 'region' in item else None
    city = item['city'] if 'city' in item else None
    doc = get_item_es_json(fuzzy_item_id, title, description, category_id, images, media_link, price, currency_code,
                           local_price, original_price, brand_id, brand_name, lat, lon, owner, weight,
                           get_category_as_json(category_id), created_at, modified_at, updated_at, is_new, country, region, city)
    #index_item_in_searchengine(item_id, doc)
    #EsService().index_item(item_id, doc)
    es_dao_item().index(item_id, doc)


def get_place(country, region, city):
    place = []
    if city and city != '':
        place.append(city)

    if region and region != '' and not region.isdigit():
        place.append(region)

    return ', '.join(place) if len(place) > 0 else ''


def facebook_app_event(request):
    app_event_params = {}
    app_event_ext_params = {}
    try:
        advertiser_id = request.META.get('HTTP_ADVERTISER_ID', '')
        # 0表示关闭,1表示开启
        advertiser_tracking_enabled = request.META.get('HTTP_ADVERTISER_TRACKING_ENABLED', 1)
        # 0表示关闭,1表示开启
        application_tracking_enabled = request.META.get('HTTP_APPLICATION_TRACKING_ENABLED', 1)
        attribution = request.META.get('HTTP_ATTRIBUTION', '')

        app_event_params['advertiser_id'] = advertiser_id
        app_event_params['advertiser_tracking_enabled'] = advertiser_tracking_enabled
        app_event_params['application_tracking_enabled'] = application_tracking_enabled
        app_event_params['attribution'] = attribution

        app_event_ext_params['_appVersion'] = request.app_version
    except Exception, e:
        app_event_params = {}
        app_event_ext_params = {}
        logger.exception(e)

    return app_event_params, app_event_ext_params
