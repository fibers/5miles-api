# -*- coding: utf-8 -*-
import ipware.ip
import math
from common import utils_func
from entity.exceptions import UnauthenticatedError
from entity.models import I18nTranslation
from entity.service_base import SrService

__author__ = 'dragon'

import pytz
import datetime
import calendar
import hashlib, hmac
import logging
import requests

from pytz import timezone
from django.conf import settings
from django.utils.timezone import utc

logger = logging.getLogger('pinnacle')

from raven import Client
client = Client(settings.RAVEN_CONFIG['dsn'])
logger.debug("raven client init")


def get_now_timestamp():
    return utils_func.get_now_ts()


def get_utcnow():
    return datetime.datetime.utcnow().replace(tzinfo=utc)

def get_utc(timestamp):
    return datetime.datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)

def get_timestamp(dt):
    timestamp = calendar.timegm(dt.timetuple())

    return timestamp


def get_now_timestamp_str():
    now = datetime.datetime.utcnow()
    return now.strftime('%Y-%m-%dT%H:%M:%SUTC')


def get_now_timestamp_str2():
    now = datetime.datetime.utcnow()
    timestamp = now.strftime('%Y-%m-%d %H:%M:%SZ')

    return timestamp


def get_now_timestamp_str3():
    now = datetime.datetime.utcnow()
    timestamp = now.strftime('%Y-%m-%d %H:%M:%S')

    return timestamp


def get_zone_date(date, zone_time, date_format):
    tz = timezone(zone_time)
    zone_date = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc)
    return zone_date.astimezone(tz).strftime(date_format)


def get_timestamp_str(timestamp):
    utc_dt = datetime.datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)
    timestamp_str = utc_dt.strftime('%Y-%m-%d %H:%M:%SZ')
    return timestamp_str


def get_timestamp_str2(timestamp, format):
    utc_dt = datetime.datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)
    timestamp_str = utc_dt.strftime(format)
    return timestamp_str


def lat_or_lon_format(lat_or_lon):
    # lat和lon保留三位精度,提高命中缓存的概率
    lat_or_lon = lat_or_lon if not math.isnan(lat_or_lon) else 0
    return float('%.6f' % lat_or_lon)


def get_real_ip(request):
    ip_address = ipware.ip.get_real_ip(request)
    return ip_address

def capture_exception(**kwargs):
    if not settings.DEBUG:
        client.captureException(**kwargs)


def capture_message(message, **kwargs):
    if not settings.DEBUG:
        client.captureMessage(message, **kwargs)


def mailgun_verify(func):
    def wrapper(request, *args, **kw):
        # print 'call %s():' % func.__name__
        timestamp = int(request.POST.get('timestamp', 0))
        token = request.POST.get('token', '')
        signature = request.POST.get('signature', '')
        if signature != hmac.new(key=settings.MAILGUN_API_KEY,
                                 msg='{}{}'.format(timestamp, token),
                                 digestmod=hashlib.sha256).hexdigest():
            logger.warn('mailgun verify error')
            raise UnauthenticatedError()
        else:
            return func(request, *args, **kw)
    return wrapper


def _get_latlng_by_geoip(ip_address):
    try:
        from geo.services import GeoService
        geo_ip_address = GeoService().lookup_geoip(ip_address)
        lat = lat_or_lon_format(float(geo_ip_address['location_latitude']))
        lon = lat_or_lon_format(float(geo_ip_address['location_longitude']))
    except Exception, e:
        logger.exception(e)
        return 0, 0
    return lat, lon


def mailgun_validate_email(email):
    r = requests.get(
        "https://api.mailgun.net/v2/address/validate",
        auth=("api", settings.MAILGUN_PUBLIC_API_KEY),
        params={"address": email})

    if r.status_code < 200 or r.status_code > 299:
        return False

    return r.json()['is_valid']


def kickbox_validate_email(email):

    import kickbox
    client = kickbox.Client(settings.KICKBOX_API_KEY)
    kb = client.kickbox()
    try:
        response = kb.verify(email)
        logger.debug('Kickbox verify email: %s, code: %d, body: %s' % (email, response.code, response.body))

        if response.code != 200:
            capture_message('kickbox verify failed, email: %s, code: %s' % (email, response.code))
            return True

        return False if response.body['result'] == 'invalid' else True
    # except ClientError:
    #     logger.exception('kickbox error, maybe exceed the request limit.')
    #     capture_exception()
    finally:
        return True


def robot_or_not(email):
    return 1 if email.endswith('@niu.la') or email.endswith('@worldbuz.com') or email.startswith('robert_') else 0


def get_i18n_message(msg_id, language_code='en'):
    if not msg_id or msg_id == -1:
        return ''

    i18n = I18nTranslation.objects.filter(message__pk=msg_id, language_code=language_code)
    if i18n:
        return i18n[0].translation

    language_code = language_code[:language_code.index('-')] if '-' in language_code else language_code
    i18n = I18nTranslation.objects.filter(message__pk=msg_id, language_code__startswith=language_code)
    if i18n:
        return i18n[0].translation

    i18n = I18nTranslation.objects.filter(message__pk=msg_id, language_code__startswith='en')
    if i18n:
        return i18n[0].translation

    return ''


def try_build_location(user_id, ip_address):
    if not user_id:
        return _get_latlng_by_geoip(ip_address)

    try:
        pos = SrService().get_redis().hmget('user:%s' % user_id, 'lat', 'lon')
        if float(pos[0]) == 0 and float(pos[1]) == 0:
            return _get_latlng_by_geoip(ip_address)
        return lat_or_lon_format(float(pos[0])), lat_or_lon_format(float(pos[1]))
    except Exception, e:
        logger.warn('get user:%s location error,msg:%s' % (user_id, e.message))
        return _get_latlng_by_geoip(ip_address)


