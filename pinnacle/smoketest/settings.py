#### local_dev
SETTINGS = {
    'local': {
        'JWT_SECRET_KEY': 'jwt_secret_key_dev',
        'DB_HOST': 'localhost',
        'DB_PWD': '',
        'V2_REDIS_HOST': 'localhost'
    },
    'test': {
        'JWT_SECRET_KEY': 'jwt_secret_key_test',
        'DB_HOST': '54.65.39.176',
        'DB_PWD': '3rdStone',
        'V2_REDIS_HOST': '54.65.18.117'
    }
}