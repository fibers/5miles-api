#encoding=utf-8

import requests
import json
import sys
import redis
import jwt
import MySQLdb
from urllib import urlencode
from settings import SETTINGS

config = SETTINGS['local']
JWT_SECRET_KEY = config['JWT_SECRET_KEY']


class SmokeTest():
    r = redis.Redis(host=config['V2_REDIS_HOST'], port=6379, db=1)

    def __init__(self, api_host):
        self.api_host = api_host
        self.data = json.load(open('data.json', 'r'))

    def find_api(self, api_name):
        for dreq in self.data['requests']:
            if api_name is not None and dreq['name'] == api_name:
                return dreq

    def test_one(self, req):
        url = req['url']
        head = req['headers']
        method = req['method']
        data = req['data']
        hh = {}
        for hc in head.split('\n'):
            hi = hc.split(':')
            if len(hi) > 1:
                hh[hi[0]] = hi[1]

        dd = {}
        for di in data:
            dd[di['key']] = di['value']
        url = 'http://' + url.replace('{{api_host}}', self.api_host).replace('http://', '')
        if url.rfind('/?', 0) > 0:
            url = url[0:url.rfind('/?')+1]
        if method == 'POST':
            if head != '':
                return requests.post(url, data=dd, headers=hh)
            else:
                return requests.post(url, data=dd)
        elif method == 'GET':
            return requests.get(url+'?'+urlencode(dd))
        else:
            print '-----------'

    @staticmethod
    def _reset_request_data(req, key, value):
        flag = False
        for item in req['data']:
            if item['key'] == key:
                item['value'] = value
                flag = True
        if not flag:
            req['data'].append({'key': key, 'value': value})

    def test_all(self):
        ret_total_cnt = 0
        ret_ok_cnt = 0

        # 注册两个账号 signup_email
        flag = False
        try:
            req_signup_email_1 = self.find_api('/signup_email_1')
            res_signup_email_1 = self.test_one(req_signup_email_1).json()
            if req_signup_email_1['expected']['email'] == res_signup_email_1['email']:
                flag = True
                ret_ok_cnt += 1
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            print "/signup_email_1\t\t\tOK"
        else:
            print "/signup_email_1\t\t\tFail"

        flag = False
        try:
            req_signup_email_2 = self.find_api('/signup_email_2')
            res_signup_email_2 = self.test_one(req_signup_email_2).json()
            if req_signup_email_2['expected']['email'] == res_signup_email_2['email']:
                flag = True
                ret_ok_cnt += 1
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            print "/signup_email_2\t\t\tOK"
        else:
            print "/signup_email_2\t\t\tFail"

        # 模拟登陆以获取token login_email
        flag = False
        try:
            req_login_email = self.find_api('/login_email')
            res_login_email = self.test_one(req_login_email).json()
            if req_login_email['expected']['email'] == res_login_email['email']:
                flag = True
                ret_ok_cnt += 1
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            print "/login_email\t\t\tOK"
        else:
            print "/login_email\t\t\tFail"

        email1 = res_signup_email_1['email']
        user_id1 = jwt.decode(res_signup_email_1['token'], JWT_SECRET_KEY)['user_id']
        fuzzy_user_id1 = res_signup_email_1['id']

        # 模拟上传商品
        flag = False
        try:
            req_post_item = self.find_api('/post_item')
            req_post_item['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            res_org = self.test_one(req_post_item)
            # print res_org.text
            res_post_item = res_org.json()
            if res_post_item['new_id'] is not None:
                flag = True
                ret_ok_cnt += 1
        except Exception, e:
            print e
        ret_total_cnt += 1
        if flag:
            print "/post_item\t\t\tOK"
        else:
            print "/post_item\t\t\tFail"

        # 模拟上传商品2
        flag = False
        try:
            req_post_item_2 = self.find_api('/post_item_2')
            req_post_item_2['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            res_post_item_2 = self.test_one(req_post_item_2).json()
            if res_post_item_2['new_id'] is not None:
                flag = True
                ret_ok_cnt += 1
        except Exception, e:
            print e.message
        ret_total_cnt += 1
        if flag:
            print "/post_item_2\t\t\tOK"
        else:
            print "/post_item_2\t\t\tFail"

        # 模拟编辑商品
        flag = False
        try:
            req_edit_item = self.find_api('/edit_item')
            req_edit_item['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            # 重新设置item_id为post_item新生成的记录
            SmokeTest._reset_request_data(req_edit_item, 'item_id', res_post_item['new_id'])
            res_edit_item = self.test_one(req_edit_item)
            if res_edit_item.status_code == 200:
                flag = True
                ret_ok_cnt += 1
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            print "/edit_item\t\t\tOK"
        else:
            print "/edit_item\t\t\tFail"

        # 模拟下架商品
        flag = True
        try:
            req_unlist_item = self.find_api('/unlist_item')
            req_unlist_item['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            SmokeTest._reset_request_data(req_unlist_item, 'item_id', res_post_item['new_id'])
            res_unlist_item = self.test_one(req_unlist_item)
            # 调用返回状态OK
            if res_unlist_item.status_code != 200:
                flag = False
                print 'unlist_item 返回码异常'
            # 下架商品状态OK
            item_id = self.r.get('fuzzy_item_id:%s:item_id' % res_post_item['new_id'])
            item_state = self.r.hget('item:%s' % item_id, 'state')
            if item_state != '6':
                flag = False
                print 'unlist_item item_state Error'
            # 下架商品从seller_list删除
            if item_id in self.r.lrange('user:%s:user_selling_list' % user_id1, 0, -1):
                flag = False
                print 'unlist_item user_selling_listError'
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            ret_ok_cnt += 1
            print '/unlist_item\t\t\tOK'
        else:
            print '/unlist_item\t\t\tFail'

        # 模拟将下架商品Mark Sold
        flag = True
        try:
            req_mark_sold = self.find_api('/mark_sold')
            req_mark_sold['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            SmokeTest._reset_request_data(req_mark_sold, 'item_id', res_post_item['new_id'])
            res_mark_sold = self.test_one(req_mark_sold)
            if res_mark_sold.status_code != 200:
                flag = False
            # 下架商品状态OK
            item_id = self.r.get('fuzzy_item_id:%s:item_id' % res_post_item['new_id'])
            item_state = self.r.hget('item:%s' % item_id, 'state')
            if item_state != '1':
                flag = False
                print 'mark_sold item_state Error'
            # 下架商品标记Sold需要增加到seller_list,以供检索
            if item_id not in self.r.lrange('user:%s:user_selling_list' % user_id1, 0, -1):
                flag = False
                print 'mark_sold user_selling_listError'
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            ret_ok_cnt += 1
            print '/mark_sold\t\t\tOK'
        else:
            print '/mark_sold\t\t\tFail'

        # 模拟下架商品Mark Sold后的user_selling
        flag = False
        try:
            req_user_sellings = self.find_api('/user_sellings')
            SmokeTest._reset_request_data(req_user_sellings, 'seller_id', res_signup_email_1['id'])
            res_user_sellings = self.test_one(req_user_sellings)
            fuzzy_item_id = res_post_item['new_id']
            for obj in res_user_sellings.json()['objects']:
                if obj['id'] == fuzzy_item_id:
                    flag = True
                    break
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            ret_ok_cnt += 1
            print '/user_sellings\t\t\tOK'
        else:
            print '/user_sellings\t\t\tFail'

        # 模拟将正常商品2 Mark Sold
        flag = True
        try:
            req_mark_sold_2 = self.find_api('/mark_sold')
            req_mark_sold_2['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            SmokeTest._reset_request_data(req_mark_sold_2, 'item_id', res_post_item_2['new_id'])
            res_mark_sold_2 = self.test_one(req_mark_sold_2)
            if res_mark_sold_2.status_code != 200:
                flag = False
            # 下架商品状态OK
            item_id = self.r.get('fuzzy_item_id:%s:item_id' % res_post_item_2['new_id'])
            item_state = self.r.hget('item:%s' % item_id, 'state')
            if item_state != '1':
                flag = False
                print 'mark_sold_2 item_state Error'
            # 下架商品标记Sold需要增加到seller_list,以供检索
            if item_id not in self.r.lrange('user:%s:user_selling_list' % user_id1, 0, -1):
                flag = False
                print 'mark_sold_2 user_selling_listError'
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            ret_ok_cnt += 1
            print '/mark_sold_2\t\t\tOK'
        else:
            print '/mark_sold_2\t\t\tFail'

        # 模拟删除商品
        flag = False
        try:
            req_delete_item = self.find_api('/delete_item')
            req_delete_item['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            SmokeTest._reset_request_data(req_delete_item, 'item_id', res_post_item['new_id'])
            res_delete_item = self.test_one(req_delete_item)
            if res_delete_item.status_code == 200:
                flag = True
                ret_ok_cnt += 1
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            print '/delete_item\t\t\tOK'
        else:
            print '/delete_item\t\t\tFail'

        # 模拟删除商品2
        flag = False
        try:
            req_delete_item_2 = self.find_api('/delete_item')
            req_delete_item_2['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 +\
                                       "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            SmokeTest._reset_request_data(req_delete_item, 'item_id', res_post_item_2['new_id'])
            res_delete_item_2 = self.test_one(req_delete_item_2)
            if res_delete_item_2.status_code == 200:
                flag = True
                ret_ok_cnt += 1
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            print '/delete_item_2\t\t\tOK'
        else:
            print '/delete_item_2\t\t\tFail'

        # 模拟修改profile
        flag = True
        try:
            req_fill_profile = self.find_api('/fill_profile')
            req_fill_profile['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 + \
                                           "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            res_fill_profile = self.test_one(req_fill_profile)
            if req_fill_profile['expected']['email'] != res_fill_profile['email']:
                flag = False
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            ret_ok_cnt += 1
            print "/fill_profile\t\t\tOK"
        else:
            print "/fill_profile\t\t\tFail"

        # 模拟恢复profile
        flag = True
        try:
            req_fill_profile = self.find_api('/fill_profile_2')
            req_fill_profile['headers'] = "X-FIVEMILES-USER-ID: " + user_id1 + \
                                          "\nX-FIVEMILES-USER-TOKEN: " + res_signup_email_1['token']
            res_fill_profile = self.test_one(req_fill_profile)
            if req_fill_profile['expected']['email'] != res_fill_profile['email']:
                flag = False
        except Exception:
            pass
        ret_total_cnt += 1
        if flag:
            ret_ok_cnt += 1
            print "/fill_profile_2\t\t\tOK"
        else:
            print "/fill_profile_2\t\t\tFail"

        # 汇总测试结果
        print '--------Total Test Case %d, OK %d, Fail %d' % (ret_total_cnt, ret_ok_cnt, (ret_total_cnt-ret_ok_cnt))

        # 清理注册信息
        self.r.delete('login:email:%s:user_id' % email1)
        self.r.delete('user:%s' % user_id1)
        self.r.delete('fuzzy_id:%s:user_id' % fuzzy_user_id1)
        self.r.delete('user:%s:password' % user_id1)

        email2 = res_signup_email_2['email']
        user_id2 = jwt.decode(res_signup_email_2['token'], JWT_SECRET_KEY)['user_id']
        fuzzy_user_id2 = res_signup_email_2['id']
        self.r.delete('login:email:%s:user_id' % email2)
        self.r.delete('user:%s' % user_id2)
        self.r.delete('fuzzy_id:%s:user_id' % fuzzy_user_id2)
        self.r.delete('user:%s:password' % user_id2)

        db = MySQLdb.connect(config['DB_HOST'], "root", config['DB_PWD'], "bizdb")
        cursor = db.cursor()
        for fuzzy_user_id in (fuzzy_user_id1, fuzzy_user_id2):
            cursor.execute('delete img from entity_image img,entity_item it,entity_user u \
                           where img.item_id=it.id and it.user_id=u.id and u.fuzzy_user_id="%s"' % fuzzy_user_id)
            cursor.execute('delete ic from entity_itemcategory ic,entity_item it,entity_user u \
                           where ic.item_id=it.id and it.user_id=u.id and u.fuzzy_user_id="%s"' % fuzzy_user_id)
            cursor.execute('delete it from entity_item it,entity_user u where it.user_id=u.id and u.fuzzy_user_id="%s"' % fuzzy_user_id)
            cursor.execute('delete from entity_user where fuzzy_user_id="%s"' % fuzzy_user_id)
        db.commit()
        db.close()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Usage: python IP:PORT [api_name]'
        exit(1)
    p = sys.argv[1]
    st = SmokeTest(p)
    if len(sys.argv) > 2:
        req = st.find_api(sys.argv[2])
        if req is None:
            print "API Not Found"
            exit(1)
        r = st.test_one(req)
        print r.text
    else:
        st.test_all()
