from __future__ import absolute_import
from celery import shared_task
__author__ = 'dragon'

import pika
import logging
import json

from django.conf import settings
# from celery.decorators import task

logger = logging.getLogger('pinnacle')


@shared_task
def send_item_to_boss(item_id , item_action=None):
    msg = {'item_id': item_id}
    if item_action:
        msg['item_action'] = item_action
    _publish_message('', 'boss.item_to_review', json.dumps(msg))


@shared_task
def send_report_to_boss(report_type, report_id):
    msg = {'report_type': report_type, 'report_id': report_id}
    _publish_message('', 'boss.report_to_review', json.dumps(msg))


@shared_task
def send_feedback_to_boss(feedback_id):
    msg = {'feedback_id': feedback_id}
    _publish_message('', 'boss.feedback_to_review', json.dumps(msg))


@shared_task
def send_review_to_boss(review_id):
    msg = {'review_id': review_id}
    _publish_message('', 'boss.review_to_audit', json.dumps(msg))


def _publish_message(exchange, routing_key, message):
    credentials = pika.PlainCredentials(settings.RABBITMQ_USER, settings.RABBITMQ_PASSWORD)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=settings.RABBITMQ_HOST, credentials=credentials))
    # connection = pika.BlockingConnection(pika.URLParameters('amqp://fivemiles:fivemiles@localhost:5672/%2F'))
    channel = connection.channel()
    channel.queue_declare(queue=routing_key, durable=True)
    channel.basic_publish(exchange=exchange,
                          routing_key=routing_key,
                          body=message)
    logger.debug('Message sent, exchange: %s, routing_key: %s, body: %s' % (exchange, routing_key, message))
    connection.close()