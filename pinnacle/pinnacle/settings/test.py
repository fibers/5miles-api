# -*- coding: utf-8 -*-
__author__ = 'dragon'

from defaults import *


DEBUG = TEMPLATE_DEBUG = True

# Add raven to the list of installed apps
INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

# Redis Config
REDIS_HOST = '172.31.5.141'
REDIS_PORT = 6379

V2_REDIS_HOST = '172.31.5.141'
V2_REDIS_PORT = 6379

# Message Thrift Interface Config
MESSAGE_THRIFT_HOST = '172.31.15.232'

# Elasticsearch Settings
ES_HOSTS = [{'host': '172.31.19.155'}]

RABBITMQ_HOST = "172.31.1.169"
RABBITMQ_PORT = 5672
BROKER_URL = 'amqp://%s:%s@%s:%d/%s' % (RABBITMQ_USER, RABBITMQ_PASSWORD, RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_VHOST)

FMMC_API_ENDPOINT = 'http://172.31.15.232/api'
SEND_OFFER_MAIL = False

# SOLD商品在搜索引擎里保留的天数
SOLD_ITEMS_HOLD_DAYS = 1

BRANCH_API_KEY = '76513650755502747'

# 商品renew间隔时间（单位：分钟）
ITEM_RENEW_INTERVAL = 1

JWT_SECRET_KEY = 'jwt_secret_key_test'

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://%s:%d/%d' % (V2_REDIS_HOST, V2_REDIS_PORT, V2_REDIS_DB_EX),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 2,  # in seconds
        }
    }
}

# Set your DSN value
RAVEN_CONFIG = {
    'dsn': 'https://8860f8f391cc4eb28a46505482d74d82:d9c0075b7b8941dab3d5aa30832536f0@app.getsentry.com/48981'
}
