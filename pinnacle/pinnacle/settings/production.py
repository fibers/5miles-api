# -*- coding: utf-8 -*-
__author__ = 'dragon'

from defaults import *


DEBUG = TEMPLATE_DEBUG = False

# Add raven to the list of installed apps
INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

# Redis Config
REDIS_HOST = '10.0.2.222'
REDIS_PORT = 6379

V2_REDIS_HOST = 'cache-master.tu1map.0001.apse2.cache.amazonaws.com'
V2_REDIS_PORT = 6379

# Message Thrift Interface Config
MESSAGE_THRIFT_HOST = '10.0.2.120'

# Elasticsearch Settings
ES_HOSTS = [{'host': '10.0.2.187'}, {'host': '10.0.2.188'}, {'host': '10.0.2.189'}, {'host': '10.0.2.32'}, {'host': '10.0.2.249'}]

RABBITMQ_HOST = "10.0.1.148"
RABBITMQ_PORT = 5672
BROKER_URL = 'amqp://%s:%s@%s:%d/%s' % (RABBITMQ_USER, RABBITMQ_PASSWORD, RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_VHOST)

FMMC_API_ENDPOINT = 'http://internal-lb-fmmc-internal-new-1882292952.ap-southeast-2.elb.amazonaws.com/api'

MAILGUN_BASE_URL = 'https://api.mailgun.net/v2/mail.5milesapp.com'
SEND_OFFER_MAIL = False

CRAIGSLIST_PROXY = 'http://cl-sms-service-78481997.ap-southeast-2.elb.amazonaws.com/proc/offer'

# SOLD商品在搜索引擎里保留的天数
SOLD_ITEMS_HOLD_DAYS = 1

BRANCH_API_KEY = '66427291143504806'

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://%s:%d/%d' % (V2_REDIS_HOST, V2_REDIS_PORT, V2_REDIS_DB_EX),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 2,  # in seconds
        }
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s %(module)s %(process)d %(thread)d %(message)s',
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'analytics': {
             'format': ' %(message)s\n',
         },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'django_debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(LOG_PATH, 'django-debug.log')
        },
        'pinnacle': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(LOG_PATH, 'pinnacle.log')
        },
        'access': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'access.log'),
        },
        'analytics': {
            'level': 'INFO',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'user',
            'formatter': 'analytics',
            'socktype': 1,
            'address': ('127.0.0.1', 10514),
        },
        'analytics_exposure': {
            'level': 'INFO',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'user',
            'formatter': 'analytics',
            'socktype': 1,
            'address': ('127.0.0.1', 10515),
        },
        'elasticsearch': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'elasticsearch.log'),
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['django_debug'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'pinnacle': {
            'handlers': ['console', 'pinnacle'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'access': {
            'handlers': ['access'],
            'level': 'INFO',
            'propagate': True,
        },
        'analytics': {
            'handlers': ['analytics'],
            'level': 'INFO',
        },
        'analytics_exposure': {
            'handlers': ['analytics_exposure'],
            'level': 'INFO',
        },
        'elasticsearch': {
            'handlers': ['elasticsearch'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}