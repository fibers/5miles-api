# -*- coding: utf-8 -*-

# Django settings for pinnacle project.

DEBUG = TEMPLATE_DEBUG = False

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',   # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'bizdb',                        # Or path to database file if using sqlite3.
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '',                             # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                             # Set to empty string for default.
        'OPTIONS': {'charset': 'utf8mb4'},
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['.amazonaws.com', '.5milesapp.com']

# Full filesystem path to the project.
import os
PROJECT_ROOT = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'GMT'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

LOCALE_PATHS = (
    os.path.join(PROJECT_ROOT, 'locale'),
)

# from django.utils.translation import ugettext_lazy as _
# LANGUAGES = (
#   ('zh', _('Chinese')),
#   ('en', _('English')),
# )

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, "site_media", "static")

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'tdyursb)81zro^)^=ardt53975pu^li=mwab=_g89h@aw7*n#e'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'middleware.common.ReqExecTimeMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'middleware.common.CommonMiddleware',
)

ROOT_URLCONF = 'pinnacle.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'pinnacle.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, "templates"),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'spam',
    'message',
    'south',
    'annoying',
    'gunicorn',
    'country_dialcode',
    'tastypie',
    'entity',
    'offertalk',
    'feedback',
    'sociallink',
    'campaign',
    'geo',
    'boss',
    'review',
    'apilog',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

AWS_ACCESS_KEY_ID = 'AKIAJ2SWT346HW7ZSW5A'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'fivemiles'

# Settings for access item server on the server side
ITEM_SERVER = '10.0.2.27/api/v1'
SERVER_SIDE_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo4fQ.bNRLvkzQasynu6Bk0u-hyfzP8mKFP2O50s5Ip7LC6HA"

# Redis Config
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 2
V2_REDIS_DB = 1
REDIS_MAX_CONNS = 32
# V2_REDIS_DB_EX 存放临时的数据DB(设置过期时间).这些数据可以在需要的时候手动清空，系统会自动重建，不会影响中断服务

V2_REDIS_HOST = 'localhost'
V2_REDIS_PORT = 6379
V2_REDIS_DB_EX = 3


# Message Thrift Interface Config
MESSAGE_THRIFT_HOST = 'localhost'

#MESSAGE_THRIFT_PORT = 9090
MESSAGE_THRIFT_PORT = 2048
V2_MESSAGE_THRIFT_PORT = 2048

#Backend Api thrift interface port
BACKENDAPI_THRIFT_PORT = 11000

# Gearman Server Config
GEARMAN_SERVERS = ['10.0.2.252:8341']

# Login exempt urls
LOGIN_EXEMPT_URLS = (
    r'^api/v1/country_dialcodes/$',
    r'^api/v2/signup_email/$',
    r'^api/v2/signup_anonymous/$',
    r'^api/v2/login_email/$',
    r'^api/v2/login_sms/$',
    r'^api/v2/login_facebook/$',
    r'^api/v2/verify_code/$',
    r'^api/v2/home/$',
    r'^api/v2/forget_password/$',
    r'^api/v2/appsflyer_callback/$',
    r'^api/v2/user_detail/$',
    r'^api/v2/user_sellings/$',
    r'^api/v2.1/appsflyer_callback/$',
    r'^api/v2/web_signup_email/$',
    r'^api/v2/mailgun_notify/$',
    r'^api/v2/mailgun_bounce/$',
    r'^api/v2/campaigns/$',
    r'^api/v2/validate_email/$',
    r'^api/v2/search/$',
    r'^api/v2/search/category/$',
    r'^api/v2/suggest/$',
    r'^api/v2/item_offerlines/$',
    r'^api/v2/item_likers/$',
    r'^api/v2/categories/$',
    r'^api/v2/item_detail/$',
    r'^api/v2/home_test/$',
    r'^api/init/$',
    r'^api/v2/report_reasons/$',
)

# Cloudinary credentials
CLOUDINARY_APIKEY = '495993672824736'
CLOUDINARY_SECRET = 'ho0wS1epe1W_ydChszuqEyeThPo'

# Currency
DEFAULT_CURRENCY_CODE = 'USD'

# Elasticsearch Settings
ES_HOSTS = [{'host': 'localhost'}]

ES_INDEX_MAIN = 'fivemiles'
ES_DOCTYPE_ITEM = 'item'
ES_DOCTYPE_USER = 'user'

# Twilio account
TWILIO_ACCOUNT_SID = 'AC2028b85740640418a2960216a235dfa2'
TWILIO_ACCOUNT_TOKEN = '3d535641ad2ebdbff58a768df7486c5f'
TWILIO_FROM_NUMBER = '+14352154456'

# Hashids config
HASH_ID_MIN_LENGTH = 10
HASH_ID_SALT = 'hello, soho0506'
ITEM_HASH_ID_MIN_LENGTH = 16
ITEM_HASH_ID_SALT = 'fivemiles-1d27ca806afc'

# JWT config
JWT_SECRET_KEY = "358e5ac8-4a2d-4b3b-9f46-2743d0fd935c3bd6fae8-3700-4063-87df-9402bcb14aee952bc072-a624-4a4c-b31c-1d27ca806afc"
JWT_TOKEN_EXPIRATION_DAYS = 60

# Password generation
PASSWORD_SALT = 'soho0506'
PASSWORD_ALGORITHM = 'pbkdf2_sha256'

DEFAULT_DECAY_DISTANCE = '40km'
DEFAULT_DECAY_DATE = '172800'  # 2 days

# Default avatar image url link
DEFAULT_AVATAR_LINK = 'http://res.cloudinary.com/fivemiles/image/upload/t_media_lib_thumb/v1419928828/person-head_magick.jpg'

# Default display number with search item
DEFAULT_SEARCH_ITEMS = 1000

# the switch of synchronizing data from redis to database
SYNCHRONIZE_SWITCH = True

# the url of the reset password
URL_RESET_PASSWORD = 'http://5milesapp.com/reset'

# facebook api configuration
FACEBOOK = {
    'API_URL': 'https://graph.facebook.com/',
    'ME_FIELDS': 'id,first_name,email,name,last_name,timezone,locale,link,gender,updated_time,age_range,currency',
    'AUTO_SHARE_ITEM_SWITCH': True,
    'SHARE_CONTENT': "Are you in %s? Check out my mobile marketplace on the #5milesapp - I'm selling a %s for %s. \
    More details here: %s",
    'APP_EVENT': {
        'URL': 'https://graph.facebook.com/1485115451746556/activities',
        'SWITCH': True,
        'OFFER_SEND': 'offer_send'
    }
}

# Celery & RabbitMQ
# import djcelery
# djcelery.setup_loader()

RABBITMQ_HOST = "localhost"
RABBITMQ_PORT = 5672
RABBITMQ_USER = "fivemiles"
RABBITMQ_PASSWORD = "fivemiles"
RABBITMQ_VHOST = "/"
BROKER_URL = 'amqp://%s:%s@%s:%d/%s' % (RABBITMQ_USER, RABBITMQ_PASSWORD, RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_VHOST)

# Login exempt urls
RECORD_EXEMPT_URLS = (
    r'^api/v2/item_offerlines/$',
    r'^api/v2/item_comments/$',
    r'^api/v2/offerline_updates/$',
    # r'^api/v2/sign_image_upload/$',
    r'^api/v2/new_message_count/$',
    # r'^api/v2/my_messages/$',
    r'^api/v2/mailgun_bounce/$',
    r'^api/v2/mailgun_notify/$',
    r'^api/v2/bind_client/$',
    r'^api/v2/unbind_client/$',
    r'^api/v2/bind_push/$',
    r'^api/v2/unbind_push/$',
    # r'^api/v2/suggest/$',
)

BITLY_ACCESS_TOKEN = "740edac9b9fadbb7077d4d1b2193ab343ca4e221"

LOG_PATH = '/var/log/pinnacle'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s %(module)s %(process)d %(thread)d %(message)s',
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'django_debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(LOG_PATH, 'django-debug.log')
        },
        'pinnacle': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(LOG_PATH, 'pinnacle.log')
        },
        'access': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'access.log'),
        },
        'analytics': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'access.log'),
        },
        'analytics_exposure': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'access_item.log'),
        },
        'elasticsearch': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'elasticsearch.log'),
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['django_debug'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'pinnacle': {
            'handlers': ['console', 'pinnacle'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'access': {
            'handlers': ['access'],
            'level': 'INFO',
            'propagate': True,
        },
        'analytics': {
            'handlers': ['analytics'],
            'level': 'INFO',
        },
        'analytics_exposure': {
            'handlers': ['analytics_exposure'],
            'level': 'INFO',
        },
        'elasticsearch': {
            'handlers': ['elasticsearch'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# Set your DSN value
RAVEN_CONFIG = {
    'dsn': 'https://806faac6124e465183fde55eb365331d:cb81bf2e95384c5587823d751939ff06@app.getsentry.com/30746',
}

# geoip database url
# GEOIP_DB_URL = os.path.join(PROJECT_ROOT, 'GeoLite2-City.mmdb')
GEOIP = {
    'DB_URL': '/var/www/deploy_fivemiles/GeoLite2-City.mmdb',
    'USER_ID': 95129,
    'LICENSE_KEY': 'jAGFrN868TSt'
}

FMMC_API_ENDPOINT = 'http://localhost:3000/api'

# APNS pem files
APNS_CERT_FILE = os.path.join(PROJECT_ROOT, '5milesv2DevCert.pem')
APNS_KEY_FILE = os.path.join(PROJECT_ROOT, '5milesv2Dev.pem')

# filter special fields in query paramters
ANALYTIC_QUERY_FILTER_FIELDS = {'password'}

# google map api url
GEOCODE_BASE_URL = 'http://maps.googleapis.com/maps/api/geocode/json'

STORE_URL_PREFIX = 'http://5milesapp.com/person/'

# promotion settings
PROMOTION_POST_ITEM_SWITCH = True
PROMOTION_POST_ITEM_ZONE_TIME = 'America/Los_Angeles'
PROMOTION_POST_ITEM_START_TIME = '2014-11-21 00:00:00'
PROMOTION_POST_ITEM_END_TIME = '2014-11-24 00:00:00'
PROMOTION_POST_ITEM_FIVE_RED_PACKAGE_TOTAL_NUM = 10000
PROMOTION_POST_ITEM_FIVE_RED_PACKAGE_TARGET = 5
PROMOTION_POST_ITEM_USER_DISPLAY_NUM = 30
PROMOTION_POST_ITEM_CAT_IDS = [0, 1, 2, 3, 4, 5, 6, 13]
PROMOTION_POST_ITEM_COUNTRIES = ['United States']

MAILGUN_API_KEY = 'key-b91f582fc1803c1b0b2a4a66a5947920'
MAILGUN_PUBLIC_API_KEY = 'pubkey-6f6ab66efe5a67b3787826c4c19c28dd'
MAILGUN_BASE_URL = 'https://api.mailgun.net/v2/sandbox9e9edc77ee48454f946655e7f26910de.mailgun.org'

# 是否发送offer邮件
SEND_OFFER_MAIL = False

KICKBOX_API_KEY = 'a287e08da31a0360865097829c39be33b1afae9f4ae22ca29ca25f5aa2abe057'

CRAIGSLIST_PROXY = 'http://6defa9c6.ngrok.com/offer'

# SOLD商品在搜索引擎里保留的天数
SOLD_ITEMS_HOLD_DAYS = 1

BRANCH_API_BASE_URL = 'https://api.branch.io'

# 商品renew间隔时间（单位：分钟）
ITEM_RENEW_INTERVAL = 240

# 新商品new标签存活时间（单位：天）
ITEM_NEW_TAG_HOLD_DAYS = 1

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://%s:%d/%d' % (V2_REDIS_HOST, V2_REDIS_PORT, V2_REDIS_DB_EX),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 2,  # in seconds
        }
    }
}
DJANGO_REDIS_IGNORE_EXCEPTIONS = True

# you can provide your own meta precedence order by
# including IPWARE_META_PRECEDENCE_ORDER in your
# settings.py. The check is done from top to bottom
IPWARE_META_PRECEDENCE_LIST = (
    'HTTP_X_FIVEMILES_REAL_IP',
    'HTTP_X_FORWARDED_FOR', # client, proxy1, proxy2
    'HTTP_CLIENT_IP',
    'HTTP_X_REAL_IP',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'HTTP_VIA',
    'REMOTE_ADDR',
)

# you can provide your own private IP prefixes by
# including IPWARE_PRIVATE_IP_PREFIX in your setting.py
# IPs that start with items listed below are ignored
# and are not considered a `real` IP address
IPWARE_PRIVATE_IP_PREFIX = (
    '0.', '1.', '2.', # externally non-routable
    '10.', # class A private block
    '169.254.', # link-local block
    '172.16.', '172.17.', '172.18.', '172.19.',
    '172.20.', '172.21.', '172.22.', '172.23.',
    '172.24.', '172.25.', '172.26.', '172.27.',
    '172.28.', '172.29.', '172.30.', '172.31.', # class B private blocks
    '192.0.2.', # reserved for documentation and example code
    '192.168.', # class C private block
    '255.255.255.', # IPv4 broadcast address
) + (  # the following addresses MUST be in lowercase)
    '2001:db8:', # reserved for documentation and example code
    'fc00:', # IPv6 private block
    'fe80:', # link-local unicast
    'ff00:', # IPv6 multicast
)

API_VERSION = 'v2'
SWITCHERS = {
    'review': True
}

ANALYTICS_NEW_API_LOG_SWITCH = True
