
struct ThriftOffer {
    1: required i32 itemId;
    2: required i32 fromUid;
    3: required i32 toUid;
    4: required double price;
    5: required bool buySide;
    6: optional string comment
}

service PriceOffers {
    ThriftOffer get_offer(1:i32 offer_id)
}
