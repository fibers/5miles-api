from django.db import models

# Create your models here.

class PublicComment(models.Model):
    item_id = models.PositiveIntegerField()
    author = models.PositiveIntegerField()
    ref_post_id = models.PositiveIntegerField(default=0)
    comment = models.CharField(max_length=255, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now=True)

class OfferThread(models.Model):
    item_id = models.PositiveIntegerField()
    buyer_id = models.PositiveIntegerField()
    seller_id = models.PositiveIntegerField()
    deal_done = models.BooleanField(default=False)
    
    def belong_to(self, user_id):
        return self.buyer_id == user_id

    def description(self, user_id, buyer_name):
        last_offer = ItemOffer.objects.filter(offer_thread=self).order_by('-timestamp')[0]

        # Seller role
        if user_id == self.seller_id:
            if self.deal_done:
                return "You sold the item!"

            if last_offer.from_buyer:
                return '%s offered: $%.2f' % (buyer_name, last_offer.price)
            else:
                return 'You offered: $%.2f' % last_offer.price

        # Buyer role
        if self.belong_to(user_id):
            if self.deal_done:
                return "You bought the item!"

            if not last_offer.from_buyer:
                return "You got counter offer : $%.2f" % last_offer.price
            else:
                return "You offered: $%.2f" % last_offer.price
        else:
            return '%s made an offer' % buyer_name

class ItemOffer(models.Model):
    offer_thread = models.ForeignKey(OfferThread)
    price = models.DecimalField(max_digits=11, decimal_places=2, default=0)
    timestamp = models.DateTimeField(auto_now=True)
    comment = models.CharField(max_length=255, null=True, blank=True)
    from_buyer = models.BooleanField(default=True)
    
    def __unicode__(self):
        return '%d: item: %d, buyer: %d, %f, from buyer: %s' % (self.pk, self.offer_thread.item_id, self.offer_thread.buyer_id, self.price, self.from_buyer)

class Rating(models.Model):
    for_seller = models.BooleanField(default=True)
    item_id = models.PositiveIntegerField()
    rater_id = models.PositiveIntegerField()
    ratee_id = models.PositiveIntegerField()
    rate_num = models.PositiveIntegerField(default=0)
    comment = models.CharField(max_length=255, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now=True)

class Deal(models.Model):
    item_id = models.PositiveIntegerField()
    buyer_id = models.PositiveIntegerField()
    seller_id = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=11, decimal_places=2, default=0)
    state = models.PositiveIntegerField(default=0)
    timestamp = models.DateTimeField(auto_now=True)

class AppRemark(models.Model):
    user_id = models.PositiveIntegerField()
    email = models.EmailField()
    body = models.CharField(max_length=255)
