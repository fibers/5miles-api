from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^make_offer/$', 'commerce.views.make_offer', name='make_offer'),
    url(r'^counter_offer/$', 'commerce.views.make_counter_offer', name='make_counter_offer'),
    url(r'^accept_offer/$', 'commerce.views.accept_offer', name='accept_offer'),
    url(r'^send_thread_message/$', 'commerce.views.send_thread_message', name='send_thread_message'),
    url(r'^thread_offers/(\d+)/$', 'commerce.views.list_thread_offers', name='list_thread_offers'),
)
