from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseBadRequest, HttpResponseNotFound, HttpResponseGone, HttpResponseNotAllowed, HttpResponseForbidden
from django.views.decorators.http import require_GET, require_POST
from django.utils.translation import ugettext as _

import json
import redis

from annoying.functions import get_object_or_None
from country_dialcode.models import *
from babel import numbers
from .models import *
from .utils import *
from message.utils import *

# Item state list:
LISTING_STATE = 2
IN_TRANSACTION_STATE = 3
CLOSED_STATE = 4
BUYER_RATED_STATE = 7

# Deal state list:
DEAL_CREATED = 0
DEAL_CLOSED = 1

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

@require_POST
def make_offer(request):
    item_id = int(request.POST.get('item_id', 0))
    price = float(request.POST.get('price', 0.00))
    comment = request.POST.get('comment', '')

    """ Get item's detail info with item_id """
    item = get_item_by_id(item_id)
    if item is None or not 'status' in item or item['status'] != LISTING_STATE:
        return HttpResponseGone(_('item not available'))

    if not 'owner' in item or item['owner']['id'] == request.user_id:
        return HttpResponseForbidden(_('cannot make offer on own item'))

    if price <= 0:
        return HttpResponseBadRequest(_('price should be positive'))

    offer_thread = get_object_or_None(OfferThread, item_id=item_id, buyer_id=request.user_id)
    if not offer_thread:
        offer_thread = OfferThread(item_id=item_id, buyer_id=request.user_id, seller_id=item['owner']['id'])
        offer_thread.save()

    # If all goes right, go create the offer and notify the seller 
    offer = ItemOffer(offer_thread=offer_thread, price=price, comment=comment)
    offer.save()
    
    with r.pipeline() as pipe:
        pipe.zadd('user:%d:buyings' % request.user_id, item_id, get_now_timestamp())
        pipe.incr('user:%d:buyings.count' % request.user_id)

        pipe.execute()

    # auto watch the item you made offer on
    ret = r.sadd('item:%d:watchers' % item_id, request.user_id)
    if ret > 0:
        r.incr('item:%d:watchers.count' % item_id)
        r.zadd('user:%d:watchings' % request.user_id, item_id, get_now_timestamp())
        r.incr('user:%d:watchings.count' % request.user_id)

    # async notify the seller
    local_price = numbers.format_currency(price, item['currency_code'], locale='en_US')
    async_send_offer_message(offer.id, item_id, offer_thread.id, offer_thread.buyer_id, offer_thread.seller_id, local_price, comment, 0)

    return HttpResponse(json.dumps({'new_id':offer.id, 'thread_id':offer_thread.id}), content_type='application/json', status=201)

@require_POST
def make_counter_offer(request):
    ref_offer_id = int(request.POST.get('ref_offer_id', 0))
    price = float(request.POST.get('price', 0.00))
    comment = request.POST.get('comment', '')

    ref_offer = get_object_or_None(ItemOffer, pk=ref_offer_id)
    if not ref_offer:
        return HttpResponseBadRequest(_('reference offer is not found'))

    print ref_offer.offer_thread.buyer_id
    print ref_offer.offer_thread.seller_id

    if ref_offer.from_buyer:
        if request.user_id != ref_offer.offer_thread.seller_id:
            return HttpResponseForbidden(_('only seller can counter this offer'))
    else:
        if request.user_id != ref_offer.offer_thread.buyer_id:
            return HttpResponseForbidden(_('only buyer can counter this offer'))

    if price <= 0:
        return HttpResponseBadRequest('price should be positive')

    item = get_item_by_id(ref_offer.offer_thread.item_id)
    if item is None or not 'status' in item or item['status'] != LISTING_STATE:
        return HttpResponseGone('item not available')

    from_buyer = ref_offer.offer_thread.buyer_id == request.user_id
    offer = ItemOffer(offer_thread=ref_offer.offer_thread, price=price, comment=comment, from_buyer=from_buyer)
    offer.save()

    local_price = numbers.format_currency(price, item['currency_code'], locale='en_US')
    async_send_offer_message(offer.id, item['id'], ref_offer.offer_thread.id, ref_offer.offer_thread.buyer_id, ref_offer.offer_thread.seller_id, local_price, comment, 0 if offer.from_buyer else 1)

    return HttpResponse(json.dumps({'new_id':offer.id}), content_type='application/json', status=201)

@require_POST
def accept_offer(request):
    offer_id = request.POST.get('offer_id', 0)
    offer = get_object_or_None(ItemOffer, pk=offer_id)
    if not offer:
        return HttpResponseNotFound('offer with the given id not found')

    print offer.from_buyer
    print offer.offer_thread.buyer_id

    # if the offer is from the seller, make sure the request user is the buyer
    if not offer.from_buyer and request.user_id != offer.offer_thread.buyer_id:
        return HttpResponseBadRequest('the request user cannot accept offer')

    # if the offer is from the buyer, make sure the request user is the seller
    if offer.from_buyer and request.user_id != offer.offer_thread.seller_id:
        return HttpResponseBadRequest('the request user cannot accept offer')

    item = get_item_by_id(offer.offer_thread.item_id)
    if item is None or not 'status' in item or item['status'] != LISTING_STATE:
        return HttpResponseGone('item not available')
    else:
        # mark the item sold
        set_item_state(offer.offer_thread.item_id, IN_TRANSACTION_STATE)

    # Generate a deal record
    deal = Deal(item_id=offer.offer_thread.item_id, buyer_id=offer.offer_thread.buyer_id, seller_id=offer.offer_thread.seller_id, price=offer.price, state=DEAL_CREATED)
    deal.save()

    # Change the offer thread's status
    offer.offer_thread.deal_done = True
    offer.offer_thread.save()

    entry = ItemOffer(offer_thread=offer.offer_thread, comment="Offer accepted with price %.2f" % offer.price, from_buyer=not offer.from_buyer)
    entry.save()

    local_price = numbers.format_currency(deal.price, item['currency_code'], locale='en_US')
    async_send_accept_offer_message(entry.id, deal.item_id, offer.offer_thread.id, offer.offer_thread.buyer_id, offer.offer_thread.seller_id, local_price, entry.comment, 0 if entry.from_buyer else 1)

    return HttpResponse(json.dumps({'new_id':entry.id}), content_type='application/json', status=201)

@require_POST
def send_thread_message(request):
    thread_id = int(request.POST.get('thread_id', 0))
    message = request.POST.get('message', '')

    offer_thread = get_object_or_None(OfferThread, pk=thread_id)
    if not offer_thread or message == '':
        return HttpResponseNotFound('thread not found or message is empty')

    if not offer_thread.deal_done:
        return HttpResponseForbidden('deal is still open')

    if request.user_id != offer_thread.buyer_id and request.user_id != offer_thread.seller_id:
        return HttpResponseForbidden('you are neither buyer or seller')

    offer = ItemOffer(offer_thread=offer_thread, comment=message, from_buyer=offer_thread.buyer_id == request.user_id)
    offer.save()

    async_send_chat_message(offer.id, offer_thread.item_id, offer_thread.id, offer_thread.buyer_id, offer_thread.seller_id, offer.comment, 0 if offer.from_buyer else 1)

    return HttpResponse(json.dumps({'new_id':offer.id}), content_type='application/json', status=201)

@require_GET
def list_item_threads(request, item_id):
    print item_id
    item_id = int(item_id)

    threads = OfferThread.objects.filter(item_id=item_id)
    buyer_uids = set()
    for thread in threads:
        buyer_uids.add(thread.buyer_id)

    # Merge user info fetched from another machine
    user_list = get_users_info(buyer_uids)

    results = []
    for thread in threads:
        for user in user_list:
            if user['id'] == thread.buyer_id:
                results.append({'id':thread.id, 'buyer':{'id':user['id'], 'nick_name':user['profile']['nick_name'], 'portrait':user['profile']['portrait']}, 'desc':thread.description(request.user_id, user['profile']['nick_name'])})
                break

    return HttpResponse(json.dumps(results), content_type='application/json')

@require_GET
def list_thread_offers(request, thread_id):
    try:
        thread_id = int(thread_id)
    except ValueError:
        return HttpResponseBadRequest('thread id in bad format')

    offer_thread = get_object_or_None(OfferThread, pk=thread_id)
    if not offer_thread:
        return HttpResponseBadRequest('thread not found')

    # if the request user is the seller or the buyer, then go return list
    if request.user_id == offer_thread.seller_id or request.user_id == offer_thread.buyer_id:
        import calendar

        buyer = get_user_info(offer_thread.buyer_id)
        seller = get_user_info(offer_thread.seller_id)
        item = get_item_by_id(offer_thread.item_id)
        if item is None:
            return HttpResponseBadRequest(_('item not exist'))
        elif len(item['images']) == 0:
            return HttpResponseBadRequest(_('item has no thumbnail image'))

        offers = ItemOffer.objects.filter(offer_thread=offer_thread).order_by('-timestamp')
        results = []
        for offer in offers:
            results.append({'id':offer.id, 'from_buyer':offer.from_buyer, 'price':float(offer.price), 'comment':offer.comment, 'timestamp':calendar.timegm(offer.timestamp.timetuple())})

        ret = {'buyer':{'id':buyer['id'], 'nick_name':buyer['nick_name'], 'portrait':buyer['portrait']}, 'seller':{'id':seller['id'], 'nick_name':seller['nick_name'], 'portrait':seller['portrait']}, 'item':{'id':item['id'], 'title':item['title'], 'thumbnail':item['images'][0]['url'], 'price':item['fixed_price'], 'currency':item['currency_code'], 'status':item['status']}, 'timeline':results}
        return HttpResponse(json.dumps(ret), content_type='application/json', status=200)
    else:
        return HttpResponseForbidden('You are not allowed to see offers belonged to other people')


@require_POST
def post_question(request):
    body = request.POST.get('body', '')
    item_id = int(request.POST.get('item_id', 0))
    ref_post_id = int(request.POST.get('ref_post_id', 0))

    if item_id == 0 or body == '':
        return HttpResponseBadRequest('item not found or body is empty')

    question = PublicComment(item_id=item_id, author=request.user_id, comment=body, ref_post_id=ref_post_id)
    question.save()

    # auto watch the item you made offer on
    timestamp = get_now_timestamp()
    ret = r.sadd('item:%d:watchers' % item_id, request.user_id)
    if ret > 0:
        r.incr('item:%d:watchers.count' % item_id)
        r.zadd('user:%d:watchings' % request.user_id, item_id, timestamp)
        r.incr('user:%d:watchings.count' % request.user_id)

    item = get_item_by_id(item_id)
    if item is not None:
        async_send_question_message(question.id, item_id, request.user_id, item['owner']['id'], question.comment)

    return HttpResponse(json.dumps({'new_id':question.id}), content_type='application/json', status=201)

@require_POST
def post_reply(request):
    body = request.POST.get('body', '')
    item_id = int(request.POST.get('item_id', 0))
    ref_post_id = int(request.POST.get('ref_post_id', 0))

    if item_id == 0 or body == '':
        return HttpResponseBadRequest('item not found or comment is empty')

    if ref_post_id == 0:
        return HttpResponseBadRequest('reference post id should be provided')

    ref_question = get_object_or_None(PublicComment, pk=ref_post_id)
    if not ref_question:
        return HttpResponseBadRequest(_('reference post not found'))

    question = PublicComment(item_id=item_id, author=request.user_id, comment=body, ref_post_id=ref_post_id)
    question.save()

    item = get_item_by_id(item_id)
    if item is not None:
        if request.user_id == item['owner']['id']:
            async_send_reply_message(question.id, item_id, ref_question.author, request.user_id, question.comment)
        else:
            async_send_question_message(question.id, item_id, request.user_id, item['owner']['id'], question.comment)

    return HttpResponse(json.dumps({'new_id':question.id}), content_type='application/json', status=201)

@require_GET
def list_item_questions(request, item_id):
    item_id = int(item_id)

    if item_id == 0:
        return HttpResponseBadRequest('item not found or comment is empty')

    questions = PublicComment.objects.filter(item_id=item_id).order_by('-timestamp')
    results = []
    import calendar

    uids = set()
    for q in questions:
        uids.add(q.author)
        results.append({'id':q.id, 'author':q.author, 'body':q.comment, 'timestamp':calendar.timegm(q.timestamp.timetuple())})

    # Merge user info fetched from another machine
    user_list = get_users_info(uids)
    for entry in results:
        user_exist = False
        for user in user_list:
            if user['id'] == entry['author']:
                user_exist = True
                entry['author'] = {'id':user['id'], 'nick_name':mask_name(user['profile']['nick_name']), 'portrait':user['profile']['portrait']}
                break

        #if not user_exist:
            #results.remove(entry)

    return HttpResponse(json.dumps(results), content_type='application/json')

@require_GET
def set_status(request):
    #set_item_status(1, 3)
    set_item_state(1, 2)
    return HttpResponse()

@require_POST
def give_feedback(request):
    item_id = int(request.POST.get('item_id', 0))
    rate_num = int(request.POST.get('rate_num', 0))
    ratee_id = int(request.POST.get('ratee_id', 0))
    comment = request.POST.get('comment', '')

    if item_id == 0:
        return HttpResponseBadRequest('item not found')

    item = get_item_by_id(item_id)
    if not item or not 'status' in item or (item['status'] != IN_TRANSACTION_STATE and item['status'] != BUYER_RATED_STATE):
        return HttpResponseGone('item not available for rating')

    # Find the related deal
    if request.user_id == item['owner']['id']:
        # feedback is from seller to buyer
        deal = get_object_or_None(Deal, seller_id=request.user_id, item_id=item_id)
        r.hset('deal:%d' % deal.id, 'seller_rated', 1)
    elif ratee_id == item['owner']['id']:
        # feedback is from buyer to seller
        deal = get_object_or_None(Deal, buyer_id=request.user_id, item_id=item_id)
        r.hset('deal:%d' % deal.id, 'buyer_rated', 1)

    if not deal or deal.state == DEAL_CLOSED:
        return HttpResponseGone('deal already closed')

    # Generate new rating record and calculate average rating number
    for_seller = ratee_id == item['owner']['id']
    rating = Rating(for_seller=for_seller, rater_id=request.user_id, ratee_id=ratee_id, rate_num=rate_num, item_id=item_id, comment=comment)
    rating.save()

    ratings = Rating.objects.filter(for_seller=for_seller, ratee_id=ratee_id).order_by('-timestamp')
    sigma_rate = 0
    for rating in ratings:
        sigma_rate += rating.rate_num

    avg_rate = sigma_rate * 1.00 / len(ratings)

    if ratee_id == item['owner']['id']:
        r.set('user:%d:rate.seller' % ratee_id, avg_rate)
    else:
        r.set('user:%d:rate.buyer' % ratee_id, avg_rate)

    if r.hget('deal:%d' % deal.id, 'buyer_rated'):
        if not r.hget('deal:%d' % deal.id, 'seller_rated'):
            set_item_state(item_id, BUYER_RATED_STATE)
        else:
            deal.state = DEAL_CLOSED
            deal.save()

            set_item_state(item_id, CLOSED_STATE)

    offer_thread = get_object_or_None(OfferThread, buyer_id=deal.buyer_id, seller_id=deal.seller_id, item_id=item_id)
    if offer_thread is not None:
        async_send_feedback_message(rating.id, item_id, offer_thread.id, deal.buyer_id, deal.seller_id, rate_num, comment, 0 if for_seller else 1)

    return HttpResponse(json.dumps({'new_id':rating.id}), content_type='application/json', status=201)

@require_GET
def list_seller_feedbacks(request, seller_id):
    seller_id = int(seller_id)

    ratings = Rating.objects.filter(for_seller=True, ratee_id=seller_id).order_by('-timestamp')
    results = []
    rater_uids = set()
    for rating in ratings:
        rater_uids.add(rating.rater_id)
        results.append({'id':rating.id, 'rate_num':rating.rate_num, 'comment':rating.comment, 'rater':rating.rater_id})

    # Merge user info fetched from another machine
    user_list = get_users_info(rater_uids)

    for obj in results:
        user_exist = False
        for user in user_list:
            if user['id'] == obj['rater']:
                user_exist = True
                obj['rater'] = {'nick_name':mask_name(user['profile']['nick_name']), 'portrait':user['profile']['portrait']}
                break

        #if not user_exist:
            #results.remove(obj)

    return HttpResponse(json.dumps(results), content_type='application/json')

@require_POST
def remark_app(request):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    body = request.POST.get('body', '')
    if body == '':
        return HttpResponseBadRequest(_('should provide some content'))

    email = request.POST.get('email', '')
    try:
        validate_email(email)
    except ValidationError:
        return HttpResponseBadRequest(_('malformed email'))

    return HttpResponse('got your remark', status=200)

@require_GET
def user_stats(request, user_id):
    user_id = int(user_id)

    count_followers = r.get('user:%d:followers.count' % user_id)
    if count_followers is None:
        count_followers = 0
    else:
        count_followers = int(count_followers)

    count_followings = r.get('user:%d:followings.count' % user_id)
    if count_followings is None:
        count_followings = 0
    else:
        count_followings = int(count_followings)

    rate_seller = r.get('user:%d:rate.seller' % user_id)
    if rate_seller is not None:
        rate_seller = float(rate_seller)

    rate_buyer = r.get('user:%d:rate.buyer' % user_id)
    if rate_buyer is not None:
        rate_buyer = float(rate_buyer)

    if request.user_id == user_id:
        count_watchings = r.get('user:%d:watchings.count' % user_id)
        if count_watchings is None:
            count_watchings = 0
        else:
            count_watchings = int(count_watchings)

        count_buyings = r.get('user:%d:buyings.count' % user_id)
        if count_buyings is None:
            count_buyings = 0
        else:
            count_buyings = int(count_buyings)

        count_sellings = get_selling_items_count(user_id)

        results = {'id':user_id, 'count_followers':count_followers, 'count_followings':count_followings, 'count_watchings':count_watchings, 'count_sellings':count_sellings, 'count_buyings':count_buyings, 'avg_rate_as_seller':rate_seller, 'avg_rate_as_buyer':rate_buyer}

        return HttpResponse(json.dumps(results), content_type='application/json')
    else:
        results = {'id':user_id, 'count_followers':count_followers, 'count_followings':count_followings, 'avg_rate_as_seller':rate_seller, 'avg_rate_as_buyer':rate_buyer}

        return HttpResponse(json.dumps(results), content_type='application/json')

@require_GET
def buyinglist(request):
    try:
        item_ids = r.zrange('user:%d:buyings' % request.user_id, 0, -1)
        items = get_items_info(item_ids)

        return HttpResponse(json.dumps(items), content_type='application/json')
    except Exception:
        return HttpResponse(json.dumps([]), content_type='application/json')

@require_GET
def dialcodes(request):
    results = []
    countries = Country.objects.all()
    for country in countries:
        results.append({'country_name':country.countryname, 'dial_code':country.countryprefix})

    return HttpResponse(json.dumps(results), content_type='application/json')
