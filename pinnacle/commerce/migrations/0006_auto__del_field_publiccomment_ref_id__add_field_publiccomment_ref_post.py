# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'PublicComment.ref_id'
        db.delete_column(u'commerce_publiccomment', 'ref_id')

        # Adding field 'PublicComment.ref_post_id'
        db.add_column(u'commerce_publiccomment', 'ref_post_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'PublicComment.ref_id'
        db.add_column(u'commerce_publiccomment', 'ref_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Deleting field 'PublicComment.ref_post_id'
        db.delete_column(u'commerce_publiccomment', 'ref_post_id')


    models = {
        u'commerce.itemoffer': {
            'Meta': {'object_name': 'ItemOffer'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'from_buyer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'offer_thread': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['commerce.OfferThread']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '11', 'decimal_places': '2'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'commerce.offerthread': {
            'Meta': {'object_name': 'OfferThread'},
            'buyer_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'deal_done': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'seller_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'commerce.publiccomment': {
            'Meta': {'object_name': 'PublicComment'},
            'author': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ref_post_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['commerce']