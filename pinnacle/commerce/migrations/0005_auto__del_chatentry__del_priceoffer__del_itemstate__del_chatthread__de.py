# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'ChatEntry'
        db.delete_table(u'commerce_chatentry')

        # Deleting model 'PriceOffer'
        db.delete_table(u'commerce_priceoffer')

        # Deleting model 'ItemState'
        db.delete_table(u'commerce_itemstate')

        # Deleting model 'ChatThread'
        db.delete_table(u'commerce_chatthread')

        # Deleting model 'Offer'
        db.delete_table(u'commerce_offer')

        # Adding field 'OfferThread.seller_id'
        db.add_column(u'commerce_offerthread', 'seller_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=8),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'ChatEntry'
        db.create_table(u'commerce_chatentry', (
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('thread', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['commerce.ChatThread'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(default='S', max_length=1)),
            ('author_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'commerce', ['ChatEntry'])

        # Adding model 'PriceOffer'
        db.create_table(u'commerce_priceoffer', (
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('to_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('buy_side', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=11, decimal_places=2)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'commerce', ['PriceOffer'])

        # Adding model 'ItemState'
        db.create_table(u'commerce_itemstate', (
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('state', self.gf('django.db.models.fields.CharField')(default='A', max_length=1)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'commerce', ['ItemState'])

        # Adding model 'ChatThread'
        db.create_table(u'commerce_chatthread', (
            ('has_offers', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('seller', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('closed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('buyer', self.gf('django.db.models.fields.PositiveIntegerField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'commerce', ['ChatThread'])

        # Adding model 'Offer'
        db.create_table(u'commerce_offer', (
            (u'chatentry_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['commerce.ChatEntry'], unique=True, primary_key=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=11, decimal_places=2)),
        ))
        db.send_create_signal(u'commerce', ['Offer'])

        # Deleting field 'OfferThread.seller_id'
        db.delete_column(u'commerce_offerthread', 'seller_id')


    models = {
        u'commerce.itemoffer': {
            'Meta': {'object_name': 'ItemOffer'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'from_buyer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'offer_thread': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['commerce.OfferThread']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '11', 'decimal_places': '2'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'commerce.offerthread': {
            'Meta': {'object_name': 'OfferThread'},
            'buyer_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'deal_done': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'seller_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'commerce.publiccomment': {
            'Meta': {'object_name': 'PublicComment'},
            'author': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ref_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['commerce']