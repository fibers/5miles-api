# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'PriceOffer.direction'
        db.delete_column(u'commerce_priceoffer', 'direction')

        # Adding field 'PriceOffer.buy_side'
        db.add_column(u'commerce_priceoffer', 'buy_side',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'PriceOffer.direction'
        db.add_column(u'commerce_priceoffer', 'direction',
                      self.gf('django.db.models.fields.CharField')(default='B', max_length=1),
                      keep_default=False)

        # Deleting field 'PriceOffer.buy_side'
        db.delete_column(u'commerce_priceoffer', 'buy_side')


    models = {
        u'commerce.chatentry': {
            'Meta': {'object_name': 'ChatEntry'},
            'author_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'thread': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['commerce.ChatThread']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'commerce.chatthread': {
            'Meta': {'object_name': 'ChatThread'},
            'buyer': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'closed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_offers': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'seller': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'commerce.itemstate': {
            'Meta': {'object_name': 'ItemState'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'})
        },
        u'commerce.offer': {
            'Meta': {'object_name': 'Offer', '_ormbases': [u'commerce.ChatEntry']},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '11', 'decimal_places': '2'}),
            u'chatentry_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['commerce.ChatEntry']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'commerce.priceoffer': {
            'Meta': {'object_name': 'PriceOffer'},
            'buy_side': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'from_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '11', 'decimal_places': '2'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'to_uid': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'commerce.publiccomment': {
            'Meta': {'object_name': 'PublicComment'},
            'author': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ref_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['commerce']