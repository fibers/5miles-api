# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChatThread'
        db.create_table(u'commerce_chatthread', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('buyer', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('seller', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('closed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_offers', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'commerce', ['ChatThread'])

        # Adding model 'ChatEntry'
        db.create_table(u'commerce_chatentry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('thread', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['commerce.ChatThread'])),
            ('author_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(default='S', max_length=1)),
        ))
        db.send_create_signal(u'commerce', ['ChatEntry'])

        # Adding model 'Offer'
        db.create_table(u'commerce_offer', (
            (u'chatentry_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['commerce.ChatEntry'], unique=True, primary_key=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=11, decimal_places=2)),
        ))
        db.send_create_signal(u'commerce', ['Offer'])

        # Adding model 'PublicComment'
        db.create_table(u'commerce_publiccomment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('author', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('ref_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'commerce', ['PublicComment'])

        # Adding model 'PriceOffer'
        db.create_table(u'commerce_priceoffer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('from_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('to_uid', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=11, decimal_places=2)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'commerce', ['PriceOffer'])

        # Adding model 'ItemState'
        db.create_table(u'commerce_itemstate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('state', self.gf('django.db.models.fields.CharField')(default='A', max_length=1)),
        ))
        db.send_create_signal(u'commerce', ['ItemState'])


    def backwards(self, orm):
        # Deleting model 'ChatThread'
        db.delete_table(u'commerce_chatthread')

        # Deleting model 'ChatEntry'
        db.delete_table(u'commerce_chatentry')

        # Deleting model 'Offer'
        db.delete_table(u'commerce_offer')

        # Deleting model 'PublicComment'
        db.delete_table(u'commerce_publiccomment')

        # Deleting model 'PriceOffer'
        db.delete_table(u'commerce_priceoffer')

        # Deleting model 'ItemState'
        db.delete_table(u'commerce_itemstate')


    models = {
        u'commerce.chatentry': {
            'Meta': {'object_name': 'ChatEntry'},
            'author_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'thread': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['commerce.ChatThread']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'commerce.chatthread': {
            'Meta': {'object_name': 'ChatThread'},
            'buyer': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'closed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_offers': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'seller': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'commerce.itemstate': {
            'Meta': {'object_name': 'ItemState'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'})
        },
        u'commerce.offer': {
            'Meta': {'object_name': 'Offer', '_ormbases': [u'commerce.ChatEntry']},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '11', 'decimal_places': '2'}),
            u'chatentry_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['commerce.ChatEntry']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'commerce.priceoffer': {
            'Meta': {'object_name': 'PriceOffer'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'from_uid': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '11', 'decimal_places': '2'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'to_uid': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'commerce.publiccomment': {
            'Meta': {'object_name': 'PublicComment'},
            'author': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ref_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['commerce']