import sys, os
thrift_path = os.path.join(os.path.dirname(__file__), '../../gen-py')
sys.path.append(thrift_path)

from django.core.management.base import BaseCommand, CommandError
from commerce.models import PriceOffer

from PriceOffers import PriceOffers
from PriceOffers.ttypes import *

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

import socket
from annoying.functions import get_object_or_None

class PriceOfferHandler:
    def __init__(self):
        self.log = {}

    def get_offer(self, offer_id):
        offer = get_object_or_None(PriceOffer, pk=offer_id)
        if offer is None:
            return None

        obj =  ThriftOffer(itemId=offer.item_id, fromUid=offer.from_uid, toUid=offer.to_uid, price=offer.price, comment=offer.comment, buySide=offer.buy_side)
        return obj

class Command(BaseCommand):
    args = '<port>'
    help = 'Run thrift server at a given port'

    def handle(self, *args, **options):
        try:
            port = int(args[0])
            print 'running on port: %d' % port

            handler = PriceOfferHandler()
            processor = PriceOffers.Processor(handler)
            transport = TSocket.TServerSocket(port=port)
            tfactory = TTransport.TBufferedTransportFactory()
            pfactory = TBinaryProtocol.TBinaryProtocolFactory()

            server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

            print 'Starting Thrift server on port: %d' % port
            server.serve()
            print 'Done!'

        except Exception, e:
            print e
            raise CommandError('port number not provided')

