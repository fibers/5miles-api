from django.conf import settings
from tornado import gen, httpclient
from tornado.httputil import HTTPHeaders
from gearman import GearmanClient

import calendar
import datetime
import json
import urllib
import logging
import pytz

ITEM_SERVER = settings.ITEM_SERVER

def debug(msg):
    logger = logging.getLogger('pinnacle')
    logger.debug(msg)

def get_now_timestamp():
    now = datetime.datetime.utcnow()
    timestamp = calendar.timegm(now.timetuple())

    return timestamp

def get_now_timestamp_str():
    now = datetime.datetime.utcnow()
    timestamp = now.strftime('%Y-%m-%dT%H:%M:%SUTC')

    return timestamp


def get_now_timestamp_str2():
    now = datetime.datetime.utcnow()
    timestamp = now.strftime('%Y-%m-%d %H:%M:%SZ')

    return timestamp


def get_timestamp_str(timestamp):
    utc_dt = datetime.datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)
    timestamp_str = utc_dt.strftime('%Y-%m-%d %H:%M:%SZ')
    return timestamp_str

def get_item_by_id(id):
    client = httpclient.HTTPClient()
    try:
        request = httpclient.HTTPRequest(
                url="http://%s/commodity/product_detail/%d?format=json" % (ITEM_SERVER, id),
                method="GET",
                headers=HTTPHeaders({"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN}))

        response = client.fetch(request)
        item = json.loads(response.body)
    except Exception, e:
        print e
        item = None

    client.close()

    return item

def set_item_status(id, status):
    args = {'msg_type':1, 'msg_value':status, 'format':'json'}

    client = httpclient.AsyncHTTPClient()
    response = yield gen.Task(client.fetch,
            "http://%s/commodity/notification/%d?format=json" % (ITEM_SERVER, id),
            method="POST",
            headers=HTTPHeaders({"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN}),
            body=urllib.urlencode(args))

    if response.error:
        print 'failed to set item status'
        return
    else:
        print response.body

    raise gen.Return(json.loads(response.body))

def set_item_state(id, state):
    args = {'msg_type':1, 'msg_value':state, 'format':'json'}
    
    def handle_response(response):
        if response.error:
            print response.error
        else:
            print response.body

    request = httpclient.HTTPRequest(
            url="http://%s/commodity/notification/%d/" % (ITEM_SERVER, id),
            method="POST",
            headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN},
            body=urllib.urlencode(args))

    #client = httpclient.AsyncHTTPClient()
    #client.fetch(request, handle_response)

    http_client = httpclient.HTTPClient()
    response = http_client.fetch(request)
    
    #print response.body

def set_user_rating_number(user_id, rate_num, sell_side):
    args = {'msg_type':2, 'msg_value':json.dumps({'score':rate_num, 'as_seller':True}), 'format':'json'}
    
    request = httpclient.HTTPRequest(
            url="http://%s/commodity/user_notification/%d/" % (ITEM_SERVER, user_id),
            method="POST",
            headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN},
            body=urllib.urlencode(args))

    #client = httpclient.AsyncHTTPClient()
    #client.fetch(request, handle_response)

    http_client = httpclient.HTTPClient()
    response = http_client.fetch(request)

def get_users_info(user_ids):
    if not user_ids:
        return []

    uids = ','.join([str(_id) for _id in user_ids])

    request = httpclient.HTTPRequest(
            url="http://%s/commodity/user/?uids=%s" % (ITEM_SERVER, uids),
            method="GET",
            headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN})

    try:
        http_client = httpclient.HTTPClient()
        response = http_client.fetch(request)
        ret = json.loads(response.body)
    except Exception, e:
        print e
        ret = None

    return ret['objects']

def get_items_info(item_ids):
    if not item_ids:
        return []

    uids = ','.join([str(_id) for _id in item_ids])

    request = httpclient.HTTPRequest(
            url="http://%s/commodity/products/?pids=%s" % (ITEM_SERVER, uids),
            method="GET",
            headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN})

    try:
        http_client = httpclient.HTTPClient()
        response = http_client.fetch(request)
        ret = json.loads(response.body)
    except Exception, e:
        print e
        ret = None

    return ret['objects']

def get_selling_items_count(user_id):
    #request = httpclient.HTTPRequest(
            #url="http://%s/commodity/user/%d/products_selling/" % (ITEM_SERVER, user_id),
            #method="GET",
            #headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN})

    request = httpclient.HTTPRequest(
            url="http://%s/commodity/user/%d/sellings_count/" % (ITEM_SERVER, user_id),
            method="GET",
            headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN})
    try:
        http_client = httpclient.HTTPClient()
        response = http_client.fetch(request)
        ret = json.loads(response.body)
    except Exception, e:
        print e
        return 0

    return ret['seller_product_count']

def get_user_info(user_id):
    user_id = int(user_id)
    if user_id <= 0:
        return None

    request = httpclient.HTTPRequest(
            url="http://%s/commodity/user/%s/" % (ITEM_SERVER, user_id),
            method="GET",
            headers={"X-FIVEMILES-USER-TOKEN":settings.SERVER_SIDE_TOKEN})

    try:
        http_client = httpclient.HTTPClient()
        response = http_client.fetch(request)

        return json.loads(response.body)
    except Exception, e:
        print e
        return None

def mask_name(name):
    if not name or not isinstance(name, str) or len(name) <= 2:
        return name

    return '%s%s%s' % (name[0], '*' * len(name[1:-1]), name[-1])

def put_message_question(question):
    pass

def put_message_feedback(feedback):
    pass

import os
gray_env_tag = 'no'
def is_gray():
    global gray_env_tag
    if gray_env_tag=='no':
        gray_env_tag = os.getenv('GRAY')
        if "True" == gray_env_tag:
            gray_env_tag = True
            return True
        else:
            gray_env_tag = False
            return False
    else:
        return gray_env_tag