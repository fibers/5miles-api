from tastypie.resources import ModelResource
from commerce.models import OfferThread, ItemOffer

class ThreadResource(ModelResource):
    class Meta:
        queryset = OfferThread.objects.all()
        resource_name = 'thread'
        list_allowed_methods = ['get']

    def apply_authorization_limits(self, request, object_list):
        item_id = int(request.POST.get('item_id', 0))
        if item_id == 0:
            return []
        return object_list.filter(item_id=item_id)

class OfferResource(ModelResource):
    class Meta:
        queryset = ItemOffer.objects.all()
        resource_name = 'offer'
        list_allowd_methods = ['get']
        fields = ['price', 'comment', 'timestamp']

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(from_uid=request.user_id)

