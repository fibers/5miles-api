import sys, os
thrift_path = os.path.join(os.path.dirname(__file__), 'gen-py')
sys.path.append(thrift_path)

from PriceOffers import PriceOffers
from PriceOffers.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

try:
    transport = TSocket.TSocket('localhost', 12306)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)
    
    client = PriceOffers.Client(protocol)

    transport.open()
    offer = client.get_offer(3)
    print offer

except Thrift.TException, tx:
    print '%s' % (tx.message)
