# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'UserFeedback.os'
        db.add_column(u'feedback_userfeedback', 'os',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50),
                      keep_default=False)

        # Adding field 'UserFeedback.os_version'
        db.add_column(u'feedback_userfeedback', 'os_version',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50),
                      keep_default=False)

        # Adding field 'UserFeedback.app_version'
        db.add_column(u'feedback_userfeedback', 'app_version',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=10),
                      keep_default=False)

        # Adding field 'UserFeedback.device'
        db.add_column(u'feedback_userfeedback', 'device',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'UserFeedback.os'
        db.delete_column(u'feedback_userfeedback', 'os')

        # Deleting field 'UserFeedback.os_version'
        db.delete_column(u'feedback_userfeedback', 'os_version')

        # Deleting field 'UserFeedback.app_version'
        db.delete_column(u'feedback_userfeedback', 'app_version')

        # Deleting field 'UserFeedback.device'
        db.delete_column(u'feedback_userfeedback', 'device')


    models = {
        u'feedback.userfeedback': {
            'Meta': {'object_name': 'UserFeedback'},
            'app_version': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'os': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50'}),
            'os_version': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'user_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['feedback']