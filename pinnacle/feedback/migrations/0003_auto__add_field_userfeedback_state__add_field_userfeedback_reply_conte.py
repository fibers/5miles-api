# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'UserFeedback.state'
        db.add_column(u'feedback_userfeedback', 'state',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'UserFeedback.reply_content'
        db.add_column(u'feedback_userfeedback', 'reply_content',
                      self.gf('django.db.models.fields.TextField')(null=True),
                      keep_default=False)

        # Adding field 'UserFeedback.updated_at'
        db.add_column(u'feedback_userfeedback', 'updated_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, null=True, blank=True),
                      keep_default=False)


        # Changing field 'UserFeedback.app_version'
        db.alter_column(u'feedback_userfeedback', 'app_version', self.gf('django.db.models.fields.CharField')(max_length=20))

    def backwards(self, orm):
        # Deleting field 'UserFeedback.state'
        db.delete_column(u'feedback_userfeedback', 'state')

        # Deleting field 'UserFeedback.reply_content'
        db.delete_column(u'feedback_userfeedback', 'reply_content')

        # Deleting field 'UserFeedback.updated_at'
        db.delete_column(u'feedback_userfeedback', 'updated_at')


        # Changing field 'UserFeedback.app_version'
        db.alter_column(u'feedback_userfeedback', 'app_version', self.gf('django.db.models.fields.CharField')(max_length=10))

    models = {
        u'feedback.userfeedback': {
            'Meta': {'object_name': 'UserFeedback'},
            'app_version': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'os': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'os_version': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'reply_content': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'state': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['feedback']