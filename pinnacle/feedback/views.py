
from django.views.decorators.http import require_POST
from django.http import HttpResponse
import json
import logging

from .models import *
from boss.tasks import send_feedback_to_boss

logger = logging.getLogger('pinnacle')

@require_POST
def post_feedback(request):
    text = request.POST.get('text', '')
    feedback = UserFeedback(user_id=request.user_id,
                            text=text,
                            os=request.os,
                            os_version=request.os_version,
                            app_version=request.app_version,
                            device=request.device)
    feedback.save()
    send_feedback_to_boss.delay(feedback.id)

    return HttpResponse(status=201)


