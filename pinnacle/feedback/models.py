from django.db import models

# Create your models here.


class UserFeedback(models.Model):
    user_id = models.PositiveIntegerField()
    text = models.TextField()
    os = models.CharField(max_length=50, blank=True, default='')
    os_version = models.CharField(max_length=50, blank=True, default='')
    app_version = models.CharField(max_length=20, blank=True, default='')
    device = models.CharField(max_length=50, blank=True, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    state = models.BooleanField(default=False)
    reply_content = models.TextField(null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)