from django.views.decorators.http import require_GET, require_POST
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest

import redis
import calendar
import datetime
import json

from commerce.utils import *
from entity.exceptions import InvalidParameterError

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

@require_POST
def follow(request):
    target_user = int(request.POST.get('user_id', 0))
    if target_user == 0 or target_user == request.user_id:
        raise InvalidParameterError()

    now = datetime.datetime.utcnow()
    timestamp = calendar.timegm(now.timetuple())
    with r.pipeline() as pipe:
        pipe.zadd('user:%d:followers' % target_user, request.user_id, timestamp)
        pipe.incr('user:%d:followers.count' % target_user)

        pipe.zadd('user:%d:followings' % request.user_id, target_user, timestamp)
        pipe.incr('user:%d:followings.count' % request.user_id)

        pipe.execute()

    return HttpResponse(status=201)

@require_POST
def unfollow(request):
    target_user = int(request.POST.get('user_id', 0))
    if target_user == 0:
        raise InvalidParameterError()

    with r.pipeline() as pipe:
        pipe.zrem('user:%d:followers' % target_user, request.user_id)
        pipe.decr('user:%d:followers.count' % target_user)

        pipe.zrem('user:%d:followings' % request.user_id, target_user)
        pipe.decr('user:%d:followings.count' % request.user_id)

        pipe.execute()

    return HttpResponse()

@require_GET
def get_followers(request, user_id):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))

    if offset < limit:
        prevLink = None
    else:
        prevLink = '?offset=%d&limit=%d' % (offset-limit, limit)

    total_count = r.get('user:%s:followers.count' % user_id)
    if total_count is None:
        return HttpResponse(json.dumps({'meta':{'limit':limit, 'next':None, 'offset':offset, 'previous':None, 'total_count':0}, 'objects':[]}), content_type='application/json')

    total_count = int(total_count)
    if offset + limit >= total_count:
        nextLink = None
    else:
        nextLink = '?offset=%d&limit=%d' % (offset+limit, limit)

    if offset+limit >= total_count:
        start = 0
    else:
        start = -offset-limit

    follower_uids = r.zrange('user:%s:followers' % user_id, start, -1+offset)
    user_list = get_users_info(follower_uids)
    results = []

    for user in user_list:
        followed = request.user_id == int(user['id']) or r.zrank('user:%d:followings' % request.user_id, user['id']) is not None

        results.append({'id':user['id'], 'portrait':user['profile']['portrait'], 'nick_name':user['profile']['nick_name'], 'followed':followed})

    ret = {'meta':{'limit':limit, 'next':nextLink, 'offset':offset, 'previous':prevLink, 'total_count':total_count}, 'objects':results}

    return HttpResponse(json.dumps(ret), content_type='application/json', status=200)

@require_GET
def get_followings(request, user_id):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))

    if offset < limit:
        prevLink = None
    else:
        prevLink = '?offset=%d&limit=%d' % (offset-limit, limit)

    total_count = r.get('user:%s:followings.count' % user_id)
    if total_count is None:
        return HttpResponse(json.dumps({'meta':{'limit':limit, 'next':None, 'offset':offset, 'previous':None, 'total_count':0}, 'objects':[]}), content_type='application/json')

    total_count = int(total_count)
    if offset + limit >= total_count:
        nextLink = None
    else:
        nextLink = '?offset=%d&limit=%d' % (offset+limit, limit)

    if offset+limit >= total_count:
        start = 0
    else:
        start = -offset-limit

    following_uids = r.zrange('user:%s:followings' % user_id, start, -1+offset)

    user_list = get_users_info(following_uids)
    results = []

    for user in user_list:
        followed = request.user_id == int(user_id) or r.zrank('user:%d:followings' % request.user_id, user['id']) is not None

        results.append({'id':user['id'], 'portrait':user['profile']['portrait'], 'nick_name':user['profile']['nick_name'], 'followed':followed})

    ret = {'meta':{'limit':limit, 'next':nextLink, 'offset':offset, 'previous':prevLink, 'total_count':total_count}, 'objects':results}

    return HttpResponse(json.dumps(ret), content_type='application/json', status=200)

@require_GET
def check_followed(request, user_id):
    result = r.zrank('user:%d:followings' % request.user_id, int(user_id))
    followed = result is not None

    ret = {'result':followed}
    return HttpResponse(json.dumps(ret), content_type='application/json', status=200)
