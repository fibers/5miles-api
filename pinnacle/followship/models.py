from django.db import models
import redis

# Create your models here.

class Activity:
    pass

class Timeline:
    def page(self, page):
        _from = (page - 1) * 10
        _to = page * 10
        return [Activity(activity_id) for activity_id in r.lrange('timeline', _from, _to)]

class Player:
    @staticmethod
    def find_by_username(username):
        pass

    @staticmethod
    def find_by_id(id):
        pass

    @staticmethod
    def create(username, password):
        pass

    def activities(self, page=1):
        pass

    def timeline(self, page=1):
        pass

    def add_activity(self, activity):
        pass

    def add_timeline_activity(self, activity):
        pass

    def follow(self, player):
        pass

    def unfollow(self, player):
        pass

    @property
    def followers(self):
        pass

    @property
    def followees(self):
        pass

    @property
    def followers_count(self):
        return r.scard("user:id:%s:followers" % self.id) or 0

    @property
    def followees_count(self):
        return r.scard("user:id:%s:followees" % self.id) or 0

    def add_follower(self, user_id):
        return r.sadd("user:id:%s:followers" % self.id, user_id)
