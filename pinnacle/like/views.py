# -*- coding: utf-8 -*-
from django.views.decorators.http import require_GET, require_POST
from django.conf import settings
from django.http import HttpResponse
from django.db.models import F

import redis
import json

from commerce.utils import get_now_timestamp
from common.biz import *
from entity.service import UserService
from entity.exceptions import UserBlockedError, InvalidParameterError, UserNotFoundError, ItemNotFoundError
from message.tasks import notify
from common.utils_es import update_field_in_searchengine
from entity.models import Item

r = SrService().get_redis()
print 'like: connect to redis'


@require_POST
def like(request):
    fuzzy_item_id = request.POST.get('item_id', '')
    item_id = unfuzzy_item_id(fuzzy_item_id)

    if item_id is None:
        raise InvalidParameterError()

    owner_id = r.get('item:%s:owner' % item_id)
    if UserService().is_blocked(owner_id, request.user_id):
        raise UserBlockedError()

    with r.pipeline() as pipe:
        # FIXME 废掉likers, 使用new_likers替代
        pipe.sadd('item:%s:likers' % item_id, request.user_id)
        pipe.zadd('item:%s:new_likers' % item_id, request.user_id, get_now_timestamp())
        pipe.zadd('user:%s:likes' % request.user_id, item_id, get_now_timestamp())

        pipe.execute()

    likes = r.zcard('item:%s:new_likers' % item_id)
    # update(item_id, {'script': 'ctx._source.likes += 1'})
    update_field_in_searchengine(item_id, 'likes', likes)
    Item.objects.filter(pk=item_id).update(likes=likes)

    # 商品被Like，给商品卖家发系统消息和PUSH
    liker_info = r.hmget('user:%s' % request.user_id, 'nickname', 'portrait')
    vars = {'liker_nickname': liker_info[0], 'liker_portrait': liker_info[1], 'fuzzy_item_id': fuzzy_item_id, 'image_type': 1}
    notify.delay(owner_id, 'TRI_LIKE', vars, request.user_id)

    return HttpResponse(status=201)


@require_POST
def unlike(request):
    item_id = unfuzzy_item_id(request.POST.get('item_id', ''))
    
    if item_id is None:
        raise InvalidParameterError()

    with r.pipeline() as pipe:
        pipe.srem('item:%s:likers' % item_id, request.user_id)
        pipe.zrem('item:%s:new_likers' % item_id, request.user_id)
        pipe.zrem('user:%s:likes' % request.user_id, item_id)

        pipe.execute()

    likes = r.zcard('item:%s:new_likers' % item_id)
    # update(item_id, {'script': 'ctx._source.likes -= 1'})
    update_field_in_searchengine(item_id, 'likes', likes)
    Item.objects.filter(pk=item_id).update(likes=likes)

    return HttpResponse()


@require_GET
def get_likes(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    fuzzy_user_id = request.GET.get('user_id', '')

    if fuzzy_user_id == '':
        user_id = request.user_id
    else:
        user_id = r.get('fuzzy_id:%s:user_id' % fuzzy_user_id)
        if user_id is None:
            raise UserNotFoundError(fuzzy_user_id)

    item_ids = r.zrevrange('user:%s:likes' % user_id, offset, offset+limit-1)
    total_count = r.zcard('user:%s:likes' % user_id)

    items = []
    for item_id in item_ids:
        item = r.hgetall('item:%s' % item_id)

        owner_id = r.get('item:%s:owner' % item_id)
        try:
            item['owner'] = get_user_from_id(owner_id)
            item['images'] = json.loads(item['images'])
            item['location'] = json.loads(item['location'])
            items.append(item)
        except Exception, e:
            logger.error('item : %s unpack error, msg:%s' % (item_id, e.message))
            print 'item: %s unpack error' % item_id
            # r.delete('item:%s' % item_id)
            # r.zrem('user:%s:likes' % user_id, item_id)


    ret = {'meta':{'total_count':total_count, 'offset':offset, 'limit':len(items), 'previous':'?offset=%d&limit=%d' % (offset - limit, limit) if offset >= limit else None, 'next':'?offset=%d&limit=%d' % (offset + limit, limit) if offset + limit < total_count else None}, 'objects':items}

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def get_item_likers(request):
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    fuzzy_item_id = request.GET.get('item_id', '')

    if fuzzy_item_id is None:
        raise InvalidParameterError()

    item_id = r.get('fuzzy_item_id:%s:item_id' % fuzzy_item_id)
    if item_id is None:
        raise ItemNotFoundError(fuzzy_item_id)

    likers = []
    liker_ids = r.zrevrange('item:%s:new_likers' % item_id, offset, offset+limit-1)
    total_count = r.zcard('item:%s:new_likers' % item_id)
    user_service = UserService()
    for liker_id in liker_ids:
        liker = get_user_from_id(liker_id)
        liker['following'] = user_service.is_following(request.user_id, liker_id) if hasattr(request, 'user_id') else False
        likers.append(liker)

    ret = {'meta':{'total_count':total_count, 'offset':offset, 'limit':len(likers), 'previous':'?offset=%d&limit=%d' % (offset - limit, limit) if offset >= limit else None, 'next':'?offset=%d&limit=%d' % (offset + limit, limit) if offset + limit < total_count else None}, 'objects':likers}

    return HttpResponse(json.dumps(ret), content_type='application/json')
