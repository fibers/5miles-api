# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import shared_task

import datetime
import json
import logging
import urllib
from celery.result import AsyncResult
import redis
import requests

from django.conf import settings
from django.conf import settings
from tornado import httpclient
from tornado.httputil import HTTPHeaders
from common.es_dao import es_dao_item
from common.utils import capture_exception, kickbox_validate_email, capture_message

from message.models import NOTIFICATION_TYPE_ALL, NOTIFICATION_METHOD_EMAIL, NOTIFICATION_METHOD_SMS, \
    NOTIFICATION_TYPE_REMINDERS
from common.utils import robot_or_not
from common.biz import *
from requests.exceptions import RequestException
from entity.models import Item, User
from django.db import connection
from .biz import is_subscribed, subscribe_notification
from message.services import MessageService

from entity.exceptions import NotFoundError
logger = logging.getLogger('pinnacle')

r = SrService().get_redis()
message_service = MessageService()

@shared_task
def send_mail(target_user, template, text=None, data=None, subject=None, from_address=None):
    text = text.encode('utf-8') if text is not None else None
    args = {'target_user': target_user, 'template': template, 'text:': text,
            'data': json.dumps(data) if data else None, 'subject': subject, 'from': from_address}
    args = {k: v for k, v in args.items() if v is not None}
    # _do_fetch('send_mail', 'POST', args, {"Accept": "application/vnd.fmmc-v2.1+json"})
    _request('send_mail', None, args, headers={"Accept": "application/vnd.fmmc-v2.1+json"})


@shared_task
def push(target_user, template=None, text=None, data=None, payload=None, source_user=None):
    text = text.encode('utf-8') if text is not None else None
    args = {'target_user': target_user, 'template': template, 'text': text,
            'data': json.dumps(data) if data else None,
            'payload': json.dumps(payload) if payload else None,
            'source_user': source_user}
    args = {k: v for k, v in args.items() if v is not None}
    # _do_fetch('push', 'POST', args, {"Accept": "application/vnd.fmmc-v2.1+json"})
    _request('push', None, args, headers={"Accept": "application/vnd.fmmc-v2.1+json"})


@shared_task
def notify(target_user, trigger_name, data=None, source_user=None):
    args = {'target_user': target_user, 'trigger_name': trigger_name,
            'data': json.dumps(data) if data else None,
            'source_user': source_user}
    args = {k: v for k, v in args.items() if v is not None}
    # _do_fetch('notify', 'POST', args, {"Accept": "application/vnd.fmmc-v2.1+json"})
    _request('notify', None, args, headers={"Accept": "application/vnd.fmmc-v2.1+json"})


@shared_task
def send_sms(target_user=None, mobile_phone=None, template=None, text=None, data=None):
    params = {'target_user': target_user, 'mobile_phone': mobile_phone, 'template': template, 'text': text,
              'data': json.dumps(data) if data else None}
    params = {k: v for k, v in params.items() if v is not None}
    _request('send_sms', None, params, headers={"Accept": "application/vnd.fmmc-v2.1+json"})


@shared_task
def welcome(user_id, email, vars, include_password=False, facebook_signup=False):
    """
    用户注册，发送欢迎邮件
    :param user_id: 用户ID
    :param email: 用户email地址
    :param vars: 携带的数据
    :return:
    """

    if robot_or_not(email) or email.endswith('@worldbuz.com'):
        return

    # kissfm contest
    #notify(user_id, 'TRI_KISSFM_CONTEST', vars)
    #notify(user_id, 'TRI_MEDIA_CONTEST', vars)

    # 首先调用kickbox API校验用户email是否合法
    if settings.DEBUG or (not settings.DEBUG and kickbox_validate_email(email)):
        # 目前web发offer注册时只能使用email进行注册
        if include_password:
            notify(user_id, 'TRI_WELCOME_WITH_PASSWORD', vars)
        else:
            # 为了区分facebook注册和email注册时发送欢迎邮件是否需要包含验证链接,如果是facebook登陆则不需要验证链接
            notify(user_id, 'TRI_WELCOME_FACEBOOK' if facebook_signup else 'TRI_WELCOME', vars)
    else:
        # 用户email不合法，不发送欢迎邮件，对用户做退订处理
        subscribe_notification(user_id, NOTIFICATION_TYPE_ALL, NOTIFICATION_METHOD_EMAIL, 0, 'invalid')


@shared_task
def prompt_to_upload_portrait(user_id, where):
    """
    催用户上传头像
    :param user_id: user id
    :param where: ITEM_POST => 上传商品后，PROFILE_VISITED => 个人页被浏览时
    :return: None
    """

    user_info = r.hmget('user:%s' % user_id, 'portrait', 'nickname')
    if user_info[0] != settings.DEFAULT_AVATAR_LINK:
        return

    if where == 'ITEM_POST':
        trigger_name = 'TRI_UPLOAD_USER_PHOTO_1'
    elif where == 'PROFILE_VISITED':
        trigger_name = 'TRI_UPLOAD_USER_PHOTO_2'

    key = 'fmmc:user_id:%s:prompt_to_upload_portrait:count' % user_id
    if not r.get(key):
        notify(user_id, trigger_name, {'nickname': user_info[1]})
        with r.pipeline() as pipe:
            pipe.incr(key)
            pipe.expire(key, 24 * 3600)
            pipe.execute()


@shared_task
def prompt_price_change(item_id, fuzzy_item_id, owner_id):
    """
    提醒like或offer过此商品的用户商品价格变化
    :param user_id:
    :param fuzzy_item_id:
    :param item_id: 商品ID
    :return: None
    """

    offerline_ids = r.lrange('item:%s:offerlines' % item_id, 0, -1)
    user_ids = set()
    for offerline_id in offerline_ids:
        buyer_id = r.hget('offerline:%s:meta' % offerline_id, 'buyer_id')
        user_ids.add(buyer_id)

    liker_ids = r.smembers('item:%s:likers' % item_id)
    for liker_id in liker_ids:
        user_ids.add(liker_id)

    if len(user_ids) == 0:
        return

    owner_info = r.hmget('user:%s' % owner_id, 'nickname', 'portrait', 'id')
    item_info = r.hmget('item:%s' % item_id, 'title', 'images', 'local_price')
    for user_id in user_ids:
        user_info = r.hmget('user:%s' % user_id, 'nickname', 'portrait')
        notify(user_id, 'TRI_PRICE_CHANGE', {'nickname': user_info[0],
                                             'owner_portrait': owner_info[1],
                                             'item_title': item_info[0],
                                             'item_image': json.loads(item_info[1])[0]['imageLink'],
                                             'item_price': item_info[2],
                                             'item_id': item_id,
                                             'fuzzy_item_id': fuzzy_item_id,
                                             'image_type': 1}, owner_id,)


@shared_task
def notify_other_buyers_and_likers(item_id, buyer_id, offerline_id):
    # timestamp = get_now_timestamp()
    item_info = r.hmget('item:%s' % item_id, 'title', 'id', 'images', 'local_price')

    # 通知卖家
    seller_id = r.get('item:%s:owner' % item_id)
    seller_nickname = r.hget('user:%s' % seller_id, 'nickname')
    notify(str(seller_id), 'TRI_SOLD_SELLER',
           {'nickname': seller_nickname,
            'item_title': item_info[0],
            'offerline_id': str(offerline_id),
            'item_image': json.loads(item_info[2])[0]['imageLink'],
            'item_price': item_info[3],
            'fuzzy_item_id': item_info[1]})

    # 通知买家
    buyer_nickname = r.hget('user:%s' % buyer_id, 'nickname')
    notify(str(buyer_id), 'TRI_SOLD_BUYER',
           {'nickname': buyer_nickname,
            'item_title': item_info[0],
            'offerline_id': str(offerline_id),
            'item_image': json.loads(item_info[2])[0]['imageLink'],
            'item_price': item_info[3],
            'fuzzy_item_id': item_info[1]}, seller_id)

    notify(None, 'TRI_SOLD_LIKER', {'item_id': str(item_id), 'buyer_id': buyer_id, 'fuzzy_item_id': item_info[1]}, seller_id)


@shared_task
def encourage_listing_when_first_deal(user_id, sold_item_id):
    """
    第一个商品sold时通知卖家，鼓励继续发商品
    :param user_id: 卖家用户ID
    :param sold_item_id: 当前已经sold的商品ID
    :return: None
    """

    sold = 0
    user_selling_list = r.zrange('user:%s:user_selling_list' % user_id, 0, -1)
    for item_id in user_selling_list:
        if item_id == sold_item_id:
            continue

        if r.get('item:%s:sold_to' % item_id) is not None:
            sold += 1

    if sold > 0:
        return

    nickname = r.hget('user:%s' % user_id, 'nickname')
    notify(user_id, 'TRI_ENCOURAGE_LISTING_2', {'nickname': nickname})


@shared_task
def prompt_followers_has_new_items(user_id, item_id):
    """
    每日发送第一个商品时通知followers
    :param user_id: 用户ID
    :param item_id: 商品ID
    :return: None
    """

    key = 'fmmc:user_id:%s:items:count' % user_id
    if not r.get(key):
        follower_ids = r.zrevrange('user:%s:followers' % user_id, 0, -1)
        user_info = r.hmget('user:%s' % user_id, 'nickname', 'portrait', 'id')
        item_info = r.hmget('item:%s' % item_id, 'title', 'images', 'local_price')
        items = list()
        items.append({'item_title': item_info[0],
                      'item_image': json.loads(item_info[1])[0]['imageLink'],
                      'item_price': item_info[2]})
        followings = list()
        followings.append({'following_portrait': user_info[1],
                           'following_nickname': user_info[0],
                           'following_items': items})
        for follower_id in follower_ids:
            # follower_nickname = r.hget('user:%s' % follower_id, 'nickname')
            notify(follower_id, 'TRI_FOLLOW_2', {'following_nickname': user_info[0],
                                                 'followings': followings,
                                                 'following_fuzzy_id': user_info[2],
                                                 'following_portrait': user_info[1],
                                                 'image_type': 1}, user_id,)

        with r.pipeline() as pipe:
            pipe.incr(key)
            pipe.expire(key, 24 * 3600)
            pipe.execute()


@shared_task(queue='celery.schedule')
def remind_to_reply_offer_by_sms_new(user_id, offerline_id, owner_id):
    # connection.close()

    if not is_subscribed(user_id, NOTIFICATION_METHOD_SMS, NOTIFICATION_TYPE_REMINDERS):
        return

    count_key = 'user:%s:daily_reply_remind_count' % user_id
    daily_count = r.get(count_key)
    if daily_count and int(daily_count) > 0:
        return

    task_id = r.get('user:%s:offerline:%s:reply_countdown:task_id' % (user_id, offerline_id))
    logger.debug('[remind_to_reply_offer_by_sms_new] user_id => %s, offerline_id => %s, task_id => %s' %
                 (user_id, offerline_id, task_id))

    if not task_id:
        return

    item_id = r.hget('offerline:%s:meta' % offerline_id, 'item_id')
    item_state = get_item_state(item_id)
    if item_state != ITEM_STATE_LISTING:
        return

    if int(user_id) == owner_id:
        template = 'TMPL_SMS_REMIND_SELLER_TO_REPLY_OFFER'
    else:
        template = 'TMPL_SMS_REMIND_BUYER_TO_REPLY_OFFER'

    item_title = r.hget('item:%s' % item_id, 'title')
    data = {'item_title': item_title.decode('utf-8')}
    #send_sms(target_user=user_id, template=template, data=data)
    # stop_reply_countdown(user_id, offerline_id)
    with r.pipeline() as pipe:
        pipe.incr(count_key)
        pipe.expire(count_key, 24 * 3600)
        pipe.execute()


@shared_task
def send_seller_guide(user_id , item_id):
    """
    用户上传第一个商品后立即发新卖家指导页面
    :param user_id: 用户ID
    :return:
    """
    from entity.service import UserService
    UserService().send_seller_guide(user_id , item_id)


@shared_task
def remind_to_renew(user_id, item_id):
    """
    每个商品上传后24小时后Listing状态且未Renew过时提醒卖家
    :param user_id: 用户ID
    :param item_id: 商品ID
    :return:
    """
    notify(user_id, 'TRI_REMIND_TO_RENEW', {'item_id': item_id, 'fuzzy_item_id': r.hget('item:%s' % item_id, 'id')})


def _do_fetch(endpoint, method, args, headers=None):
    logger.debug('query string: %s' % urllib.urlencode(args))
    client = httpclient.HTTPClient()
    try:
        request = httpclient.HTTPRequest(
            url='%s/%s' % (settings.FMMC_API_ENDPOINT, endpoint),
            method=method,
            headers=HTTPHeaders(headers) if headers else None,
            body=urllib.urlencode(args))

        response = client.fetch(request)
        result = response.body

        if settings.DEBUG:
            logger.debug(response.body)

        return result
    except Exception, e:
        logger.exception(e)
        capture_exception()
    finally:
        client.close()

    return None


def _request(endpoint, method, params, **kwargs):
    if settings.DEBUG:
        logger.debug('query string: %s' % params)

    try:
        resp = requests.post(
            '%s/%s' % (settings.FMMC_API_ENDPOINT, endpoint),
            data=params,
            **kwargs)
        # result = json.loads(resp.json())
        result = resp.text
        status_code = resp.status_code
        if settings.DEBUG:
            logger.debug('status code: %s, result: %s' % (status_code, result))

        if status_code < 200 or status_code > 299:
            message = 'Failed to request: %s/%s, status_code: %d, text: %s' % \
                      (settings.FMMC_API_ENDPOINT, endpoint, status_code, result)
            logger.warn(message)
            capture_message(message)

        return result
    except Exception, e:
        logger.exception(e)
        capture_exception()

    return None


def start_reply_countdown(user_id, offerline_id, owner_id):
    """
    启动offer回复倒计时
    :param user_id:
    :param offerline_id:
    :return:
    """
    if not is_subscribed(user_id, NOTIFICATION_METHOD_SMS, NOTIFICATION_TYPE_REMINDERS):
        return

    key = 'user:%s:offerline:%s:reply_countdown:task_id' % (user_id, offerline_id)
    if r.exists(key):
        return

    task = remind_to_reply_offer_by_sms_new.apply_async((user_id, offerline_id, owner_id,),
                                                        eta=datetime.datetime.utcnow() + datetime.timedelta(days=1),
                                                        queue='celery.schedule')

    # 为避免redis过期时间与celery定时任务不同步，延长key的过期时间10分钟
    r.set(key, task.id, 24 * 3600 + 600)
    logger.debug('[start_reply_countdown] user_id => %s, offerline_id => %s, task_id => %s'
                 % (user_id, offerline_id, task.id))


def stop_reply_countdown(user_id, offerline_id):
    """
    停止offer回复倒计时（用户回复offer时调用）
    :param user_id:
    :param offerline_id:
    :return:
    """
    key = 'user:%s:offerline:%s:reply_countdown:task_id' % (user_id, offerline_id)
    if r.exists(key):
        task_id = r.get(key)
        # app.control.revoke(last_task_id) app object has no attribute 'control'
        AsyncResult(task_id).revoke()
        r.delete(key)
        logger.debug('[stop_reply_countdown] user_id => %s, offerline_id => %s, task_id => %s'
                     % (user_id, offerline_id, task_id))


@shared_task(queue='celery.misc')
def update_user_current_device(user_id, os, os_version, app_version, device):
    if not user_id:
        return

    r.hmset('user:%s' % user_id, {'current_os': os, 'current_os_version': os_version,
                                  'current_app_version': app_version, 'current_device': device})
    user = User.objects.get(pk=user_id)
    user.current_os = os
    user.current_os_version = os_version
    user.current_app_version = app_version
    user.current_device = device
    user.save()


@shared_task
def register_device_task(user_id, device_id, installation_id, provider, push_token, device, os, os_version, app_version):
    update_user_current_device.delay(user_id, os, os_version, app_version, device)
    message_service.register_device(user_id, device_id, installation_id, provider, push_token, device, os, os_version, app_version)


@shared_task()
def unregister_device_task(user_id, provider, installation_id):
    message_service.unregister_device(user_id, provider, installation_id)


@shared_task(queue='celery.misc')
def mark_read_message(user_id, message_id, offerline_id):
    if message_id != '':
        if not r.exists('message:%s' % message_id):
            raise NotFoundError()

    elif offerline_id != '':
        if not r.exists('offerline:%s:meta' % offerline_id):
            raise NotFoundError()

        offerline = r.hgetall('offerline:%s:meta' % offerline_id)
        buyer_id = offerline['buyer_id']
        item_id = offerline['item_id']
        owner_id = r.get('item:%s:owner' % item_id)
        role = 1 if str(user_id) == owner_id else 0
        if role == 1:
            # request user is the seller
            message_id = r.get('from:%s:to:%s:item:%s:message_id' % (buyer_id, user_id, item_id))
        else:
            # request user is the buyer
            message_id = r.get('from:%s:to:%s:item:%s:message_id' % (owner_id, user_id, item_id))

    msg_attr = r.hmget('message:%s' % message_id, 'type', 'count')
    inbox_key_count = 'user:%s:inbox:%s:new_count' % (user_id, msg_attr[0])
    r.incr(inbox_key_count, int(msg_attr[1] if msg_attr[1] is not None else '1')*-1)
    new_count = r.get(inbox_key_count)
    # 老版本刚切换时,会出现负值数据,此处做防御处理
    if int(new_count) < 0:
        r.set(inbox_key_count, 0)
        logger.warn('new_count < 0, offerline_id:%s, message: %s' % (offerline_id, message_id))
    r.hmset('message:%s' % message_id, {'unread': False, 'count': 0})
