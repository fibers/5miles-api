__author__ = 'dragon'

import logging
import requests
import json

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from message.biz import subscribe_notification
from message.models import *
from entity.models import User

logger = logging.getLogger('pinnacle')


class Command(BaseCommand):
    help = 'Get mailgun bounces'

    def handle(self, *args, **options):
        try:
            r = requests.get("%s/bounces" % settings.MAILGUN_BASE_URL,
                             auth=("api", settings.MAILGUN_API_KEY))
            if r.status_code == 200:
                bounces_list = json.loads(r.text)
                for bounce in bounces_list['items']:
                    print bounce['address']
                    users = User.objects.filter(email=bounce['address'])
                    for user in users:
                        subscribe_notification(user.id, NOTIFICATION_TYPE_ALL, NOTIFICATION_METHOD_EMAIL, 0, 'bounced')

            print 'Done!'
        except Exception, e:
            print e
            raise CommandError(e)