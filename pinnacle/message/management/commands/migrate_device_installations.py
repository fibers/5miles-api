# -*- coding: utf-8 -*-
from common.utils import get_utc
from message.models import DeviceInstallation

__author__ = 'dragon'

from django.core.management.base import BaseCommand, CommandError
from entity.service import SrService


class Command(BaseCommand):
    args = ''
    help = 'migrate device installations from redis to db'

    def handle(self, *args, **options):
        r = SrService().get_redis()

        try:
            current_user_id = int(r.get('global:next_user_id'))
            for user_id in xrange(1, current_user_id + 1):
                print 'processing user: %s' % user_id

                if DeviceInstallation.objects.filter(user_id=user_id).exists():
                    print 'user %s installation exists.' % user_id
                    continue

                found = False
                if r.scard('push:getui:user:%s:insts' % user_id) > 0:
                    print 'user %s latest version(getui)'
                    found = True
                    for inst_id in r.smembers('push:getui:user:%s:insts' % user_id):
                        inst = r.hgetall('push:getui:inst:%s' % inst_id)
                        print inst
                        if not inst:
                            continue

                        created_at = inst['created_at']
                        updated_at = inst['updated_at'] if 'updated_at' in inst else created_at
                        DeviceInstallation(user_id=user_id,
                                           device_id=inst['device_id'],
                                           installation_id=inst_id,
                                           provider='getui',
                                           push_token=inst['push_token'],
                                           device=inst['device'],
                                           os=inst['os'],
                                           os_version=inst['os_version'],
                                           app_version=inst['app_version'],
                                           created_at=created_at,
                                           updated_at=updated_at).save()

                if r.scard('push:parse:user:%s:insts' % user_id) > 0:
                    print 'user %s latest version(parse)'
                    found = True
                    for inst_id in r.smembers('push:parse:user:%s:insts' % user_id):
                        inst = r.hgetall('push:parse:inst:%s' % inst_id)
                        if not inst:
                            continue

                        created_at = inst['created_at']
                        updated_at = inst['updated_at'] if 'updated_at' in inst else created_at
                        DeviceInstallation(user_id=user_id,
                                           device_id=inst['device_id'],
                                           installation_id=inst_id,
                                           provider='parse',
                                           push_token=inst['push_token'],
                                           device=inst['device'],
                                           os=inst['os'],
                                           os_version=inst['os_version'],
                                           app_version=inst['app_version'],
                                           created_at=created_at,
                                           updated_at=updated_at).save()

                if found:
                    continue

                if r.scard('user:%s:installation_ids' % user_id) > 0:
                    print 'user %s old version(parse)'
                    for inst_id in r.smembers('user:%s:installation_ids' % user_id):
                        inst = r.hgetall('user:%s:installation_id:%s:device' % (user_id, inst_id))
                        if not inst:
                            continue

                        created_at = inst['created_at']
                        updated_at = inst['updated_at'] if 'updated_at' in inst else created_at
                        DeviceInstallation(user_id=user_id,
                                           device_id='',
                                           installation_id=inst_id,
                                           provider='parse',
                                           push_token=inst['device_token'],
                                           device=inst['device'],
                                           os=inst['os'],
                                           os_version=inst['os_version'],
                                           app_version=inst['app_version'],
                                           created_at=created_at,
                                           updated_at=updated_at).save()
                elif r.scard('user:%s:clients' % user_id) > 0:
                    print 'user %s earlier version(getui)'
                    for inst_id in r.smembers('user:%s:clients' % user_id):
                        inst = r.hgetall('user:%s:client:%s' % (user_id, inst_id))
                        if not inst:
                            continue

                        created_at = inst['created_at']
                        updated_at = inst['updated_at'] if 'updated_at' in inst else created_at
                        DeviceInstallation(user_id=user_id,
                                           device_id='',
                                           installation_id=inst_id,
                                           provider='getui',
                                           push_token='',
                                           device=inst['device'] if 'device' in inst else '',
                                           os=inst['os'] if 'os' in inst else '',
                                           os_version=inst['os_version'] if 'os_version' in inst else '',
                                           app_version=inst['app_version'] if 'app_version' in inst else '',
                                           created_at=created_at,
                                           updated_at=updated_at).save()
        except Exception, e:
            raise CommandError(e)


