# -*- coding: utf-8 -*-
from entity.service_base import singleton, BaseService, SrService
from message.models import DeviceInstallation

__author__ = 'dragon'

@singleton
class MessageService(BaseService):
    srs = SrService()

    def __init__(self):
        pass

    def register_device(self, user_id, device_id, installation_id, provider, push_token, device, os, os_version, app_version):
        if DeviceInstallation.objects.filter(provider=provider, installation_id=installation_id, device_id=device_id, user_id=user_id).exists():
            return

        # 一个设备只允许一个安装，因此需要先检查device是否有其他installation，若是，则删除
        if device_id:
            insts = DeviceInstallation.objects.filter(provider=provider, device_id=device_id).exclude(installation_id=installation_id)
            for inst in insts:
                inst.delete()

        # 一个installation只允许一个用户使用
        insts = DeviceInstallation.objects.filter(provider=provider, installation_id=installation_id)
        for inst in insts:
            inst.delete()

        inst = DeviceInstallation(user_id=user_id,
                                  installation_id=installation_id,
                                  provider=provider,
                                  device_id=device_id,
                                  push_token=push_token,
                                  os=os,
                                  os_version=os_version,
                                  app_version=app_version,
                                  device=device)
        inst.save()

    def unregister_device(self, user_id, provider, installation_id):
        insts = DeviceInstallation.objects.filter(user_id=user_id, provider=provider, installation_id=installation_id)
        for inst in insts:
            inst.delete()

