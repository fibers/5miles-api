import sys, os

thrift_path = os.path.join(os.path.dirname(__file__), 'gen-py')
sys.path.append(thrift_path)

from fmmc import FmmcApi
from fmmc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

MESSAGE_THRIFT_HOST = '54.84.19.98'
MESSAGE_THRIFT_PORT = 2048

try:
    transport = TSocket.TSocket(MESSAGE_THRIFT_HOST, MESSAGE_THRIFT_PORT)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    thrift_client = FmmcApi.Client(protocol)

    item_id = 16
    item = Item(id=item_id, image='http://s3.server/item.png', title='kiwi')

    buyer_id = 7
    buyer = Party(id=buyer_id, head='http://s3.server/head1.png', nick='Wusong')

    seller_id = 5
    seller = Party(id=seller_id, head='http://s3.server/head2.png', nick='XiaoPan')

    transport.open()

    thrift_client.sendOffer(OfferDto(entryId=901, item=item, buyer=buyer, seller=seller, price=50.0, comment='hello', sellSide=0, threadId=1))

    transport.close()

except Thrift.TException, tx:
    print tx
    #print 'Something went wrong : %s' % (tx.message)

