import gearman
import json
import sys, os

thrift_path = os.path.join(os.path.dirname(__file__), 'gen-py')
sys.path.append(thrift_path)

from fmmc import FmmcApi
from fmmc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from .utils import *

MESSAGE_THRIFT_HOST = '54.84.19.98'
MESSAGE_THRIFT_PORT = 9090

GEARMAN_SERVERS = ['54.86.134.138:8341']

def task_send_chat_message(worker, job):
    chat = chatDto(item=item, buyer=buyer, seller=seller, threadId=thread_id, content=body, sellSide=0)
    thrift_client.sendChat(chat)

def task_send_offer_message(worker, job):
    params = json.loads(job.data)
    print params

    item_id = params['item_id']
    item = Item(id=item_id, image='http://s3.server/item.png', title='kiwi')

    buyer_id = params['buyer_id']
    buyer = Party(id=buyer_id, head='http://s3.server/head1.png', nick='Wusong')

    seller_id = params['seller_id']
    seller = Party(id=seller_id, head='http://s3.server/head2.png', nick='XiaoPan')

    try:
        transport = TSocket.TSocket(MESSAGE_THRIFT_HOST, MESSAGE_THRIFT_PORT)
        transport = TTransport.TBufferedTransport(transport)
        protocol = TBinaryProtocol.TBinaryProtocol(transport)

        thrift_client = FmmcApi.Client(protocol)

        transport.open()

        thrift_client.sendOffer(OfferDto(item=item, buyer=buyer, seller=seller, price=params['price'], comment=params['comment'], sellSide=params['sell_side'], threadId=params['thread_id']))

        transport.close()

        print 'done thrift call'

        return 'OK'
    except Thrift.TException, tx:
        print 'Something went wrong : %s' % (tx.message)
        return 'Failed'


worker = gearman.GearmanWorker(GEARMAN_SERVERS)
#worker.register_task('putChatMsg', task_send_chat_message)
worker.register_task('putOfferMsg', task_send_offer_message)

print 'working...'
worker.work()
