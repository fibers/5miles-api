__author__ = 'dragon'

from message.models import UserSubscription, NOTIFICATION_TYPE_ALL


def subscribe_notification(user_id, notification_type, notification_method, action, comment=''):
    user_subs = UserSubscription.objects.filter(user_id=user_id,
                                                notification_method=notification_method,
                                                notification_type=notification_type)

    state = True if action == 1 else False
    if user_subs.count() > 0:
        user_sub = user_subs[0]
        user_sub.state = state
        user_sub.save()
    else:
        user_sub = UserSubscription(user_id=user_id,
                                    notification_type=notification_type,
                                    notification_method=notification_method,
                                    state=state,
                                    comment=comment)
        user_sub.save()


def is_subscribed(user_id, notification_method, notification_type):
    user_subs = UserSubscription.objects.filter(user_id=user_id, notification_method=notification_method,
                                                notification_type__in=[notification_type, NOTIFICATION_TYPE_ALL])
    if user_subs.count() == 0:
        return True
    else:
        return user_subs[0].state