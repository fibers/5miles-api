# -*- coding: utf-8 -*-

from django.db import models
from entity.models import User
from django.utils.translation import ugettext as _


NOTIFICATION_TYPE_ALL = 0
NOTIFICATION_TYPE_NEW_FOLLOWERS = 1
NOTIFICATION_TYPE_RECOMMENDATIONS = 2
NOTIFICATION_TYPE_MARKETING_ACTIVITIES = 3
NOTIFICATION_TYPE_FUNCTIONAL = 4
NOTIFICATION_TYPE_OFFERS_AND_MESSAGES = 5
NOTIFICATION_TYPE_REMINDERS = 6
NOTIFICATION_TYPE_COURTESY_CALLS = 7

NOTIFICATION_TYPES = (
    (NOTIFICATION_TYPE_ALL, _('All Notifications')),
    (NOTIFICATION_TYPE_NEW_FOLLOWERS, _('New Followers')),
    (NOTIFICATION_TYPE_RECOMMENDATIONS, _('Recommendations')),
    (NOTIFICATION_TYPE_MARKETING_ACTIVITIES, _('Marketing Activities')),
    (NOTIFICATION_TYPE_FUNCTIONAL, _('Functional')),
    (NOTIFICATION_TYPE_OFFERS_AND_MESSAGES, _('Offer/Message(s)')),
    (NOTIFICATION_TYPE_REMINDERS, _('Reminders')),
    (NOTIFICATION_TYPE_COURTESY_CALLS,  _('Courtesy Calls')),
)

NOTIFICATION_TYPES_DICT = dict([(k, v) for k, v in NOTIFICATION_TYPES])

NOTIFICATION_METHOD_PUSH = 1
NOTIFICATION_METHOD_EMAIL = 2
NOTIFICATION_METHOD_SMS = 3
NOTIFICATION_METHOD_PHONE_CALLS = 4

NOTIFICATION_METHODS = (
    (NOTIFICATION_METHOD_PUSH, _('Push Notifications')),
    (NOTIFICATION_METHOD_EMAIL, _('Email Notifications')),
    (NOTIFICATION_METHOD_SMS, _('SMS Notifications')),
    (NOTIFICATION_METHOD_PHONE_CALLS, _('Phone calls')),
)

NOTIFICATION_METHODS_DICT = dict([(k, v) for k, v in NOTIFICATION_METHODS])

NOTIFICATION_GROUP = {
    NOTIFICATION_METHOD_PUSH: [NOTIFICATION_TYPE_NEW_FOLLOWERS, NOTIFICATION_TYPE_RECOMMENDATIONS, NOTIFICATION_TYPE_MARKETING_ACTIVITIES],
    NOTIFICATION_METHOD_EMAIL: [NOTIFICATION_TYPE_NEW_FOLLOWERS, NOTIFICATION_TYPE_RECOMMENDATIONS, NOTIFICATION_TYPE_MARKETING_ACTIVITIES],
    NOTIFICATION_METHOD_SMS: [NOTIFICATION_TYPE_REMINDERS],
    NOTIFICATION_METHOD_PHONE_CALLS: [NOTIFICATION_TYPE_COURTESY_CALLS]
}


class UserSubscription(models.Model):
    class Meta:
        db_table = 'fmmc_user_subscription'

    user = models.ForeignKey(User, db_index=True)
    notification_type = models.SmallIntegerField()
    notification_method = models.SmallIntegerField()
    # state 1 => subscribed, 0 => unsubscribed
    state = models.BooleanField(db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    comment = models.CharField(max_length=255, default='')


PUSH_PROVIDER_CHOICES = (
    ('getui', 'getui'),
    ('parse', 'parse'),
)

class DeviceInstallation(models.Model):
    user_id = models.PositiveIntegerField(db_index=True)
    installation_id = models.CharField(max_length=100, db_index=True)
    provider = models.CharField(max_length=20, choices=PUSH_PROVIDER_CHOICES)
    device_id = models.CharField(max_length=100, blank=True, default='', db_index=True)
    push_token = models.CharField(max_length=255, blank=True, default='')
    os = models.CharField(max_length=20, blank=True, default='')
    os_version = models.CharField(max_length=20, blank=True, default='')
    app_version = models.CharField(max_length=20, blank=True, default='')
    device = models.CharField(max_length=255, blank=True, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

