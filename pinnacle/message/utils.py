from gearman import GearmanClient
from django.conf import settings
from apns import APNs, Payload
from django.utils.translation import ugettext_lazy as _
from .tasks import send_sms

import json

gm_client = GearmanClient(settings.GEARMAN_SERVERS)
apns = APNs(use_sandbox=True, cert_file=settings.APNS_CERT_FILE, key_file=settings.APNS_KEY_FILE)

def async_send_offer_message(offer_id, item_id, thread_id, buyer_id, seller_id, price, comment, sell_side):
    params = {'offer_id':offer_id, 'item_id':item_id, 'buyer_id':buyer_id, 'seller_id':seller_id, 'price':price, 'thread_id':thread_id, 'comment':comment, 'sell_side':sell_side}

    gm_client.submit_job('putOfferMsg', json.dumps(params), background=True)
    print 'job sent'

def async_send_accept_offer_message(offer_id, item_id, thread_id, buyer_id, seller_id, price, comment, sell_side):
    params = {'offer_id':offer_id, 'item_id':item_id, 'buyer_id':buyer_id, 'seller_id':seller_id, 'price':price, 'thread_id':thread_id, 'comment':comment, 'sell_side':sell_side, 'accepted':1}

    gm_client.submit_job('putOfferMsg', json.dumps(params), background=True)
    print 'job sent'

def async_send_chat_message(chat_id, item_id, thread_id, buyer_id, seller_id, content, sell_side):
    params = {'chat_id':chat_id, 'item_id':item_id, 'buyer_id':buyer_id, 'seller_id':seller_id, 'thread_id':thread_id, 'content':content, 'sell_side':sell_side}

    gm_client.submit_job('putChatMsg', json.dumps(params), background=True)

def async_send_question_message(entry_id, item_id, buyer_id, seller_id, content):
    params = {'entry_id':entry_id, 'item_id':item_id, 'buyer_id':buyer_id, 'seller_id':seller_id, 'is_reply':False, 'content':content}

    gm_client.submit_job('putQuestionMsg', json.dumps(params), background=True)

def async_send_reply_message(entry_id, item_id, buyer_id, seller_id, content):
    params = {'entry_id':entry_id, 'item_id':item_id, 'buyer_id':buyer_id, 'seller_id':seller_id, 'is_reply':True, 'content':content}

    gm_client.submit_job('putQuestionMsg', json.dumps(params), background=True)

def async_send_feedback_message(entry_id, item_id, thread_id, buyer_id, seller_id, rate_num, comment, sell_side):
    params = {'entry_id':entry_id, 'item_id':item_id, 'buyer_id':buyer_id, 'seller_id':seller_id, 'rate_num':rate_num, 'thread_id':thread_id, 'comment':comment, 'sell_side':sell_side}

    gm_client.submit_job('putFeedbackMsg', json.dumps(params), background=True)

def send_push_notification(device_token, device_type='ios', text='', user_info={}):
    if device_token is None or len(device_token) == 0:
        return

    if device_type == 'ios':
        payload = Payload(alert=text, sound='default', badge=1, custom=user_info)
        apns.gateway_server.send_notification(device_token, payload)


def send_passcode_to_phone(mobile_phone, passcode):
    msg = _('[5miles] Your verification code is: %s. Enter this in 5miles to confirm your identity.') % passcode
    send_sms.delay(mobile_phone=mobile_phone, text=msg)