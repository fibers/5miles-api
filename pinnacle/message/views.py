# -*- coding: utf-8 -*-

from django.views.decorators.http import require_GET, require_POST, require_http_methods
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseGone
from django.utils.translation import ugettext as _
from apns import APNs, Payload

import redis
import json
import logging

from commerce.utils import *
from entity.exceptions import NotFoundError, InvalidParameterError
from message.models import UserSubscription
from common.biz import *
from common.utils import capture_exception, capture_message, mailgun_verify
from message.models import *
from message.biz import subscribe_notification
from message.tasks import update_user_current_device, register_device_task, unregister_device_task, mark_read_message
from entity.service import UserService

r = SrService().get_redis()
print 'message: connect to redis'

logger = logging.getLogger("pinnacle")

# @require_POST
# def register_device(request):
#     device_token = request.POST.get('device_token', '')
#     if device_token == '':
#         return HttpResponseBadRequest(_('Device token not provided'))
#
#     r.set('user:%s:device_token' % request.user_id, device_token)
#
#     return HttpResponse()


@require_GET
def get_my_messages(request):
    msg_type = request.GET.get('type', '')
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    target_app_version = request.app_version

    new_message_count = _new_message_count(request)

    inbox_key = 'user:%s:inbox:%s' % (request.user_id, msg_type)
    # 此处属于之前理解分歧,只要用户没有点开消息查看,就认为有消息未读(new_count不清零),对于system消息,始终清零
    if msg_type == 'system':
        r.set('%s:new_count' % inbox_key, 0)

    msg_ids = r.zrange(inbox_key, -(offset+limit), -offset-1)
    messages = []
    for msg_id in msg_ids:
        message = r.hgetall('message:%s' % msg_id)
        try:
            if 'type' not in message or message['type'] != 'system':
                item_id = message['item_id']
                message['from'] = get_user_from_id(message['from'])
                message['to'] = get_user_from_id(message['to'])

                item = r.hmget('item:%s' % item_id, 'images', 'id', 'title')
                images = json.loads(item[0])
                message['item_image'] = images[0]
                message['item_id'] = item[1]
                message['item_title'] = item[2]
                owner_id = r.get('item:%s:owner' % item_id)
                owner_nickname = r.hget('user:%s' % owner_id, 'nickname')
                message['owner_nickname'] = owner_nickname
                if 'count' not in message:
                    count = (1 if message['unread'] == 'True' else 0)
                    message['count'] = count
                    r.hset('message:%s' % msg_id, 'count', count)

                if 'count' not in message:
                    count = 1 if message['unread'] == 'True' else 0
                    message['count'] = count
                    r.hset('message:%s' % msg_id, 'count', count)

            messages.insert(0, message)
        except Exception, e:
            print e
            r.delete('message:%s' % msg_id)
            r.zrem(inbox_key, msg_id)

    total_count = r.zcard(inbox_key)
    ret = {'meta': {'total_count': total_count, 'type': msg_type, 'offset': offset, 'limit': len(messages),
                    'previous': '?offset=%d&limit=%d' % (offset - limit, limit) if offset >= limit else None,
                    'next': '?offset=%d&limit=%d' % (offset + limit, limit) if offset + limit < total_count else None,
                    'res_ext': new_message_count},
           'objects': messages}

    if hasattr(request, 'user_id'):
        UserService().user_daily_checkin(request.user_id)

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_GET
def get_new_message_count_details(request):
    buying_offerer_count = _get_offerer_count(request.user_id, 'buying')
    selling_offerer_count = _get_offerer_count(request.user_id, 'selling')
    notification_count = _get_message_type_flag(request.user_id, 'system')

    return HttpResponse(json.dumps({'offerers': buying_offerer_count + selling_offerer_count, 'notifications': notification_count}),
                        content_type='application/json')


def _new_message_count(request):
    buying_count = _get_message_type_flag(request.user_id, 'buying')
    selling_count = _get_message_type_flag(request.user_id, 'selling')
    system_count = _get_message_type_flag(request.user_id, 'system')

    return {'buying': buying_count, 'selling': selling_count, 'system': system_count}


@require_GET
def get_new_message_count(request):
    return HttpResponse(json.dumps(_new_message_count(request)), content_type='application/json')


def _get_offerer_count(user_id, msg_type):
    """
    获取新的make offer的人数
    :param user_id: 用户ID
    :param msg_type: 消息类型，buying / selling
    :return:
    """
    inbox_key = 'user:%s:inbox:%s' % (user_id, msg_type)
    if r.exists('%s:new_count' % inbox_key):
        new_count = int(r.get('%s:new_count' % inbox_key))
        if new_count == 0:
            return 0
    else:
        return 0

    msg_ids = r.zrange(inbox_key, 0, -1)
    offerer = set()
    for msg_id in msg_ids:
        message = r.hgetall('message:%s' % msg_id)
        if int(message['to']) == int(user_id) and message['unread'] == 'True':
            offerer.add(message['from'])
    return len(offerer)


def _get_message_type_flag(user_id, msg_type):
    flag = r.get('user:%s:inbox:%s:new_count' % (user_id, msg_type))
    try:
        flag = int(flag)
        return flag
    except Exception:
        return 0


@require_POST
def read_message(request):
    message_id = request.POST.get('message_id', '')
    offerline_id = request.POST.get('offerline_id', '')

    mark_read_message.apply_async((request.user_id, message_id, offerline_id), queue='celery.misc')

    return HttpResponse()


@require_POST
def read_system_message(request):
    inbox_key = 'user:%s:inbox:system' % request.user_id
    r.set('%s:new_count' % inbox_key, 0)
    msg_ids = r.zrange(inbox_key, 0, -1)
    for msg_id in msg_ids:
        r.hmset('message:%s' % msg_id, {'unread': False})

    return HttpResponse()


@require_POST
def delete_message(request):
    message_id = request.POST.get('message_id', '')
    #if not r.exists('message:%s' % message_id):
    #    raise NotFoundError()

    offerline_id = r.hget('message:%s' % message_id, 'thread_id')
    if offerline_id and hasattr(request, 'user_id'):
        mark_read_message(request.user_id, message_id, offerline_id)

    msg_type = request.POST.get('type', '')
    if msg_type == '':
        raise InvalidParameterError()

    # TODO: iOS客户端传递了错误的type值：0,1,2，需做准换
    if msg_type.isdigit():
        if msg_type == '0':
            msg_type = 'buying'
        elif msg_type == '1':
            msg_type = 'selling'
        elif msg_type == '2':
            msg_type = 'system'


    message = r.hmget('message:%s' % message_id, 'from', 'to', 'item_id')
    from_uid, to_uid, item_id = message[0], message[1], message[2]

    with r.pipeline() as pipe:
        pipe.zrem('user:%s:inbox:%s' % (request.user_id, msg_type), message_id)
        key = 'from:%s:to:%s:item:%s:message_id' % (from_uid, to_uid, item_id)
        if r.exists(key) and r.get(key) == message_id:
            pipe.delete(key)
        else:
            key = 'from:%s:to:%s:item:%s:message_id' % (to_uid, from_uid, item_id)
            # print type(r.get(key))
            # print type(message_id)
            # print r.get(key) == message_id
            if r.exists(key) and r.get(key) == message_id:
                pipe.delete(key)

        pipe.delete('message:%s' % message_id)
        pipe.execute()

    return HttpResponse()


@require_POST
def bind_client(request):
    client_id = request.POST.get('client_id', '')
    os = request.os
    os_version = request.os_version
    app_version = request.app_version
    device = request.device

    if os == '':
        os = request.POST.get('os', '').lower()

    if os_version == '':
        os_version = request.POST.get('os_version', '').lower()

    if client_id == '' or os == '':
        raise InvalidParameterError()

    # 首先检查clientId是否已与其他user绑定，若是，则解绑
    former_user_id = r.get('client:%s:user_id' % client_id)
    if former_user_id is not None and int(former_user_id) != int(request.user_id):
        with r.pipeline() as pipe:
            pipe.delete('client:%s:user_id' % client_id)
            pipe.srem('user:%s:clients' % former_user_id, client_id)
            pipe.delete('user:%s:client:%s' % (former_user_id, client_id))
            pipe.execute()

    now = get_now_timestamp_str2()
    if not r.sismember('user:%s:clients' % request.user_id, client_id):
        with r.pipeline() as pipe:
            pipe.sadd('user:%s:clients' % request.user_id, client_id)
            pipe.hmset('user:%s:client:%s' % (request.user_id, client_id),
                       {'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device, 'created_at': now})
            pipe.set('client:%s:user_id' % client_id, request.user_id)
            pipe.execute()
    else:
        r.hmset('user:%s:client:%s' % (request.user_id, client_id), {'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device, 'updated_at': now})

    register_device_task.delay(user_id=request.user_id,
                               device_id='',
                               installation_id=client_id,
                               provider='getui',
                               push_token='',
                               device=device,
                               os=os,
                               os_version=os_version,
                               app_version=app_version)

    return HttpResponse()


@require_POST
def unbind_client(request):
    client_id = request.POST.get('client_id', '')

    if client_id == '':
        raise InvalidParameterError()

    if r.sismember('user:%s:clients' % request.user_id, client_id):
        r.srem('user:%s:clients' % request.user_id, client_id)
    else:
        raise NotFoundError()

    unregister_device_task.delay(user_id=request.user_id, provider='getui', installation_id=client_id)

    return HttpResponse()


@require_POST
def bind_push(request):
    """
    绑定用户ID与Parse installation id，用于push
    :param request:
    :return:
    """
    device_token = request.POST.get('device_token', '')
    os = request.os
    os_version = request.os_version
    app_version = request.app_version
    device = request.device
    push_type = request.POST.get('push_type', '')
    installation_id = request.POST.get('installation_id', '')
    parse_version = request.POST.get('parse_version', '')

    if os == '':
        os = request.POST.get('os', '').lower()

    if os_version == '':
        os_version = request.POST.get('os_version', '').lower()

    if installation_id == '' or os == '':
        raise InvalidParameterError()


    # 首先检查device token是否已与其他user绑定，若是，则解绑
    former_user_id = r.get('installation_id:%s:user_id' % installation_id)
    if former_user_id is not None and int(former_user_id) != int(request.user_id):
        with r.pipeline() as pipe:
            pipe.delete('installation_id:%s:user_id' % installation_id)
            pipe.srem('user:%s:installation_ids' % former_user_id, installation_id)
            pipe.delete('user:%s:installation_id:%s:device' % (former_user_id, installation_id))
            pipe.execute()

    now = get_now_timestamp_str2()
    if not r.sismember('user:%s:installation_ids' % request.user_id, installation_id):
        with r.pipeline() as pipe:
            pipe.sadd('user:%s:installation_ids' % request.user_id, installation_id)
            pipe.hmset('user:%s:installation_id:%s:device' % (request.user_id, installation_id),
                       {'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device,
                        'push_type': push_type, 'device_token': device_token,
                        'parse_version': parse_version, 'created_at': now})
            pipe.set('installation_id:%s:user_id' % installation_id, request.user_id)
            pipe.execute()
    else:
        r.hmset('user:%s:installation_id:%s:device' % (request.user_id, installation_id),
                {'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device,
                 'push_type': push_type, 'device_token': device_token,
                 'parse_version': parse_version, 'updated_at': now})

    register_device_task.delay(user_id=request.user_id,
                               device_id='',
                               installation_id=installation_id,
                               provider='parse',
                               push_token=device_token,
                               device=device,
                               os=os,
                               os_version=os_version,
                               app_version=app_version)

    return HttpResponse()


@require_POST
def unbind_push(request):
    """
    解绑用户ID与Parse installation id
    :param request:
    :return:
    """

    installation_id = request.POST.get('installation_id', '')

    if installation_id == '':
        raise InvalidParameterError()

    if r.sismember('user:%s:installation_ids' % request.user_id, installation_id):
        with r.pipeline() as pipe:
            pipe.srem('user:%s:installation_ids' % request.user_id, installation_id)
            pipe.delete('installation_id:%s:user_id' % installation_id)
            pipe.delete('user:%s:installation_id:%s:device' % (request.user_id, installation_id))
            pipe.execute()
    else:
        raise NotFoundError()

    unregister_device_task.delay(user_id=request.user_id, provider='parse', installation_id=installation_id)

    return HttpResponse()


@require_POST
def register_device(request):
    """
    绑定用户ID与installation id，用于push
    :param request:
    :return:
    """
    os = request.os
    os_version = request.os_version
    app_version = request.app_version
    device = request.device
    provider = request.POST.get('provider', '')
    inst_id = request.POST.get('installation_id', '')
    device_id = request.POST.get('device_id', '')
    push_token = request.POST.get('push_token', '')  # GCM RegistrationId or Apple device token

    if provider == '' or inst_id == '' or os == '':
        raise InvalidParameterError()

    if os == '':
        os = request.POST.get('os', '').lower()

    if os_version == '':
        os_version = request.POST.get('os_version', '').lower()

    if os == 'ios' and device_id == '':
        device_id = push_token  # iOS设备如果没提供device ID，把device token当device id

    inst_user_map_key = 'push:%s:inst:%s:user' % (provider, inst_id)
    insts_key = 'push:%s:user:%s:insts'     # 一个用户允许多个安装（在不同设备上）
    inst_key = 'push:%s:inst:%s'
    device_inst_map_key = 'push:%s:device:%s:inst'   # 一个设备只允许一个安装

    # 一个installation只允许一个用户使用，因此需要先检查installation是否已与其他user绑定，若是，则解除关系
    former_user_id = r.get(inst_user_map_key)
    if former_user_id is not None and int(former_user_id) != int(request.user_id):
        with r.pipeline() as pipe:
            pipe.delete(inst_user_map_key)
            pipe.srem(insts_key % (provider, former_user_id), inst_id)
            pipe.delete(inst_key % (provider, inst_id))
            pipe.execute()

    now = get_now_timestamp_str2()
    if not r.sismember(insts_key % (provider, request.user_id), inst_id):
        with r.pipeline() as pipe:
            if device_id != '':
                # 一个设备只允许一个安装，因此需要先检查device是否已与其他installation绑定，若是，则解除关系
                old_inst = r.get(device_inst_map_key % (provider, device_id))
                if old_inst:
                    pipe.delete(device_inst_map_key % (provider, device_id))
                    pipe.delete(inst_key % (provider, old_inst))
                    old_user = r.get('push:%s:inst:%s:user' % (provider, old_inst))
                    pipe.delete('push:%s:inst:%s:user' % (provider, old_inst))
                    pipe.srem(insts_key % (provider, old_user), old_inst)

                pipe.set(device_inst_map_key % (provider, device_id), inst_id)

            pipe.sadd(insts_key % (provider, request.user_id), inst_id)
            pipe.hmset(inst_key % (provider, inst_id),
                       {'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device,
                        'push_token': push_token, 'device_id': device_id, 'user_id': request.user_id,
                        'created_at': now})
            pipe.set(inst_user_map_key, request.user_id)
            pipe.execute()
    else:
        r.hmset(inst_key % (provider, inst_id),
                {'os': os, 'os_version': os_version, 'app_version': app_version, 'device': device,
                 'push_token': push_token, 'device_id': device_id, 'user_id': request.user_id,
                 'updated_at': now})

    register_device_task.delay(user_id=request.user_id,
                               device_id=device_id,
                               installation_id=inst_id,
                               provider=provider,
                               push_token=push_token,
                               device=device,
                               os=os,
                               os_version=os_version,
                               app_version=app_version)

    return HttpResponse()


@require_POST
def unregister_device(request):
    """
    解绑用户ID与Parse installation id
    :param request:
    :return:
    """

    provider = request.POST.get('provider', '')
    inst_id = request.POST.get('installation_id', '')

    if provider == '' or inst_id == '':
        raise InvalidParameterError()

    inst_user_map_key = 'push:%s:inst:%s:user' % (provider, inst_id)
    insts_key = 'push:%s:user:%s:insts'     # 一个用户允许多个安装（在不同设备上）
    inst_key = 'push:%s:inst:%s'
    device_inst_map_key = 'push:%s:device:%s:inst'   # 一个设备只允许一个安装

    if r.sismember(insts_key % (provider, request.user_id), inst_id):
        with r.pipeline() as pipe:
            pipe.srem(insts_key % (provider, request.user_id), inst_id)
            pipe.delete(inst_user_map_key)
            inst = r.hgetall(inst_key % (provider, inst_id))
            device_id = inst['device_id']
            if device_id != '' and r.get(device_inst_map_key % (provider, device_id)) == inst_id:
                pipe.delete(device_inst_map_key % (provider, device_id))

            pipe.delete(inst_key % (provider, inst_id))
            pipe.execute()
    else:
        raise NotFoundError()

    unregister_device_task.delay(user_id=request.user_id, provider=provider, installation_id=inst_id)

    return HttpResponse()


@require_GET
def user_subscriptions(request):
    """
        读取用户订阅状态列表，如果某项通知类型不在返回结果中，当订阅处理
    """
    ret = []
    for k, v in NOTIFICATION_GROUP.iteritems():
        for noti_type in NOTIFICATION_GROUP[k]:
            ret.append({'method': k, 'type': noti_type, 'state': True})

    user_subs = UserSubscription.objects.filter(user_id=request.user_id)
    for user_sub in user_subs:
        for entry in ret:
            if entry['method'] == user_sub.notification_method and entry['type'] == user_sub.notification_type:
                entry['state'] = user_sub.state
                break

    return HttpResponse(json.dumps(ret), content_type='application/json')


@require_POST
def subscribe(request):
    """
        订阅或取消订阅Push，Email
    """

    notification_type = int(request.POST.get('type', 0))
    notification_method = int(request.POST.get('method', 0))
    action = int(request.POST.get('action', -1))

    if notification_type == 0 or notification_method == 0 or action == -1:
        raise InvalidParameterError()

    subscribe_notification(request.user_id, notification_type, notification_method, action)

    return HttpResponse()


@require_POST
def bulk_subscribe(request):
    """
    批量订阅或取消订阅Push，Email
    :param request:[{"type": 1, "method": 2,"action": 1},{"type": 2, "method": 1,"action": 0}]
    :return: 200 ok
    """
    params = json.loads(request.body)
    for param in params:
        notification_type = param['type']
        notification_method = param['method']
        action = param['action']
        subscribe_notification(request.user_id, notification_type, notification_method, action)

    return HttpResponse()


@require_POST
@mailgun_verify
def mailgun_bounce(request):
    """
    This api is for receiving the hard bounce webhook from mailgun.
    :param request:
    :return: 200 success
    """

    try:
        event = request.POST.get('event', '')
        recipient = request.POST.get('recipient', '')
        domain = request.POST.get('domain', '')
        message_headers = request.POST.get('message-headers', '')
        code = request.POST.get('code', '')
        error = request.POST.get('error', '')
        notification = request.POST.get('notification', '')
        campaign_id = request.POST.get('campaign-id', '')
        campaign_name = request.POST.get('campaign-name', '')
        tag = request.POST.get('tag', '')
        mailing_list = request.POST.get('mailing-list', '')
        logger.info('event: %s, recipient: %s, domain: %s, message-headers: %s, code: %s, error: %s, notification: %s,'\
                    'campaign-id: %s, campaign-name: %s, tag: %s, mailing-list: %s' %
                    (event, recipient, domain, message_headers, code, error, notification,
                     campaign_id, campaign_name, tag, mailing_list))

        users = User.objects.filter(email=recipient)
        for user in users:
            subscribe_notification(user.id, NOTIFICATION_TYPE_ALL, NOTIFICATION_METHOD_EMAIL, 0, 'bounced')
    finally:
        return HttpResponse('OK')
